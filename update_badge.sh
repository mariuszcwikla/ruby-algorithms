#!/usr/bin/env sh
# Add pre-push commit hook:
#
# #!/bin/sh
# ./update_badge.sh

shopt -s globstar
loc=$(cat **/*.rb | wc -l)
url=":loc-badge: https://img.shields.io/badge/lines%20of%20code-$loc-lightgrey"
echo $url > loc_badge.adoc

if ! git diff --exit-code loc_badge.adoc > /dev/null; then
	echo 'Aborting...'
	echo "LOC changed ($loc). Please commit loc_badge.adoc."
	git add loc_badge.adoc
	exit 1
fi
