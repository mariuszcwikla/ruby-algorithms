require_relative '../../lib/numbertheory/numbers'

def set_of_modular_inverse(n)
  (1..(n - 1)).map { |i| Numbers.modular_inverse(i, n) }.reject { |i| i == -1 }
end

def inv_to_s(n)
  set = set_of_modular_inverse(n)
  "set_of_modular_inverse(#{n})={" + set.join(', ') + "} (size=#{set.size})"
end

puts inv_to_s(11)
puts inv_to_s(12)
puts inv_to_s(35)
