require_relative '../../lib/numbertheory/numbers'
require 'scanf'

loop do
  a, b = scanf('%d%d')
  puts "gcd(#{a},#{b})=#{Numbers.gcd(a, b)}"
end
