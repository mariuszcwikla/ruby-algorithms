require_relative '../../lib/numbertheory/numbers'
require 'scanf'

loop do
  a, b = scanf('%d%d')
  puts "mod_inv(#{a},#{b})=#{Numbers.modular_inverse(a, b)}"
end
