# NIE DZIALA
# Miało udowodnić że że wystarczy sqrt(N) żeby złamać hash
# ale nawet jeśli prawdopodbieństwo ustalam na 0.99 to i tak potrzebuje zbyt dużo iteracji...

PRIME = 101_267
NUM_OF_LETTERS = 'z'.ord - 'a'.ord
puts "Birthday attack, MAX=#{PRIME}"

def hash_fun(s)
  # return s.hash%PRIME
  h = 0
  s.each_char do |c|
    h = h + c.ord - NUM_OF_LETTERS
  end
  h % PRIME
end

def random_string(n)
  x = ''
  n.times do
    x << rand(NUM_OF_LETTERS) + 'a'.ord
  end
  x
end

def plusone2(s)
  s[0] += 1
  i = 0
  while (i < s.size) && (s[i] - 'a'.ord >= NUM_OF_LETTERS)
    s[i] -= NUM_OF_LETTERS
    i += 1
    s[i] += 1 if i < s.size - 1
  end
end

def plusone(s)
  s[s.size - 1] += 1
  i = s.size - 1
  while (i >= 0) && (s[i] - 'a'.ord >= NUM_OF_LETTERS)
    s[i] -= NUM_OF_LETTERS
    i -= 1
    s[i] += 1 if i > 0
  end
end

def run_attack(n, h, num_of_iterations)
  s = random_string(n)
  puts "running birthday attack iterations: #{num_of_iterations}, random string=#{s}"
  s = s.bytes
  num_of_iterations.times do |i|
    s = random_string(n)
    s = s.bytes
    # plusone(s)
    ss = s.pack('c*')
    hh = hash_fun(ss)
    puts "#{ss} #{hh}"
    if h == hh
      puts "detected collision with string: #{ss}"
      return i
    end
  end
  puts 'no collision detected'
  -1
end

def num_of_iterations_for_birthday_attack(max, prob)
  n = Math.sqrt(2 * max * Math.log(1 / (1 - prob))).to_i
  puts "#{n} iterations required for probability=#{prob}, size of a set=#{max}"
  n
end

# s='Premature optimization is the root of all evil'
s = 'foobarblabla'
h = hash_fun(s)
puts "Original hash value: #{h}"

# max = (1.25*Math.sqrt(PRIME)).to_i
max = num_of_iterations_for_birthday_attack(PRIME, 0.5)

1.upto(2000) do |i|
  k = run_attack(s.size, h, max)
  if k > -1
    puts "#{(i - 1) * max + k} iterations needed. Size of a set is #{PRIME} factor=(#{i * max.to_f / PRIME})"
    return
  end
end
