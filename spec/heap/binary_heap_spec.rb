require 'rspec'
require_relative '../../lib/heap/BinaryHeap'

require "allure-rspec"

RSpec.configure do |config|
  # Allure 2.14.0 introduced an error?
  # C:/tools/ruby26/lib/ruby/gems/2.6.0/gems/allure-rspec-2.14.0/lib/allure_rspec/metadata_parser.rb:88:in `package_label': uninitialized constant AllureRspec::RspecMetadataParser::Pathname 
  # config.formatter = AllureRspecFormatter
end

AllureRspec.configure do |config|
  config.results_directory = "allure/rspec"
  config.clean_results_directory = true
  config.logging_level = Logger::INFO
  # config.link_tms_pattern = "http://www.jira.com/browse/{}"
  # config.link_issue_pattern = "http://www.jira.com/browse/{}"
end

describe BinaryHeap do
  it { is_expected.to be_empty }

  it 'has size 0' do
    expect(subject.size).to equal(0)
  end

  it 'is not empty after 1 push' do
    subject.push 1
    expect(subject).not_to be_empty
  end

  it 'has size 1 after 1 push' do
    subject.push 1
    expect(subject.size).to equal(1)
  end

  it 'peek should throw error on empty heap' do
    expect { subject.peek }.to raise_error(RuntimeError)
  end
end
