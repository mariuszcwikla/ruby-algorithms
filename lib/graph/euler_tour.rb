def parent_map(g)
  parents = {}
  g.each do |n, children|
    children.each do |child|
      parents[child]=n
    end
  end
  parents
end

def euler_tour1(g, root)
  parents = parent_map(g)
  tour = []
  stack = [[root, :in]]
  until stack.empty?
    n, type = stack.pop
    if type == :in
      tour << [parents[n], n] unless parents[n].nil?
      stack << [n, :out]
      g[n].reverse.each do |child|
        stack << [child, :in]
      end
    else
      tour << [n, parents[n]] unless parents[n].nil?
    end
  end
  tour
end

def euler_tour2(g, root)
  parents = parent_map(g)
  tour = []
  stack = [[root, :in]]
  until stack.empty?
    n, type = stack.pop
    if type == :in
      tour << n
      stack << [n, :out]
      g[n].reverse.each do |child|
        stack << [child, :in]
      end
    else
      tour << n
    end
  end
  tour
end

def euler_tour3(g, root)
  parents = parent_map(g)
  tour = []
  stack = [[root, :in]]
  until stack.empty?
    n, type = stack.pop
    if type == :in
      tour << n
      stack << [n, :out]
      g[n].reverse.each do |child|
        stack << [child, :in]
      end
    else
      tour << parents[n] unless parents[n].nil?
    end
  end
  tour
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  g = Hash.new{|h, k| h[k]=[]}
  g[1]=[2, 5]
  g[2]=[3, 4]

  puts euler_tour1(g, 1).inspect
  puts euler_tour2(g, 1).inspect
  puts euler_tour3(g, 1).inspect
end
# :nocov: