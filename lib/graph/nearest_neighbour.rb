# Nearest neighbour method for Travelling Salesman problem

require_relative 'graph'

module TravellingSalesman
  def self.nearest_neighbour(g, start = g.vertices[0])
    path = []
    visited = {start => true}
    stack = [start]
    until stack.empty?
      v = stack.last
      e = v.edges.filter { |e| !visited[e.other_end(v)]}.min_by {|a| a.weight}
      if e.nil?
        stack.pop
      else
        path << e
        u = e.other_end(v)
        stack << u
        visited[u] = true
      end
    end
    path << g.edge(start, u)
    normalize_cycle(path)
  end

  # this method "normalizes" paths, e.g. before: [[0,1], [2, 1], [2, 3], ...], after: [[0,1], [1,2], [2,3], ...]
  # It modifies Edge object, but it should not do any damage if it's unidirectional graph!
  def self.normalize_cycle(path)
    return if path.size <= 1

    unless path[0].second == path[1].first
      if path[0].second == path[1].second
        path[1].reverse
      else
        path[0].reverse
      end
    end
    last = path[0].second
    path[1..-1].each do |e|
      unless last == e.first
        e.reverse
      end
      last = e.second
    end
    path
  end

  def self.two_opt_iteration(g, path)
    normalize_cycle(path)
    path = path.dup
    updated = false
    0.upto(path.size).each do |i|
      e1 = path[i % path.size]
      e2 = path[(i + 2) % path.size]

      e3 = g.edge(e1.first, e2.first)
      e4 = g.edge(e2.second, e1.second)
      if e3.weight + e4.weight < e1.weight + e2.weight
        path[i % path.size ] = e3
        path[(i + 2) % path.size] = e4
        path[(i + 1) % path.size].reverse
        updated = true
      end
    end
    [path, updated]
  end

  def self.two_opt(g, path, debug: false)
    n = 0
    loop do
      n += 1
      path, updated = two_opt_iteration(g, path)
      if debug
        g.display_neato_path(path, make_digraph: false, path_color: 'red')
        path.each {|e| e.graphviz_color = 'brown'}
      end
      break unless updated
    end
    puts "Number of iterations: #{n}" if debug
    path
  end

  def self.brute_force(g)
    best = nil
    best_length = 0
    g.vertices.permutation.each do |vertices|
      path = []
      0.upto(vertices.size-1) do |i|
        v = vertices[i]
        u = vertices[(i+1) % vertices.size]
        e = g.edge(v, u)
        if e.nil?
          e = g.edge(u, v)
        end
        raise "there is no edge between #{v.index} and #{u.index}" if e.nil?

        path << e
      end
      # puts vertices
      # puts path
      length = path.map{|e| e.weight}.sum
      if best.nil? || length < best_length
        best = path
        best_length = length
      end
    end
    best
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME  
  require_relative 'graphml'
  require_relative 'graphviz'
  g = Graph.new
  
  # g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph4_200.graphml', true)
  # g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph3.graphml', true)
  # g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph2.graphml', true)
  g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph5_10.graphml', true)
  # g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph1.graphml', true)
  g.vertices.each do |v|
    g.vertices.each do |u|
      next if u == v

      g.uedge_by_distance(v, u) if g.edge(v, u).nil?
    end
  end

  path = TravellingSalesman.nearest_neighbour(g)
  path.each_with_index do |e, i|
    e.graphviz_color = 'brown'
    e.graphviz_label = "#{e.weight.round(2)} - #{i}"
  end
  g.display_neato(make_digraph: false)
  g.edges.each {|e| e.graphviz_hidden = true unless path.include? e }
  puts "Nearest neighbour. Total sum: #{path.map{|e| e.weight}.sum}"
  g.display_neato(make_digraph: false)

  path = TravellingSalesman.two_opt(g, path)
  g.hide_edges
  g.display_neato_path(path, make_digraph: false)
  puts "After 2-opt algorith. Total sum: #{path.map{|e| e.weight}.sum}"

  best_path = path
  best_weight = path.map{|e| e.weight}.sum
  g.vertices[1..-1].each do |v|
    path = TravellingSalesman.nearest_neighbour(g, v)
    weight = path.map{|e| e.weight}.sum
    if weight < best_weight
      best_weight = weight
      best_path = path
    end
  end

  puts "Best weight: #{best_weight}"
  g.hide_edges
  best_path.each_with_index { |e, i| e.graphviz_label = "#{e.weight.round(2)} - #{i}"}
  g.display_neato_path(path, make_digraph: false, path_color: 'brown')

  best_path = TravellingSalesman.two_opt(g, best_path)
  g.hide_edges
  puts "After 2-opt algorith. Total sum: #{best_path.map{|e| e.weight}.sum}"
  g.display_neato_path(path, make_digraph: false)

  g.show_edges
  puts 'Calculating route with brute-force method. Press Ctrl-C if it takes too long.'
  best_path = TravellingSalesman.brute_force(g)
  g.display_neato_path(path, make_digraph: false, path_color: 'blue', debug: true)
end
# :nocov: