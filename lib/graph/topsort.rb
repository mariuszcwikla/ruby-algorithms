require_relative 'graph'

class Graph
  def topsort_dfs()
    visited = {}
    list = []
    for v in vertices
      if !visited[v]
        stack = [[v, :enter]]
        until stack.empty?
          v, action = stack.pop
          if action == :enter
            stack.push [v, :leave]
            v.each_edge do |_, u|
              unless visited[u]
                stack.push [u, :enter]
                visited[u] = true
              end
            end
          else
            list << v
          end
        end
      end
    end
    list.reverse
  end

  def topsort_dfs_recursive(v=nil, list = [], visited={})
    if v.nil?
      for v in vertices
        topsort_dfs_recursive(v, list, visited) unless visited[v]
      end
      return list.reverse
    else
      v.each_edge do |_, u|
        unless visited[u]
          topsort_dfs_recursive(u, list, visited)
          visited[u] = true
        end
      end
      list << v
    end
  end
end



# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  g={}
  g[1]=[3]
  g[2]=[3]
  g[3]=[4, 5]
  g[4]=[5]
  graph = Graph.from_adj_list(g)
  sorted = graph.topsort_dfs
  p sorted
  sorted = graph.topsort_dfs_recursive
  p sorted
end
# :nocov: