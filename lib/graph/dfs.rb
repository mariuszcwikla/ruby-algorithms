require_relative 'graph'

class Graph
  class DFS
    def initialize(v, g)
      unless g.vertex_exists?(v)
        raise ArgumentException, "starting vertex #{v} does not exist"
      end

      @v = g.vertex(v)
      @graph = g
      @path = []
    end

    def run_dfs
      @visited = {}
      run_dfs_recursive(@v)
    end

    def run_dfs_recursive(v)
      @path << v.index
      @visited[v] = true
      v.edges.each do |e|
        run_dfs_recursive(e.second) unless @visited[e.second]
      end
    end

    attr_reader :path
  end

  private_constant :DFS

  def dfs(starting_vertex)
    d = DFS.new(starting_vertex, self)
    d.run_dfs
    d.path
  end
end
