require_relative 'graph'
require_relative '../sets/disjoint_sets'
require 'set'

class Graph
  def kruskal
    sorted_edges = edges
    sorted_edges.sort! { |a, b| a.weight <=> b.weight }

    et = Set.new

    n = vertices.size - 1
    k = 0

    dj = DisjointSetsQuickFind.new
    vertices.each { |v| dj.insert v }

    while n > 0
      raise 'end reached' if k >= sorted_edges.size

      edge = sorted_edges[k]

      next if edge.head == edge.tail

      unless dj.same_set?(edge.head, edge.tail)
        dj.merge(edge.head, edge.tail)
        et << edge
        n -= 1
      end
      k += 1
    end

    et
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  require_relative 'graphviz'

  g = Graph.new
  g.add_uedge(0, 1).weight = 4
  g.add_uedge(0, 7).weight = 7
  g.add_uedge(1, 2).weight = 8
  g.add_uedge(1, 7).weight = 11
  g.add_uedge(2, 3).weight = 7
  g.add_uedge(2, 8).weight = 2
  g.add_uedge(2, 5).weight = 4
  g.add_uedge(3, 4).weight = 9
  g.add_uedge(3, 5).weight = 14
  g.add_uedge(4, 5).weight = 10
  g.add_uedge(5, 6).weight = 2
  g.add_uedge(6, 7).weight = 1
  g.add_uedge(6, 8).weight = 6
  g.add_uedge(7, 8).weight = 7

  g.kruskal.each { |e| e.graphviz_color = 'red' }
  g.display_neato(false)
end
# :nocov:
