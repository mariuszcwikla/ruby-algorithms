#
# Format used by http://graphonline.ru/en/
#
# <?xml version="1.0" encoding="UTF-8"?>
# <graphml>
#   <graph id="Graph" uidGraph="14" uidEdge="10004">
#     <nodes>
#       <node positionX="155" positionY="53" id="0" mainText="0" upText=""/>
#       <node positionX="147" positionY="141" id="1" mainText="1" upText=""/>
#     </nodes>
#     <edges>
#       <edge vertex1="0" vertex2="1" isDirect="false" weight="5" useWeight="true" hasPair="false" id="10000" text="" arrayStyleStart="" arrayStyleFinish=""/>
#     </edges>
#   </graph>
# </graphml>
#
require_relative 'graph'
require_relative 'graphviz'
require_relative 'planar_graph'
require 'rexml/document'
include REXML

class Graph
  def load_from_graphml_file(filename, make_bidirectional = false)
    xmldoc = Document.new(File.new(filename))

    xmldoc.elements.each('graphml/graph/nodes/node') do |n|
      x = n.attributes['positionX'].to_i
      y = n.attributes['positionY'].to_i
      # need to use scaling, otherwise graphviz generates error
      # failure to create cairo surface: out of memory
      x /= 100.0
      y /= 100.0
      id = n.attributes['id']

      v = vertex(id)
      v.set_position(x, y)
    end

    # different version of graphml has different formats? This is format used by Gephi
    xmldoc.elements.each('graphml/graph/node') do |n|
      id = n.attributes['id']

      x = n.elements["data[@key='x']"].text.to_i
      y = n.elements["data[@key='y']"].text.to_i

      # need to use scaling, otherwise graphviz generates error
      # failure to create cairo surface: out of memory
      x /= 100.0
      y /= 100.0

      y = -y

      v = vertex(id)
      v.set_position(x, y)
      # v.add_graphviz_attrib('pos', "\"#{v.x},#{-v.y}!\"")
    end

    xmldoc.elements.each('graphml/graph/edges/edge') do |e|
      a = e.attributes['vertex1']
      b = e.attributes['vertex2']
      if make_bidirectional
        uedge_by_distance(a, b)
      else
        edge_by_distance(a, b)
      end
    end

    # different version of graphml has different formats? This is format used by Gephi
    xmldoc.elements.each('graphml/graph/edge') do |e|
      a = e.attributes['source']
      b = e.attributes['target']
      if make_bidirectional
        uedge_by_distance(a, b)
      else
        edge_by_distance(a, b)
      end
    end
  end
end
