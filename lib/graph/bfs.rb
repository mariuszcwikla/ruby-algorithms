require_relative 'graph'
require_relative 'graphviz'

class BFS
  # if target=nil then algorithm finishes when target is found
  # otherwise it finds routes to all connected nodes
  def initialize(graph, start, target = nil)
    @graph = graph
    @start = start
    @target = target
    @prev = {}
  end

  def run(&block)
    q = []
    q << @start

    visited = {}
    visited[@start] = true
    found = false

    until q.empty? || found
      v = q.shift
      v.edges.each do |e|
        next if visited[e.tail]

        block.call(v, @prev) if block_given?

        visited[e.tail] = true
        q << e.tail

        @prev[e.tail] = e

        found = true if e.tail == @target
        break if found
      end
    end

    return [] if @prev[@target].nil?

    route = []
    n = @target
    while n != @start
      e = @prev[n]
      route << e
      n = e.head
    end
    route
  end
end
