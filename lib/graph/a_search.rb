require_relative '../heap/BinaryHeap'
require_relative 'planar_graph'

class Graph
  class Vertex
    attr_accessor :f_score_ref
  end
end

class OpenSet
  class Entry
    include Comparable
    attr_reader :vertex, :fscore

    def initialize(vertex, fscore)
      @vertex = vertex
      @fscore = fscore
    end

    def <=>(other)
      fscore <=> other.fscore
    end
  end
  def initialize
    @heap = BinaryHeap.new
    @set = {}
  end

  def add(vertex, fscore)
    ref = @heap.push(Entry.new(vertex, fscore))
    @set[vertex] = ref
    vertex.f_score_ref = ref
  end

  def update_fscore(vertex, fscore)
    @heap.decrease_key(vertex.f_score_ref, Entry.new(vertex, fscore))
  end

  def pop
    e = @heap.pop
    @set.delete e.vertex
    e.vertex
  end

  def empty?
    @heap.empty?
  end

  def include?(vertex)
    @set.include? vertex
  end
end

#  A* search algorithm

class ASearch
  def initialize(from, to, &calc_distance_block)
    unless from.instance_of? Graph::Vertex
      raise "from is not a Vertex #{from.class}"
    end
    unless to.instance_of? Graph::Vertex
      raise "to is not a Vertex but #{to.class}"
    end

    @from = from
    @to = to
    @calc_distance_function = calc_distance_block
  end

  def run(&block)
    openset = OpenSet.new
    closedset = {}

    prevs = {}

    g_score = { @from => 0 }
    g_score.default = Float::INFINITY

    openset.add(@from, @calc_distance_function.call(@from, @to))

    until openset.empty?
      v = openset.pop
      closedset[v] = true
      break if v == @to

      block.call(v, prevs, closedset) if block_given?

      v.edges.each do |edge|
        next if closedset.include? edge.tail

        score = g_score[v] + edge.weight

        next if score >= g_score[edge.tail]

        prevs[edge.tail] = edge
        g_score[edge.tail] = score

        f_score = score + @calc_distance_function.call(edge.tail, @to)

        if openset.include? edge.tail
          openset.update_fscore(edge.tail, f_score)
        else
          openset.add(edge.tail, f_score)
        end
      end
    end

    n = @to
    route = []
    while n != @from
      e = prevs[n]
      break if e.nil?

      route << e

      n = e.head
    end

    route.reverse!

    route
  end
end

class Graph
  def a_search(from, to, &calc_distance)
    ASearch.new(vertex(from), vertex(to), &calc_distance).run
  end
end
