require 'tempfile'
require 'open3'
require_relative 'graph'

# --
#   #fixme:
#   #- zmień na to_graphviz w open-klasie Graph
#
#   Moze tak:
#
#   graphviz = graph.to_graphviz
#   puts graphviz.to_s
#   graphviz.save_to_png('graph.png')
#   graphviz.open()                   #otwiera ostatni plik
# ++

class Graph
  attr_reader :graphviz
  def init_graphviz
    @graphviz = Graphviz.new
  end

  class Graphviz
    attr_accessor :ranks
  end
  # TODO: przerob ustawianie wlasciwosci graphviz dla wezlow i krawedzi mniej wiecej tak
  # g.vertex(5).graphviz.color=:red
  # edge.graphviz.color=:red

  class Vertex
    attr_reader :graphviz_attribs
    attr_accessor :label

    def add_graphviz_attrib(key, value)
      @graphviz_attribs = {} unless defined?(@graphviz_attribs)
      @graphviz_attribs[key] = value
    end

    alias graphviz_label label

    def graphviz_name=(name)
      @graphviz_name = name
    end

    def graphviz_name
      @graphviz_name = nil unless defined?(@graphviz_name)
      @graphviz_name || @index
    end

    def graphviz_fill_color=(color)
      add_graphviz_attrib('fillcolor', color)
      add_graphviz_attrib('style', 'filled')
    end
  end

  class Edge
    attr_accessor :graphviz_color, :graphviz_label, :graphviz_hidden
    attr_accessor :graphviz_source_field_id    # field name in record (<f0>)
  end

  def escape(val)
    return 'nil' if val.nil?

    val = val.to_s

    return val if /w*/ =~ val

    '"' + val + '"'
  end

  def graphviz_define_vertices_order(vertices_order)
    @vertices_order = vertices_order.map { |v| vertex(v) }
  end

  def vertices_order
    @vertices_order ||= nil
  end

  def edge_to_graphviz(e, edge_style)
    content = ''

    label1 = escape(e.first.graphviz_name)
    # TODO: rename "graphviz_source_field_id" to graphviz_nodeport ????
    label1 << ":#{e.graphviz_source_field_id}" unless e.graphviz_source_field_id.nil?
    if e.has_weight? || !e.graphviz_label.nil? || !e.graphviz_color.nil?
      if !e.graphviz_label.nil?
        label = "\"#{e.graphviz_label}\""
      elsif e.has_weight?
        label =
          if e.weight.instance_of? Float
            '%.2f' % e.weight
          else
            e.weight.to_s
          end
      end

      content << "  #{label1} #{edge_style} #{escape(e.second_vertex.graphviz_name)}"
      content << " ["
      content << "label=#{label}" unless label.nil?
      content << " color=#{e.graphviz_color}" unless e.graphviz_color.nil?
      content << "];\n"
    else
      content << "  #{label1} #{edge_style} #{escape(e.second_vertex.graphviz_name)};\n"
      #          unweighted << " #{edge_style} #{escape(e.second_vertex.graphviz_name)}"
    end
    content
  end

  private def has_position?(v)
    v.respond_to?(:x) && v.respond_to?(:y) && !v.x.nil? && !v.y.nil?
  end

  private def print_vertex(content, v, _make_digraph, _edge_style)
    if !v.graphviz_attribs.nil? || !v.label.nil? || !vertices_order.nil? || has_position?(v)
      name = v.graphviz_name
      content << "  #{escape(name)} ["
      v.graphviz_attribs&.each do |k, attrib|
        content << "#{k}=#{attrib} "
      end
      content << "label=\"#{v.label}\" " unless v.label.nil?
      content << "pos=\"#{v.x},#{-v.y}!\"" if has_position?(v)
      content << "]\n"
    end
  end

  def to_graphviz(make_digraph = true, rankdir = nil, ranks=[], node_attributes: nil)
    if ranks.empty?
      unless @graphviz&.ranks.nil?
        ranks = graphviz.ranks
      end
    end
    type = make_digraph ? 'digraph' : 'graph'

    content = "#{type} G{\n"
    content << "  rankdir=#{rankdir}\n" unless rankdir.nil?
    content << "node [shape=circle,\n"
    # TODO: po co mi to było potrzebne? Zdisablowałem w ramach prac nad drzewem AVL
    # content << "  fixedsize=true, \n"
    # content << "  width=1\n"
    content << node_attributes.map {|k, v| "#{k}=#{v}"}.join(' ') unless node_attributes.nil?
    content << "  ]\n"

    # TODO: this require refactoring!

    edge_style = make_digraph ? '->' : '--'

    visited_vertices = {}
    vertices_order&.each do |v|
      print_vertex(content, v, make_digraph, edge_style)
      visited_vertices[v] = true
    end

    vertices.each do |v|
      unless visited_vertices[v]
        print_vertex(content, v, make_digraph, edge_style)
      end

      unweighted = ''
      if make_digraph
        v.edges.each do |e|
          next if e.graphviz_hidden

          content << edge_to_graphviz(e, edge_style)
        end
      end
      unless unweighted.empty?
        #        unweighted = "\t#{escape(v.graphviz_name)} " + unweighted + ";\n"
        #        content << unweighted
      end
    end

    unless make_digraph
      edges.each do |e|
        next if e.graphviz_hidden

        content << edge_to_graphviz(e, edge_style)
      end
    end
    unless ranks.empty?
      ranks.each do |rank|
        #content << "{rank=same; #{rank.join ';'}}\n"
        content << "{rank=same; #{rank.join '->'}[style=invis]}\n" if rank.size > 1
      end
    end
    content + '}'
  end

  def display_graph(**opts)
    display_dot(layout: 'dot', **opts)
  end

  def display_digraph(**opts)
    display_dot(layout: 'dot', make_digraph: true, **opts)
  end

  def display(**opts)
    display_dot(**opts)
  end

  def display_circo(**opts)
    display_dot(layout: 'circo', **opts)
  end

  # use "neato" when you want to draw nodes using x,y coordinates
  def display_neato(**opts)
    display_dot(layout: 'neato', **opts)
  end

  # from https://stackoverflow.com/a/5471032
  def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
      exts.each do |ext|
        exe = File.join(path, "#{cmd}#{ext}")
        return exe if File.executable?(exe) && !File.directory?(exe)
      end
    end
    nil
  end

  def display_dot(layout: nil, make_digraph: true, debug: false, rankdir: nil, output_format: 'png', ranks:[], node_attributes: nil)
    raise "#{layout} not found in PATH" if which('dot').nil?

    Dir.mkdir './tmp' unless Dir.exist? './tmp'
    dotfile = Tempfile.new('graph.dot', './tmp')

    body = to_graphviz(make_digraph, rankdir, ranks, node_attributes: node_attributes)

    puts body if debug

    dotfile.write(body)
    dotfile.close

    filename = dotfile.path + '.' + output_format
    puts "Temporary file: #{dotfile.path}"
    dot_command = "dot #{layout.nil? ? "" : "-K" + layout} #{dotfile.path} -o #{filename} -T#{output_format}"

    # if you get this error:
    # Format: "png" not recognized. Use one of:
    # run "dot -c" as admin (windows) - https://stackoverflow.com/questions/35688320/graphviz-seems-doesnt-support-png-and-map
    if system(dot_command)
      puts "File generated: #{filename}"
      if Gem.win_platform?
        system "cmd /c \"start #{filename}\" "
      else
        system "xdg-open #{filename}"
      end
    else
      puts 'Error when running dot command: '
      puts dot_command
      puts ''
      puts body
    end
  end

  # TODO: need to refactor API for Graphviz
  # perhaps something like this; for simple display:
  # Graphviz.new(g).display
  # or configuration:
  # viz = Graphviz.new(g)
  # viz.hide_all_edges
  # viz.other_stuff
  # viz.display
  def hide_edges
    edges.each { |e| e.graphviz_hidden = true }
  end

  def show_edges
    edges.each { |e| e.graphviz_hidden = false }
  end

  def display_path(path, path_color: 'red', **opts)
    path.each do |e|
      e.graphviz_color = path_color
      e.graphviz_hidden = false
    end
    display(**opts)
  end

  def display_neato_path(path, **opts)
    display_path(path, layout: 'neato', **opts)
  end
end
