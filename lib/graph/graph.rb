# Metody tworzenia grafu
#
# Metoda 1
#
#  g = Graph.new
#  v1 = g.vertex(1)
#  v2 = g.vertex(2)
#  e = g.add_edge(v1, v2)
#
# Metoda 2
#
#  g = Graph.new({
#    1 => [2, 3],
#    2 => [4]
#  })
#
# Metoda 3
#  g = Graph.new
#  g[1]<<2
#  g[2]<<3
#
#
# Grafy o wagach
#
#  g=

require 'set'

class Graph
  class Vertex
    attr_reader :index, :incoming_edges

    def initialize(index, graph)
      @index = index
      @edges = {} # key=vertex, value=Array of Edges (array because two vertices may have more than 1 edge)
      @incoming_edges = []
      @graph = graph
    end

    alias key index

    def add_edge(v)
      v = @graph.vertex(v)
      e = Edge.new(self, v)
      insert_edge(v, e)
      e
    end

    def insert_edge(v, e)
      if @edges[v]
        @edges[v] << e
      else
        @edges[v] = [e]
      end
      v.insert_incoming_edge e
    end

    def remove_edge(e)
      u = e.other_end(self)
      @edges[u].delete(e)
    end

    def insert_incoming_edge(e)
      @incoming_edges << e
    end

    def first_edge_to(v)
      edges = @edges[v]
      return nil if edges.nil?

      edges[0]
    end

    def has_edge?(v)
      @edges.include? v
    end

    # return array of Edge
    def edges
      @edges.values.flatten
    end

    def each_edge(&block)
      edges.each do |e|
        other = e.other_end(self)
        yield [e, other]
      end
    end

    def to_s
      "Vertex [#{index}]"
    end
    alias inspect to_s

    def <<(v)
      v = @graph.vertex(v)
      add_edge(v)
    end
  end

  class Edge
    attr_reader :vertex, :second_vertex
    attr_accessor :weight
    alias second second_vertex
    alias head vertex
    alias tail second_vertex
    alias first vertex

    def initialize(vertex, other_end, isdirected = true)
      @vertex = vertex
      @second_vertex = other_end
      @weight = nil
      @sibling = isdirected ? nil : self
    end

    def reverse
      @vertex, @second_vertex = @second_vertex, @vertex
    end

    def other_end(vertex)
      return first if vertex == second

      second
    end

    def directed?
      @sibling == self
    end

    def set_weight(w)
      @weight = w
    end

    def has_weight?
      !@weight.nil?
    end

    def to_s
      s = "Edge #{head.index}-#{tail.index}"
      s += " w=#{@weight}" unless @weight.nil?
      s
    end
    alias inspect to_s

    def to_a
      [vertex.index, second_vertex.index]
    end
  end

  def initialize(*args)
    @vertices = {}
    if args.size == 1
      case args[0]
      when Hash
        load_adj_list_graph(args[0])
      end
    end
  end

  def vertex(n, create = true)
    return n if n.instance_of? Vertex

    v = @vertices[n]
    if v.nil? && create
      v = Vertex.new(n, self)
      @vertices[n] = v
    end
    v
  end

  # Tworzy węzeł skierunkowany (directed edge)
  #
  def add_edge(v1, v2)
    v1 = vertex(v1)
    v2 = vertex(v2)

    v1.add_edge(v2)
  end

  # Krawedzi miedzy wierzcholkiem v1 a v2 moze byc teoretycznie wiele
  # ale w wiekszosci przypadkow bedzie jedna
  #
  # Ta funkcja zwraca PIERWSZĄ krawędź z wierzchołka v1 do v2
  # i istnieje DLA WYGODY
  def edge(v1, v2)
    v1 = vertex(v1, false) unless v1.instance_of? Vertex
    v2 = vertex(v2, false) unless v2.instance_of? Vertex

    v1.first_edge_to(v2) if !v1.nil? && !v2.nil?
  end

  # Tworzy węzeł nieskierunkowany (undirected edge)
  #
  def add_uedge(v1, v2)
    v1 = vertex(v1)
    v2 = vertex(v2)

    e = Edge.new(v1, v2, true)
    v1.insert_edge(v2, e)
    v2.insert_edge(v1, e)

    e

    # v1.add_edge(v2) unless v1.has_edge?(v2)
    # v2.add_edge(v1) unless v2.has_edge?(v1)
  end

  def remove_edge(e)
    e.first.remove_edge(e)
    e.second.remove_edge(e)
  end

  def vertices
    @vertices.values
  end

  def edges
    e = Set.new
    @vertices.each do |_n, v1|
      v1.edges.each { |edge| e << edge }
    end
    e.to_a
  end

  def edge_indices
    e = Set.new
    @vertices.each do |_n, v1|
      v1.edges.each { |edge| e << [edge.head.index, edge.tail.index] }
    end
    e.to_a
  end

  def vertex_exists?(v)
    @vertices.key? v
  end

  # ładuje graf na podstawie Hash'a
  #
  # Przykład 1
  #  graph = Graph.new
  #  graph.load_adj_list_graph(g)
  #  graph = Graph.new
  #  g={}
  #  g[1]=[2, 3]
  #  g[2]=[4]
  #  g[3]=[5]
  #  graph.load_adj_list_graph(g)

  def load_adj_list_graph(g)
    g.each do |v, edges|
      edges.each { |v2| add_edge(v, v2) }
    end
  end

  def self.from_adj_list(adj)
    g = Graph.new
    g.load_adj_list_graph(adj)
    g
  end

  def [](v)
    vertex(v)
  end

  def to_s
    s = ''
    vertices.each do |v|
      s += "#{v.index} ->"
      v.edges.map do |e|
        s += " #{e.second.index}"
        s += " (weight=#{e.weight})" unless e.weight.nil?
      end
      s += "\n"
    end
    s
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  # Method #1
  puts 'Graph #1'
  g = Graph.new
  v1 = g.vertex(1)
  v2 = g.vertex(2)
  e = g.add_edge(v1, v2)
  e.weight = 5

  puts g.to_s

  puts 'Graph #2'
  g = Graph.new({ 1 => [2, 3], 2 => [4] })
  puts g.to_s

  # g.load
end
# :nocov:
