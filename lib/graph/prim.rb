require_relative 'graph'
require_relative '../heap/BinaryHeap'
require 'set'

class Graph
  # TODO: - przerobic tak, aby BinaryHeap dosaw
  class Edge
    include Comparable
    def <=>(other)
      weight <=> other.weight
    end
  end

  class Vertex
    attr_accessor :prim_visited
  end

  def prim_binary_heap
    vertices.each { |v| v.prim_visited = false }

    v0 = vertices[0]
    v0.prim_visited = true
    et = Set.new

    heap = BinaryHeap.new
    v0.edges.each { |e| heap.push e }

    (1..vertices.size - 1).each do |_|
      until heap.empty?
        e = heap.pop
        break if e.first.prim_visited != e.second.prim_visited
      end

      return nil if e.nil? # no solution

      if e.first.prim_visited
        u = e.second
        e.second.prim_visited = true
      else
        u = e.first
        e.first.prim_visited = true
      end

      u.edges.each { |edge| heap.push edge }

      et << e
    end
    et
  end

  def prim_first_implementation
    v0 = vertices[0]
    vt = Set[v0]
    et = Set.new

    (1..vertices.size - 1).each do |i|
      e =
        edges.find_all do |ee|
          (vt.include?(ee.first) && (!vt.include? ee.second)) || (vt.include?(ee.second) && (!vt.include? ee.first))
        end.min { |e1, e2| e1.weight <=> e2.weight }

      puts "Iteration #{i}. Picking edge #{e.first.index}-#{e.second.index}, weight=#{e.weight}"

      vt <<
        if vt.include? e.first
          e.second
        else
          e.first
        end

      et << e
    end
    et
  end

  # alias prim prim_first_implementation
  alias prim prim_binary_heap
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  require_relative 'graphviz'

  g = Graph.new
  g.add_uedge(0, 1).weight = 4
  g.add_uedge(0, 7).weight = 7
  g.add_uedge(1, 2).weight = 8
  g.add_uedge(1, 7).weight = 11
  g.add_uedge(2, 3).weight = 7
  g.add_uedge(2, 8).weight = 2
  g.add_uedge(2, 5).weight = 4
  g.add_uedge(3, 4).weight = 9
  g.add_uedge(3, 5).weight = 14
  g.add_uedge(4, 5).weight = 10
  g.add_uedge(5, 6).weight = 2
  g.add_uedge(6, 7).weight = 1
  g.add_uedge(6, 8).weight = 6
  g.add_uedge(7, 8).weight = 7

  g.prim.each { |e| e.graphviz_color = 'red' }
  g.display_neato(false)
end
# :nocov:
