#
# Maximum bipartite matching (PL: skojarzenie maksymalne grafu)
#

require_relative 'graph'

require 'set'

class Graph
  # Simplistic implementation of Edmond's algorithm for finding maximum bipartite matching of graph G=(V+U, E)
  # left - V set
  # right - U set
  # Simplistic because algorithm keeps "restarts" with each iteration. Real Edmond's algorithm does not do restarts.
  def max_bipartite_matching(left, right)
    m = {}
    while find_augmenting_path(m, left, right)
    end
    # map contains both mappings: a -> b and b ->a. Let's make it unique
    m.map{|a, b| [a, b].sort_by &:key }.uniq
  end

  def find_augmenting_path(m, left, right)
    # "free" vertices are vertices that have no matching yet
    free_left = Set.new(left.map{|v| vertex(v)}) - m.flatten
    free_right = Set.new(right.map{|v| vertex(v)}) - m.flatten

    free_left.each do |f|
      q = [f]
      prev = {}
      visited = {}
      visited[f] = true
      until q.empty?
        v = q.shift

        v.edges.each do |e|
          second = e.other_end(v)
          if free_right.include?(second)
            n = prev[v]
            m[v] = second
            m[second] = v

            until n.nil?
              p = prev[n]
              m[n] = p
              m[p] = n
              n = prev[p]
            end

            return true
          else
            next if visited[second]

            prev[second] = v
            q << second
            visited[second] = true
          end
        end
      end
    end
    false
  end

  def edmond_max_bipartite_matching(vset, uset)
    vset = vset.map{|v| vertex(v)} # make sure that it contains Vertex objects, not vertex keys!
    uset = uset.map{|v| vertex(v)}

    m = {}

    q = vset.dup

    free = Set.new(vset + uset)
    mate = {}
    prev = {}
    until q.empty?
      w = q.shift
      if vset.include? w # w in V
        w.edges.each do |e|
          u = e.other_end(w)
          if free.include? u
            m[w] = u
            mate[u] = w
            free.delete u
            free.delete w
            v = w
            while prev[v]
              u = prev[v]
              v = prev[u]
              m[v] = u
              mate[u] = v
            end
            q = vset.select { |v| !m.include? v } # reset Q
            prev = {}
            break
          else # u is from U and already taken
            if prev[u].nil?
              prev[u] = w
              q << u
            end
          end
        end
      else # w in U but it has corresponding vertex v in V (v is match of w; v is mate of w)
        prev[mate[w]]=w
        q << mate[w]
      end
    end
    m
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  require_relative 'graphviz'

  def do_test(g, left, right)
    # m = g.max_bipartite_matching(left, right)
    m = g.edmond_max_bipartite_matching(left, right)
    puts m
    m.each do |k, v|
      g.edge(k, v).graphviz_color = 'red'
    end
    g.display(make_digraph: false, ranks:[left, right], debug: true)
  end

  g = Graph.new
  g.add_uedge(1, 2)
  g.add_uedge(1, 3)
  g.add_uedge(1, 5)
  g.add_uedge(2, 4)
  g.add_uedge(2, 5)
  g.add_uedge(1, 6)

  # do_test(g, [1,2], [3,4,5,6])
  
  g = Graph.new
  g.add_uedge(1, 6)
  g.add_uedge(1, 7)
  g.add_uedge(2, 6)
  g.add_uedge(3, 6)
  g.add_uedge(3, 8)
  g.add_uedge(4, 8)
  g.add_uedge(4, 9)
  g.add_uedge(4, 10)
  g.add_uedge(5, 9)
  g.add_uedge(5, 10)

  # do_test(g, [1, 2, 3, 4, 5], [6, 7, 8, 9, 10])

  g = Graph.new
  g.add_uedge(1, 6)
  g.add_uedge(1, 9)
  g.add_uedge(2, 6)
  g.add_uedge(2, 7)
  g.add_uedge(3, 7)
  g.add_uedge(3, 8)
  g.add_uedge(4, 8)


  do_test(g, [1, 2, 3, 4], [6, 7, 8, 9])

  #    g = Graph.new
  #    g.add_uedge(1,2)
  #    g.add_uedge(1,3)
  #    g.add_uedge(2,4)
  #    g.add_uedge(3,5)
  #    g.add_uedge(3,4)
  #    g.add_uedge(5,2)
  #    g.add_uedge(6,1)
  #    g.add_uedge(6,3)
  #
  #    m=g.max_bipartite_matching()
  #    puts m
  #    m.each{|k,v|
  #      g.edge(k,v).graphviz_color='red'
  #    }
  #    g.display(false, 'LR')

  #    g = Graph.new
  #    g.add_uedge(1,2)
  #    g.add_uedge(1,3)
  #    g.add_uedge(2,4)
  #    g.max_bipartite_matching.each{|k,v| g.edge(k,v).graphviz_color='red'}
  #    g.display(false, 'LR')

  #    g = Graph.new
  #    (1..20).each{|i|
  #      (1..(rand(3))).each{|j|
  #        g.add_uedge(i, rand(20))
  #      }
  #    }
  #    g.max_bipartite_matching.each{|k,v| g.edge(k,v).graphviz_color='red'}
  #    g.display(false, 'LR')
end
# :nocov: