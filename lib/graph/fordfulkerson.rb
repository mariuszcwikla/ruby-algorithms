# Rozwiązuje problem Max-Flow w sieci (network flow)
require_relative 'graph'

class Graph
  INFINITY = 9999

  def ford_fulkerson(source, sink)
    source = vertex(source)
    sink = vertex(sink)

    x = {}
    x.default = 0
    # x[source]=INFINITY #TODO change to infinity

    prev = {}

    q = []
    q << source

    f = {}
    f.default = 0
    l = {}
    l[source] = INFINITY
    until q.empty?
      v = q.shift
      v.edges.each do |e|
        next if l.key? e.second

        r = e.weight - f[e]
        next unless r > 0

        r = [r, l[v]].min
        l[e.second] = r
        prev[e.second] = [v, e, :forward_edge]

        q << e.second
      end
      v.incoming_edges.each do |e|
        next if l.key? e.first # if e.first has been labelled

        r = f[e]
        next unless r > 0

        r = [r, l[v]].min
        l[e.first] = r
        prev[e.first] = [v, e, :backward_edge]
        q << e.first
      end
      next unless l[sink] # if sink has been labelled

      v = sink
      while v != source
        vv, edge, edge_type = prev[v]
        if edge_type == :forward_edge
          x[v] += l[sink]
          f[edge] += l[sink]
        else
          x[v] -= l[sink]
          f[edge] -= l[sink]
        end
        v = vv
      end
      q = []
      q << source
      l = {}
      l[source] = INFINITY
    end

    x.delete source
    [x, f]
  end
end

# :nocov:

if __FILE__ == $PROGRAM_NAME
  require_relative 'graphviz'

  def demo_ford_fulkerson(g, from, to)
    x, edges = g.ford_fulkerson(from, to)
    puts x
    x.each { |v, flow| v.label = "#{v.index} (#{flow})" }
    edges.each do |e, flow|
      next if flow == 0

      e.graphviz_color = flow == e.weight ? 'red' : 'brown'
      e.graphviz_label = "#{flow}/#{e.weight}"
    end
    g.display(make_digraph: true, rankdir: 'LR')
  end

  g = Graph.new
  g.add_edge(1, 2).weight = 2
  g.add_edge(1, 4).weight = 3
  g.add_edge(2, 3).weight = 1
  g.add_edge(2, 5).weight = 2
  g.add_edge(4, 3).weight = 2
  g.add_edge(3, 6).weight = 2
  g.add_edge(5, 6).weight = 4

  demo_ford_fulkerson(g, 1, 6)

  g = Graph.new
  g.add_edge(1, 2).weight = 5
  g.add_edge(1, 4).weight = 3
  g.add_edge(2, 3).weight = 1
  g.add_edge(2, 5).weight = 3
  g.add_edge(4, 3).weight = 1
  g.add_edge(3, 6).weight = 2
  g.add_edge(5, 6).weight = 4
  demo_ford_fulkerson(g, 1, 6)

end
# :nocov:
