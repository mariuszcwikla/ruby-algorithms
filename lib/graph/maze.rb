require_relative 'graph'
require_relative 'planar_graph'

class Graph
  # load graph and returns array with two elements: start vertex (S) and target vertex(T)
  def load_maze_from_file(filename)
    lines = IO.readlines(filename)
    width = lines[0].strip.size
    height = lines.size

    blanks = [' ', '.', 'S', 'T']

    start = nil
    target = nil

    lines.each_with_index do |line, y|
      line = line.strip
      (0..(line.size - 1)).each do |x|
        next unless blanks.include? line[x]

        v = vertex([x, y])
        v.set_position(x, y)
        start = v if line[x] == 'S'
        target = v if line[x] == 'T'

        [[-1, 0], [1, 0], [0, -1], [0, 1]].each do |dx, dy|
          nx = x + dx
          ny = y + dy
          unless (nx >= 0) && (nx <= width - 1) && (ny >= 0) && (ny <= height - 1)
            next
          end

          next unless blanks.include? lines[ny][nx]

          v2 = vertex([nx, ny])
          v2.set_position(nx, ny)
          edge_by_distance(v, v2) unless v.has_edge?(v2)
        end
      end
    end
    [start, target, width, height]
  end

  def to_maze_str(start, target, width, height)
    lines = []
    (0..(height - 1)).each do |y|
      line = ''
      (0..(width - 1)).each do |x|
        v = vertex([x, y], create = false)
        line <<
          if !v.nil?
            ' '
          else
            '#'
          end
      end
      lines << line
    end
    lines[start.y][start.x] = 'S' unless start.nil?
    lines[target.y][target.x] = 'T' unless target.nil?
    lines
  end

  def draw_maze(width, height)
    puts to_maze_str(width, height)
  end
end
