require_relative 'graph'
require_relative 'graphviz'
require_relative '../heap/BinaryHeap'

class Graph
  class Vertex
    attr_accessor :distance, :heap_handle

    def <(other)
      @distance < other.distance
    end
  end
end

class Dijkstra
  # if target=nil then algorithm finishes when target is found
  # otherwise it finds routes to all connected nodes
  def initialize(graph, start, target = nil)
    @graph = graph
    @start = start
    @target = target
    @prev = {}
  end

  def run(&block)
    heap = BinaryHeap.new

    @graph.vertices.each { |v| v.distance = Float::INFINITY }
    @start.distance = 0
    @graph.vertices.each do |v|
      v.heap_handle = heap.push(v)
    end
    v = nil
    until heap.empty?
      v = heap.pop
      v.heap_handle = nil

      break if v == @target

      v.edges.each do |e|
        next unless v.distance + e.weight < e.tail.distance

        block.call(v, @prev) if block_given?
        e.tail.distance = v.distance + e.weight

        heap.key_decreased(e.tail.heap_handle)
        @prev[e.tail] = e
      end
    end

    return [] if @prev[@target].nil?

    route = []
    n = @target
    while n != @start
      e = @prev[n]
      route << e
      n = e.head
    end
    route
  end
end
