require_relative 'graph'

class Graph
  class Vertex
    attr_reader :x, :y

    def set_position(x, y)
      @x = x
      @y = y
    end

    def distance_to(v)
      dx = v.x - @x
      dy = v.y - @y
      Math.sqrt(dx * dx + dy * dy)
    end

    def manhattan_distance_to(v)
      (@x - v.x).abs + (@y - v.y).abs
    end
  end

  # makes edge by euclidean distance
  def edge_by_distance(v1, v2)
    v1 = vertex(v1)
    v2 = vertex(v2)
    d = v1.distance_to(v2)
    add_edge(v1, v2).set_weight(d)
  end

  def uedge_by_distance(v1, v2)
    v1 = vertex(v1)
    v2 = vertex(v2)
    d = v1.distance_to(v2)
    e = add_uedge(v1, v2)
    e.set_weight(d)
    e
  end

  def edge_by_manhattan_distance(v1, v2)
    v1 = vertex(v1) unless v1.instance_of? Vertex
    v2 = vertex(v2) unless v2.instance_of? Vertex
    d = v1.manhattan_distance_to(v2)
    add_edge(v1, v2).set_weight(d)
  end
end
