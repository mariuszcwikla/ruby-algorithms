# This file is copy-pasted from https://github.com/japgolly/astacus/blob/master/vendor/plugins/ci_reporter/lib/ci/reporter/report_manager.rb
# in order to fix those annoying warnings
# C:/tools/ruby27/lib/ruby/gems/2.7.0/gems/ci_reporter-2.0.0/lib/ci/reporter/report_manager.rb:51: warning: File.exists? is a deprecated name, use File.exist? instead
# ci_reporter is unfortunately not maintained any more so I have redefined the method

require 'fileutils'

module CI #:nodoc:
  module Reporter #:nodoc:
    class ReportManager
      def initialize(prefix)
        @basedir = ENV['CI_REPORTS'] || File.expand_path("#{Dir.getwd}/#{prefix.downcase}/reports")
        @basename = "#{@basedir}/#{prefix.upcase}"
        FileUtils.mkdir_p(@basedir)
      end

      def write_report(suite)
        File.open("#{@basename}-#{suite.name.gsub(/[^a-zA-Z0-9]+/, '-')}.xml", 'w') do |f|
          f << suite.to_xml
        end
      end
    end
  end
end
