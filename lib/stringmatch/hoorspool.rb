def hoorspool_table(s)
  t = {}
  t.default = s.size
  #for i in 0...(s.size-1)
  (0...s.size-1).each do |i|
    t[s[i]] = s.size - i - 1
  end
  t
end
# Sometimes this algorithm is called "Hoorspool's". Sometimes Boyer-Moore-Hoorspool.
def hoorspool_string_match(a, b, table: nil)  
  t = table || hoorspool_table(b)
  i = b.size - 1
  while i < a.size
    j = 0
    j += 1 while (j < b.size) && (a[i - j] == b[b.size - 1 - j])

    return i - (b.size - 1) if j == b.size

    raise "shift==0 char=#{a[i-1]}" if t[a[i - j]] == 0 # something clearly wrong

    i += t[a[i]]
  end
  -1
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  a = 'ABBBBRBBFOOBARCCC'
  b = 'FOOBAR'

  puts "Hoorspool's table:"
  hoorspool_table(b).each { |k, v| puts "#{k} - #{v}" }

  puts "Match at: #{hoorspool_string_match(a, b)}"
end
# :nocov:
