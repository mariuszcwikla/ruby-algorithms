# TODO: move this somewhere else

class Trie
  class Node
    attr_reader :children, :level, :char, :parent
    attr_accessor :final, :string
    def initialize(parent=nil, c=nil)
      @parent = parent
      @string = nil
      @char = c
      # Children are stored as hash for fast lookup
      # Some sources use linked list instead (lower memory footprint)
      # I wonder how big is the difference
      # E.g. in Aho-Corasick we need to find sibling. With hash it's easy:
      #   while !n.failure_node.nil? && n.failure_node.children[c].nil?
      # But some sources iterate over the list, more or less like below:
      #   while !n.nil?
      #     x = n
      #     while !x.nil?
      #       break if x.char == c
      #       x = x.next
      # Even empty hash requires 232 bytes in Ruby:
      # require 'objspace'
      # ObjectSpace.memsize_of({})
      # So this might be an issue... Might result in huge memory consumption for big Tries.
      @children = {}
      @final = false
      @level = if parent.nil?
                0
               else
                parent.level + 1
               end
    end

    def find_child(c)
      @children[c]
    end

    def final?
      final
    end

    def insert(string, i)
      n = self
      while i < string.length
        c = string[i]
        child = Node.new(n, c)
        n.children[c] = child
        n = child
        i += 1
      end
      n.final = true
      n.string = string
      n
    end

    def path
      n = self
      s = ""
      until n.parent.nil?
        s += n.char
        n = n.parent
      end
      s.reverse
    end
  end

  def initialize
    @root = Node.new
  end

  def insert(string)
    n = @root
    string.chars.each_with_index do |c, i|
      nn = n.find_child(c)
      if nn.nil?
        n = n.insert(string, i)
        break
      else
        n = nn
      end
    end
    n.final = true
    n.string = string
  end

  #:nocov:
  def to_graph
    require_relative '../graph/graph'
    require_relative '../graph/graphviz'
    g = Graph.new
    q = [@root]
    indexes = {@root => 0}
    last_idx = 1
    until q.empty?
      n = q.shift
      label = n.char || " "
      v = g.vertex(indexes[n])
      v.label = label
      v.graphviz_fill_color = 'grey' if n.final
      n.children.values.each do |child|
        indexes[child] = last_idx
        last_idx += 1
        g.add_edge(indexes[n], indexes[child])
        q << child
      end
    end
    g
  end
  #:nocov:
end