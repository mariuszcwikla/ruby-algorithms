require_relative 'trie'

# Reopened class, adds some attributes
class Trie
  class Node
    attr_accessor :failure_node, :output_node
  end
end

class AhoCorasickTrie < Trie

  def find_failure_node(n)
    return nil if n == @root
    c = n.char
    n = n.parent

    while !n.failure_node.nil? && n.failure_node.children[c].nil?
      n = n.failure_node
    end
    return @root if n.failure_node.nil?

    n.failure_node.children[c]
  end

  def find_output_node(n)
    return nil if n == @root
    n = n.failure_node

    if n.final?
      n
    else
      n.output_node
    end
  end

  def aho_corasick_prepare
    @root.failure_node = nil
    q = [@root]
    until q.empty?
      n = q.shift
      n.failure_node = find_failure_node(n)
      n.output_node = find_output_node(n)
      n.children.values.each do |child|
        q << child
      end
    end
  end

  def search(text)
    n = @root
    text.chars.each_with_index do |c, i|
      nn = n.children[c]
      if nn.nil?
        while !n.failure_node.nil? && n.failure_node.children[c].nil?
          n = n.failure_node
        end
        if n.failure_node.nil?
          n = @root.children[c] || @root
        else
          n = n.failure_node.children[c]
        end
      else
        n = nn
      end

      yield [n.string, i - n.string.length + 1] if n.final?
      output = n.output_node
      until output.nil?
        yield [output.string, i - output.string.length + 1]
        output = output.output_node
      end
    end
  end

  #:nocov:
  def to_graph(display_output_nodes: true)
    require_relative '../graph/graph'
    require_relative '../graph/graphviz'
    g = Graph.new
    g.init_graphviz
    q = [@root]
    indexes = { @root => 0 }
    last_idx = 1

    ranking = Hash.new { |h, k| h[k] = [] }
    until q.empty?
      n = q.shift
      ranking[n.level] << indexes[n]
      label = n.char || ' '
      v = g.vertex(indexes[n])
      v.label = label
      v.graphviz_fill_color = 'grey' if n.final
      n.children.values.each do |child|
        indexes[child] = last_idx
        last_idx += 1
        g.add_edge(indexes[n], indexes[child])
        q << child
      end

      unless n.failure_node.nil?
        e = g.add_edge(indexes[n], indexes[n.failure_node])
        e.graphviz_color = 'blue'
      end
      if display_output_nodes && !n.output_node.nil?
        e = g.add_edge(indexes[n], indexes[n.output_node])
        e.graphviz_color = 'red'
      end
    end
    g.graphviz.ranks = ranking.values
    g
  end
  #:nocov:
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  t = AhoCorasickTrie.new
  t.insert("a")
  t.insert("ab")
  t.insert("bab")
  t.insert("bc")
  t.insert("bca")
  t.insert("c")
  t.insert("caa")
  t.to_graph.display(debug: true)    # displays Trie only
  t.aho_corasick_prepare
  t.to_graph(display_output_nodes: false).display(debug: true)    # displays Trie with failure nodes
  t.to_graph.display        # displays Trie with failure nodes and output nodes
  
  text = "baaaaababaaaacaa"
  puts "Searching in text #{text}"
  t.search(text) { |string, i| puts "match at #{i}: #{string}"}
end
# :nocov: