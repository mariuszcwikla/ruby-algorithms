require_relative '../hash/rolling_hash'

# hash_type: :poly, :buzz
def rabin_karp_match(a, b, hash_type = :poly)
  raise 'b is longer than a' if b.size > a.size

  if hash_type == :poly
    h = PolyRollingHash.new(b).hash_value
    rh = PolyRollingHash.new(a[0...(b.size)])
  elsif hash_type == :buzz
    h = BuzzHash.new(b).hash_value
    rh = BuzzHash.new(a[0...(b.size)])
  else
    raise "Unknown hash type: " + hash_type
  end

  if h == rh.hash_value
    return 0 if b == a[0...b.size]
  end
  (b.size...a.size).each do |i|
    rh.roll(a[i])

    if h == rh.hash_value
      return i - b.size + 1 if b == a[(i - b.size + 1)..i]
    end
  end
  -1
end
