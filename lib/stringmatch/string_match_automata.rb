# Here are 3 implementations of automata building algorithm
# O(m^4 |E|) - build_automata+delta (first implementation; easy to grasp)
# O(m^3 |E|) - next iteration
# O(m |E|) - final optimization; idea taken from KMP

def delta(x, pattern)
  k = x.length - 1
  while k >= 0
    return k + 1 if pattern.end_with?(x[0..k])
    k -= 1
  end
  0
end

# O(m^3 |E|)
# Implementation as per Cormen et al.
def build_automata(s, debug: false)
  alphabet = s.chars.uniq.sort #sorting not needed but I'm using it for display purposes
  automata = Array.new(s.length + 1) do
    Hash.new.tap {|h| h.default=0}
  end

  for q in 0..s.length
    for j in 0...alphabet.length
      c = alphabet[j]
      d = delta(s[0..q], s[0...q] + c)  #actually s[0..q] increases complexity to O(m^4 |E|). But I used this for simplicity. See below for O(m^3 |E|) solution
      puts "delta(#{s[0...q]} + #{c})=#{d}" if debug
      automata[q][c] = d
    end
  end
  [automata, alphabet]
end

def delta_Om3(string, len, new_char)
  return len+1 if string[len] == new_char
  k = len - 1
  while k >= 0
    if new_char == string[k]
      i = k - 1
      j = len - 1
      while i >= 0
        break if string[i] != string[j]
        i -= 1
        j -= 1
      end
      return k + 1 if i == -1
    end
    k -= 1
  end
  0
end

def build_automata_Om3(s)
  alphabet = s.chars.uniq.sort #sorting not needed but I'm using it for display purposes
  automata = Array.new(s.length + 1) do
    Hash.new.tap {|h| h.default =0}
  end

  for q in 0..s.length
    for j in 0...alphabet.length
      c = alphabet[j]
      d = delta_Om3(s, q, c)
      automata[q][c] = d
    end
  end
  [automata, alphabet]
end

# O(m |E|)
def build_automata_Om(s)
  alphabet = s.chars.uniq.sort #sorting not needed but I'm using it for display purposes
  automata = Array.new(s.length + 1) do
    Hash.new.tap { |h| h.default =0 }
  end

  pi = Array.new(s.length)
  pi[0] = 0
  for q in 0...s.length
    if q > 0
      k = pi[q - 1]
      while k > 0 && s[k] != s[q]
        k = pi[k] - 1
      end
      k = 0 if k < 0
      if s[k]==s[q]
        pi[q] = k + 1
      else
        pi[q] = 0
      end
    end

    for j in 0...alphabet.length
      c = alphabet[j]
      if c == s[q]
        automata[q][c] = q+1
      elsif q==0
        automata[q][c] = 0
      else
        k = pi[q - 1]
        while k > 0 && s[k] != c
          k = pi[k] - 1
        end
        k = 0 if k < 0
        k += 1 if c==s[k]
        automata[q][c] = k

      end
    end
  end
  [automata, alphabet]
end

def automata_match(automata, text)
  q = 0
  table, _ = automata
  final_state = table.length - 1
  for i in 0...text.length
    #binding.pry if i==
    c = text[i]
    q = table[q][c]
    #puts q
    return i - final_state + 1 if q == final_state
  end  
  -1
end

# :nocov:
def display_automata(automata)
  require 'terminal-table'
  table, alphabet = automata

  tt = Terminal::Table.new do |t|
    t << ['q'] + alphabet
    t << :separator
    table.each_with_index do |h, i|
      row = [i] + alphabet.map { |c| h[c]}
      t.add_row row
    end 
  end

  puts tt
end

if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  #automata = build_automata("ababaca")
  automata = build_automata("ababa", debug: true)
  display_automata automata

  puts "match at #{automata_match(automata, "cababafoo")}"
end
# :nocov: