# This algorithm is describewd in String algorithms in C: https://www.apress.com/gp/book/9781484259191
# However the book has few errors.
def compute_borders(string)
  b = Array.new string.length
  b[0] = 0
  for i in 1...string.length
    j = i - 1
    while j > 0 && string[b[j]] != string[i]
      j = b[j] - 1
    end
    j = 0 if j < 0
    b[i] = if string[b[j]] == string[i]
            b[j] + 1
           else
            b[j]
           end
  end
  b
end

def print_borders(string)
  puts "finding borders for #{string}"
  borders = compute_borders(string)
  puts borders.inspect
  b = borders.last
  if b==0
    puts 'no borders'
    return
  end
  while b > 0
    puts string[0...b]
    b = borders[b] - 1
  end
end

def border_string_match(string, pattern, sentinel_char='$', borders: nil)
  #raise 'wrong sentinel characther' unless string.index(sentinel_char).nil?

  borders ||= compute_borders(pattern + sentinel_char + string)
  borders[pattern.length..-1].each_with_index do |b, i|
    if b == pattern.length
      #yield i - 2 * pattern.length   # to get all the results
      return i -  pattern.length
    end
  end
  -1
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  print_borders("ababa")
  print_borders("bbbaa")
  print_borders("bbbaa$abbbaaaabbb")
  puts "string matching"
  puts border_string_match("abbbaaaabbb", "bbbaa")
end
# :nocov: