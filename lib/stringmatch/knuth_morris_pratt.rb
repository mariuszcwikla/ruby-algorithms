# Not optimized version; O(m^3)
# This is a groundwork for O(m) version (later)
def compute_pi_function_not_optimized(s)
  pi = Array.new(s.length)
  pi[0] = 0
  for i in 1...s.length
    # what is max(k: s[0..i].end_with?(s[0..k])
    k = i
    while k > 0 && !s[0..i].end_with?(s[0...k])
      k -= 1
    end
    pi[i] = k
  end
  pi
end

# O(m)
def compute_pi_function(s)
  pi = Array.new(s.length)
  pi[0] = 0
  k = 0
  for i in 1...s.length
    if s[i] == s[pi[i - 1]]
      k = pi[i - 1] + 1
    else
      k = pi[i - 1]
      while k > 0 && s[i] != s[k]
        k = pi[k - 1]
      end
      k = 0 if k < 0
      k += 1 if s[i] == s[k]
    end
    pi[i] = k
  end
  pi
end

def kmp_match(text, pattern, pi: nil)
  pi ||= compute_pi_function(pattern)
  k = 0
  text.chars.each_with_index do |c, i|
    while k > 0 && c != pattern[k]
      k = pi[k - 1]
    end

    if c == pattern[k]
      k += 1
    end

    return i - k + 1 if k == pattern.length
  end
  -1
end

# This method is not used by the algorithm. It just helps me understand behavior on KMP algorithm.
def pi_star(pattern, i)
  pi = compute_pi_function_not_optimized(pattern)
  set = []
  k = pi[i] - 1
  while k >= 0
    set << pattern[0..k]
    k = pi[k] - 1
  end
  set
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  pattern = "aabaaab"
  text = "abaaaabaaab"
  puts pattern
  puts compute_pi_function_not_optimized(pattern).inspect
  puts compute_pi_function(pattern).inspect

  pattern = "bbabbb"
  puts pattern
  for i in 0...pattern.length
    puts "pi*[#{i}]=#{pi_star(pattern, i).inspect}"
  end

  puts pi_star("aabaab", 5).inspect
  #puts "match(#{text}, #{pattern}=#{kmp_match(text, pattern)}"



  
end
# :nocov: