class SqrtDecomposition
  def initialize(array, function, optimized_array_operations: false)
    @array = array
    @function = function
    @optimized_array_operations = optimized_array_operations
    build_segments
  end

  private def build_segments
    @segment_size = Math.sqrt(@array.size).to_i
    num_segments = if @array.size % @segment_size == 0
                      @array.size / @segment_size
                   else
                      @array.size / @segment_size + 1
                   end
    @segments = Array.new(num_segments)

    seg = 0
    i = 0
    val = nil
    @array.each do |elem|
      if i == 0
        val = elem
      else
        val = @function.call(val, elem)
      end
      i+=1
      if i == @segment_size
        @segments[seg] = val
        seg += 1
        i = 0
      end
    end
    if i > 0
      @segments[seg] = val
    end
  end

  def inspect
    "array=#{@array.inspect}\nsegments=#{@segments.inspect}\nsegment size=#{@segment_size}"
  end

  def query(a, b)
    s1 = a.divmod(@segment_size)
    s2 = b.divmod(@segment_size)

    partials = []

    if s1[0]==s2[0]
      if @optimized_array_operations
        # replacing each by while loop increased performance by 23%:
        # with each:
        # Sqrt decomposition (optimized) 10000  1.907000   0.000000   1.907000 (  1.913572)
        # with while loop
        # Sqrt decomposition (optimized) 10000  1.453000   0.000000   1.453000 (  1.461819)
        val = @array[a]
        from = a
        to = b
        from += 1
        while from < to
          val = @function.call(val, @array[from])
          from += 1
        end
        #((a+1)...b).each {|i| val = @function.call(val, @array[i])}
        return val
      else
        return @array[a...b].reduce(&@function)
      end
    end

    if s1[1] != 0
      from = s1[0]*@segment_size + s1[1]
      to = s1[0]*@segment_size + @segment_size
      if @optimized_array_operations
        val = @array[from]
        from += 1
        while from < to
          val = @function.call(val, @array[from])
          from += 1
        end
        # ((from+1)...to).each {|i| val = @function.call(val, @array[i])}
        partials << val
      else
        sub = (from...to).map{|i| @array[i]}
        partials << sub.reduce(&@function)
      end
      mid_a = s1[0] + 1
    else
      mid_a = s1[0]
    end

    if s2[1] != 0
      from = s2[0]*@segment_size
      to = s2[0]*@segment_size + s2[1]
      
      if @optimized_array_operations
        val = @array[from]
        from += 1
        while from < to
          val = @function.call(val, @array[from])
          from += 1
        end
        # ((from+1)...to).each {|i| val = @function.call(val, @array[i])}
        partials << val
      else
        sub = (from...to).map{|i| @array[i]}
        partials << sub.reduce(&@function)
      end
    end
    if @optimized_array_operations
      if mid_a < s2[0]
        val = @segments[mid_a]
        from = mid_a + 1
        to = s2[0]
        while from < to
          val = @function.call(val, @segments[from])
          from += 1
        end
        # ((mid_a+1)...s2[0]).each {|i| val = @function.call(val, @segments[i])}
        partials << val
      end
    else
      if mid_a < s2[0]
        sub = (mid_a...s2[0]).map{|i| @segments[i]}
        partials << sub.reduce(&@function)
      end
    end
    

    partials.reduce(&@function)
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  require 'colorize'
  arr = 10.times.map{ rand(10) }
  arr = *(1..10)
  func = ->(a, b) {a+b}
  # func = ->(a, b) {[a,b].min}
  # func = ->(a, b) {[a,b].max}
  # func = ->(a, b) {a*b}
  # func = ->(a, b) {a^b}
  decomp = SqrtDecomposition.new(arr, func, optimized_array_operations:true)
  puts decomp.inspect

  def brute(arr, a, b, func)
    arr[a..(b - 1)].reduce(&func)
  end

  def test(arr, decomp, a, b, func)
    c = brute(arr, a, b, func)
    d = decomp.query(a, b)
    puts "Testing #{a}..#{b}, brute=#{c}, sqrt decomposed array=#{d} #{c!=d ? 'NOT OK'.red : 'OK'.green }"
  end

  # for i in 0...(arr.length) do 
  #   for j in (i+1)...(arr.length) do
  #     test(arr, t, i, j)
  #   end
  # end

  test(arr, decomp, 0, 2, func)

  100.times do
    a = rand(arr.size)
    b = rand(arr.size)
    next if a == b
    a, b = b, a if b < a
    test(arr, decomp, a, b, func)
  end
end
# :nocov: