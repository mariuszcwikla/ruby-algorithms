def measure(title = nil, &block)
  t1 = Time.now
  result = block.call
  total = Time.now - t1
  if title
    puts "#{title}: finished in #{total.round(2)} seconds"
  else
    puts "Finished in #{total.round(2)} seconds"
  end
  result
end
