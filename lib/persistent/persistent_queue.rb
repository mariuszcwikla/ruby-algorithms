module Persistent
  class Queue
    class Node
      attr_accessor :next, :value

      def initialize(value, next_ = nil)
        @value = value
        @next = next_
      end

      def include?(x)
        n = self
        until n.nil?
          return true if x == n.value

          n = n.next
        end
        false
      end

      def reverse
        node = self
        last = nil
        until node.nil?
          last = Node.new(node.value, last)
          node = node.next
        end
        last
      end
    end

    def initialize(first = nil, last = nil)
      @first = first
      @last = last
    end

    def empty?
      @last.nil? && @first.nil?
    end

    def insert(x)
      Queue.new(@first, Node.new(x, @last))
    end

    def include?(x)
      @first&.include?(x) || @last&.include?(x) || false
    end

    def pop
      if @first.nil?
        raise 'queue is empty' if @last.nil?

        first = @last.reverse
        [first.value, Queue.new(first.next, nil)]
      else
        [@first.value, Queue.new(@first.next, @last)]
      end
    end
    alias push insert
  end
end
