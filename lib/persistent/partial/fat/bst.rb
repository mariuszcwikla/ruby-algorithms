module Fat
  class BinarySearchTree
    attr_reader :version

    class Node
      attr_accessor :parent, :left, :right, :value

      Version = Struct.new(:version, :value, :left, :right, :parent, :node)

      def initialize(parent, value, version)
        @value = value
        @parent = parent
        @left = nil
        @right = nil
        @version = version
        @versions = []
      end

      def size
        s = 1
        s += @left.size unless @left.nil?
        s += @right.size unless @right.nil?
        s
      end

      def take_snapshot(version)
        raise 'snapshot already taken' if @versions.last&.version == version

        @versions << Version.new(@version, @value, @left, @right, @parent, self)
        @version = version
      end

      def version_at(version)
        return self if version >= @version

        idx = @versions.bsearch_index { |v| v.version >= version }
        return @versions.last if idx.nil?

        if @versions[idx].version == version
          @versions[idx]
        else
          @versions[idx - 1]
        end
      end

      def node
        self
      end

      def sentinel?
        @value == :sentinel
      end
    end

    def initialize
      @root = Node.new(nil, :sentinel, 0)
      @version = 0
    end

    def insert(value)
      @version += 1

      if @root.sentinel?
        @root.take_snapshot(@version)
        @root.value = value # nasty HACK... Node is not immutable any more
        @root
      else
        n = @root
        loop do
          if value < n.value
            if n.left.nil?
              n.take_snapshot(@version)
              n.left = Node.new(n, value, @version)
              return n.left
            else
              n = n.left
            end
          else
            if n.right.nil?
              n.take_snapshot(@version)
              n.right = Node.new(n, value, @version)
              return n.right
            else
              n = n.right
            end
          end
        end
      end
    end

    def size
      # TODO: size can be easily optimized to O(1), but don't have time to do it;)
      return 0 if @root.sentinel?

      @root.size
    end

    def find(value, version = @version)
      n = @root.version_at(version)
      return nil if n.value == :sentinel

      until n.nil?
        if value == n.value
          return n
        elsif value < n.value
          n = n.node.version_at(version).left
        else
          n = n.node.version_at(version).right
        end
      end
      nil
    end

    def include?(value, version = @version)
      find(value, version) != nil
    end

    def empty?
      @root.sentinel?
    end

    def remove(value)
      @version += 1
      n = find(value)
      remove_node(n) unless n.nil?
    end

    private def remove_node(n)
      if n.left.nil? && n.right.nil? # i.e. n is leaf
        if n == @root
          @root.take_snapshot(@version)
          @root.value = :sentinel
        else
          n.parent.take_snapshot(@version)
          if n.parent.left == n
            n.parent.left = nil
          else
            n.parent.right = nil
          end
        end
      elsif n.left.nil?
        if n.parent.nil?
          @root.take_snapshot(@version)
          @root.value = n.right.value
          @root.right = nil
        else
          n.parent.take_snapshot(@version)
          if n.parent.left == n
            n.parent.left = n.right
          else
            n.parent.right = n.right
          end
          unless n.right.nil?
            n.right.take_snapshot(@version)
            n.right.parent = n.parent
          end
        end
      elsif n.right.nil?
        if n.parent.nil?
          @root.take_snapshot(@version)
          @root.value = n.left.value
          @root.left = nil
        else
          n.parent.take_snapshot(@version)
          if n.parent.left == n
            n.parent.left = n.left
          else
            n.parent.right = n.left
          end
          unless n.left.nil?
            n.left.take_snapshot(@version)
            n.left.parent = n.parent
          end
        end
      else
        # here we can get rightmost of left subtree
        # or leftmost of right subtree
        rightmost = n.left
        rightmost = rightmost.right until rightmost.right.nil?
        rightmost.take_snapshot(@version)

        if rightmost.parent != n
          rightmost.parent.take_snapshot(@version)
          rightmost.parent.right = rightmost.left
          # unless rightmost.left.nil?
          rightmost.left = n.left
          unless rightmost.left.nil?
            rightmost.left.take_snapshot(@version)
            rightmost.left.parent = rightmost.parent unless rightmost.left.nil?
          end
          # end
        else
          n.take_snapshot(@version)
          n.left = nil
        end

        if n.parent.nil?
          raise "not implemented"
          # @root = rightmost
        else
          n.parent.take_snapshot(@version)
          if n.parent.left == n
            n.parent.left = rightmost
          else
            n.parent.right = rightmost
          end
        end

        rightmost.parent = n.parent
        # rightmost.left = n.left
        rightmost.right = n.right
        # unless rightmost.left.nil?
        #   rightmost.left.take_snapshot(@version)
        #   rightmost.left.parent = rightmost
        # end
        unless rightmost.right.nil?
          rightmost.right.take_snapshot(@version)
          rightmost.right.parent = rightmost
        end
      end
    end

    def visit(version, n = @root, &block)
      return if n.nil?

      n = n.version_at(version)
      return if n.value == :sentinel

      visit(version, n&.left, &block)
      block.call(n.value)
      visit(version, n&.right, &block)
    end

    def values(version)
      return [] if version == 0 # temporary workaround

      values = []
      visit(version) { |v| values << v }
      values
    end

    # Returns a read-only "View" of the tree at version "n"
    # View provides simpler API
    def version_at(n)
      TreeView.new(self, n)
    end

    class TreeView
      def initialize(bst, version)
        @bst = bst
        @version = version
      end

      def include?(value)
        @bst.include?(value, @version)
      end

      def values
        @bst.values(@version)
      end

      def visit(&block)
        @bst.visit(version, nil, &block)
      end
    end
  end
end # module

# :nocov:
if __FILE__ == $PROGRAM_NAME
  t = Fat::BinarySearchTree.new
  t.insert 0
  t.insert 5
  t.insert 15
  t.insert 20
  t.insert 2
  t.insert 7

  t.remove 5
  t.remove 20

  puts "version 0: #{t.values(0)}"
  puts "version 1: #{t.values(1)}"
  puts "version 2: #{t.values(2)}"
  puts "version 3: #{t.values(3)}"
  puts "version 4: #{t.values(4)}"
  puts "version 5: #{t.values(5)}"
  puts "version 6: #{t.values(6)}"
  puts "version 7: #{t.values(7)}"
  puts "version 8: #{t.values(8)}"
end
# :nocov:
