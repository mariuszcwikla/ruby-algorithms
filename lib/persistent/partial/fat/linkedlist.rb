# "Fat" module contains "fat-node" implementations of persistent collections

module Fat
  class LinkedList
    attr_reader :head, :version

    class Node
      attr_accessor :left, :right, :version
      attr_reader :value

      Version = Struct.new(:version, :value, :left, :right)

      def initialize(value, version)
        @left = nil
        @right = nil
        @value = value
        @version = version
        @versions = []
      end

      def take_snapshot(version)
        @versions << Version.new(@version, @value, @left, @right)
        @version = version
      end

      def version_at(version)
        return self if version >= @version

        idx = @versions.bsearch_index { |v| v.version >= version }
        return @versions.last if idx.nil?

        if @versions[idx].version == version
          @versions[idx]
        else
          @versions[idx - 1]
        end
      end
    end

    def initialize
      @head = Node.new(:sentinel, 0) # head is sentinel node for easier implementation of some operations
      @version = 0
    end

    def insert_after(n, value)
      @version += 1
      n.take_snapshot(@version)
      n.right&.take_snapshot(@version)

      nn = Node.new(value, @version)
      nn.right = n.right
      nn.left = n
      n.right&.left = nn
      n.right = nn

      nn
    end

    def remove(n)
      raise "can't remove head" if n == @head

      @version += 1
      n.left&.take_snapshot(@version)
      n.right&.take_snapshot(@version)

      n.left&.right = n.right
      n.right&.left = n.left
    end

    def visit(version, &block)
      v = @head.version_at(version).right

      until v.nil?
        block.call(v.value)
        v = v.version_at(version).right
      end
    end

    def values(version)
      values = []
      visit(version) { |v| values << v }
      values
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  list = Fat::LinkedList.new
  n = list.insert_after(list.head, 10)
  list.insert_after(n, 20)
  list.insert_after(list.head, 30)

  puts "number of versions=#{list.version}"
  puts 'version 0:'
  puts list.values(0)
  puts 'version 1:'
  puts list.values(1)

  puts 'version 2:'
  puts list.values(2)

  puts 'version 3:'
  puts list.values(3)

  list.remove(n)
  puts 'version 4 (after remove 10):'
  puts list.values(4)
end
# :nocov:
