#!/usr/bin/env ruby

module Persistent
  class Stack
    class Node
      attr_accessor :next, :value

      def initialize(value, tail)
        @value = value
        @next = tail
      end
    end

    def initialize(last = nil)
      @last = last
    end

    def empty?
      @last.nil?
    end

    def push(x)
      last = Node.new(x, @last)
      Stack.new(last)
    end

    def peek
      @last&.value
    end

    def pop
      [@last&.value, Stack.new(@last&.next)]
    end

    def to_s
      s = '['
      n = @last
      first = true
      until n.nil?
        s << ',' unless first
        first = false
        s << n.value.to_s
        n = n.next
      end
      s + ']'
    end

    def include?(x)
      n = @last
      until n.nil?
        return true if n.value == x

        n = n.next
      end
      false
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  a = Persistent::Stack.new
  a = a.push 5

  b = a.push 10

  puts "a=#{a}"
  puts "b=#{b}"

  puts 'popping from a'
  c = a.pop

  puts "a=#{a}"
  puts "b=#{b}"
  puts "c=#{c}"
end
# :nocov:
