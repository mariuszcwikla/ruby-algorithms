require_relative '../../../order/order_maintenance'
require_relative '../../../tree/avltree'
# Trying to implement Fat-Node BST based on Driscoll, Sleator, Tarjan
# In my implementation I have some simplifications compared to the paper
# The paper is bit overcomplicated in some areas
# e.g. when they talk about searching i1, i2 (i.e. versions of given "field" that are "rightmost/leftmost").
# It seems that they want to store all the pointers in single structure
# With separate "lefts/rights/parents" structures for holding the pointers it seems that it's bit easier to understand the whole concept...

module Persistent
  module Full
    class BinarySearchTree
      attr_reader :version

      class Node
        attr_accessor :value

        Version = Struct.new(:version, :left, :right, :parent)

        def initialize(parent, value, version)
          @value = value
          @parent = parent
          @lefts = AVLTreeMap.new
          @rights = AVLTreeMap.new
          @parents = AVLTreeMap.new
          assign(:parent, parent, version)
        end

        def inspect
          @value
        end

        def assign(field, pointer, version_node)
          map = case field
                  when :left then @lefts
                  when :right then @rights
                  when :parent then @parents
                  else raise "invalid value #{field}"
                end

          map.put(version_node, pointer)
          i1 = map.prev_key(version_node)
          iplus = version_node.next

          return if iplus.nil?

          i2 = map.next_key(version_node)
          if i2 == iplus
            # i.e. iplus was created at this node in one of the previous inserts
            return
          end

          return unless map.get_entry(iplus).nil?

          # Driscoll, Sleator, Tarjan: If in addition i is the version stamp of node x, v1 is a null pointer, and i+ exists, we store in x a null pointer with field name f and version stamp i+
          # ???
          i1val = if i1.nil? then nil
                  else map.get(i1)
                  end
          map.put(iplus, i1val)
        end

        private def value_at_version(map, version)
          map.not_greater_entry(version)&.value
          # previous version is below. However I didn't observe improvements in benchmark
          # v = map.get_entry(version)
          # return v.value unless v.nil?

          # val = map.prev_entry(version)&.value
          # val
        end

        def left(version)
          value_at_version(@lefts, version)
        end

        def right(version)
          value_at_version(@rights, version)
        end

        def parent(version)
          value_at_version(@parents, version)
        end

        def size(version)
          s = 1
          s += left(version).size(version) unless left(version).nil?
          s += right(version).size(version) unless right(version).nil?
          s
        end
      end

      def initialize
        @version = 0
        @version_head = OrderMaintenance::Node.new(@version)
        @version_tail = @version_head
        @root = AVLTreeMap.new
      end

      def root(version)
        @root.not_greater_entry(version)&.value
      end

      def insert(value, version = nil)
        version = @version_tail if version.nil?
        @version += 1
        version_node = version.insert(@version)
        @version_tail = version_node if @version_tail == version

        root = root(version)
        if root.nil?
          root = Node.new(nil, value, version_node)
          @root[version_node] = root
          root
        else
          n = root
          while true
            if value < n.value
              left = n.left(version)
              if left.nil?
                left = Node.new(n, value, version_node)
                n.assign(:left, left, version_node)
                break
              else
                n = left
              end
            else
              right = n.right(version)
              if right.nil?
                right = Node.new(n, value, version_node)
                n.assign(:right, right, version_node)
                break
              else
                n = right
              end
            end
          end
        end
        version_node
      end

      def size(version = @version_tail)
        root = root(version)
        return 0 if root.nil?

        root.size(version)
      end

      def find(value, version = @version_tail)
        n = root(version)
        return nil if n.value == :sentinel

        until n.nil?
          if value == n.value
            return n
          elsif value < n.value
            n = n.left(version)
          else
            n = n.right(version)
          end
        end
        nil
      end

      def include?(value, version = @version_tail)
        find(value, version) != nil
      end

      def empty?(version = @version_tail)
        root(version).nil?
      end

      def current_version
        @version_tail
      end

      def remove(value, version = @version_tail)
        version = @version_tail if version.nil?
        @version += 1

        n = find(value, version)
        unless n.nil?
          version_node = version.insert(@version)
          @version_tail = version_node if @version_tail == version
          remove_node(n, version_node) unless n.nil?
          version_node
        end

      end

      private def remove_node(n, version)
        if n.left(version).nil? && n.right(version).nil? # i.e. n is leaf
          parent = n.parent(version)
          if parent.nil?
            @root[version] = nil
          else
            if parent.left(version) == n
              parent.assign(:left, nil, version)
            else
              parent.assign(:right, nil, version)
            end
          end
        elsif n.left(version).nil?
          parent = n.parent(version)
          if parent.nil?
            right = n.right(version)
            @root[version] = right
            right.assign(:parent, nil, version)
          else
            if parent.left(version) == n
              parent.assign(:left, n.right(version), version)
            else
              parent.assign(:right, n.right(version), version)
            end
            unless n.right(version).nil?
              n.right(version).assign(:parent, parent, version)
            end
          end
        elsif n.right(version).nil?
          parent = n.parent(version)
          if parent.nil?
            left = n.left(version)
            @root[version] = left
            left.assign(:parent, nil, version)
          else
            if parent.left(version) == n
              parent.assign(:left, n.left(version), version)
            else
              parent.assign(:right, n.left(version), version)
            end
            unless n.left(version).nil?
              n.left(version).assign(:parent, parent, version)
            end
          end
        else
          # here we can get rightmost of left subtree
          # or leftmost of right subtree
          rightmost = n.left(version)
          rightmost = rightmost.right(version) until rightmost.right(version).nil?

          if rightmost.parent(version) == n
            rightmost.assign(:right, n.right(version), version)
          else
            rightmost.parent(version).assign(:right, rightmost.left(version), version)
            rightmost.left(version).assign(:parent, rightmost.parent(version), version) unless rightmost.left(version).nil?
            rightmost.assign(:left, n.left(version), version)
            rightmost.assign(:right, n.right(version), version)
          end
          
          parent = n.parent(version)
          if parent.nil?
            @root[version] = rightmost
            rightmost.assign(:parent, nil, version)
          else
            if parent.left(version) == n
              parent.assign(:left, rightmost, version)
            else
              parent.assign(:right, rightmost, version)
            end
          end
          rightmost.assign(:parent, parent, version)
          rightmost.left(version).assign(:parent, rightmost, version)  unless rightmost.left(version).nil?
          rightmost.right(version).assign(:parent, rightmost, version) unless rightmost.right(version).nil?
        end
      end

      def visit(version, n = root(version), &block)
        return if n.nil?

        visit(version, n.left(version), &block)
        block.call(n.value)
        visit(version, n.right(version), &block)
      end

      def values(version = @version_tail)
        raise 'version is not a order maintenance node' unless version.instance_of?(OrderMaintenance::Node)

        values = []
        visit(version) { |v| values << v }
        values
      end

    end
  end
end # module

# :nocov:
if __FILE__ == $PROGRAM_NAME
  t = Persistent::Full::BinarySearchTree.new
  v1 = t.insert 'M'
  v2 = t.insert 'C'
  v3 = t.insert 'I'
  v4 = t.insert 'P'
  v5 = t.remove 'C'

  v1_1 = t.insert('A', v1)
  v1_2 = t.insert('L', v1_1)
  v1_3 = t.remove('A', v1_2)

  puts "version (latest): #{t.values}"
  [v1, v2, v3, v4, v5, v1_1, v1_2, v1_3].each do |v|
    puts "version #{v.label}: #{t.values(v).join ' '}"
  end
end
# :nocov:
