class SkipList
  attr_reader :size

  class Node
    attr_reader :value
    attr_accessor :left, :right, :up, :down

    def initialize(value)
      @value = value
      @left = nil
      @right = nil
      @up = nil
      @down = nil
    end

    def insert(n)
      n.right = @right
      n.right.left = n unless n.right.nil?
      @right = n
      n.left = self
    end

    def lower?(value)
      # below line in practice is not needed
      # after I commented out I noticed ~50% boost on performance

      # return true if value.nil?   # i.e. sentinel node
      # return true if @value == :header
      @value < value
    end

    def dump
      s = ''
      n = self
      until n.nil?
        s += n.value.to_s
        s += ' '
        n = n.right
      end
      s
    end

    def inspect
      @value.to_s
    end

    def to_s
      @value.to_s
    end

    def detach
      @left.right = @right
      @right.left = @left unless @right.nil?
    end

    def height
      h = 1
      n = up
      until n.nil?
        h += 1
        n = n.up
      end
      n = down
      until n.nil?
        h += 1
        n = n.down
      end
      h
    end
  end

  def initialize(probability = 0.5)
    # :header represents "sentinel node" -  head of the list
    # nil represents "tail sentinel" node
    @topleft = Node.new(:header)
    @probability = probability
    @size = 0
  end

  def find(x)
    n = @topleft
    while true
      if !n.right.nil? && n.right.value < x
        while !n.right.nil? && n.right.value < x
          n = n.right
        end
      elsif !n.down.nil?
        n = n.down
      else
        break
      end
    end
    n
  end

  def height
    h = 0
    n = @topleft
    until n.nil?
      h += 1
      n = n.down
    end
    h
  end

  def insert(x)
    node = find(x)
    down = nil
    begin
      n = Node.new(x)
      n.down = down
      down.up = n unless down.nil?
      down = n
      node.insert(n)

      while !node.left.nil? && node.up.nil?
        node = node.left
      end
      node = node.up
      if node.nil?
        topleft = Node.new(:header)
        topleft.down = @topleft
        @topleft.up = topleft
        @topleft = topleft
        break
      end
    end while rand > @probability

    @size += 1
  end

  alias << insert

  def include?(value)
    n = find(value)
    n.right&.value == value
  end

  def empty?
    @size == 0
  end

  def remove(value)
    n = find(value).right
    return unless n&.value == value

    until n.nil?
      n.detach
      n = n.up
    end
    @size -= 1
  end

  def dump
    n = @topleft
    height = 0
    until n.nil?
      puts n.dump
      n = n.down
      height += 1
    end
    puts "height = #{height}. Expected size is log2(#{size})=#{Math.log2(size).round(1)}"
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  N = 50
  probability = 0.5
  # srand 0
  s = SkipList.new(probability)
  s << 1
  puts s.dump
  s << 5
  puts s.dump
  s << 2
  puts s.dump
  s << 8
  puts s.dump
  s << 4
  puts s.dump

  t = Time.now

  N.times do
    s << rand(100)
  end
  t2 = Time.now
  puts 'dumping'
  puts s.dump
  puts "Inserted #{N} elements in #{t2 - t}"

  puts 'testing with custom rand function'

  $xn = 13
  $modulus = 71
  $a = 1
  $c = 91

  def rand(max = nil)
    $xn = ($a * $xn + $c) % $modulus
    if max.nil?
      $xn.to_f / $modulus
    else
      $xn % max
    end
  end
  s = SkipList.new
  N.times do
    s << rand(100)
  end
  t2 = Time.now
  puts "Inserted #{N} elements in #{t2 - t}"
end
# :nocov:
