class SkipList2
  attr_reader :size

  class Node
    attr_reader :value
    attr_accessor :left, :right

    def initialize(value, height)
      @value = value
      @left = Array.new(height, nil)
      @right = Array.new(height, nil)
    end

    def height
      @left.size
    end

    def insert(n, level)
      n.right[level] = @right[level]
      @right[level].left[level] = n unless @right[level].nil?
      @right[level] = n
      n.left[level] = self
    end

    def lower?(value)
      # below line in practice is not needed
      # after I commented out I noticed ~50% boost on performance

      # return true if value.nil?   # i.e. sentinel node
      # return true if @value == :header
      @value < value
    end

    def dump(level)
      s = ''
      n = self
      until n.nil?
        s += n.value.to_s
        s += ' '
        n = n.right[level]
      end
      s
    end

    def inspect
      'Node: ' + @value.to_s
    end

    def to_s
      'Node: ' + @value.to_s
    end

    def detach
      @left.right = @right
      @right.left = @left unless @right.nil?
    end
  end

  def initialize(probability = 0.5)
    # :header represents "sentinel node" -  head of the list
    # nil represents "tail sentinel" node
    @topleft = Node.new(:header, 1)
    @probability = probability
    @size = 0
  end

  def find(x)
    n = @topleft
    i = @topleft.height - 1
    loop do
      while n.right[i]&.lower?(x)
        n = n.right[i]
      end

      i -= 1
      break if i < 0
    end
    n
  end

  def height
    @topleft.right.size
  end

  def compute_height_with_flip_coins
    return 1 if empty?

    h = 1
    while h <= height && rand < @probability
      h += 1
    end
    h
  end

  def insert(x)
    node = find(x)

    return if node.value == x

    h = compute_height_with_flip_coins
    n = Node.new(x, h)

    if h > height
      @topleft.left << nil
      @topleft.right << nil
    end

    level = 0
    while level < h
      while level >= node.height && !node.left[level - 1].nil?
        node = node.left[level - 1]
      end
      node.insert(n, level)
      level += 1
    end

    @size += 1
  end

  alias << insert

  def include?(value)
    n = find(value)
    n.right[0]&.value == value
  end

  def empty?
    @size.zero?
  end

  def remove(value)
    n = find(value).right[0]
    return unless n&.value == value

    n.left.each_with_index do |left, level|
      left.right[level] = n.right[level]
      # n.right[level]&.left = left
    end

    n.right.each_with_index do |right, level|
      right.left[level] = n.left[level] unless right.nil?
    end

    @size -= 1
  end

  def dump
    (0...height).reverse_each do |h|
      puts @topleft.dump(h)
    end
    puts "height = #{height}. Expected size is log2(#{size})=#{Math.log2(size).round(1)}"
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  N = 1000
  probability = 0.5
  # srand 0
  s = SkipList2.new(probability)
  s << 1
  puts s.dump
  s << 5
  puts s.dump
  s << 2
  puts s.dump
  s << 8
  puts s.dump
  s << 4
  puts s.dump

  t = Time.now

  N.times do
    s << rand(100)
  end
  t2 = Time.now
  puts 'dumping'
  puts s.dump
  puts "Inserted #{N} elements in #{t2 - t}"
end
# :nocov:
