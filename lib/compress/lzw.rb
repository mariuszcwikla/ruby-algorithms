LZW_DEFAULT_DICTIONARY = (0..255).map { |i| [i.chr, i] }.to_h

def lzw_encode(s, dictionary = nil)
  dictionary = LZW_DEFAULT_DICTIONARY.dup if dictionary.nil?

  dict_index = dictionary.values.max + 1
  buffer = ''
  output = []
  s.each_char.with_index do |c, _i|
    if dictionary.has_key? buffer + c
      buffer += c
    else
      output << dictionary[buffer]
      dictionary[buffer + c] = dict_index
      dict_index += 1
      buffer = c
    end
  end
  output << dictionary[buffer]
  [output, dictionary]
end

def lzw_decode(compressed, encoding_dictionary = nil, debug: false)
  if encoding_dictionary.nil?
    dictionary = LZW_DEFAULT_DICTIONARY.invert
  else
    dictionary = encoding_dictionary.invert
  end

  dict_index = dictionary.keys.max + 1
  last = dictionary[compressed[0]]
  output = last
  puts "Parsed first code #{compressed[0]}" if debug
  compressed[1..-1].each do |code|
    word = dictionary[code]
    puts "Parsing #{code}" if debug

    if word.nil?
      puts "  #{code} does not exist. Adding #{dict_index}=#{last + last[0]}" if debug
      dictionary[dict_index] = last + last[0]
    else
      puts "  Adding #{dict_index}=#{last + word[0]}" if debug
      dictionary[dict_index] = last + word[0]
    end
    dict_index += 1
    last = dictionary[code]
    output += last
  end
  [output, dictionary.invert]
end

# This method returns binary string "00110011..."
def lzw_compress_to_bits(string, dictionary: nil, bits: 9)
  encoded, = lzw_encode(string, dictionary)
  out = ''
  encoded.each do |code|
    s = code.to_s(2).rjust(bits, '0')
    throw "binary-encoded LZW code does not fit #{bits} bits" if s.length > bits
    out += s
  end
  out
end

# This method returns (compressed) string
def lzw_compress(string, dictionary: nil, bits: 9)
  encoded, = lzw_encode(string, dictionary)
  out = []
  buf = ''
  encoded.each do |code|
    s = code.to_s(2).rjust(bits, '0')
    throw "binary-encoded LZW code does not fit #{bits} bits" if s.length > bits
    buf += s
    while buf.length >= 8
      out << buf[0...8].to_i(2)
      buf = buf[8..-1]
    end
  end
  out << buf.ljust(8, '0').to_i(2) if buf.length > 0
  out.pack('C*')
end

def lzw_decompress(string, dictionary: nil, bits: 9)
  # not well optimized ;)
  temp = string.unpack('C*').map { |c| c.to_s(2).rjust(8, '0').split '' }.flatten
  encoded = []
  until temp.length < bits
    encoded << temp.shift(bits).join('').to_i(2)
  end
  lzw_decode(encoded, dictionary)[0]
end

# :nocov:

if __FILE__ == $PROGRAM_NAME
  def run_interactive
    puts 'Interactive mode...'
    loop do
      print '> '
      line = gets.strip
      test(line)
    end
  end

  def test(string, initial_dictionary = nil, debug: true)
    output, final_dictionary = lzw_encode(string, initial_dictionary)
    puts "Input string #{string}" if debug
    puts "Output from algorithm: #{output.join ','}" if debug
    puts "Extended Dictionary: #{final_dictionary.select { |_k, v| v >= 256 }}" if debug
    ratio = string.length / output.length.to_f
    size_ratio = output.length.to_f / string.length * 100
    puts "String length #{string.length}. Output length #{output.length}. Compression ratio #{ratio.round(2)}, size=#{size_ratio.round(2)}%"
    bits = 11
    compressed = lzw_compress_to_bits(string, dictionary: initial_dictionary, bits: bits)
    puts "Compressed: #{compressed}. Num bits: #{compressed.size}, bytes = #{(compressed.size / 8.0).ceil}, original size=#{string.length}"
    decoded, final_dictionary = lzw_decode(output, initial_dictionary, debug: debug)
    puts "Decoded value: #{decoded}" if debug
    puts "Extended Dictionary: #{final_dictionary.select { |_k, v| v >= 256 }}" if debug
    puts "Decoded value #{decoded == string ? 'matches' : 'DOES NOT MATCH!'}"
    puts '---------------------------'
  end
  # run_interactive
  s = 'abccd_abccd_acd_acd_acd_'
  test(s)

  # return
  test('aaaaaaaaabbbbbaaaccddddef')

  dictionary = 1.upto(26).map { |i| [(65 + i - 1).chr, i] }.to_h
  dictionary['#'] = 0
  s = 'TOBEORNOTTOBEORTOBEORNOT#'
  test(s, dictionary)

  long_string =
    "The scenario described by Welch's 1984 paper[1] encodes sequences of 8-bit data as fixed-length 12-bit codes. The codes from 0 to 255 represent 1-character sequences consisting of the corresponding 8-bit character, and the codes 256 through 4095 are created in a dictionary for sequences encountered in the data as it is encoded. At each stage in compression, input bytes are gathered into a sequence until the next character would make a sequence with no code yet in the dictionary. The code for the sequence (without that character) is added to the output, and a new code (for the sequence with that character) is added to the dictionary.
  The idea was quickly adapted to other situations. In an image based on a color table, for example, the natural character alphabet is the set of color table indexes, and in the 1980s, many images had small color tables (on the order of 16 colors). For such a reduced alphabet, the full 12-bit codes yielded poor compression unless the image was large, so the idea of a variable-width code was introduced: codes typically start one bit wider than the symbols being encoded, and as each code size is used up, the code width increases by 1 bit, up to some prescribed maximum (typically 12 bits). When the maximum code value is reached, encoding proceeds using the existing table, but new codes are not generated for addition to the table.
  Further refinements include reserving a code to indicate that the code table should be cleared and restored to its initial state (a \"clear code\", typically the first value immediately after the values for the individual alphabet characters), and a code to indicate the end of data (a \"stop code\", typically one greater than the clear code). The clear code lets the table be reinitialized after it fills up, which lets the encoding adapt to changing patterns in the input data. Smart encoders can monitor the compression efficiency and clear the table whenever the existing table no longer matches the input well.
  Since codes are added in a manner determined by the data, the decoder mimics building the table as it sees the resulting codes. It is critical that the encoder and decoder agree on the variety of LZW used: the size of the alphabet, the maximum table size (and code width), whether variable-width encoding is used, initial code size, and whether to use the clear and stop codes (and what values they have). Most formats that employ LZW build this information into the format specification or provide explicit fields for them in a compression header for the data."

  test(long_string, debug: false)
  run_interactive
end
# :nocov:
