require_relative 'lzw'

from = File.new(ARGV[0], 'rb')
data = from.read

bits = 12
bits = ARGV[2].to_i if ARGV[2]

compressed = lzw_compress(data, bits: bits)

ratio = compressed.length.to_f / data.length * 100

puts "Compression finished. Old size = #{data.size}, compressed size = #{compressed.size}. Ratio #{ratio.round(2)}%"

File.open(ARGV[1], 'wb') { |f| f.write compressed }
