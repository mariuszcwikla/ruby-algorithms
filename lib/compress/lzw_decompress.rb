require_relative 'lzw'

from = File.open(ARGV[0], 'rb')
data = from.read

decompressed = lzw_decompress(data, bits: 12)

puts decompressed
