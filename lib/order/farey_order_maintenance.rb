# This is the same algorithm for order mainenance list that is implemented in 'order_maintenance'
# except for internal representation. 'order_maintenance' is using Float while this is using Rationals.
# Float have drawback - dividing range (0,1) fails after 1074 tries (due to IEEE 754) when sequencing towards 0: 0.5, 0.25, 0.125, ...
# When sequencing towards 1 (0.5, 0.75, 0.825, ...) it takes only 54 iterations!
#
# This one is using Rational numbers to compute labels.
# When record is inserted between nodes with labels a/b and c/d then
# we calculate new rational number (a+c)/(b+d) (this is one of the properties of Farey sequence)
module FareyOrderMaintenance

  class Node
    include Enumerable
    include Comparable
    attr_reader :value, :label
    attr_accessor :next

    def initialize(value, label = Rational(0))
      @value = value
      @next = nil
      @label = label
    end

    def insert(value)
      label = if @next.nil?
                @label + 1
              else
                n = @label.numerator + @next.label.numerator
                d = @label.denominator + @next.label.denominator
                Rational(n, d)
              end
      n = Node.new(value, label)
      n.next = @next
      @next = n
    end

    def after?(node)
      @label > node.label
    end

    def before?(node)
      !after?(node)
    end

    def to_s
      s = "#{@value} (#{@label})"
      if @next
        s + ', ' + @next.to_s
      else
        s
      end
    end

    def inspect
      "#{@value} (#{@label}) next=#{@next&.value}"
    end

    def each(&block)
      n = self
      until n.nil?
        block.call(n)
        n = n.next
      end
    end

    def <=>(other)
      @label <=> other.label
    end

    def hash
      @label.hash
    end

    def find_value(value)
      n = self
      until n.nil? || n.value == value
        n = n.next
      end
      n
    end
  end

end
# :nocov:
if __FILE__ == $PROGRAM_NAME
  include FareyOrderMaintenance
  # This demo shows difference between Float representation for labels vs Rationals
  # Float fails after 54 tries.
  # Just insert data right after head. It will divide (0,1) range by 2 in the loop and at some point after?/before? will be failing
  require_relative 'order_maintenance'
  head = OrderMaintenance::Node.new(31789)   # random number as value; does not matter
  n = 0
  while true
    node = head.insert(432)
    unless node.after?(head)
      puts "Fixnum version failed after #{n} tries. Label of head: #{head.label}. Label of 'broken' node #{node.label}"
      break
    end
    n += 1
  end

  # Let's test the same with Farey property
  head = FareyOrderMaintenance::Node.new(31789)   # random number as value; does not matter
  n = 0
  while true
    node = head.insert(432)
    unless node.after?(head)
      puts "Fixnum version failed after #{n} tries. Label of head: #{head.label}. Label of 'broken' node #{node.label}"
      break
    end
    n += 1
    if n % 100_000 == 0 
      puts "Iteration #{n}: #{node.label}"
    end
  end
end
# :nocov:
