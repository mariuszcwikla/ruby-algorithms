# Simple order-maintenance list
# - when new element is inserted at the end of the list, label(node)=label(prev(node)) + 1
# - when new element is inserted in the middle of the list then label(node) = (label(prev(node))  + label(next(node))) / 2
# then labels determine order
module OrderMaintenance

  class Node
    include Enumerable
    include Comparable
    attr_reader :value, :label
    attr_accessor :next

    def initialize(value, label = 0.0)
      @value = value
      @next = nil
      @label = label
    end

    def insert(value)
      label = if @next.nil?
                @label + 1.0
              else
                (@label + @next.label) / 2.0
              end
      n = Node.new(value, label)
      n.next = @next
      @next = n
    end

    def after?(node)
      @label > node.label
    end

    def before?(node)
      !after?(node)
    end

    def to_s
      s = "#{@value} (#{@label})"
      if @next
        s + ', ' + @next.to_s
      else
        s
      end
    end

    def inspect
      "#{@value} (#{@label}) next=#{@next&.value}"
    end

    def each(&block)
      n = self
      until n.nil?
        block.call(n)
        n = n.next
      end
    end

    def <=>(other)
      @label <=> other.label
    end

    def hash
      @label.hash
    end

    def find_value(value)
      n = self
      until n.nil? || n.value == value
        n = n.next
      end
      n
    end
  end

end
# :nocov:
if __FILE__ == $PROGRAM_NAME
  include OrderMaintenance
  head = Node.new(5)
  a = head.insert(2)
  b = head.insert(3)
  c = head.insert(18)
  d = a.insert(7)
  e = a.insert(8)
  puts head.to_s
  puts head.to_a.map{|n| n.value}.join ' '
  [head, c, b, a, d, e].combination(2).each do |x, y|
    puts "#{x.value} after #{y.value} ?: #{x.after?(y)}"
    puts "#{y.value} after #{x.value} ?: #{y.after?(x)}"
  end
end
# :nocov: