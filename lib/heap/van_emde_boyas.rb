SQRT = 0.upto(32).map { |i| [2**i, Math.sqrt(2**i).to_i] }.to_h
SQRT.freeze

class VanEmdeBoyasTree
  attr_reader :size

  class VebTree
    attr_accessor :min, :max
    attr_reader :summary, :clusters, :u

    def initialize(u, lazy)
      @u = u
      if u > 2
        sqrt_u = SQRT[u]
        @clusters = if lazy
                      Array.new(sqrt_u)
                    else
                      Array.new(sqrt_u){VebTree.new(sqrt_u, lazy)}
                    end
        if lazy
          @summary = nil
        else
          @summary = VebTree.new(sqrt_u, lazy)
        end
      else
        @clusters = []
        # Array.new(u){false}
        @summary = nil
      end
      @min = nil
      @max = nil
    end

    def index(x, y, sqrt_u)
      x * sqrt_u + y
    end

    def insert(value)
      if @min.nil?
        @min = value
        @max = value
      else
        if value < @min
          @min, value = value, @min
        end
        if @u > 2
          sqrt_u = SQRT[@u]
          hi, lo = value.divmod(sqrt_u)
          v = @clusters[hi]
          if v.nil?
            v = @clusters[hi] = VebTree.new(sqrt_u, true)
          end
          if @summary.nil?
            @summary = VebTree.new(sqrt_u, true)
          end
          @summary.insert(hi) if v.min.nil?
          v.insert(lo)
        end
        if value > @max
          @max = value
        end

      end
    end

    def include?(value)
      return false if @min.nil?
      return true if @min == value || @max == value
      return false if value < @min || value > @max

      sqrt_u = SQRT[@u]
      hi, lo = value.divmod(sqrt_u)
      return false if @clusters[hi].nil?

      @clusters[hi].include?(lo)
    end

    def empty?
      @min.nil?
    end

    def successor(value)
      if @u == 2
        if value == 0 && @max == 1
          1
        else
          nil
        end
      else
        return nil if @min.nil?
        return @min if value < @min

        sqrt_u = SQRT[@u]
        hi, lo = value.divmod(sqrt_u)
        c = @clusters[hi]
        if !c.nil? && !c.max.nil? && c.max > lo
          succ = c.successor(lo)
          return nil if succ.nil?

          index(hi, succ, sqrt_u)
        else
          return nil if @summary.nil?

          successor_index = @summary.successor(hi)
          unless successor_index.nil?
            c = @clusters[successor_index]
            index(successor_index, c.min, sqrt_u)
          end
        end
      end
    end

    def predecessor(value)
      if @u == 2
        if value == 1 && @min == 0
          0
        else
          nil
        end
      elsif !@max.nil? && value > @max
        @max
      else
        sqrt_u = SQRT[@u]
        hi, lo = value.divmod(sqrt_u)
        c = @clusters[hi]
        if !c.nil? && !c.min.nil? && c.min < lo
            pred = c.predecessor(lo)
            return nil if pred.nil?

            index(hi, pred, sqrt_u)
        else
          return nil if @summary.nil?

          pred_index = @summary.predecessor(hi)
          if !pred_index.nil?
            c = @clusters[pred_index]
            index(pred_index, c.max, sqrt_u)
          else
            if !@min.nil? && value > @min
              @min
            end
          end
        end
      end
    end

    def remove(value)
      return if @min.nil?

      if @min == @max
        @min = nil
        @max = nil
      elsif @u==2
        if value == 0
          @min = 1
        else
          @min = 0
        end
        @max = @min
      else
        sqrt_u = SQRT[u]
        if value == @min
          first_cluster = @clusters[@summary.min]
          value = index(@summary.min, first_cluster.min, sqrt_u)
          @min = value
        end
        hi, lo = value.divmod(sqrt_u)
        c = @clusters[hi]
        unless c.nil?
          c.remove(lo)
          if c.max.nil?   # i.e. c was made empty in above remove
            @summary.remove(hi)
            if @max == value
              if @summary.max.nil?
                @max = @min
              else
                @max = index(@summary.max, @clusters[@summary.max].max, sqrt_u)
              end
            end
          elsif value == @max
            @max = index(hi, @clusters[hi].max, sqrt_u)
          end
        end
      end
    end

    #:nocov:
    def dump(tabs)
      s = "  " * tabs
      s += "min=#{min} max=#{max} u=#{u}\n"
      s += "  " * (tabs + 1)
      if @summary.nil?
        s += " summary=nil\n"
      else
        s += "summary:\n"
        s += "  " * (tabs+1)
        s += "#{@summary.dump(tabs+1)}\n"
      end
      @clusters.each_with_index do |c, i|
        next if c.nil?

        s += "  " * (tabs + 1)
        s += "cluster #{i}\n"
        if c.is_a? VebTree
          s += "#{c.dump(tabs+2)}\n"
        else
          s += "  " * (tabs + 2)
          s += c.to_s + "\n"
        end
      end
      s
    end
    #:nocov:
  end

  def initialize(universe_size = 2**32, lazy: true)
    @universe_size = universe_size
    @root = VebTree.new(universe_size, lazy)
    @size = 0
  end

  def insert(value)
    raise "value can't be negative" if value < 0
    raise "value greater than universe size" if value >= @universe_size

    @root.insert(value)
    @size += 1
  end

  def include?(value)
    @root.include?(value)
  end

  def empty?
    @root.empty?
  end

  alias push insert

  def min
    raise 'empty heap' if empty?

    @root.min
  end

  def max
    raise 'empty heap' if empty?

    @root.max
  end

  def pop
    raise 'empty heap' if empty?

    min = @root.min
    remove(@root.min)
    min
  end

  alias peek min

  def dump
    @root.dump(0)
  end

  def successor(value)
    @root.successor(value)
  end

  def predecessor(value)
    @root.predecessor(value)
  end

  def remove(value)
    @root.remove(value)
    @size -= 1
  end

  alias << insert
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  require 'ruby-prof'

  t = VanEmdeBoyasTree.new(4, lazy: true)
  t.insert(0)
  t.insert(2)
  # t.insert(1)
  t.insert(3)
  puts t.dump

  t.remove(3)
  puts t.dump
end
# :nocov: