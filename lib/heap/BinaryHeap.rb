class BinaryHeap
  class Element
    # position wskazuje na indeks w tablicy gdzie znajduje się ten element
    attr_reader :value, :position
    attr_writer :value, :position

    def initialize(value, position)
      @value = value
      @position = position
    end
  end

  attr_reader :elems # for debugging

  def initialize(&block)
    @elems = []
    @comparator =
      if block.nil?
        :<.to_proc
      else
        block
      end
  end

  def empty?
    @elems.empty?
  end

  def size
    @elems.size
  end

  def push(n)
    e = Element.new(n, @elems.size)
    @elems << e
    heapify
    e
  end

  alias << push
  alias insert push

  private def heapify
    i = @elems.size - 1
    heapify_up(i)
  end

  def swap_elements(ia, ib)
    @elems[ia], @elems[ib] = @elems[ib], @elems[ia]
    @elems[ia].position = ia
    @elems[ib].position = ib
  end

  private def heapify_up(i)
    while i > 0
      p = (i - 1) >> 1
      if @comparator.call(@elems[i].value, @elems[p].value)
        swap_elements(i, p)
        i = p
      else
        break
      end
    end
  end

  private def heapify_down(i)
    val = @elems[i].value
    while i * 2 + 1 < @elems.size
      l = i * 2 + 1
      r = i * 2 + 2

      lowest = l
      lowest = r if r < @elems.size && @comparator.call(@elems[r].value, @elems[l].value)
      if @comparator.call(@elems[lowest].value, val)
        swap_elements(i, lowest)
        i = lowest
      else
        return
      end
    end
  end

  def pop
    raise 'heap is empty' if empty?

    if @elems.size == 1
      @elems.pop.value
    else
      e = @elems[0]
      @elems[0] = @elems.pop
      @elems[0].position = 0
      heapify_down(0)
      e.value
    end
  end

  def peek
    raise 'empty heap' if @elems.empty?

    @elems[0].value
  end

  def update_key(e, n)
    if @comparator.call(e.value, n)
      increase_key(e, n)
    elsif @comparator.call(n, e.value)
      decrease_key(e, n)
    end
  end

  def decrease_key(e, n)
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    raise 'new key is not lower' unless @comparator.call(n, e.value)
    if (e.position > @elems.size) || (@elems[e.position] != e)
      raise 'element not part of this list'
    end

    e.value = n
    heapify_up(e.position)
  end

  def key_decreased(e)
    # TODO: w przypadku dijstry mam tak:
    #
    # second.distance = v.distance + e.weight
    # heap.decrease_key(second.heap_handle, second)
    #
    # a to powoduje ze walidacja "if n <= e.value" failuje bo przeciez wartosc zostala zmodyfikowana z zewnątrz

    # TODO: wydmuszka; brakuje testów; powstalo tylko na potrzeby Dijkstry
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    if (e.position > @elems.size) || (@elems[e.position].object_id != e.object_id)
      raise 'element not part of this list'
    end

    heapify_up(e.position)
  end

  def increase_key(e, n)
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    raise 'new key is not greater' if n <= e.value
    if (e.position > @elems.size) || (@elems[e.position] != e)
      raise 'element not part of this list'
    end

    e.value = n
    heapify_down(e.position)
  end
end
