class BinomialHeap
  class Element
    attr_accessor :children, :parent, :value

    def initialize(v)
      @value = v
      @children = []
      @parent = nil
    end

    def inspect
      "value=#{@value} degree=#{degree}"
    end

    def to_s
      inspect
    end

    def degree
      @children.size
    end
  end

  def initialize
    @trees = []
  end

  def empty?
    @trees.empty?
  end

  def size
    size = 0
    @trees.each do |e|
      size += 2**e.degree
    end
    size
  end

  def push(value)
    elem = Element.new(value)
    merge_forest([elem])
    elem
  end

  alias << push

  private def merge_forest(other_heap)
    i = 0
    until other_heap.empty?
      t = other_heap.shift
      i += 1 while (i < @trees.size) && (@trees[i].degree < t.degree)
      if i >= @trees.size
        @trees.push t
      else
        if t.degree == @trees[i].degree
          @trees[i] = merge_root(@trees[i], t)
          # jesli po prawej pojawi sie teraz drzewo o takim samym stopniu
          # to nalezy je zmerdzować
          j = i + 1
          new_trees = @trees[0..i]

          while (j < @trees.size) && (new_trees.last.degree == @trees[j].degree)
            new_trees[new_trees.size - 1] = merge_root(new_trees.last, @trees[j])
            j += 1
          end

          new_trees += @trees[j..-1]

          @trees = new_trees
        else
          @trees.insert(i, t)
        end
      end
    end
  end

  private def merge_root(root, other)
    root, other = other, root if other.value < root.value
    other.parent = root
    root.children.unshift(other)
    root
  end

  def peek
    e, = find_min_tree
    e.value
  end

  private def find_min_tree
    raise 'empty heap' if @trees.empty?

    min = 0
    i = 1
    while i < @trees.size
      min = i if @trees[i].value < @trees[min].value
      i += 1
    end
    [@trees[min], min]
  end

  def pop
    t, i = find_min_tree

    if t.degree == 0
      raise "tree of degree=0 should be 0th but was: #{i}" if i > 0

      @trees.shift
    else
      @trees.delete_at(i)
      t.children.each { |c| c.parent = nil }
      merge_forest(t.children.reverse)
    end
    t.value
  end

  def decrease_key(e, n)
    raise 'e is not BinomialHeap::Element' unless e.instance_of? Element
    raise 'new key is not lower' if n >= e.value

    # not possible
    # raise 'element not part of this list' if e.position > @elems.size or @elems[e.position]!=e

    e.value = n
    pull_up(e) while !e.parent.nil? && (e.value < e.parent.value)
  end

  def key_decreased(e)
    # TODO: wydmuszka; brakuje testów; powstalo tylko na potrzeby Dijkstry
    pull_up(e) while !e.parent.nil? && (e.value < e.parent.value)
  end

  private def pull_up(e)
    p = e.parent
    e.parent = p.parent
    p.parent = e
    i = p.children.index e
    p.children[i] = p

    e.children, p.children = p.children, e.children
    e.children.each { |c| c.parent = e }
    p.children.each { |c| c.parent = p }

    if e.parent.nil?
      i = @trees.index p
      @trees[i] = e
      # NOTE: when replacing root I am searching it in @trees array using "index", which is O(n)
      # Instead of this I could store index inside Element to bypass linear searching
    else
      i = e.parent.children.index p
      e.parent.children[i] = e
    end
  end

  def increase_key(e, value)
    raise 'e is not BinomialHeap::Element' unless e.instance_of? Element
    raise 'new key is not greater' if value <= e.value

    e.value = value
    while (c = e.children.index { |ch| ch.value < e.value })
      e = e.children[c]
      pull_up(e)
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  h = BinomialHeap.new
  nodes = []
  5.times do |i|
    # nodes << h.push(rand(10000))
    nodes << h.push(i)
  end

  nodes.each do |n|
    if !n.parent.nil? && n.parent.children.index(n).nil?
      puts 'detected unpinned child'
    end
  end
end
# :nocov:
