=begin

Optimization history in pop method

Initial implementation

  FibonacciHeap - 100000    1.797000   0.000000   1.797000 (  1.795621)
  FibonacciHeap - 1000000  24.031000   0.000000  24.031000 ( 24.039042)

Change #1 - change Hash to Array of arrays of size=(Math.log(size) / Math.log(2)).floor

  FibonacciHeap - 100000    1.375000   0.000000   1.375000 (  1.361470)
  FibonacciHeap - 1000000  18.500000   0.000000  18.500000 ( 18.514749)

Change #2 - change log_2 calculation from Math.log to binary one - even worse

  FibonacciHeap - 100000    1.406000   0.000000   1.406000 (  1.407220)
  FibonacciHeap - 1000000  19.453000   0.000000  19.453000 ( 19.470669)

Change #3 - change log_2 to Math.log2 - no difference. Sometimes lower, sometimes higher. Not noticeable

  FibonacciHeap - 100000    1.344000   0.016000   1.360000 (  1.361300)
  FibonacciHeap - 1000000  18.484000   0.000000  18.484000 ( 18.498306)

Change #2 - change Array of arrays to Array

Tests from another machine

Before optimization

BinaryHeap - 100000       1.421000   0.000000   1.421000 (  1.422231)
BinomialHeap - 100000     0.875000   0.000000   0.875000 (  0.873336)
FibonacciHeap - 100000    1.906000   0.015000   1.921000 (  1.942554)
BinaryHeap - 1000000     19.734000   0.000000  19.734000 ( 19.773593)
BinomialHeap - 1000000   11.563000   0.000000  11.563000 ( 11.563130)
FibonacciHeap - 1000000  25.328000   0.000000  25.328000 ( 25.401512)

After optimization

BinaryHeap - 1000000     19.734000   0.000000  19.734000 ( 19.752215)
BinomialHeap - 1000000   11.593000   0.000000  11.593000 ( 11.602985)
FibonacciHeap - 1000000  17.375000   0.000000  17.375000 ( 17.375860)

=end

class FibonacciHeap
  class Sentinel
    attr_accessor :prev, :next
    alias tail next
    alias head prev
    def initialize
      @prev = self
      @next = self
    end

    def each_sibling
      n = self.next
      # TODO: czy tu nie brakuje:
      # yield n
      # ?
      while n != self
        current = n
        n = n.next
        yield current
      end
    end

    def find_min
      min = self.next
      each_sibling { |n| min = n if n.value < min.value }
      return nil if min == self

      min
    end
  end

  class Element
    attr_reader :value
    attr_accessor :degree, :parent, :next, :prev, :marked, :first_child, :debug_id

    def initialize(v)
      @value = v
      @first_child = nil
      @parent = nil
      @next = self
      @prev = self
      @degree = 0
      @marked = false
    end

    def is_root?
      @parent.nil?
    end

    def inspect
      "#{@value}(degree=#{degree})"
    end

    def update_value(n)
      @value = n
    end

    def merge_list(e)
      # merge 2 lists - insert list after self:

      #   self <-> next
      #
      #   self <-> e <-> .... <-> e.prev <-> next

      @next.prev = e.prev
      e.prev.next = @next
      e.prev = self
      @next = e
    end

    def find_non_sentinel(node)
      if node.instance_of? Sentinel
        if node.next.instance_of? Sentinel
          return nil
        else
          return node.next
        end
      end
      node
    end

    def disconnect_node
      # Tutaj Sentinel sprawia troche problemow, bo
      # parent.first_child = @next
      # powodowalo ze pojawial sie Sentinel w first_child

      if @parent&.first_child == self
        @parent.first_child = find_non_sentinel(@next)
        if @parent.first_child == self
          @parent.first_child = nil
        end
        max_degree = 0
        @parent.first_child&.each_sibling { |n| max_degree = n.degree if n.degree > max_degree }
        @parent.degree = max_degree + 1
      end
      @parent = nil
      @prev.next = @next
      @next.prev = @prev
      @next = @prev = self
    end

    def append_child(n)
      raise 'append_child(self)' if n == self

      n.parent = self
      if @first_child.nil?
        @first_child = n
      else
        @first_child.merge_list(n)
      end
    end

    def to_s
      "#{@value} (#{@degree}, id=#{@debug_id})"
    end

    def siblings_to_s
      s = ''
      n = 10
      each_sibling do |e|
        p = parent.nil? ? '' : parent.value
        s << "#{e.value} (#{e.degree}, id=#{e.debug_id}, p=#{p}) "
        n -= 1
        if n <= 0
          s << '...'
          return s
        end
      end
      s
    end

    def each_sibling(&block)
      n = self.next
      # TODO: czy tu nie brakuje:
      # yield self
      block.call self
      # ?
      while n != self
        current = n
        n = n.next
        # yield current unless current.instance_of? Sentinel
        block.call(current) unless current.instance_of? Sentinel
      end
    end

    def each_child(&block)
      first_child&.each_sibling(&block)
    end
  end

  attr_reader :size, :min

  def initialize
    @roots = Sentinel.new
    @min = nil
    @size = 0
    @next_debug_id = 0
  end

  def empty?
    @min.nil?
  end

  def push(value)
    @size += 1

    elem = Element.new(value)
    elem.debug_id = @next_debug_id
    @next_debug_id += 1
    if @min.nil?
      @min = elem
      @min.merge_list(@roots)
    else
      @min.merge_list(elem)
      if value < @min.value
        @min = elem
      end
    end
    elem
  end

  alias << push

  def peek
    raise 'empty heap' if @min.nil?

    @min.value
  end

  def pop
    raise 'empty heap' if @min.nil?

    @size -= 1
    min_v = @min.value

    if size == 0
      @min = nil
      @roots = Sentinel.new
    else
      unless @min.first_child.nil?
        @min.first_child.each_sibling { |c| c.parent = nil }
        @min.first_child.merge_list(@min.prev)
      end

      @min.disconnect_node

      method = 1

      # r = []
      # @roots.each{|e| r << e}

      # root = @roots.find{|x| x!=nil}

      if method == 1
        max = Math.log2(size).floor
        roots = Array.new(max + 1)

        steps = 0

        @roots.each_sibling do |e|
          until roots[e.degree].nil?
            a = e
            b = roots[e.degree]
            roots[e.degree] = nil
            a, b = b, a if b.value < a.value
            a.degree += 1
            b.parent = a
            b.disconnect_node
            a.append_child(b)

            e = a
            steps += 1
          end
          roots[e.degree] = e
        end
      else
        # :nocov:
        # excluded from code coverage. Probably I should have remove it;)

        #      all_roots = Hash.new{|h, k|h[k]=[]}
        #      @min.each_sibling{|n| all_roots[n.degree]<<n}
        #      max = all_roots.keys.max

        # max = (Math.log(size) / Math.log(2)).floor
        max = Math.log2(size).floor
        all_roots = Array.new(max + 1) { [] }
        @roots.each_sibling { |n| all_roots[n.degree] << n }

        root = nil

        (0..max).each do |d|
          roots = all_roots[d]
          while roots.size > 2
            a = roots.shift
            b = roots.shift

            a, b = b, a if b.value < a.value
            a.degree += 1
            b.parent = a
            b.disconnect_node
            a.append_child(b)

            all_roots[a.degree] << a
          end
          root = roots[0] if root.nil?
          d += 1
        end
        # :nocov:
      end
      @min = @roots.find_min # phase 2 as per wikipedia
    end
    min_v
  end

  def decrease_key(e, n)
    raise 'e is not FibonacciHeap::Element' unless e.instance_of? Element
    raise 'new key is not lower' if n >= e.value

    e.update_value n
    @min = e if e.value < @min.value

    if !e.parent.nil? && e.value < e.parent.value
      parent = e.parent
      e.disconnect_node
      e.merge_list(@roots)
      unless parent.is_root?
        parent.marked = true
        while !parent.parent.is_root? && parent&.marked
          p = parent.parent
          parent.disconnect_node
          parent.marked = false
          parent.merge_list(@roots)
          parent = p
        end
      end
    end
  end

  def key_decreased(e)
    # TODO: wydmuszka; brakuje testów; powstalo tylko na potrzeby Dijkstry
    while !e.parent.nil? and e.value < e.parent.value
      pull_up(e)
    end
  end

  def increase_key(e, n)
    raise 'e is not BinomialHeap::Element' unless e.instance_of? Element
    raise 'new key is not greater' if n <= e.value

    raise 'not implemented'
  end


end

# :nocov:

require_relative '../graph/graphviz'
class FibonacciHeap
  def dump
    g = Graph.new
    visited = {}
    idx = 0
    q = []
    @roots.each_sibling { |r| q << r }

    until q.empty?
      node = q.shift
      v = g.vertex(node)
      v.label = "#{node.value} (#{node.debug_id})"
      if node.is_root?
        v.label += '(*)'
      end

      v.label = node.value.to_s
      if visited[node]
        puts 'cycle detected'
        next
      end
      visited[node] = true
      v.graphviz_name = idx
      idx += 1

      node.each_child do |c|
        # next if c == node
        v2 =
          g.vertex(c)
        g.add_edge(v, v2)
        q << c
      end
    end
    g.display
  end
end

if __FILE__ == $PROGRAM_NAME

  f = FibonacciHeap.new
  500.times do
    f.push(rand(10_000))
  end

  f.pop
  f.dump

  until f.empty?
    puts f.pop
  end
end
# :nocov:
