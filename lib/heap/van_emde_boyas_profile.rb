require_relative 'van_emde_boyas'

require 'ruby-prof'

t = VanEmdeBoyasTree.new(lazy: true)
RubyProf.start
50_000.times do
  t.insert rand(2**32-1)
end
result = RubyProf.stop

printer = RubyProf::FlatPrinter.new(result)
printer.print(STDOUT)
