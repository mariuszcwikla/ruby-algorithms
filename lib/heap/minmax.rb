class MinMaxHeap
  class Element
    attr_reader :value, :position
    attr_writer :value, :position

    def initialize(value, position)
      @value = value
      @position = position
    end
  end

  def initialize(&block)
    @elems = []
    @comparator =
      if block.nil?
        :<=>.to_proc
      else
        block
      end
  end

  def empty?
    @elems.empty?
  end

  def size
    @elems.size
  end

  def inspect
    @elems.map {|e| e.value}.join ', '
  end

  def push(n)
    e = Element.new(n, @elems.size)
    @elems << e
    heapify
    e
  end

  alias << push
  alias insert push

  private def height_of(index)
    (index+1).to_s(2).size-1    # or Math.log2(elems.size).to_i? Which is faster?
  end

  private def heapify
    i = @elems.size - 1
    height = height_of(i)
    heapify_up(i, height)
  end

  def swap_elements(ia, ib)
    @elems[ia], @elems[ib] = @elems[ib], @elems[ia]
    @elems[ia].position = ia
    @elems[ib].position = ib
  end

  #
  #           1             # min
  #     10        15        # max
  #   2   3     4      9    # min
  #  5 7 6 8  11 12  14  0  # max. We are inserting 0.
  #
  # replace 9 and 1
  #
  #           1             # min
  #     10        15        # max
  #   2   3     4      0    # min   # now we need to compare with grandparent
  #  5 7 6 8  11 12  14  9  # max.
  private def heapify_up(i, height)
    while i > 0
      p = (i - 1) / 2
      # p = i/2
      cmp = compare(height - 1, @elems[i].value, @elems[p].value)
      if cmp < 0
        swap_elements(i, p)
        i = p
        height -= 1
      else
        gp = (p-1) / 2
        return if gp < 0

        cmp = compare(height - 2, @elems[i].value, @elems[gp].value)
        if cmp < 0
          swap_elements(i, gp)
          i = gp
          height -= 2
        else
          break
        end
        # break
      end
    end
  end

  private def compare(height, a, b)
    cmp = @comparator.call(a, b)
    cmp = -cmp if height.odd?   # i.e. odd levels are "max" elements
    cmp
  end

  private def heapify_down(i, height)
    while i * 2 + 1 < @elems.size
      l = i * 2 + 1
      r = i * 2 + 2
      grandchild = l * 2 + 1    # first grandchild index
      # only left child
      if l == @elems.size - 1
        cmp = compare(height, @elems[l].value, @elems[i].value)
        if cmp < 0
          swap_elements(i, l)
        end
        return
      else
          # we are popping from
          #           1             # min
          #     10        15        # max
          #   2   3     4      9    # min
          #  5 7 6 8  11 12  14
          # replace 14 and 1
          #          14             # min
          #     10        15        # max
          #   2   3     4      9    # min
          #  5 7 6 8  11 12
          # (14 vs children) is ok (14 vs 15)
          # (14 vs grandchildren) - not ok. Need to verify 4 grandchildren
        min_grandchild = grandchild.upto([grandchild + 3, @elems.size - 1].min).min {|a, b| compare(height + 2, @elems[a].value, @elems[b].value)}
        if min_grandchild.nil?
          lowest = l
        else
          lowest= min_grandchild
          lowest = l if compare(height, @elems[l].value, @elems[lowest].value) < 0
        end
        lowest = r if compare(height, @elems[r].value, @elems[lowest].value) < 0
        if lowest == l || lowest == r
          if compare(height, @elems[lowest].value, @elems[i].value) < 0
            swap_elements(i, lowest)
            i = lowest
            height += 1
          else
            return
          end
        else
          cmp = compare(height+2, @elems[min_grandchild].value, @elems[i].value)
          if cmp < 0
            swap_elements(i, min_grandchild)
            p = (min_grandchild - 1)/2
            if compare(height + 1, @elems[min_grandchild].value, @elems[p].value) < 0
              swap_elements(p, min_grandchild)
            end
            i = min_grandchild
            height += 2
          else
            return
          end
        end
      end
    end
  end

  def pop
    raise 'heap is empty' if empty?

    if @elems.size == 1
      @elems.pop.value
    else
      e = @elems[0]
      @elems[0] = @elems.pop
      @elems[0].position = 0
      heapify_down(0, 0)
      e.value
    end
  end

  def pop_max
    raise 'heap is empty' if empty?

    case @elems.size
    when 1
      @elems.pop.value
    when 2
      @elems.pop.value
    else
      i = if compare(1, @elems[1].value, @elems[2].value) < 0 # note less than zero, because due to height=1 compare negates the result
            1
          else
            2
          end
      e = @elems[i]
      @elems[i] = @elems.pop
      @elems[i].position = i
      heapify_down(i, 1)
      e.value
    end
  end

  def peek
    raise 'empty heap' if @elems.empty?

    @elems[0].value
  end

  def update_key(e, n)
    if @comparator.call(e.value, n)
      increase_key(e, n)
    elsif @comparator.call(n, e.value)
      decrease_key(e, n)
    end
    # if n==e.value -> nothing to do. It is still fine
  end

  def decrease_key(e, n)
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    raise 'new key is not lower' unless @comparator.call(n, e.value)
    if (e.position > @elems.size) || (@elems[e.position] != e)
      raise 'element not part of this list'
    end

    e.value = n
    heapify_up(e.position, e.position.to_s(2).size - 1)
  end

  def key_decreased(e)
    # TODO: w przypadku dijstry mam tak:
    #
    # second.distance = v.distance + e.weight
    # heap.decrease_key(second.heap_handle, second)
    #
    # a to powoduje ze walidacja "if n <= e.value" failuje bo przeciez wartosc zostala zmodyfikowana z zewnątrz

    # TODO: wydmuszka; brakuje testów; powstalo tylko na potrzeby Dijkstry
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    if (e.position > @elems.size) || (@elems[e.position].object_id != e.object_id)
      raise 'element not part of this list'
    end

    heapify_up(e.position)
  end

  def increase_key(e, n)
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    raise 'new key is not greater' if n <= e.value
    if (e.position > @elems.size) || (@elems[e.position] != e)
      raise 'element not part of this list'
    end

    e.value = n
    heapify_down(e.position)
  end

  def max
    raise 'heap is empty' if empty?

    if @elems.size == 1
      @elems[0].value
    else
      if @elems.size == 2
        @elems[1].value
      else
        if @comparator.call(@elems[1].value, @elems[2].value) > 0
          @elems[1].value
        else
          @elems[2].value
        end
      end
    end
  end
end