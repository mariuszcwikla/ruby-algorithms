# Skrypt buduje plik 'pl.bktree' z preprocesowanym słownikiem z pliku pl.dic
# Zob. demo_bktree2.rb

require_relative 'bktree'

tree = BKTree.new
tree.insert('motyl')

start = Time.new

File.open('pl.dic') do |f|
  n = 0
  start = Time.new
  f.readlines.each do |line|
    word = line.split('/')[0].chomp
    next if word.size == 1

    tree.insert word
    n += 1
    puts "#{n} #{word}" if n % 1000 == 0
  end
end

finish = Time.new
puts "Preprocessing time: #{(finish - start).to_i} seconds. Total words: #{tree.size}"

File.open('pl.bktree', 'wb') do |f|
  Marshal.dump(tree, f)
  puts "File #{f.path} created"
end
