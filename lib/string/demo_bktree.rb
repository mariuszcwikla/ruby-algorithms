require_relative 'bktree'

start = Time.new

tree = BKTree.new
tree.insert('bladefupa')
n = 0

num_of_words_to_read = 100_000
if ARGV.size > 0
  num_of_words_to_read = ARGV[0].to_i
end

File.open('pl.dic') do |f|
  f.readlines.each do |line|
    word = line.split('/')[0].chomp
    next if word.size == 1

    tree.insert word
    n += 1
    puts "#{n} #{word}" if n % 1000 == 0

    if n == num_of_words_to_read
      puts 'Last word:' + line
      break
    end
  end
end

finish = Time.new
puts "Preprocessing time: #{(finish - start).to_i} seconds"

def benchmark(&block)
  start = Time.new
  value = block.call
  finish = Time.new
  [((finish - start) * 1000).to_i, value]
end

loop do
  print '> '
  line = STDIN.gets.strip
  ms, similar = benchmark { tree.find_similar(line) }
  puts "1: #{similar.join ','} (queries: #{tree.num_queries}, #{ms} ms)"
  ms, similar = benchmark { tree.find_similar(line, 2) }
  puts "2: #{similar.join ','} (queries: #{tree.num_queries}, #{ms} ms)"
end
