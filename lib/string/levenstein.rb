#     k  i  t  t  e  n
#   0  1  2  3  4  5  6
# s  1  1  2  3  4  5  6
# i  2  2  1  2  3  4  5
# t  3  3  2  1  2  3  4
# t  4  4  3  2  1  2  3
# i  5  5  4  3  2  2  3
# n  6  6  5  4  3  3  2
# g  7  7  6  5  4  4  3

def levenstein(a, b)
  row = []
  (0..a.size).each do |i|
    row << i
  end

  (1..b.size).each do |i|
    p = i - 1
    row[0] = i
    (1..a.size).each do |j|
      left = row[j - 1]
      upleft = p
      up = row[j]
      cost = b[i - 1] == a[j - 1] ? 0 : 1
      p = row[j]
      row[j] = [left + 1, up + 1, upleft + cost].min
    end
  end
  row.last
end
