# Demo drzewa BKTree
# Budowanie drzewa trwa długo. Zatem:
# 1. Najpierw uruchom 'ruby bktree_build.rb'. Ten program stworzy plik 'pl.bktree'
# 2. Następnie uruchom 'ruby demo_bktree2.rb'. Załaduje stworzone drzewo.

require_relative 'bktree'

tree_file_name = 'pl.bktree'
unless File.exist?(tree_file_name)
  puts "#{tree_file_name} does not exist"
  return
end

tree = File.open(tree_file_name, 'rb') { |f| tree = Marshal.load(f) }
puts "Dictionary loaded. Total words: #{tree.size}"

def benchmark(&block)
  start = Time.new
  value = block.call
  finish = Time.new
  [((finish - start) * 1000).to_i, value]
end

loop do
  print '> '
  line = STDIN.gets.strip
  ms, similar = benchmark { tree.find_similar(line) }
  puts "1: #{similar.join ','} (queries: #{tree.num_queries}, #{ms} ms)"
  ms, similar = benchmark { tree.find_similar(line, 2) }
  puts "2: #{similar.join ','} (queries: #{tree.num_queries}, #{ms} ms)"
end
