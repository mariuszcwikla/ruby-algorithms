require_relative '../automata/automata'

def levenhstein_automata_nfa(string, max_errors = 2)
  automaton = Automaton.new
  automaton.start_state = '0_0'
  (1..(string.length)).each do |i|
    (0..max_errors).each do |j|
      from = "#{i - 1}_#{j}"
      to = "#{i}_#{j}"
      automaton.add_transition(from, to, string[i - 1])
      next unless j < max_errors

      automaton.add_transition(from, "#{i - 1}_#{j + 1}", :other) # delete
      automaton.add_transition(from, "#{i}_#{j + 1}", :other) # substitution
      automaton.add_transition(from, "#{i}_#{j + 1}", :epsilon) # insert
    end
  end
  (0..max_errors).each do |j|
    if j > 0
      automaton.add_transition("#{string.length}_#{j - 1}", "#{string.length}_#{j}", :other)
    end
    final_state = "#{string.length}_#{j}"
    automaton.final_states << final_state
  end
  automaton
end

def levenhstein_automata_dfa(string, max_errors = 2)
  levenhstein_automata_nfa(string, max_errors).eliminate_epsilon.to_dfa
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  puts 'Building Levenhstein NFA'
  # automata = levenhstein_automata_nfa('Interdisciplinary', 2)
  automata = levenhstein_automata_nfa('food', 2)
  automata.to_graph.display(debug: true)

  puts 'Converting NFA to DFA'
  g = automata.eliminate_epsilon.to_dfa
  puts 'Conversion completed. Generating Graphviz image now. Might be VERY SLOW for large diagrams.'
  g.to_graph.display(output_format: 'svg')
end
# :nocov:
