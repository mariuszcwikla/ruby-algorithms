require_relative 'levenstein'

# Burkhard-Keller Tree
class BKTree
  attr_reader :num_queries, :size

  class Node
    attr_reader :value, :children

    def initialize(value)
      @value = value
      @children = {}
    end
  end

  def initialize
    @root = nil
    @size = 0
  end

  def insert(word)
    @size += 1
    if @root.nil?
      @root = Node.new(word)
    else
      n = @root
      loop do
        lev = levenstein(word, n.value)
        if n.children[lev].nil?
          n.children[lev] = Node.new(word)
          return
        else
          n = n.children[lev]
        end
      end
    end
  end

  def find_similar(word, similarity = 1, node = @root)
    @num_queries = 0 if node == @root
    return [] if node.nil?

    d = levenstein(node.value, word)
    @num_queries += 1
    words = []
    words << node.value if d <= similarity

    node.children.each do |k, n|
      if ((d - similarity)..(d + similarity)).include? k
        words += find_similar(word, similarity, n)
      end
    end
    words
  end
end
