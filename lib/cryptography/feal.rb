def hex_to_a(s)
  [s].pack("H*").unpack("C*")
end

alias from_hex hex_to_a

def a_to_hex(a)
  a.pack("C*").unpack("H*")[0].upcase
end

class Array
  def xor(other)
    raise 'size not equal' unless length == other.length
    zip(other).map {|a,b| a^b}
  end

  def to_hex
    a_to_hex(self)
  end

  def self.from_hex(s)
    hex_to_a(s)
  end
end

class String

end

module Feal

  def self.xor(x, y)
    # raise 'size not equal' unless x.length == y.length
    # x.zip(y).map {|a,b| a^b}
    x.xor y
  end

  def self.rot2(x)
    ((x << 2) & 0xff) | (x >> 6)
  end

  def self.s0(a, b)
    rot2((a+b) % 256)
  end

  def self.s1(a, b)
    rot2((a+b+1) % 256)
  end

  def self._f(a, y)
    t1 = a[0]^a[1]^y[0]
    t2 = a[2]^a[3]^y[1]
    u1 = s1(t1, t2)
    u2 = s0(t2, u1)
    u0 = s0(a[0], u1)
    u3 = s1(a[3], u2)
    [u0, u1, u2, u3]
  end

  def self._fk(a, b)
    t1 = a[0]^a[1]
    t2 = a[2]^a[3]
    u1 = s1(t1, t2^b[0])
    u2 = s0(t2, u1^b[1])
    u0 = s0(a[0], u1^b[2])
    u3 = s1(a[3], u2^b[3])
    [u0, u1, u2, u3]
  end

  def self.generate_keys(k)
    u0 = [0, 0, 0, 0]
    u1 = k[0...4]
    u2 = k[4...8]

    keys = []
    for i in 1..8
      u = _fk(u1, xor(u0, u2))
      keys[2*i - 2] = u[0..1]
      keys[2*i - 1] = u[2..3]
      u0 = u1
      u1 = u2
      u2 = u
    end
    keys
  end

  def self.encrypt_block(m, k)
    raise "Invalid message block length #{m.length}" unless m.length == 8
    raise "Invalid key block length #{k.length}" unless k.length == 8

    ml, mr = m[0...4], m[4...8]

    k = generate_keys(k)

    left = ml.xor k[8] + k[9]
    right = mr.xor k[10] + k[11]
    right = right.xor left

    for i in 1..8
      left, right = right, xor(left, _f(right, k[i-1]))
    end
    left = xor(left, right)
    right, left = xor(right, k[12]+k[13]), xor(left, k[14] + k[15])
    right + left
  end

  def self.decrypt_block(c, k)
    raise "Invalid ciphertext block length #{c.length}" unless c.length == 8
    raise "Invalid key block length #{k.length}" unless k.length == 8

    cl, cr = c[0...4], c[4...8]

    k = generate_keys(k)

    left = cl.xor k[12] + k[13]
    right = cr.xor k[14] + k[15]
    right = right.xor left

    for i in 8.downto 1
      left, right = right, xor(left, _f(right, k[i-1]))
    end
    left = xor(left, right)
    right, left = xor(right, k[8]+k[9]), xor(left, k[10] + k[11])
    right + left
  end

end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  p "A0FF".unpack('H*')
  p s=["A0FF"].pack('H*')
  p ["A0FF"].pack('H*').unpack('C*')

  m = from_hex '0000000000000000'
  k = from_hex '0123456789abcdef'

  puts "message: #{m.to_hex}"
  p k
  p "encrypted=#{(c=Feal.encrypt_block(m, k)).to_hex}"
  p "decrypted=#{Feal.decrypt_block(c, k).to_hex}"
end
# :nocov: