require_relative '../numbertheory/numbers'

class PublicKey
  attr_reader :n, :exponent, :size_in_bytes
  def initialize(n, exponent, size_in_bytes)
    @n = n
    @exponent = exponent
    @size_in_bytes = size_in_bytes
  end

  alias size size_in_bytes

  def size_in_bits
    size_in_bytes * 8
  end
end

class PrivateKey
  attr_reader :p, :q, :exponent, :size_in_bytes
  def initialize(p, q, exponent, size_in_bytes)
    @p = p
    @q = q
    @exponent = exponent
    @size_in_bytes = size_in_bytes
  end
  alias size size_in_bytes

  def size_in_bits
    size_in_bytes * 8
  end
end

def size_in_bytes(value)
  num_bits = Math.log2(value).ceil
  num_bits += 8 - num_bits % 8
  num_bits / 8
end

module TextbookRSA
  # @@p = 131
  # @@q = 89
  @@p = 312791951
  @@q = 42367138469

  def self.generate_key_pair
    n = @@p * @@q
    size = size_in_bytes(n)
    exponent = 3
    priv = PrivateKey.new(@@p, @@q, exponent, size)
    pub  = PublicKey.new(n, exponent, size)
    [priv, pub]
  end

  def self.encrypt(m, public_key)
    raise 'message too big' if m > public_key.n 

    m.pow(public_key.exponent, public_key.n)
  end

  def self.decrypt(c, private_key)
    p = private_key.p
    q = private_key.q
    e = private_key.exponent
    d = Numbers.modular_inverse(e, (p-1) * (q-1))
    c.pow(d, p * q)
  end
end

def byte_array_to_i(array)
  value = 0
  array.each {|b| value = (value << 8) + b}
  value
end

def i_to_byte_array(value, block_length=nil)
  return [0] if value == 0 

  array = []
  while value > 0
    array << value % 256
    value /= 256
  end

  unless block_length.nil?
    raise 'unable to pad block; value too big' if array.length > block_length

    array.unshift 0 while block_length < array.length
  end
  array.reverse
end

module RSA
  def self.pkcs15_pad_value(m, public_key)
    r_length_in_bits = (public_key.size_in_bytes - size_in_bytes(m) - 3) * 8
    raise "message too big for PKCS 1.5 padding#{r_length_in_bits}" if r_length_in_bits < 8

    # PKCS padding scheme
    # x = 0x00 || 0x02 || r || 0x00 || m
    # 1. first 0x00 can't be simulated here because I'm using plain numbers, not byte arrays
    # 2. second 0x00 - we must make sure that "r" does not end with 00 (i.e. is not divided by 256)
    # r cannot contain byte 00 anywhere
    pad = [0, 2]
    r = 0
    iterations = 0
    while i_to_byte_array(r).include? 0x00
      r = rand (1 << r_length_in_bits)
      raise 'too many iterations' if iterations == 1000

      iterations += 1
    end

    pad += i_to_byte_array(r)
    pad << 0
    pad
  end

  def self.encrypt(m, public_key)
    pad = pkcs15_pad_value(m, public_key)
    padded = pad + i_to_byte_array(m, public_key.size - pad.length)
    m = byte_array_to_i padded
    encrypted = TextbookRSA.encrypt(m, public_key)
    i_to_byte_array(encrypted)
  end

  def self.decrypt(c, private_key)
    value = byte_array_to_i c

    decrypted = TextbookRSA.decrypt(value, private_key)
    bytes = i_to_byte_array(decrypted)
    bytes.shift   # drop 0x2
    bytes.shift until bytes[0]==0 or bytes.empty?
    raise 'not a pkcs padding' if bytes.empty?

    bytes.shift   # drop 0x0
    decrypted = byte_array_to_i bytes
    decrypted
  end
end

def byte_array_to_str(array)
  array.map {|c| c.to_s(2).ljust(8, '0')}.join ''
end

def byte_array_to_hex_str(array)
  array.map {|c| c.to_s(16).ljust(2, '0')}.join ''
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  priv, pub = TextbookRSA.generate_key_pair
  puts "public key: #{pub.n}"
  puts "private key: p=#{priv.p} q=#{priv.q}"
  m = 13
  # m = 209415619081904207
  puts "=== Textbook RSA ==="
  c = TextbookRSA.encrypt(m, pub)
  decrypted = TextbookRSA.decrypt(c, priv)
  puts "message=#{m}, #{m.to_s 2}"
  puts "encrypted=#{c}, #{c.to_s 2}"
  puts "decrypted=#{decrypted} #{decrypted.to_s 2}"
  
  puts "=== RSA with PKCS 1.5 padding ==="

  puts "message=#{m}, #{m.to_s 2}"
  c = RSA.encrypt(m, pub)
  
  puts "encrypted=#{byte_array_to_i(c)} #{byte_array_to_hex_str(c)} #{byte_array_to_str(c)}"
  decrypted = RSA.decrypt(c, priv)
  puts "key len: #{pub.size} encrypted length=#{c.length}"
  puts "decrypted=#{decrypted} #{decrypted.to_s 2}"

  m1 = 121
  m2 = 233
  puts "m1*m2=#{m1*m2}"
  e1 = RSA.encrypt(m1, pub)
  e2 = RSA.encrypt(m2, pub)
  e1 = byte_array_to_i e1
  e2 = byte_array_to_i e2
  d = RSA.decrypt(i_to_byte_array(e1*e2), priv)
  puts "D(E(m1)*E(m2))=#{d}"
end
# :nocov: