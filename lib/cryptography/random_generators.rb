# ANSI X9.17

module AnsiX9_17
  @@seed = 2446976725563658891
  
  def rng_ansi_x917

  end
end

require_relative '../numbertheory/miller_rabin_test'

module BlumBlumShub
  def self._is_prime?(n)
    return Numbers.is_prime_brute(n) if n < 1000
    miller_rabin_test(n)
  end

  def self._initialize(max: nil, p: nil, q: nil, seed: nil, debug: false)
    max = 2**31 if max.nil? && p.nil? && q.nil?
    unless max.nil?
      p = nil
      loop do
        p = rand(max)

        next if !_is_prime?(p)
        break if p % 4 == 3
      end
      q = nil
      loop do
        q = rand(max)
        next if !_is_prime?(q)
        break if q % 4 == 3
      end
    end

    modulus = p * q
    if seed.nil?
      loop do
      seed = rand(1...modulus) 
      break if seed % p > 0 && seed % q > 0
      end
      @@xn = (seed * seed) % modulus
    else
      raise 'seed is coprime with M' unless seed.gcd(modulus) == 1
      @@xn = seed
    end

    if debug
      carmichael = Numbers.carmichael(Numbers.carmichael(modulus))
      puts "Blum Blum Shub parameters p=#{p}, q=#{q}, seed=#{@@xn}, expected cycle length=#{carmichael}" if debug
    end

    @@modulus = modulus
  end

  @@modulus = _initialize(max: 2**31)

  def self.random
    @@xn = (@@xn * @@xn) % @@modulus
    [@@xn % 2, @@xn]
  end

  def self.random_bit
    random[0]
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  puts "Blum Blum Shub algorithm"
  BlumBlumShub._initialize(max: 10000, debug: true)  
  #BlumBlumShub._initialize(p: 3, q: 11, debug: true) 
  #BlumBlumShub._initialize(p: 11, q: 23, seed: 3, debug: true) 

  
  25.times do
    p BlumBlumShub.random
  end

  p 50.times.map{BlumBlumShub.random_bit}

  def period_length
    n = BlumBlumShub.random[1]
    k = 1
    while n != BlumBlumShub.random[1]
      k+=1
    end
    k
  end

  puts "Period length = #{period_length}"
end
# :nocov: