# Disjoint Sets
#
# "quick-find" version
#
# Disjoint sets provides operations
#
# insert - creates new set
# merge  - merges two sets into one set - O(min(n, m)), where n-size of set A, m-size of set B
# same_set? - test if both element belong to the same set

class DisjointSetsQuickFind
  class List
    class Node
      attr_reader :value
      attr_accessor :next

      def initialize(value)
        @next = nil
        @value = value
      end
    end
    attr_reader :size, :head, :tail, :representative

    def initialize(value)
      @size = 1
      @representative = value
      @head = @tail = Node.new(value)
    end

    def each
      n = @head
      until n.nil?
        yield n.value
        n = n.next
      end
    end

    def merge(list)
      @tail.next = list.head
      @tail = list.tail
      @size += list.size
    end
  end

  def initialize
    @elements = {}
  end

  def insert(x)
    return if @elements[x]

    @elements[x] = List.new(x)
  end

  def include?(x)
    @elements[x] != nil
  end

  def same_set?(a, b)
    arep = @elements[a]
    brep = @elements[b]
    arep == brep
  end

  def merge(a, b)
    # representatives of a and b
    arep = @elements[a]
    brep = @elements[b]
    raise "#{a} does not exist in set" if arep.nil?
    raise "#{b} does not exist in set" if brep.nil?

    return if arep.eql? brep

    if arep.size > brep.size
      brep.each { |x| @elements[x] = arep }
      arep.merge(brep)
    else
      arep.each { |x| @elements[x] = brep }
      brep.merge(arep)
    end
  end

  def each(&block)
    visited = {}
    @elements.each do |_key, list|
      next if visited[list.representative]

      list.each do |value|
        block.call(value)
      end
      visited[list.representative] = true
    end
  end

  def values
    v = []
    each { |x| v << x }
    v
  end
end

# :nocov:
def stopwatch(log)
  now = Time.now
  yield
  puts log + " #{Time.now - now}"
end

if __FILE__ == $PROGRAM_NAME
  puts '### Disjoint Sets, Quick-Find version'
  s = DisjointSetsQuickFind.new
  s.insert(0)
  s.insert(1)
  s.insert(2)
  s.insert(3)

  puts "same set 1,2 => #{s.same_set? 1, 2}"
  puts s.values.join(' ')

  s.merge(1, 2)
  puts "same set 1,2 => #{s.same_set? 1, 2}"
  puts s.values.join(' ')

  s.merge(2, 3)
  puts s.values.join(' ')

  4.upto(100_000) { |x| s.insert(x) }

  stopwatch('same_set?') do
    10_000.times { s.same_set? rand(100_000), rand(100_000) }
  end

  stopwatch('merge') do
    500_000.times { s.merge(rand(1_000), rand(1_000)) }
  end

  stopwatch('same_set?') do
    1_000_000.times { s.same_set? rand(1000), rand(1000) }
  end
end
# :nocov:
