# DisjointSets w wersji QuickMerge
#
# Zob. https://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf

class DisjointSetsQuickMerge
  class Node
    attr_accessor :parent, :height
    attr_reader :value

    def initialize(value)
      @value = value
      @parent = nil
      @height = 1
    end
  end

  def initialize
    @elements = {}
  end

  def insert(x)
    return if @elements[x]

    @elements[x] = Node.new(x)
  end

  def merge(a, b)
    na = root(a)
    nb = root(b)
    raise "#{a} does not exist in set" if nb.nil?
    raise "#{b} does not exist in set" if na.nil?

    return if na.eql? nb

    if na.height > nb.height
      nb.parent = na
    else
      na.parent = nb
      nb.height = [na.height + 1, nb.height].max
    end
  end

  def include?(x)
    @elements[x] != nil
  end

  private def root(x)
    n = @elements[x]
    return nil if n.nil?

    nn = n
    n = n.parent until n.parent.nil?

    # Optymalizacja - Path Compression
    # "if" jest po to aby uniknąć pętli nieskończonej (przypisanie parent do samego siebie)
    nn.parent = n unless nn.parent.nil?

    n
  end

  def same_set?(a, b)
    na = root(a)
    nb = root(b)
    return false if na.nil? || nb.nil?

    na.eql?(nb)
  end

  def each(&block); end

  def values; end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  def stopwatch(log)
    now = Time.now
    yield
    puts log + " #{Time.now - now}"
  end

  puts '### Disjoint Sets, Quick-Merge version'
  s = DisjointSetsQuickMerge.new
  s.insert(0)
  s.insert(1)
  s.insert(2)
  s.insert(3)

  puts "same set 1,2 => #{s.same_set? 1, 2}"

  s.merge(1, 2)
  puts "same set 1,2 => #{s.same_set? 1, 2}"

  s.merge(2, 3)

  4.upto(100_000) { |x| s.insert(x) }

  # Uwaga: Quick-Merge powinien być teoretycznie szybszy
  # niż Quick-Find, ale w testach jest odwrotnie
  # być może wynika to z tego, że zestaw danych jest losowy
  # Zaś gdyby był specjalny zestaw danych, to prawdopodobnie by było widać różnicę
  stopwatch('same_set?') do
    10_000.times { s.same_set? rand(100_000), rand(100_000) }
  end

  stopwatch('merge') do
    500_000.times { s.merge(rand(1_000), rand(1_000)) }
  end

  stopwatch('same_set?') do
    1_000_000.times { s.same_set? rand(1000), rand(1000) }
  end
end
# :nocov:
