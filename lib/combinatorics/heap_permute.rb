# Algorytm Heap'a - generowanie permutacji
def heap_permute(array, n = array.size, &block)
  if n == 1
    block.call(array)
    1
  else
    k = 0
    (0..n - 1).each do |i|
      k += heap_permute(array, n - 1, &block)
      if n.odd?
        array[0], array[n - 1] = array[n - 1], array[0]
      else
        array[i], array[n - 1] = array[n - 1], array[i]
      end
    end
    k
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  puts 'Permutations 1..3'
  array = (1..3).to_a
  k = heap_permute(array, array.size) { |arr| puts arr.join(' ') }
  puts "Total permutations: #{k}"
  puts ''

  puts 'Permutations 1..5'
  array = (1..5).to_a
  k = heap_permute(array, array.size) { |arr| puts arr.join(' ') }
  puts "Total permutations: #{k}"
  puts ''
end
# :nocov:
