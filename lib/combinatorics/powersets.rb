# frozen_string_literal: true

# generuje zbiory potęgowe (powersets), tzn. wszystkie podzbiory
# zbioru n-elementowego
#
# tzn.: zbiór pusty, [1], [2], [1, 2], [3], [1,3], [2,3], [1,2,3]

def powersets(n)
  nn = 2**n - 1
  0.upto(nn) do |i|
    set = []
    i.to_s(2).reverse.each_char.with_index do |c, k|
      set << (k + 1) if c == '1'
    end
    puts set.join(',')
  end
end

powersets(5)
