LEFT = false
RIGHT = true

def permut(n)
  tab = []
  (1..n).each { |x| tab << [LEFT, x] }

  stop = false
  until stop
    stop = false
    max = nil

    puts tab.map { |_, b| b }.join(' ')

    (1..n - 1).each do |i|
      if (tab[i - 1][0] == RIGHT) && (tab[i - 1][1] > tab[i][1])
        max = i - 1 if max.nil? || (tab[max][1] < tab[i - 1][1])
      end
      next unless (tab[i][0] == LEFT) && (tab[i][1] > tab[i - 1][1])

      max = i if max.nil? || (tab[max][1] < tab[i][1])
    end
    stop = true if max.nil?

    next if max.nil?

    if tab[max][0] == LEFT
      tab[max - 1], tab[max] = tab[max], tab[max - 1]
      max -= 1
    else
      tab[max + 1], tab[max] = tab[max], tab[max + 1]
      max += 1
    end

    (0..n - 1).each do |i|
      if (i != max) && (tab[i][1] > tab[max][1])
        tab[i][0] = tab[i][0] == LEFT ? RIGHT : LEFT
      end
    end
  end
end

permut(5)
