require_relative '../selection/hoare'

# simple quicksort
# simple=concatenating arrays
def quicksort_hoare_simple(array)
  return array if array.size <= 1

  s = hoare_partition(array)
  left = quicksort_hoare_simple(array[0..s])
  right = quicksort_hoare_simple(array[(s + 1)..(array.size - 1)])
  left + right
end

def quicksort_hoare(array, l = 0, r = array.size)
  return if r - l <= 1

  s = hoare_partition(array, l, r)
  quicksort_hoare(array, l, s)
  quicksort_hoare(array, s + 1, r)
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  x = [5, 1, 2, 8, 9, 10, 3, 11, 4]

  puts "before   : #{x.join ' '}"

  puts "quicksort (Hoare's partition), simple : #{quicksort_hoare_simple(x).join ' '}"
  quicksort_hoare(x)
  puts "quicksort (Hoare's partition), optimiz: #{x.join ' '}"
end
# :nocov:
