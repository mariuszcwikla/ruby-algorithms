def selection_sort(array)
  n = array.size
  (0..n - 1).each do |i|
    min = i
    ((i + 1)..n - 1).each do |j|
      min = j if array[j] < array[min]
    end
    array[i], array[min] = array[min], array[i]
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  array = []

  20.times { array << rand(20) }
  puts 'Array:           ' + array.join(' ')

  selection_sort(array)
  puts 'Selection sort:  ' + array.join(' ')
end
# :nocov:
