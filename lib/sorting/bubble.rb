def bubble_sort(a)
  n = a.length - 1
  while n > 0
    (1..n).each do |i|
      next unless a[i - 1] > a[i]

      temp = a[i]
      a[i] = a[i - 1]
      a[i - 1] = temp
    end
    n -= 1
  end
end

def bubble_sort_optimiz(a)
  n = a.length - 1
  while n > 0
    changed = false
    (1..n).each do |i|
      if a[i - 1] > a[i]
        a[i], a[i - 1] = a[i - 1], a[i]
        changed = true
      end
    end
    break unless changed

    n -= 1
  end
end
