def merge_arrays(left, right)
  out = []
  i = 0
  j = 0
  while (i != left.size) || (j != right.size)
    if i == left.size
      out << right[j]
      j += 1
    elsif j == right.size
      out << left[i]
      i += 1
    else
      if left[i] < right[j]
        out << left[i]
        i += 1
      else
        out << right[j]
        j += 1
      end
    end
  end
  out
end

def merge_sort(array, a = 0, b = array.size)
  return [] if b == a
  return [array[a]] if a + 1 == b

  mid = (a + b) / 2
  left = merge_sort(array, a, mid)
  right = merge_sort(array, mid, b)
  merge_arrays(left, right)
end

#
# =begin
# - jesli mnimimum jest w lewej tablicy- przesuwamy go prawo
# - jesli minimum jest w prawej - robimy swap, ale element większy trafia do "środkowej tablicy
# - jesli minmum jest w srodkowej tablicy -> przesuwamy wskazniki, bo napewno elementy w srodkowej są większe niż w lewej
#
# 7 9 11 13           1 2 4 5
# 1 9 11 13     7       2 4 5
# 1 2 11 13     7 9       4 5
# 1 2 4  13     7 9 11      5
# 1 2 4  5      7 9 11 13
#
#
#
# 7 9 11 13           1 2 10 15
# 1 9 11 13      7      2 10 15
# 1 2 11 13      7 9      10 15       <jesli minimum mamy wziąć ze srodkowej tablicy, to srodkowa jest mniejsza niż prawa
# czyli możemy przesunąć wskazniki i zaczac od poczatku
# 1 2 11 13           7 9 10 15      -> tu tylko przesuniete wskazniki
# 1 2 7 13     11       9 10 15
# 1 2 7 9      11 13      10 15
# 1 2 7 9      10 13      11 15
# 1 2 7 9 10 13           11 15      -> kontynuujemy algorytm od początku
def merge(array, l, mid, r)
  mid_l = mid

  while l < r
    min = l
    min = mid if (mid < r) && (array[mid] < array[min])

    if mid_l < mid
      min = mid_l if array[mid_l] < array[min]
    end
    #     1 5 9 13 | 2 4 8 10
    #     w tym scenariuszu w pewnym momencie jest
    #
    # posortowane  L    M     R
    #     1 2 4 | 13| 4 9| 8 10

    if min == l
      l += 1
      mid = l if mid < l
    elsif min == mid
      array[mid], array[l] = array[l], array[mid]
      mid += 1
      l += 1
      # mid=l if mid<l
    else
      mid_l = mid
    end
  end
end

def merge_sort_optimiz(array, a = 0, b = array.size)
  return if b == a
  return if a + 1 == b

  mid = (a + b) / 2
  merge_sort_optimiz(array, a, mid)
  merge_sort_optimiz(array, mid, b)

  merge(array, a, mid, b)
end


class List
  class Node
    attr_accessor :next, :value
  end
  
  attr_reader :head, :tail

  def initialize
    @head = nil
    @tail = nil
  end

  def empty?
    @head.nil?
  end

  def insert_first(value)
    @head = @tail = Node.new
    @head.value = value
    @head
  end

  def insert_after(value, node)
    vn = Node.new
    vn.value = value
    vn.next = node.next
    node.next = vn
    @tail = vn if @tail == node
    vn
  end

  def insert_last(value)
    if @head.nil?
      insert_first(value)
    else
      insert_after(value, @tail)
    end
  end
  alias add_last insert_last
end
class List
  def size
    n = @head
    i = 0
    until n.nil?
      i += 1
      n = n.next
    end
    i
  end

  def to_a
    n = @head
    out = []
    until n.nil?
      out << n.value
      n = n.next
    end
    out
  end
end

def merge_sort_list(list)
  return list if list.head == list.tail

  n = list.head
  left = List.new
  right = List.new
  i = 0
  s = list.size

  until n.nil?
    if i < s / 2
      left.add_last(n.value)
    else
      right.add_last(n.value)
    end
    n = n.next
    i += 1
  end

  left = merge_sort_list(left)
  right = merge_sort_list(right)

  merge_lists(left, right)
end

# used by merge_sort -> need to split it in order to fit it in asciidoctor-revealjs slide
def merge_lists(left, right)
  a = left.head
  b = right.head

  out = List.new
  while !a.nil? || !b.nil?
    if a.nil?
      out.add_last(b.value)
      b = b.next
    elsif b.nil?
      out.add_last(a.value)
      a = a.next
    else
      if a.value <= b.value
        out.add_last(a.value)
        a = a.next
      else
        out.add_last(b.value)
        b = b.next
      end
    end
  end
  out
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  array = []

  20.times { array << rand(20) }
  puts 'Array:       ' + array.join(' ')

  puts 'Merge sort:  ' + merge_sort(array.dup).join(' ')

  x = array.dup
  merge_sort_optimiz(x)
  # puts "Merge sort (in-place, NOT WORKING YET):  " + x.join(" ")
end
# :nocov:
