require 'strscan'
require 'set'
require_relative '../../lib/graph/graph'
require_relative '../../lib/graph/graphviz'

class Automaton
  attr_accessor :start_state, :final_states
  attr_reader :transitions

  class Transition
    attr_reader :state1, :state2, :symbol

    def initialize(state1, symbol, state2)
      @state1 = state1
      @state2 = state2
      @symbol = symbol
    end

    def to_a
      [@state1, @symbol, @state2]
    end
  end

  def initialize
    @start_state = nil
    @final_states = Set.new
    @transitions = []
    @next_state_id = 0
    @states = Hash.new { |h, k| h[k] = [] }
    @alphabet = Set.new
  end

  def final_state?(state)
    @final_states.include? state
  end

  def add_transition(state1, state2, symbol)
    t = Transition.new(state1, symbol, state2)
    @transitions << t
    @states[state1] << t
    @states[state2] = [] if @states[state2].nil?
    @alphabet << symbol unless symbol == :epsilon
    t
  end

  def create_state
    s = 'S' + @next_state_id.to_s
    @next_state_id += 1
    s.to_sym
  end

  def include_transition?(state1, state2, symbol)
    !@states[state1].find { |t| t.symbol == symbol && t.state2 == state2 }.nil?
  end

  def concat(end1, start2)
    add_transition(end1, start2, :epsilon)
  end

  def or(a1, a2, b1, b2)
    c1 = create_state
    c2 = create_state
    add_transition(c1, a1, :epsilon)
    add_transition(c1, b1, :epsilon)
    add_transition(a2, c2, :epsilon)
    add_transition(b2, c2, :epsilon)
    [c1, c2]
  end

  def klenee(a, b)
    na = create_state
    nb = create_state
    add_transition(na, a, :epsilon)
    add_transition(b, nb, :epsilon)
    add_transition(na, nb, :epsilon)
    add_transition(nb, na, :epsilon)
    [na, nb]
  end

  def to_graph
    vertex_aliases = {}
    idx = 0
    states.each do |s|
      vertex_aliases[s] = idx
      idx += 1
    end

    g = Graph.new
    order = Set.new
    unless @start_state.nil?
      order << vertex_aliases[@start_state]
      g.vertex(vertex_aliases[@start_state]).add_graphviz_attrib('style', 'filled')
    end
    @transitions.each do |t|
      order << vertex_aliases[t.state1] unless final_state?(t.state1)
      order << vertex_aliases[t.state2] unless final_state?(t.state2)
    end

    @final_states.each do |f|
      order << vertex_aliases[f]
      g.vertex(vertex_aliases[f]).add_graphviz_attrib('shape', 'doublecircle')
      g.vertex(vertex_aliases[f]).add_graphviz_attrib('style', 'filled')
    end

    g.graphviz_define_vertices_order(order)
    @transitions.each do |t|
      g.add_edge(vertex_aliases[t.state1], vertex_aliases[t.state2]).graphviz_label =
        escape_graphviz_label(t.symbol.to_s)
      state1_label = escape_graphviz_label(t.state1.to_s)
      state1_label += '*' if t.state1 == @start_state
      g.vertex(vertex_aliases[t.state1]).label = state1_label
      g.vertex(vertex_aliases[t.state2]).label = escape_graphviz_label(t.state2.to_s)
    end
    g
  end

  def escape_graphviz_label(string)
    string.gsub(/"/, '"')
  end

  def display
    to_graph.display(rankdir: 'LR')
  end

  def states
    @states.keys
  end

  def symbols
    @alphabet
  end

  def transitions_of(state)
    @states[state]
  end

  def delta(state, symbol)
    transitions = @states[state].select { |t| t.symbol == symbol }
    if transitions.size.zero?
      transitions = @states[state].select { |t| t.symbol == :other }
    end
    transitions.map(&:state2)
  end

  # epsilon_closure(:S1) - zwraca domknięcie epsilon dla danego stanu
  # epsilon_closure(:S1, :S2, :S3) - zwraca domknięcie epsilon dla podanych stanów jako suma zbiorów
  def epsilon_closure(*states)
    closure = Set.new
    closure += states
    q = *states
    until q.empty?
      s = q.shift
      transitions_of(s).select { |t| t.symbol == :epsilon }.each do |t|
        unless closure.include? t.state2
          closure << t.state2
          q << t.state2
        end
      end
    end
    closure.to_a
  end

  def eliminate_epsilon
    n = Automaton.new
    states.each do |state|
      cl = epsilon_closure(state)
      symbols.each do |symbol|
        deltas = Set.new
        cl.each do |c|
          deltas += delta(c, symbol)
          n.final_states << state if final_state?(c)
        end
        deltas.each { |d| n.add_transition(state, d, symbol) }
      end
    end
    n.start_state = @start_state
    n
  end

  def to_dfa(include_unreachable = false)
    dfa = Automaton.new
    start_state = [@start_state]
    dfa.start_state = start_state
    visited = Set.new
    q = []
    if include_unreachable
      states.each do |s|
        visited << [s]
        q << [s]
      end
    else
      visited << start_state
      q << start_state
    end
    alphabet = symbols
    until q.empty?
      state = q.shift
      alphabet.each do |symbol|
        outgoing = []
        state.each do |substate|
          outgoing += delta(substate, symbol)
        end
        outgoing.sort!.uniq!
        if !outgoing.empty? && !visited.include?(outgoing)
          q << outgoing
          visited << outgoing
        end
        dfa.add_transition(state, outgoing, symbol) unless outgoing.empty?
      end
    end
    dfa.states.each do |dstate|
      unless dstate.find { |s| final_state?(s) }.nil?
        dfa.final_states << dstate
      end
    end
    dfa
  end

  def is_dfa?
    @states.each_entry do |_k, v|
      outgoing = v.map(&:symbol)
      return false if outgoing.uniq.size != outgoing.size
      return false unless v.find { |t| t.symbol == :epsilon }.nil?
    end
    true
  end

  def accept?(input)
    raise 'supported only for DFA' unless is_dfa?

    s = @start_state
    input.each_char do |c|
      d = delta(s, c)
      raise "accept? should be called only for DFA. Apparently this is NDFA. Detected at state #{s}" if d.size > 1

      s = d[0]
      return false if s.nil?
    end
    return true if final_state?(s)

    false
  end
end

class REParser
  def parse(input)
    input = StringScanner.new(input) unless input.is_a? StringScanner
    @stack = []
    @n = Automaton.new
    a, b = do_parse(input)
    @n.start_state = a
    @n.final_states = Set.new [b]
    @n
  end

  private

  def do_parse(input)
    return if input.eos?

    until input.eos?
      c = input.getch
      case c
      when '|'
        d, e = do_parse(input)
        a, b = @n.or(a, b, d, e)
      when '*'
        a, b = @n.klenee(a, b)
      when '('
        d, e = do_parse(input)
        if a.nil?
          a = d
        else
          @n.concat(b, d)
        end
        b = e
      when ')'
        return a, b
      else
        d = @n.create_state
        e = @n.create_state
        @n.add_transition(d, e, c)
        if a.nil?
          a = d
        else
          @n.concat(b, d)
        end
        b = e
      end
    end
    [a, b]
  end
end

class RegularExpression
  def initialize(input)
    builder = REParser.new
    @dfa = builder.parse(input).eliminate_epsilon.to_dfa
  end

  def self.ndfa(input)
    builder = REParser.new
    builder.parse(input)
  end

  def matches(input)
    @dfa.accept?(input)
  end

  def display
    @dfa.display
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  n = RegularExpression.ndfa('(ab|c*)*|d')
  n.display
end
# :nocov:
