require_relative '../math/euclid'
module KMeans

  def KMeans.clusters(points, k = 2, initial_centroids: nil, &block)
    raise if points.size < k
    centroids = []
    if initial_centroids.nil?
      while centroids.size < k
        p = points[rand(points.size)]
        centroids << p unless centroids.include? p
      end
    else
      raise "size of centroids should be equal to #{k} but is #{initial_centroids.size}" unless initial_centroids.size == k
      centroids = initial_centroids
    end

    clusters = []
    while true do
      # new_clusters = Hash.new {|h, k| h[k]=[]}
      new_clusters = Array.new(k){[]}
      points.each do |p|
        nearest = k.times.min_by { |i| squared_distance(p, centroids[i])}
        new_clusters[nearest] << p
      end
      break if clusters == new_clusters

      clusters = new_clusters
      block.call centroids, clusters unless block.nil?
      # recalc centroids
      # puts clusters.inspect
      # puts centroids.inspect
  # clusters.map do |c| puts c.join ',' end
      k.times do |i|
        if clusters[i].size == 0
          x, y = centroids[i]
        else
          x = clusters[i].map {|x, y| x}.sum / clusters[i].size
          y = clusters[i].map {|x, y| y}.sum / clusters[i].size
          centroids[i] = [x, y]
        end
      end
    end
    puts "centroids: " + centroids.inspect
    puts "clsters: " + clusters.inspect
    [centroids, clusters]
  end

  private
  def scale(x, y)
    [
      x * @canvas_width / @maxx,
      y * @canvas_height / @maxy
    ]
  end

  def KMeans.plot(centroids, clusters, width: 200, height:200, filename: 'kmeans.png')
    require 'rmagick'
    png = Magick::Image.new(width, height) { self.background_color = "white" }

    maxx = clusters.flatten(1).map{|x, y| x}.max
    maxy = clusters.flatten(1).map{|x, y| y}.max

    colors = %w(red blue green gold gray olive pink orchid lime fuchsia aqua tan teal tomato purple orange magenta SeaGreen PaleTurquoise DodgerBlue)

    draw = Magick::Draw.new
    clusters.each_with_index do |c, i|
      c.each do |x, y|
        x = x*width / maxx
        y = y*height / maxy
        draw.stroke(colors[i])
        draw.fill(colors[i])

        draw.ellipse( x , y, 2, 2, 0, 360)
      end
    end

    centroids.each_with_index do |c, i|
      draw.stroke(colors[i])
      draw.fill(colors[i])
      x, y = c
      x = x * width / maxx
      y = y * height / maxy
      draw.ellipse( x , y, 8, 8, 0, 360)
    end
    draw.draw(png)
    png.write(filename)
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  p = []
  10000.times do |i|
    p << [rand(1000), rand(1000)]
  end
  i = 0
  Dir.mkdir('tmp') unless File.exist?('tmp')

  if ARGV[0]=='-a'
    initial_centroids = 5.times.map {[0, 0]}
    centroid, clusters = KMeans.clusters(p, 5, initial_centroids: initial_centroids) do |centroid, clusters|
      puts "Generating image step=#{i}"
      KMeans::plot(centroid, clusters, width: 500, height: 600, filename: format("tmp/kmeans-%05d.png", i))
      i += 1
    end
    puts 'Images generated. Converting to gif'
    `convert -delay 50 tmp/*.png kmeans.gif`
    puts 'kmeans.gif generated'
  else
    centroid, clusters = KMeans.clusters(p, 5)
  end
  KMeans::plot(centroid, clusters, width: 500, height: 600)
end
# :nocov: