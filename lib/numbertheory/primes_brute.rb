require_relative 'numbers'

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  require 'colorize'
  n = ARGV[0]&.to_i || 8278487999
  puts "Testing if #{n} is prime"
  t1 = Time.now
  puts "#{Numbers.is_prime_brute(n, debug: true).to_s.red}"
  t2 = Time.now
  puts "Found in #{(t2-t1).round(2)} seconds"
end
# :nocov: