def floyd_cycle_detection(x0, f, debug: false)
  # f.(x) is the same as f.call(x) in newer Ruby
  h = f.(f.(x0))
  t = f.(x0)

  iterations = 0
  function_calls = 3

  puts "tortoise=#{t} hare=#{h}" if debug==2
  while t!=h
    h = f.(f.(h))
    t = f.(t)
    puts "tortoise=#{t} hare=#{h}" if debug==2
    iterations += 1
    function_calls += 3
  end

  h = x0
  start = 0
  while t!=h
    t = f.(t)
    h = f.(h)
    start += 1
    puts "tortoise=#{t} hare=#{h}" if debug==2
    iterations += 1
    function_calls += 2
  end

  cycle_len = 1
  h = f.(h)
  function_calls += 1
  while t!=h
    h = f.(h)
    cycle_len += 1
    iterations += 1
    function_calls += 1
  end

  puts "Floyd total iterations=#{iterations}, function calls=#{function_calls}" if debug

  [start, cycle_len]
end

def brent_cycle(x0, f, debug: false)
  hl = tl = 1
  t = x0
  h = f.(x0)
  iterations = 0
  while h != t
    if hl == tl
      t = h
      tl = hl * 2
      hl = 0
    end
    h = f.(h)
    hl += 1

    iterations += 1
  end

  t = h = x0
  hl.times do
    h = f.(h)
    iterations += 1
  end

  function_calls = iterations + 1 # +1 because of call at the top

  start = 0
  while h != t
    h = f.(h)
    t = f.(t)
    start += 1
    iterations += 1
    function_calls += 1
  end

  puts "Brent total iterations=#{iterations}, function calls=#{function_calls}" if debug
  [start, hl]
end


# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  # computes function composition f^n(x)=f(f(...(f(x))))
  # Actually this is called "iterated function" - https://en.wikipedia.org/wiki/Function_composition#Functional_powers
  def compose(x0, function, n)
    x = x0
    n.times do x = function.(x) end
    x
  end

  max = ARGV[0]&.to_i || 17
  function = ->(x) {(x**2 + 1) % max}

  vals = [0]
  20.times do vals << function.(vals.last) end
  puts "values prodced by function: #{vals.join ' '} "

  start, len = floyd_cycle_detection(0, function, debug: true)
  puts "Floyd, start is at i=#{start}, f^i(x0)=#{compose(0, function, start)}, length=#{len}"

  start, len = brent_cycle(0, function, debug: true)
  puts "Brent, start is at i=#{start}, f^i(x0)=#{compose(0, function, start)}, length=#{len}"
end
# :nocov: