require_relative 'jacobi'

def strassen_test(n, t=1, debug: false)
  t.times do
    a = rand(2..(n-1))
    pow = a.pow((n-1)/2, n)
    j = jacobi2(a, n) % n
    return false if pow != 1 && pow != n-1 && j != pow
  end
  true
end

# This is interesting: turns out that removing Jacobi calculation does not degrade results
# Both versions in most cases give the same results.
# But this version is more than 2x faster
def strassen_test2(n, t=1, debug: false)
  t.times do
    a = rand(2..(n-1))
    pow = a.pow((n-1)/2, n)
    return false if pow != 1 && pow != n-1
  end
  true
end


# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  require_relative '../utils'
  require 'colorize'
  i = 7

  successful = 0
  total = 0
  failed = []
  iterations = 0
  max = 1_000_000
  puts "Building sieve"  
  sieve = measure{Numbers.sieve(max)}
srand 10
  measure("Strassent test up to #{max}"){
  while i < max
    s = strassen_test(i, 5)
    b = sieve[i]

    total += 1
    successful += 1 if s==b
    if s == b
      s = s ? 'Y' : 'N'
    else
      if b
        s = 'N'.red
      else
        s = 'N'.yellow
      end
      failed << i
    end
    iterations += 1
    rate = (successful.* 100.0 / total)
    puts "#{iterations} rate=#{rate.round(5)}" if iterations % 100_000 == 0
    puts "#{i} strassen=#{s}, brute force=#{b ? 'Y':'N'}" if false
    i+=2
  end
  }
  rate = (successful.* 100.0 / total)
  puts "Strassen successful rate=#{rate.round(5)}"
  puts "Strassen failures: #{failed}"
end
# :nocov: