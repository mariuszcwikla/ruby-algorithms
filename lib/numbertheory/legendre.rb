require_relative 'Numbers'
def legendre(a, p)
  x = Numbers.modexp(a, (p-1)/2, p)
  if x == p-1
    -1
  else
    x
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  def test(a, p)
    puts "legendre(#{a}, #{p})=#{legendre(a, p)}"
  end
  test(2, 5)
  test(6, 3)
  test(2, 331)
  test(12345, 331)
  test(11, 9907)
  test(13, 9907)
end
# :nocov: