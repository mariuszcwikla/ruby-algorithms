require_relative 'jacobi'

def miller_rabin_test(n, t=1, debug: false)
  s = 0
  r = n-1
  while r.even?
    s += 1
    r /= 2
  end
  t.times do 
    a = rand(2..(n-2))
    #redo if a.gcd(n)!=1
    # LOL: performance is significantly increased when changed from my ruby modexp to built-in Integer.pow which does modular exponentation (since Ruby 2.5)
    # for range 3..10e6, performance increased by 10x (from 22.21 seconds down to 2.05 seconds)
    #y = Numbers.modexp(a, r, n)    
    y = a.pow(r, n)
    next if y == 1 || y == n-1    # ok, probable prime!
    
    found = false
    for j in 0..(s-1)
      # 
      # x = a.pow((2**j)*r, n)
      # if x == n -1 
      #   all = false
      #   break
      # end
      #
      # Previous implementation was using x=a.pow((2**j)*r, n)
      # y = y^2 mod n    is equivalent
      # and faster by 33%
      y = y.pow(2, n)
      if y == n-1
        found = true
        break
      end
    end
    return false unless found
    
  end
  true
end


# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  require_relative '../utils'
  require 'colorize'
  i = 7
  successful = 0
  total = 0
  failed = []
  iterations = 0
  max = 1_500_000
  puts "Building sieve"  
  sieve = measure{Numbers.sieve(max)}

  verbose = false

  measure("Miller-Rabin test test up to #{max}"){
  while i < max
    s = miller_rabin_test(i, 5)
    b = sieve[i]

    total += 1
    successful += 1 if s==b
    if s == b
      s = s ? 'Y' : 'N'
    else
      if b
        s = 'N'.red
      else
        s = 'N'.yellow
      end
      failed << i
    end
    iterations += 1
    rate = (successful.* 100.0 / total)
    puts "#{iterations} rate=#{rate.round(5)}" if iterations % 100_000 == 0
    puts "#{i} miller-rabin=#{s}, brute force=#{b ? 'Y':'N'}" if verbose
    i+=2
  end
  }
  rate = (successful.* 100.0 / total)
  puts "Miller-rabin successful rate=#{rate.round(5)}"
  puts "Miller-rabin failures: #{failed}"
end
# :nocov: