require 'bigdecimal'
def is_square(x)
  s = Integer.sqrt(x)
  return s*s==x
end

def fermat_factorize(n)
  a = a0 = Integer.sqrt(n)
  b2 = a ** 2 - n
  iterations = 0
  i = 0
  until b2 >= 0 && is_square(b2)
    i += 1
    a = a0 + i
    b2 = a ** 2 - n
    return nil if a >= n
    iterations += 1 
  end

  puts "Total iterations: #{iterations}"
  c = a - Integer.sqrt(b2)
  d = n / c
  [c, d]
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  # Testing:
  # ruby lib/numbertheory/fermat_factorization.rb 895649414291294604941588381871244924626104121562042227318384494381723497514540860474803494041479529
  require_relative 'prime_samples'
  x = ARGV[0]&.to_i
  if x.nil?
    a = PrimeSamples.random(:medium)
    b = PrimeSamples.random(:medium)
    x = a * b
    puts "Factorizing #{x}=#{a}*#{b}"
  else
    puts "Factorizing #{x}"
  end
  a, b = fermat_factorize(x)  
  puts [a, b].inspect
end
# :nocov: