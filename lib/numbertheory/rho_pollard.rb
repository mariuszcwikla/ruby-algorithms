require_relative 'numbers'

def rho_pollard(n, debug: false)

  x = 2
  y = 2
  c = 1
  d = 1
  iteration = 0
  while d == 1
    iteration += 1
    puts "Iteration #{iteration}" if debug and iteration % 100_000 == 0

    x = (x*x + c) % n
    y = (y*y + c) % n
    y = (y*y + c) % n

    # Switching from 
    # Numbers.gcd((x-y).abs, n)
    # to built-in gcd resulted in speedup
    # e.g. for 384941741629942975937281
    # with Numbers.gcd - 6.2 seconds
    # with Ruby gcd - 2.28
    d = (x-y).abs.gcd n
    puts "checking #{x} and #{y}, gcd=#{d}" if debug == 2
    break if d == n
  end
  puts "Total iterations: #{iteration}" if debug
  return nil if d==n
  d
end

# rho Pollardd algorithm with Bent's cycle detections
def rho_pollard_brent(n, debug: false)

  x = 2     # tortoise
  y = 2     # hare
  c = 1
  d = 1
  iteration = 0

  hl = tl = 1

  while d == 1
    iteration += 1
    puts "Iteration #{iteration}" if debug and iteration % 100_000 == 0

    if hl == tl
      x = y
      tl *= 2
      hl = 0
    end

    hl += 1
    y = (y*y + c) % n

    d = (x-y).abs.gcd n
    puts "checking #{x} and #{y}, gcd=#{d}" if debug == 2
    break if d == n
  end
  puts "Total iterations: #{iteration}" if debug
  return nil if d==n
  d
end

def rho_pollard_factorize(n, debug: false)
  factors = []
  while n > 1
    d = rho_pollard(n)
    if d.nil?
      puts 'rho pollard returned nil'
      factors << n
      break
    else
      factors << d
      n/=d
    end
  end
  factors
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  # examples:
  # ruby lib/numbertheory/rho_pollard.rb 543563821
  # ruby lib/numbertheory/rho_pollard.rb 543563843
  # To factorize Fermat F8 number (12 mins on my laptop)
  # ruby lib/numbertheory/rho_pollard.rb 115792089237316195423570985008687907853269984665640564039457584007913129639937
  debug = false
  size = :medium
  rho_only = false
  require 'optparse'
  remaining = OptionParser.new do |opts|
    opts.on('-d', '--debug', 'turn on debugging') {debug = true}
    opts.on('-v', 'more verbose debugging debugging') {debug = 2}
    opts.on('-h', 'print help') { puts opts.help; return}
    opts.on('-s', '-1', '--small', "use small primes only") {size = :small}
    opts.on('-m', '-2', '--medium', "use medium primes only") {size = :medium}
    opts.on('-3', '--medium-large', "use medium-large primes only") {size = :medium_large}    
    opts.on('-l', '-4', '--large', "use large primes only") {size = :large}
    opts.on('-r', '--rho-only', "runs only rho Pollard (Floyd and Brents) without factorizing") {rho_only = true}

    # below solution based on
    # https://stackoverflow.com/a/5561070
    # However this also worked (not exactly like the one from stackoverflow, though)
    # opts.on('NUM'){} 
    opts.banner += " [NUM]"
  end.parse(ARGV)
  n = remaining.first&.to_i

  if n.nil?
    require_relative 'prime_samples'
    a = PrimeSamples.random(size)
    b = PrimeSamples.random(size)
    n = a*b
    puts "Picked random primes: "
    puts "p1: #{a}"
    puts "p1: #{b}"
  end
  puts "Computing rho Pollard for #{n}"
  t1 = Time.now
  x = rho_pollard(n, debug: debug)
  t2 = Time.now

  if x.nil?
    puts "rho pollard returned nil (probable prime?)"
  else    
    puts "rho pollard: #{x.inspect}"
    puts "divides? #{n % x == 0}"
    puts "computed in #{(t2-t1).round(2)} seconds"

    puts "Computing rho Pollard (Bent's cycle detection) for #{n}"
    t1 = Time.now
    brent = rho_pollard_brent(n, debug: debug)
    t2 = Time.now
    puts "rho pollard (Brent's): #{brent.inspect}"
    puts "computed in #{(t2-t1).round(2)} seconds"
    unless rho_only
      puts "Factorizing..."
      factors = rho_pollard_factorize(n)
      puts "Factors found: #{factors.join ','}, composite=#{factors.reduce(&:*)}"
    end
  end
end
# :nocov: