require_relative 'strassen_test'

# This script compares behavior of two versions of Strassen primality test
# Turns out that Jacobi step is not that necessary. This step takes long time
# and rarely improves result
# I've run it many times and it found one case so far: 2189700833 (Jacobi version outputs "false", but non-Jacobi output "true")

total_tests = 0
total_misses = 0

1_000_000.times do |n|
  x = rand(10**10)
  x+=1 if x % 2 == 0
  2.times do 
    r1 = strassen_test(x, 5)
    r2 = strassen_test2(x, 5)

    if r1!=r2
      puts "strassen 1=#{r1}, strassen2=#{r2} for #{x}"
      total_misses += 1
    end
    x+=2
    total_tests += 1
  end
  puts n if n % 10_000 == 0
end

puts "Total tests=#{total_tests}, total misses=#{total_misses}"