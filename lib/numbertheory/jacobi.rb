require_relative 'numbers'

# TODO: This is my implementation based on properties of Jacobi
# but it has some bug :(
# Below there is another implementation that is working fine, but it's copy-paste from wikipedia :()
def jacobi(a, n)
  if a % n == 0
    return 0
  end
  return 1 if a == 1 || n == 1
  return 1 if a == -1 and n % 4 == 1
  return -1 if a == -1 and n % 4 == 3
  x = 0
  y = a
  while y.even?
    x += 1
    y /= 2
  end

  if [1, 7].include? (n % 8)
    r = 1
  else
    if x.even?
      r = 1
    else
      r = -1
    end
  end
puts (n-1)*(y-1)/4
  r = -r if ((n-1)*(y-1)/4).odd?  

  r * jacobi(n % y, y)
end

def jacobi2(a, n)
  raise 'even' if n.even?
  return 0 if a == 0
  return 1 if a == 1
  
  x = 0
  y = a
  while y.even?
    x += 1
    y /= 2
  end

  if x.odd? && [3, 5].include?(n % 8)
    r = -1
  else
    r = 1
  end

  if n % 4 == 3 && y % 4 == 3
    r = -1
  end

  if y == 1
    return r
  else
    return r * jacobi2(n % y, y)
  end
  
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  def test(a, p)
    puts "jacobi(#{a}, #{p})=#{jacobi(a, p)}, jacobi2=#{jacobi2(a, p)}"
  end
  test(2, 5)
  test(6, 3)
  test(2, 331)
  test(12345, 331)
  test(11, 9907)
  test(13, 9907)
end
# :nocov: