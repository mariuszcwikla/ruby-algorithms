module Numbers
  def self.gcd_recursive(a, b)
    raise 'a is negative' if a < 0
    raise 'b is negative' if b < 0

    return b if a == 0
    return a if b == 0

    if a > b
      gcd_recursive(b, a % b)
    else
      gcd_recursive(a, b % a)
    end
  end

  def self.gcd(a, b)
    raise 'a is negative' if a < 0
    raise 'b is negative' if b < 0

    a, b = b, a if a > b

    a, b = b % a, a while a > 0
    b
  end

  def self.gcd_by_subtraction(a, b)
    raise 'a is negative' if a < 0
    raise 'b is negative' if b < 0

    a, b = b, a if a > b

    a, b = b - a, a while a > 0
    b
  end

  def self.binary_gcd(x, y)
    g = 1
    while x & 1 == 0 && y & 1 == 0
      x >>= 1
      y >>= 1
      g <<= 1
    end
    while x != 0
      x >>= 1 while x & 1 == 0
      y >>= 1 while y & 1 == 0
      t = (x - y).abs >> 1
      if x >= y
        x = t
      else
        y = t
      end
    end
    g * y
  end

  # wylicza inwersję modulo
  # zwraca -1 gdy liczba nie ma inwersji
  #
  # implementacja przy pomocy "Extended Euclidean algorithm"
  def self.modular_inverse(a, n)
    raise 'a is negative' if a < 0
    raise 'n is negative' if n < 0

    a = a % n
    b = n
    t0 = 0
    t1 = 1
    while a > 1
      r = b / a
      t0, t1 = t1, t0 - t1 * r
      a, b = b % a, a
    end

    return -1 if a == 0 # np. 6 i 12 - nie mają inwersji bo gcd(6,12)=2 != 1

    t1 % n
  end

  # potęgowanie modulo, tj. a**b mod n
  def self.modexp(a, b, n)
    return 1 if b == 0
    return a % n if b == 1

    exponents = { 0 => 1, 1 => a }
    i = 2
    squared = a
    while i <= b
      squared = squared * squared % n
      exponents[i] = squared
      i *= 2
    end

    exponent = i / 2
    v = exponents[exponent]

    add_to_exponent = exponent / 2
    while exponent < b
      next_exponent = exponent + add_to_exponent
      if next_exponent == b
        return (v * exponents[add_to_exponent]) % n
      elsif next_exponent > b
        add_to_exponent /= 2
      else
        v = (v * exponents[add_to_exponent]) % n
        exponent = next_exponent
      end
    end

    raise "current!=b: #{exponent} <-> #{b}" unless exponent == b

    v
  end

  def self.sieve(n)
    sieve = Array.new(n+1){true}
    sieve[0]=false
    sieve[1]=false
    sieve[2]=true

    (2..n).each do |i|
      if sieve[i]
        k = 2*i
        while k<=n do
          sieve[k]=false
          k += i
        end
      end
    end
    sieve
  end

  def self.primes_by_sieve(n)
    s = sieve(n)
    s.each_with_index.select {|flag, i| flag}.map{|flag, i| i}
  end

  def self.is_prime_brute(n, debug: false)
    return true if n == 2 || n == 3
    return false if n % 2 == 0
    i = 3
    max = Integer.sqrt(n)
    iterations = 0
    puts "#{max} (max)" if debug
    while i <= max
      return false if n % i == 0
      i += 2
      iterations += 1
      puts i if iterations % 1_000_000 == 0 and debug
    end
    puts "Iterations: #{iterations}" if debug
    return true
  end

  def self.factorize_brute(n)
    factors = {}
    while n > 1 && n % 2 == 0
      factors[2] ||= 0
      factors[2] += 1
      n/=2
    end
    i = 3
    while n > 1
      while n > 1 && n % i == 0
        factors[i] ||= 0
        factors[i] += 1
        n/=i
      end
      i += 2
    end

    factors
  end

  # Euler totient (phis)
  def self.euler(n)
    factors = factorize_brute(n)
    e = 1
    factors.each do |k, v|
      e *= k-1
      (2..v).each { e *= k}   # because phi(p^k q^j)=p^(k-1) (p-1) q^(j-1) (q-1)
    end
    e
  end

  def self._carmichael(p, r)
    if p % 2 == 0 && ![2, 4].include?(p)
      euler(p**r) / 2
    else
      euler(p**r)
    end
  end

  def self.carmichael(n)
    factors = factorize_brute(n)
    if factors.size == 1
      k, v = factors.first
      _carmichael(k, v)
    else
      factors.map {|k, v|
        _carmichael(k, v)
      }.reduce(&:lcm)
    end
  end
end
