require_relative 'numbers'
module PrimeSamples
  M1 = 46810234759
  M2 = 47055831517
  M3 = 520436736289
  M4 = 620436734591
  M5 = 950466437287
  M6 = 35023438609
  M7 = 3267000013
  M8 = 2147483647
  M9 = 8278487999
  M10 = 9767473717

  # 15 digit primes
  ML1 = 889292677731979
  ML2 = 737373737373737
  ML3 = 914148152112161
  ML4 = 870530414842019
  ML5 = 468395662504823
  ML6 = 518463576809201
  ML7 = 789012345678901

  # 20 -24 digit Mrimes
  L1 = 1079180598685959785401
  L2 = 25211713952371115192327
  L3 = 32226659275055927557999
  L4 = 77755555333222332222233  
  L5 = 561142648578701032870967
  L6 = 89726156799336363541
  L7 = 373353313191181151131

  SMALL = Numbers.primes_by_sieve(10_000)
  VERY_SMALL = SMALL.select{|x| x < 20}
  MEDIUM = [M1, M2, M3, M4, M6, M7, M8, M9, M10, M1, M2]
  MEDIUM_LARGE = [ML1, ML2, ML3, ML4, ML5, ML6, ML7]
  LARGE = [L1, L2, L3, L4, L5, L6, L7]

  ALL = MEDIUM + MEDIUM_LARGE + LARGE + SMALL
  def self.random(size= :any)
    case size
    when :any then ALL.sample
    when :very_small then VERY_SMALL.sample
    when :small then SMALL.sample
    when :medium then MEDIUM.sample
    when :medium_large then MEDIUM_LARGE.sample
    when :large then LARGE.sample
    else
      raise "Unsupported size: #{size}"
    end
  end

end