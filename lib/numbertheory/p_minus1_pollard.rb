require_relative 'numbers'

MAX_PRIME = 1_000_000
PRIMES = Numbers.primes_by_sieve(MAX_PRIME)

# This implementation is as per wikipedia https://en.wikipedia.org/wiki/Pollard%27s_p_%E2%88%92_1_algorithm#Algorithm_and_running_time
# But empirically it's not that good as the one described in Applied Cryptography (Mezenes et al.)
def p_minus1_pollard_wikipedia(n, max_tries: 100_000, max_bound:, bound: 19, debug: false, rng: Random::DEFAULT)

  tries = 0
  b = bound
  max_bound = [Math.sqrt(n).to_i, PRIMES.last, max_bound].min
      
  while tries < max_tries
    a = rng.rand(2..(n-1))
    for q in PRIMES
      break if q > b
      c = q ** (Math.log(b, q).floor)
      puts "#{q} - #{c}" if debug == 2
      # M is too large. I'm using fast modulo exponentation: a^(m1*m2*..*mk) mod n= (((a^m1)^m2)...^mk) mod n = (((a^m1 mod n)^m2 mod n)...^mk mod n)
      # m = m * c 
      a = Numbers.modexp(a, c, n)
    end

    g = (a - 1).gcd(n)
    puts "g=#{g}" if debug

    if g > 1 && g < n
      return g
    else
      # From wikipedia:
      # if g = 1 then select a larger B and go to step 2 or return failure
      # if g = n then select a smaller B and go to step 2 or return failure
      # I've tested this by picking either b+=1 or a random value in range (b+1)..max_bound
      # Neither worked as good as p_minus1_pollard_round
      if g == 1
        if b + 1 > max_bound
          return nil
        end
        #b = rng.rand((b+1)..max_bound)
        b += 1
      else
        b -= 1
        #b = rng.rand(2..(b-1))
      end
      #b = rand(2..max_bound)
    end

    tries += 1
    if b > max_bound
      puts 'max bound exceeded' if debug
      return nil
    end
    puts "retrying (#{tries}) with b=#{b}" if debug
  end
  nil
end

# Single round of P-1 Pollard algorithm
# Implementation as per Applied Cryptography book (Mezenes et al.)
def p_minus1_pollard_round(n, bound: 5, debug: false, rng: Random::DEFAULT)

  a = rng.rand(2..(n-1))
  d = a.gcd(n)
  return d if d >= 2

  raise "bound #{bound} too big. Max=#{PRIMES.last}" if bound > PRIMES.last

  iteration = 0
  for q in PRIMES
    break if q > bound
    # By mistake I've used log(bound, q) but in the book they use log(n, q)
    # However I don't in practice there is no difference...
    #l = Math.log(bound, q).floor
    l = Math.log(n, q).floor
    ql = q ** l
    a = Numbers.modexp(a, ql, n)
    iteration += 1
    puts "q=#{q} l=#{l} a=#{a}, bound=#{bound} iteration=#{iteration}" if debug == 2

  end

  d = n.gcd(a-1)
  return d if d > 1 && d < n

  nil
end

def p_minus1_pollard(n, max_tries: 100, max_bound: PRIMES.last, debug: false, rng: Random::DEFAULT)
  max_bound = [Math.sqrt(n).to_i, PRIMES.last, max_bound].min

  max_tries.times do |i|
    bound = rng.rand(2..max_bound)
    d = p_minus1_pollard_round(n, bound: bound, debug: debug, rng: rng)
    return d unless d.nil?
    puts "retrying (#{i})" if debug
  end
  nil
end

def p_minus1_pollard_factorize(n, debug: false, rng: Random::DEFAULT, max_bound: PRIMES.last)
  factors = []
  while n > 1
    if [2, 3, 5].include? n
      d = n
    else
      # d = p_minus1_pollard_wikipedia(n, debug: debug, max_tries: 100, max_bound: max_bound, rng: rng)
      # Algorithm from 
      d = p_minus1_pollard(n, debug: debug, max_tries: 100, max_bound: max_bound, rng: rng)
      if d.nil?
        d = n
      end
    end
    factors << d
    n/=d
    puts "found factor #{d}" if debug
  end
  factors
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  # examples:
  # ruby lib/numbertheory/p_minus1_pollard.rb 543563821
  # ruby lib/numbertheory/p_minus1_pollard.rb 543563843

  debug = false
  size = :small
  max_bound = PRIMES.last
  num_primes = 2
  require 'optparse'
  remaining = OptionParser.new do |opts|
    opts.on('-d', '--debug', 'turn on debugging') {debug = true}
    opts.on('-v', 'more verbose debugging debugging') {debug = 2}
    opts.on('-bMAX', '--max-bound=MAX', Integer, 'max bound') { |n| max_bound = n}
    opts.on('-0', '--very-small', 'use very small primes only') {size = :very_small}
    opts.on('-s', '-1', '--small', "use small primes only") {size = :small}
    opts.on('-m', '-2', '--medium', "use medium primes only") {size = :medium}
    opts.on('-3', '--medium-large', "use medium-large primes only") {size = :medium_large}
    opts.on('-l', '--large', "use large primes (by default: small, up to #{MAX_PRIME})") {size = :large}
    opts.on('-nNUM_PRIMES', '--num-factors=NUM_PRIMES', Integer, 'choose random NUM_PRIMES. By default it chooses 2 primes and computes their product x=a*b and runs p-1 Pollard on x.') do |n|
      puts 'NUM_PRIMES must be >= 2' if n < 2
      num_primes = n
    end
    opts.on('-h', 'print help') { puts opts.help; return}
    opts.banner += " [NUM]"
  end.parse(ARGV)
  n = remaining.first&.to_i

  require_relative 'prime_samples'
  seed = Random.new_seed
  puts "seed=#{seed}" if debug
  if n.nil?
    # when "big primes" are used algorithm fails to find factor
    # e.g. for n=29042789199312085848569, after 10000 iterations
    # still didn't found nothing 
    # 29042789199312085848569=46810234759*620436734591
    #
    # Therefore rho Pollard is much better?
    # I'm switching to some small prime numbers due to this
    primes = num_primes.times.map { PrimeSamples.random(size)}
    n = primes.reduce(&:*)
    puts "Picked random primes: #{primes.join ' '}"
  end
  rng = Random.new(seed)
  x = p_minus1_pollard_round(n, debug: debug, rng: rng, bound: 19)
  puts "Single round of p-1 Pollard returned: #{x.inspect}"

  puts "Computing p-1 Pollard for #{n}"
  t1 = Time.now
  rng = Random.new(seed)
  x = p_minus1_pollard(n, debug: debug, rng: rng, max_bound: max_bound)
  t2 = Time.now
  if x.nil?
    puts "p-1 pollard returned nil (probable prime?)"
  else
    puts "p-1 pollard: #{x.inspect}"
    puts "Computed in #{(t2-t1).round(2)} seconds"
    puts "Factorizing..."
    rng = Random.new(seed)
    t1 = Time.now
    factors = p_minus1_pollard_factorize(n, rng: rng, debug: debug, max_bound: max_bound)
    t2 = Time.now
    puts "Factors found: #{factors.join ','}, composite=#{factors.reduce(&:*)}. Computed in #{(t2-t1).round(2)} seconds"
  end
end
# :nocov: