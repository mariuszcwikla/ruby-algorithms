require_relative 'rho_pollard'

def rho_pollard_test(n)
  d = rho_pollard(n)
  return true if d.nil?

  false
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  require_relative '../utils'
  require 'colorize'
  i = 3

  successful = 0
  total = 0
  failed = []
  iterations = 0
  max = 1_000_000
  puts "Building sieve"  
  sieve = measure{Numbers.sieve(max)}

  verbose = false

  measure("Rho-Pollard test up to #{max}"){
  while i < max
    s = rho_pollard_test(i)
    b = sieve[i]

    total += 1
    successful += 1 if s==b
    if s == b
      s = s ? 'Y' : 'N'
    else
      if b
        s = 'N'.red
      else
        s = 'N'.yellow
      end
      failed << i
    end
    iterations += 1
    rate = (successful.* 100.0 / total)
    puts "#{iterations} rate=#{rate.round(5)}" if iterations % 100_000 == 0
    puts "#{i} rho-Pollard=#{s}, brute force=#{b ? 'Y':'N'}" if verbose
    i+=2
  end
  }
  rate = (successful.* 100.0 / total)
  puts "rho Pollard successful rate=#{rate.round(5)}"
  puts "rho Pollard failures: #{failed}"
end
# :nocov: