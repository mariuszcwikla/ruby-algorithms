#!/usr/bin/env ruby

# Struktura danych która działa jak stos
# ale jednoczesnie dostarcza w czasie O(1) odczyt wartości minimalnej
#
# Idea: każdy element na stosie w rzeczywistości przechowuje krotkę [element, minimalny_element]
#
class MinStack
  def initialize
    @ary = []
  end

  def push(e)
    if @ary.empty?
      @ary << [e, e]
    else
      min = [@ary.last[1], e].min
      @ary << [e, min]
    end
  end

  def min
    return nil if @ary.empty?

    @ary.last[1]
  end

  def pop
    return nil if @ary.empty?

    @ary.pop[0]
  end

  def peek
    return nil if @ary.empty?

    @ary.last[0]
  end

  def empty?
    @ary.empty?
  end

  def to_s
    @ary.map { |x| x[0] }.to_s
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  s = MinStack.new
  s.push 5
  s.push 10
  s.push 11

  puts "#{s} min: #{s.min}"

  s.push 3
  s.push 0
  s.push 9

  puts "#{s} min: #{s.min}"

  s.pop

  puts "#{s} min: #{s.min}"

  s.pop

  puts "#{s} min: #{s.min}"
end
# :nocov:
