#!/usr/bin/env ruby

# Algorytm Duvala - faktoryzacja Lyndona.

def duval(s)
  lyndon = []

  i = 0
  while i < s.length
    k = i
    j = k + 1
    while j < s.length
      if s[j] == s[k]
        j += 1
        k += 1
      elsif s[j] > s[k]
        k = i
        j += 1
      else
        # s[j]<s[k]
        break
      end
    end
    while i <= k
      lyndon << s[i, (j - k)]
      i += j - k
    end
  end
  # if i<s.length
  #   p s[i..-1]
  # end

  lyndon
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  word = 'bbcjbbcjba'
  words = duval(word)
  puts "Duval word '#{word}', Lyndon factorization: #{words}"

  word = 'bbcjbbcjab'
  words = duval(word)
  puts "Duval word '#{word}', Lyndon factorization: #{words}"

  word = 'bbcjbbcjaz'
  words = duval(word)
  puts "Duval word '#{word}', Lyndon factorization: #{words}"
end
# :nocov:
