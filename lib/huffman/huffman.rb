require_relative '../heap/BinaryHeap'

class HuffmanNode
  attr_accessor :value, :frequency, :height, :left, :right, :parent

  def initialize(value: nil, frequency: -1, left: nil, right: nil, parent: nil)
    @height = 1
    @value = value
    @frequency = frequency
    @parent = parent
    @left = left
    @right = right
  end

  def self.merge(left, right)
    x = HuffmanNode.new(value: nil, frequency: left.frequency + right.frequency)
    x.left = left
    x.right = right
    left.parent = x
    right.parent = x
    x
  end

  def to_s
    ls = @left.nil? ? '' : @left.to_s
    rs = @right.nil? ? '' : @right.to_s
    "(#{value}: #{frequency} -> #{ls}, #{rs})"
  end

  def visit(node, bits = '', &proc)
    proc.call(node, bits)
    if node.value.nil?
      visit(node.left, bits + '0', &proc)
      visit(node.right, bits + '1', &proc)
    end
  end

  def to_h
    h = {}
    visit(self) do |node, bits|
      unless node.value.nil?
        h[node.value] = bits
      end
    end
    h
  end

  def serialize
    out = ''
    visit(self) do |node, _bits|
      if node.value.nil?
        # internal node
        out << '0'
      else
        out << '1'
        out << node.value.ord.to_s(2).rjust(8, '0')
      end
    end
    out
  end

  def attach(left, right)
    @left = left
    @right = right
    left.parent = self
    right.parent = self
  end

  def self.deserialize(serialized)
    serialized = serialized.split('')
    root = HuffmanNode.new
    stack = [root]
    bits_consumed = 0
    until stack.empty?
      type = serialized.shift
      bits_consumed += 1
      node = stack.pop
      if type == '0'
        left = HuffmanNode.new
        right = HuffmanNode.new
        node.attach(left, right)
        stack.push right
        stack.push left
      else
        x = serialized.shift(8)
        bits_consumed += 8
        node.value = x.join('').to_i(2).chr
      end
    end
    [root, bits_consumed]
  end
end

def huffman_tree(s)
  f = {}
  f.default = 0
  s.each_char { |c| f[c] += 1 }

  h = BinaryHeap.new { |a, b| a.frequency < b.frequency }
  f.each { |c, freq| h << HuffmanNode.new(value: c, frequency: freq) }

  while h.size > 1
    a = h.pop
    b = h.pop

    c = HuffmanNode.merge(a, b)
    h.push c
  end
  h.pop
end

def huffman_encode(s)
  t = huffman_tree(s)
  h = t.to_h
  o = ''
  s.each_char { |c| o << h[c] }

  [t, o]
end

def huffman_decode(tree, bits)
  if tree.instance_of? HuffmanNode
    tree = tree.to_h
  end
  tree = tree.invert
  s = ''
  probe = ''
  (0..bits.size - 1).each do |i|
    probe << bits[i]

    v = tree[probe]
    unless v.nil?
      s << v
      probe = ''
    end
  end
  s
end

def append_padding_byte(string)
  # padding
  if string.size % 8 == 0
    string << ('0' * 8)
  else
    rem = string.size % 8
    string << ('0' * (8 - rem))
    string << (8 - rem).to_s(2).rjust(8, '0')
  end
  string
end

def drop_padding(string)
  padding = string[-8..-1].to_i(2)
  string[0..-8 - padding - 1]
end

def huffman_compress(string)
  t, encoded = huffman_encode(string)
  compressed = t.serialize + encoded
  compressed = append_padding_byte(compressed)
  out = []
  (0..compressed.size - 8).step(8) do |i|
    out << compressed[i, 8].to_i(2)
  end
  out.pack('C*')
end

def huffman_decompress(compressed)
  compressed = compressed.unpack('C*')
  bits = compressed.map { |c| c.to_s(2).rjust(8, '0') }.join ''
  tree, bits_consumed = HuffmanNode.deserialize(bits)
  bits = drop_padding(bits[bits_consumed..-1])
  huffman_decode(tree, bits)
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  if ARGV[0] == '-i'
    s = STDIN.gets.strip
  else
    s = 'aaaaaaaaabbbbbaaaccddddef'
  end
  puts "original string: #{s}"
  h, hs = huffman_encode(s)

  puts "Compressed: #{hs}"
  puts "string length bits=#{s.size * 8}, bytes = #{s.size}"
  puts "huffman encoded string length bits=#{hs.size}, bytes = #{(hs.size / 8.0).ceil}. Ratio #{(100.0 * hs.size / (s.size * 8)).round(2)}%"
  puts "serialized huffman tree: #{h.serialize}"

  tree_serialized = h.serialize
  h2, = HuffmanNode.deserialize(tree_serialized)
  h2serialized = h2.serialize
  puts "after deserializing and serializing tree again: #{h2serialized}. Matches? #{tree_serialized == h2serialized}"

  puts "decoded: #{huffman_decode(h, hs)}"

  puts 'Compression ratio with serialized static Huffman tree:'
  size = ((tree_serialized.size + hs.size) / 8.0).ceil
  puts "  original size: #{s.size}"
  puts "  compressed size in bytes: #{size}"
  puts "  ratio: #{(100.0 * size / s.size).round(2)}%"

  puts '-------------- Complete compression and decompression'

  compressed = huffman_compress(s)
  puts "After decompressing: #{huffman_decompress(compressed)}"
end
# :nocov:
