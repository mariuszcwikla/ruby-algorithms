# For testing:

# ruby huffman_compress.rb huffman.rb huffman.huf
# ruby huffman_decompress.rb huffman.huf

require_relative 'huffman'

content = File.open(ARGV[0], 'rb') { |f| f.read }
compressed = huffman_compress(content)
File.open(ARGV[1], 'wb') do |f|
  f.write compressed
end

puts "Compression ratio #{(100.0 - compressed.size * 100.0 / content.size).round(2)}%."
puts "Original size #{content.size} bytes"
puts "Compressed size #{compressed.size} bytes"
