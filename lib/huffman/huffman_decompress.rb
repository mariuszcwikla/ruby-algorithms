require_relative 'huffman'

content = File.open(ARGV[0], 'rb') { |f| f.read }
string = huffman_decompress(content)

puts string
