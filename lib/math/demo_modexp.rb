require 'pry'
require_relative 'modexp'

a = 123_456_789
b = 123_456_789
c = 31_235

puts modexp(5, 2, 10)
puts modexp(123, 2, 7)
puts modexp(7_777_777_777, 888, 432_432_783)
puts modexp(123_456_789, 123_456_789, 31_235)
