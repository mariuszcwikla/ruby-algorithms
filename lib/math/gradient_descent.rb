def numerical_derivative_function_wrapper(function, dx = 0.0001)
  lambda do |x|
    y0 = function.call(x)
    y1 = function.call(x + dx)
    return (y1 - y0) / dx
  end
end

def gradient_descent(function, x, learning_r, error = 0.01, derivative = nil, log = false)
  function = method(function)
  derivative =
    if derivative.nil?
      numerical_derivative_function_wrapper(function)
    else
      method(derivative)
    end

  while true
    x2 = x - learning_r * derivative.call(x)
    break if (x2 > x) || ((x2 - x).abs < error)

    x = x2
    puts x if log
  end
  x
end

def x_squared(x)
  x * x
end

def x_squared_derivative(x)
  2 * x
end

def x_squared2(x)
  2 * x * x - 3 * x
end

def x_squared2_derivative(x)
  4 * x - 3
end

x = gradient_descent(:x_squared, 5, 0.01, 0.00001, :x_squared_derivative, false).round(2)
puts "x*x: #{x} => #{x_squared(x)}"
x = gradient_descent(:x_squared2, 5, 0.01, 0.00001, :x_squared2_derivative).round(2)
puts "2*x^2-3x: #{x} => #{x_squared2(x)}"

x = gradient_descent(:x_squared, 5, 0.01, 0.00001).round(2)
puts "x*x: #{x} => #{x_squared(x)}"
x = gradient_descent(:x_squared2, 5, 0.01, 0.00001).round(2)
puts "2*x^2-3x: #{x} => #{x_squared2(x)}"
