# this version is using doubles which are not very good. Work nice up to 32k
def morris_count_0_1(k)
  r = rand
  if r < 1.0/(2**k)
    k + 1
  else
    k
  end
end

# improved version works better because it's using integers
def morris_count(k)
  r = rand(2**k)
  if r <= 1   # <= or < ?
    k + 1
  else
    k
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  actual = 0
  morris = 0
  loop do
    morris = morris_count(morris)
    actual += 1
    puts "actual=#{actual} morris=#{2**morris}"
  end
end
# :nocov: