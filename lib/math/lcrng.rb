=begin
Examples:


  #good example
  ruby lcrng.rb 13 71 1 91 | grep 33

  #bad example (period too short)
  ruby lcrng.rb 13 71 17 23 | grep 31
  ruby lcrng.rb 13 71 2 0 | grep 11
  ruby lcrng.rb 13 71 2 5 | grep 11

=end

$xn = 13
$modulus = 71
$a = 1
$c = 91

def lcrng_rand
  $xn = ($a * $xn + $c) % $modulus
  $xn
end

if ARGV.size > 0
  $xn, $modulus, $a, $c, num_of_samples = ARGV.map &:to_i
end

num_of_samples = $modulus if num_of_samples.nil?

puts "#{num_of_samples} Pseudo random numbers: "
num_of_samples.times do |_i|
  print "#{lcrng_rand} "
end
puts ''
