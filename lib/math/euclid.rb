
def squared_distance(a, b)
  a.zip(b).reduce(0) { |sum, p| sum + (p[0] - p[1]) ** 2 }
end

def centroid(points, k)
  return [0] * k if points.size.zero?

  k.times.map do |i|
    d = points.map {|p| p[i]}.sum / points.size.to_f
    d
  end
end

def radius(points, center)
  sqr = points.map{|p| squared_distance(center, p)}.max
  Math.sqrt(sqr)
end

def nearest_neighbour_brute(points, p)
  points.min_by{ |pp| squared_distance(p, pp) }
end