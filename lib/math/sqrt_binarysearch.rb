def sqrt(x, n)
  a = 0.0
  b = x
  (0..n).each do |_i|
    half = (a + b) / 2

    return half if half * half == x # we are lucky ;)

    # true binary search
    if half * half > x
      b = half
    else
      a = half
    end
  end
  (a + b) / 2
end

puts "sqrt(16)=#{sqrt(16, 10)}"
puts "sqrt(100, 10)=#{sqrt(100, 10)}"
puts "sqrt(100, 100)=#{sqrt(100, 100)}"
puts "sqrt(150)=#{sqrt(150, 10)}"
puts "sqrt(256)=#{sqrt(256, 10)}"
puts "sqrt(104230, 10)=#{sqrt(104_230, 10)}"
puts "sqrt(104230, 100)=#{sqrt(104_230, 100)}"
