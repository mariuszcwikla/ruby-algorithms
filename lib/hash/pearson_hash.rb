def construct_pearson_table
  x = *(0..255)
  x.shuffle
end

$pearson_table = construct_pearson_table()

def pearson_hash(string, table = $pearson_table)
  h = 0
  string.each_char do |c|
    h = table[h ^ c.ord]
  end
  h
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || PROGRAM_NAME.include?('ruby-prof')
  words = %w(hello world ruby buuu)
  words.each do |w|
    puts "pearson(#{w})=0x#{pearson_hash(w).to_s(16)}"
  end
end
# :nocov: