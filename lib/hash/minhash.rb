module MinHash
  def self.signature(set, k: 4, hash_function: nil)
    hashes = {}
    if hash_function.nil?
      hash_function = ->(x) { x.hash }
    end
    set.each { |x| hashes[hash_function.call(x)] = x }

    hashes.sort[0...k].map { |_k, v| v }
  end

  def self.similarity(sig_a, sig_b, hash_function: nil)
    # as per formulas in wikipedia
    x = MinHash.signature(sig_a + sig_b, k: sig_a.length, hash_function: hash_function)
    y = x & sig_a & sig_b
    k = x.length

    y.length.to_f / k
  end
end

# Computes true Jaccard index (O(n log n))
def jaccard(a, b)
  union = a & b
  sum = (a + b).uniq
  union.length.to_f / sum.length
end

# :nocov:

if __FILE__ == $PROGRAM_NAME
  require 'terminal-table'
  def test(a, b, k, debug = false)
    sig1 = MinHash.signature(a, k: k)
    sig2 = MinHash.signature(b, k: k)
    puts "signature 1: #{sig1.join ','}" if debug
    puts "signature 2: #{sig2.join ','}" if debug
    sim = MinHash.similarity(sig1, sig2)
    j = jaccard(a, b).round(3)
    sim1 = sim.round(3)
    puts "Minhash (n=#{a.size}) estimated value: #{sim1}, Jaccard index=#{j} (Object.hash)"
    h = ->(x) { (x * 3371) % 65_646_543 }
    sig1 = MinHash.signature(a, k: k, hash_function: h)
    sig2 = MinHash.signature(b, k: k, hash_function: h)
    sim = MinHash.similarity(sig1, sig2, hash_function: h)
    sim2 = sim.round(3)
    puts "Minhash (n=#{a.size}) estimated value: #{sim2}, Jaccard index=#{j} (custom hash fun)"

    [a.size, k, j, sim1, (j - sim1).abs.round(3), sim2, (j - sim2).abs.round(3)]
  end
  a = [1, 2, 8, 9, 10, 12, 28, 34, 28, 39, 44, 45]
  b = [1, 2, 8, 9, 10, 12, 27, 33, 39, 44, 45, 46, 47]

  rows = []
  rows << ['size', 'k', 'Jaccard index', 'similarity (1)', 'diff', 'similarity (2)', 'diff']
  rows << :separator
  (2..8).each do |k|
    puts "testing with k=#{k}"
    rows << test(a, b, k)
  end

  [500, 1000, 1500, 2000, 5000, 10_000].each_with_index do |max_rand, i|
    puts '**************************************************'
    puts "Testing arrays of 1000 records (max_rand=#{max_rand})"
    puts '**************************************************'
    rows << ["Sample #{i + 1}"]
    a = Array.new(1000) { rand(max_rand) }
    b = Array.new(1000) { rand(max_rand) }
    [5, 10, 20, 35, 50, 75, 100].each do |k|
      puts "Testing with k=#{k}"
      rows << test(a, b, k)
    end
    rows << :separator
  end

  puts Terminal::Table.new rows: rows
end
# :nocov:
