class ConsistentHash
  MAX_HASH = (1<<16) - 1
  def initialize(number_of_buckets: 11, init_strategy: :random)
    @number_of_buckets = number_of_buckets
    case init_strategy
    when :random
      # fully random initialization sucks. Sometimes two consecutive locations are too close which lead to very very unbalanced load
      @buckets = Array.new(number_of_buckets){ [rand(MAX_HASH), [] ] }
    when :random_start
      # this lead to balanced load
      location = rand(MAX_HASH)
      @buckets = []
      number_of_buckets.times do
        @buckets << [location, []]
        location = (location + (MAX_HASH / @number_of_buckets)) % MAX_HASH
      end
      @bucket
    else
      raise "Unknown strategy #{init_strategy}"
    end
    @buckets.sort_by!{|b| b[0]}
  end

  def insert(value)
    find_bucket(value)[1] << value
  end

  private def hash_value(value)
    value.hash % MAX_HASH
  end

  private def find_bucket(value)
    h = hash_value(value)
    @buckets.bsearch{|b| b[0] >= h} || @buckets.first
  end

  alias << insert

  def to_s
    @buckets.map {|b| "#{b[0]}, size=#{b[1].size}"}.join "\n"
  end

  def drop_bucket(i)
    raise "Bucket #{i} does not exist. Valid buckets are (0..#{@buckets.size - 1}) " unless i >= 0 && i < @buckets.size

    b = @buckets[i][1]
    @buckets.delete_at(i)
    b.each { |value| insert value } # this might as well be changed to:
                                    # @buckets[(i + 1) % @buckets.size][1] << b[1]
                                    # but for simplicity I'm just calling insert
  end

  def include?(x)
    find_bucket(x)[1].include? x
  end

  def create_bucket
    retries = 0
    loop do
      raise 'too many retries' if retries > 5
      retries += 1
      location = rand(MAX_HASH)
      location = 500
      idx = @buckets.bsearch_index { |b| b[0] >= location} || 0
      redo if @buckets[idx][0] == location

      data = @buckets[idx][1]
      @buckets[idx][1]=[]
      @buckets.insert(idx, [location, []])
      data.each {|v| insert v}
      return idx
    end
    # not implemented yet...
  end
end 


# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  # NOTE: due to the fact that ruby hash method implements "Universal hashing" running this program many times
  # gives different results each time
  # to workaround this issue (only for the sake of the demo!) I am overriding hash implementation as follows
  class Integer
    def hash
      (to_i * 1979397757) % (1 << 32)
    end
  end
  srand 0
  h = ConsistentHash.new(number_of_buckets: 7, init_strategy: :random_start)

  10.times do h << rand(1_000) end

  # This demo demonstrates use of consistent hashing
  # 1. initialy we have 7 buckets. Data is evenly distributed due to "init_strategy" as "random_start" (note: change it to "random" - you will observe unbalanced distrbution randomly!)
  # 2. after dropping one bucket we see that next bucket will take it's data (size of bucket increased from 8 to 17)
  # 3. another drop - again size increased from 17 to 26
  # but this demo demonstrates that all other buckets do not increase in size
  puts 'After inserting elements'
  puts h

  puts 'dropping index 2'
  h.drop_bucket 2
  puts h

  puts 'dropping index 1'
  h.drop_bucket 1
  puts h

  puts 'creating random bucket'
  h.create_bucket
  puts h
end
# :nocov: