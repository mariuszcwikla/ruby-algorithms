require 'murmurhash3'
class MinimalPerfectHash
  class HashFunction    # I don't like this class :(
    def initialize(hash_function, size)
      @size = size
      @hash_function = hash_function || (->(i, value) { MurmurHash3::V32.int32_hash(i, value) })
    end

    def calc(i, value)
      @hash_function.call(i, value) % @size
    end
  end
  class MinimalPerfectHashBuilder

    def initialize(keys, hash_function = nil)
      @keys = keys
      @h = HashFunction.new(hash_function, keys.size)
    end

    def size
      @keys.size
    end

    def build_hash
      intermediary_table = Array.new(size){[]}    # according to the paper, size of intermediary table is "r" and it can be that "r < n". For simplicity I'm using r=n
                                                  # TODO: rework algorithm later to use r < n
      @keys.each do |k|

        h = @h.calc(0, k)
        intermediary_table[h] << k
      end
      # TODO rework sorting. According to the paper it should be O(n)
      intermediary_table.sort_by!{|t| t.size}.reverse!
      table1 = Array.new(size)
      table2 = Array.new(size)
      iterations = 0
      intermediary_table.each do |t|
        break if t.empty?
        i = 0
        loop do
          found = true
          hashes = []
          t.each do |x|
            h = @h.calc(i, x)
            if !table2[h].nil? || hashes.include?(h)
              found = false
              break
            end
            hashes << h
          end
          break if found
          i += 1
        end
        h = @h.calc(0, t[0])
        table1[h] = i
        t.each do |x|
          h = @h.calc(i, x)
          table2[h] = x
        end
        iterations += 1
        #puts iterations if (iterations % 5_000)==0
      end
      MinimalPerfectHash.new(table1, table2, @h)
    end
  end

  private def initialize(table1, table2, hash_function)
    @table1=table1
    @table2=table2
    @h = hash_function
  end

  def include?(key)
    h = @h.calc(0, key)
    return false if @table1[h].nil?
    h2 = @h.calc(@table1[h], key)
    @table2[h2]==key
  end

  def self.build(keys, hash_function: nil)
    MinimalPerfectHashBuilder.new(keys, hash_function).build_hash
  end

end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  n = 100
  data = n.times.map {|r| rand(n * 10)}.uniq
  puts "generated #{data.size} elements"
  # puts "data: #{data.sort.join ','}"
  h = MinimalPerfectHash.build(data)

  count = 0
  for i in 0...(n * 10)
    match = (data.include?(i) == h.include?(i))
    count += 1 if match
    if !match
      puts "inconsistency at #{i}"
    end
  end
  puts "Matched #{count} elements"
end
# :nocov: