# NOTES:
# - there is no special handling for duplicates. Effectively this allows for duplicate values.
# This is "version 1" of linear hash that I've implemented. It exploits certian properties of base-2 calculations, e.g.:
#
# h = h & ((1 << @i) - 1)
#
# Litwin's paper on Linear Hash describes very generic approach to linear hashing, e.g. hash function h_i(c)= c mod N*2^i
#
# If N is not power of 2, then "mod" will not work.
# For generic implementation - see LinearHash instead.
# In benchmarks LinearHashBase2 I see no difference compared to LinearHash. Still I want to keep both implementations.
class LinearHashBase2
  attr_reader :size

  class Block
    attr_accessor :prev

    #:next
    def initialize(array, address)
      @array = array
      @address = address
      @last_idx = 0
      @prev = nil
    end

    def full?
      @last_idx >= @array.size
    end

    def empty?
      @last_idx.zero?
    end

    def insert(value)
      raise :block_is_full if full?

      @array[@last_idx] = value
      @last_idx += 1
    end

    def include?(value)
      return true if @array.include?(value)

      return @prev.include?(value) unless @prev.nil?

      false
    end

    def records
      @array[0...@last_idx]
    end

    def block_size
      @array.size
    end

    def replace_records(new_records)
      raise "incompatible sizes #{new_records.size}, #{@last_idx}" if new_records.size > @array.size

      @array[0...new_records.size] = new_records
      new_records.size.upto(@array.size) { |i| @array[i] = nil }
      @last_idx = new_records.size
    end

    def reset
      @last_idx = 0
      @prev = nil
    end

    def remove(value)
      idx = @array.index value
      if !idx.nil?
        @last_idx -= 1
        @array[idx] = @array[@last_idx]
        @array[@last_idx] = nil
        true
      elsif !@prev.nil?
        @prev.remove(value)
      else
        false
      end
    end
  end

  class Bucket
    attr_reader :hash_bits

    def initialize(hash_bits, block)
      @block = block
      @hash_bits = hash_bits
    end

    def to_s
      "#{hash_bits.to_s(2)} [#{records.join ','}]"
    end

    def insert(value)
      @block.insert(value)
    end

    def include?(value)
      @block.include?(value)
    end

    def append_block(block)
      block.prev = @block
      @block = block
    end

    def remove(value)
      @block.remove(value)
    end

    def full?
      @block.full?
    end

    def records
      r = []
      b = @block
      until b.nil?
        r += b.records
        b = b.prev
      end
      r
    end

    # returns blocks as array. Oldest at the end of array.
    def blocks
      b = @block
      ret = []
      until b.nil?
        ret << b
        b = b.prev
      end
      ret
    end

    def replace_records(new_records)
      blocks.reverse.each do |b|
        b.replace_records(new_records.shift(b.block_size))
      end
      raise 'not enough space to insert all records' unless new_records.empty?
    end

    def compact_blocks
      compacted = []
      while !@block.prev.nil? && @block.empty?
        b = @block
        @block = @block.prev
        b.prev = nil
        compacted << b
      end
      compacted
    end
  end

  def initialize(initial_blocks: 2, block_size: 2, load_factor_min: 0.7, load_factor_max: 0.9)
    @blocks_pool = []
    @size = 0
    @load_factor = load_factor_max
    @load_factor_min = load_factor_min
    @block_size = block_size
    @i = 0
    @allocate_block_index = 0
    @buckets = []
    initial_blocks.times do |i|
      b = Bucket.new(i, allocate_block)
      @buckets << b
      @i += 1 if power_of_two?(i)
    end

    # @buckets = [Bucket.new(0, allocate_block), Bucket.new(1, allocate_block)]

    # allocate_block_index variable simulates disk access.
    # It's correspondence to disk is as follows:
    # - if it grows then block on disk is allocated
    #   e.g. @allocate_block_index+=1 means allocating n bytes on disk
    #        @allocate_block_index+=2 means allocating n*2 bytes
    # - if it's decreased, then block is released (file is shrunk by n bytes)
  end

  def to_s
    s = "\n"
    @buckets.each do |b|
      s << "#{b.hash_bits.to_s(2)}: "
      s << b.records.join(',')
      s << "\n"
    end
    s
  end

  def empty?
    @size.zero?
  end

  # _hash, not hash, to avoid name clash with Object#hash
  def _hash(value)
    # IMPORTANT: Object#hash returns random values!. Seems that each time you run the app, hash implementations rely on some random seed!
    # for simplicity, to get repeatable tests, i'm using identity function for Integer hash

    h = value.hash
    if value.instance_of? Integer
      h = value
    end

    h &= ((1 << @i) - 1)

    if h < @buckets.size
      h
    else
      h ^ (1 << (@i - 1))
    end
  end

  def allocate_block
    if @blocks_pool.empty?
      block_data = Array.new(@block_size) # i.e. this is "allocation" of "block_size" bytes
      block = Block.new(block_data, @allocate_block_index)
      @allocate_block_index += 1
      block
    else
      @blocks_pool.pop
    end
  end

  def insert(value)
    h = _hash(value)
    bucket = @buckets[h]
    if bucket.full?
      block = allocate_block
      bucket.append_block(block)
    end

    bucket.insert(value)
    @size += 1

    if @size > @block_size * @buckets.size * @load_factor
      split
    end
  end

  def include?(value)
    h = _hash(value)
    @buckets[h].include?(value)
  end

  def split
    block = allocate_block
    hash_bits = @buckets.size
    bucket = Bucket.new(hash_bits, block)
    @buckets << bucket

    from_idx = hash_bits ^ (1 << (hash_bits.bit_length - 1))
    from = @buckets[from_idx]

    @i += 1 if power_of_two?(hash_bits)

    reallocate(from, bucket)
  end

  def power_of_two?(num)
    # x.to_s(2).include?('0')
    num != 0 && (num & (num - 1)) == 0
  end

  def reallocate(from, to)
    # TODO: srednio podoba mi sie ta implementacja
    # Sprobuj ją zrefkatoryzowac!
    # może lepiej zrobić tak:
    # tj. erase_all "odlinkowuje" wszystkie bloki za wyjatkiem jednego
    # następnie zwrócić je do puli bloków
    #
    # records = from.records
    # unused_blocks = from.release_overflow_blocks
    # return_blocks_to_pool(unused_blocks)

    records = from.records
    from_records = []
    records.each do |r|
      hh = _hash(r)
      if hh == to.hash_bits
        if to.full?
          to.append_block(allocate_block)
        end
        to.insert(r)
      elsif hh & from.hash_bits == from.hash_bits
        from_records << r
      else
        raise 'this hash does not match neither from nor to'
      end
    end

    from.replace_records(from_records)
    blocks = from.compact_blocks
    return_blocks_to_pool(blocks)
  end

  def return_blocks_to_pool(blocks)
    blocks.each do |b|
      b.reset
      @blocks_pool << b
    end
  end

  def remove(value)
    h = _hash(value)
    if @buckets[h].remove(value)
      blocks = @buckets[h].compact_blocks
      return_blocks_to_pool blocks
      @size -= 1
      if @size < @block_size * @buckets.size * @load_factor_min
        puts 'MERGE not implemented yet'
      end
      true
    else
      false
    end
  end
end

if __FILE__ == $PROGRAM_NAME
end
