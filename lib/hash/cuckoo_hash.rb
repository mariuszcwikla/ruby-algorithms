require 'murmurhash3'

class CollisionException < StandardError; end

#NOTE: current implementation works on integers. Slight modification is needed to support e.g. strings

module HashFunctions
  def self.hash1
    ->(value) {MurmurHash3::V32.int32_hash(value, 432781)}
  end
  def self.hash2
    ->(value) {MurmurHash3::V32.int32_hash(value, 781233)}
  end
end

# First implementation of Cuckoo hash (2 internal tables)
class CuckooHash
  def initialize(hash_table_size, h1 = HashFunctions.hash1, h2 = HashFunctions.hash2)
    @table1 = Array.new(hash_table_size/2)
    @table2 = Array.new(hash_table_size/2)
    @table_size = hash_table_size / 2
    @h1 = h1
    @h2 = h2
  end

  private def _hash1(value)
    @h1.call(value) % @table_size
  end
  private def _hash2(value)
    @h2.call(value) % @table_size
  end

  def include?(value)
    return @table1[_hash1(value)] == value || @table2[_hash2(value)] == value
  end

  def insert(value)
    return if include?(value)

    iterations = 0
    until value.nil?
      iterations += 1
      h1 = _hash1(value)
      h2 = _hash2(value)
      if @table1[h1].nil?
        @table1[h1] = value
        break
      else
        v = @table1[h1]
        @table1[h1] = value
        value = @table2[h2]
        @table2[h2] = v
      end
      # I don't want to do rehashing. I'm just testing efficiency of cucko hashing
      raise CollisionException if iterations >= @table_size * 2
    end
    #puts "Took #{iterations} iterations"
  end
end

# Second implementation - variable number of internal tables. 
class CuckooHashN
  def initialize(capacity, hash_functions)
    @hash_functions = hash_functions
    @num_of_hashes = hash_functions.size
    @table_size = capacity / @num_of_hashes
    @tables = Array.new(@num_of_hashes)
    for i in 0...@num_of_hashes
      @tables[i] = Array.new(@table_size)
    end
  end

  private def hash_function(i, value)
    @hash_functions[i].call(value) % @table_size
  end
  
  def include?(value)
    for i in 0...@num_of_hashes
      val = @tables[i][hash_function(i, value)]
      return true if val == value
    end
    false
  end

  def insert(value)
    return if include?(value)

    iterations = 0
    until value.nil?
      iterations += 1
      for i in 0...@num_of_hashes
        h = hash_function(i, value)
        if @tables[i][h].nil?
          @tables[i][h] = value
          return
        else
          v = @tables[i][h]
          @tables[i][h] = value
          value = v
        end
      end
      # I don't want to do rehashing. I'm just testing efficiency of cucko hashing
      raise CollisionException if iterations >= capacity
    end
  end

  def capacity
    @table_size * @num_of_hashes
  end
end
class StandardHash

  def initialize(size)
    @table = Array.new(size)
  end

  def _hash(value)
    MurmurHash3::V32.int32_hash(value, 432781) % @table.size
  end

  def include?(value)
    @table[_hash(value)] == value
  end
  def insert(value)
    #h = value.hash % @table.size
    h = _hash(value)
    return if value == @table[h]
    if @table[h].nil?
      @table[h] = value
    else
      raise CollisionException
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  size = 100
  puts "Inserting #{size} elements to hash tables"
  ch = CuckooHash.new(size
    #  ->(value) { value.hash ^ 878781},
    #  ->(value) { value.hash ^ 12378793}
  )
  ch2 = CuckooHash.new(size, 
    ->(value) { value.hash},
    ->(value) { value.hash * 17 + 33}
  )
  ch3 = CuckooHashN.new(size, 
    [
      ->(value) { MurmurHash3::V32.int32_hash(value, 432781) },
      ->(value) { MurmurHash3::V32.int32_hash(value, 781233) },
      ->(value) { MurmurHash3::V32.int32_hash(value, 66643) }
    ]
  )
  ch4 = CuckooHashN.new(size,
    [
      ->(value) { MurmurHash3::V32.int32_hash(value, 432781) },
      ->(value) { MurmurHash3::V32.int32_hash(value, 781233) },
      ->(value) { MurmurHash3::V32.int32_hash(value, 66643) },
      ->(value) { MurmurHash3::V32.int32_hash(value, 321) }
      # ->(value) { value.hash ^ 42637689},
    ]
  )
  data = []
  size.times { data << rand(1_000_000)}
  def test(label, hash_table, data)
    i = 0
    begin
      data.each do |x|
        hash_table.insert x
        i += 1
      end
    rescue CollisionException
      load_factor = i.to_f * 100/ data.size
      puts "#{label}: collision after inserting #{i} elements. Load factor=#{load_factor.round(1)}%"
    else
      puts "#{label}: NO collision after inserting #{i} elements. Load factor=100%"
    end
  end
  test("Cucko hash         ", ch, data)  
  test("Standard hash table", StandardHash.new(size), data)
  test("Cucko hash (shitty hash functions)", ch2, data)
  test("Cucko hash (3 hash functions)", ch3, data)
  test("Cucko hash (4 hash functions)", ch4, data)

end
# :nocov: