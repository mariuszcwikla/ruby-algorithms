class PolyRollingHash
  attr_reader :hash_value

  def initialize(array, a: 13, mod: 71)
    @array = array.dup
    @a = a
    @index = 0
    @mod = mod
    @hash_value = 0
    @a0 = 1
    k = array.size
    (0...k).each do |i|
      @hash_value = (@hash_value * a + value_of(array[i])) % @mod
    end
    (k - 1).times { @a0 = (@a0 * a) % @mod }
  end

  def value_of(element)
    if element.instance_of? String
      element.ord
    else
      element
    end
  end

  def roll(value)
    sub = (@a0 * value_of(@array[@index])) % @mod
    @hash_value = ((@hash_value - sub) * @a + value_of(value)) % @mod

    @array[@index] = value
    @index = (@index + 1) % @array.length

    @hash_value
  end
end

class BuzzHash
  attr_reader :hash_value

  def initialize(array)
    @array = array.dup
    @index = 0
    @hash_value = 0
    k = array.size
    (0...k).each do |i|
      @hash_value ^= array[i].hash
    end
  end

  def roll(value)
    #sub = (@a0 * value_of(@array[@index])) % @mod
    @hash_value = @hash_value ^ @array[@index].hash ^ value.hash

    @array[@index] = value
    @index = (@index + 1) % @array.length

    @hash_value
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  a = [8, 5, 13, 7, 18, 19, 20, 4, 2, 1]
  puts 'hashes of subarrays:'
  puts PolyRollingHash.new(a[0...4]).hash_value
  puts PolyRollingHash.new(a[1...5]).hash_value
  puts PolyRollingHash.new(a[2...6]).hash_value
  puts PolyRollingHash.new(a[3...7]).hash_value

  puts 'Now rolling hash'
  rh = PolyRollingHash.new(a[0...4])
  puts rh.hash_value
  puts rh.roll(a[4])
  puts rh.roll(a[5])
  puts rh.roll(a[6])
end
# :nocov:
