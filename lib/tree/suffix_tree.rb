require_relative 'radix_tree'

# for brute force algorith we just need to add all sufixes to the tree
# Complexity:
# - O(n^2) - building the tree
# - O(m) - when matching the pattern. Actually will be bit more due to non-optimized RadixTree::Node::include?.
#          It iterates over children and does start_with?. It could be optimized. But I think it's not a big deal.
def build_suffix_tree_brute_force(string)
  t = RadixTree.new
  (string.length - 1).downto(0) { |i| t << string[i..-1] }
  def t.matches?(word)
    include? word
  end
  t
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  word = 'banana'
  t = build_suffix_tree_brute_force(word)
  t.dump

  def locate(t, word, pattern)
    puts "[#{word}] #{t.include?(word) ? 'matches' : 'DOES NOT match'} pattern [#{pattern}]"
  end

  locate(t, 'banana', word)
  locate(t, 'baanana', word)
  locate(t, 'ana', word)
  locate(t, 'nana', word)
  locate(t, 'aana', word)

  loop do
    print '> '
    word = STDIN.gets.strip
    t = build_suffix_tree_brute_force(word)
    t.dump
  end
end
# :nocov:
