class IntervalTree
  class Node
    attr_accessor :left, :right, :max
    attr_reader :lo, :hi

    def initialize(lo, hi)
      @lo = lo
      @hi = hi
      @max = hi
    end

    def overleap?(a, b)
      return false if b <= @lo
      return false if a >= @hi

      true
    end
  end

  def initialize
    @root = nil
  end

  def insert(a, b, node = @root)
    if @root.nil?
      @root = Node.new(a, b)
    elsif node.nil?
      Node.new(a, b)
    else
      if a < node.lo
        node.left = insert(a, b, node.left)
      else
        node.right = insert(a, b, node.right)
      end
      node.max = [node.left&.max, node.right&.max, node.max].map(&:to_i).max
      node
    end
  end

  def interval_search(a, b)
    node = @root
    until node.nil?
      return node if node.overleap?(a, b)

      if node.max < a
        node = node.left
      else
        node = node.right
      end
    end
  end
end
