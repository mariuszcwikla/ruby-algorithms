
class SegmentTreeArrayBased
  attr_reader :function, :iterations
  def initialize(arr, function)
    @function = function
    @num_leaves = arr.length
    #@nodes = Array.new(2 * arr.length - 1)
    @nodes = Array.new(4 * arr.length)    # Need 4*N space because algorithm creates "holes" in the array
    build(arr, 0, 0, arr.length)
  end

  def query(a, b)
    @iterations = 0

    left = 0
    right = @num_leaves
    query_internal(a, b, 0, left, right)
  end

  private def query_internal(a, b, n, left, right)
    @iterations += 1
    if a == left && b == right
      @nodes[n]
    else
      mid_node = (left + right) / 2
      if b <= mid_node
        query_internal(a, b, n * 2 + 1, left, mid_node)
      elsif a >= mid_node
        query_internal(a, b, n * 2 + 2, mid_node, right)
      else
        m = (a + b )/2 
        vl = query_internal(a, mid_node, n * 2 + 1, left, mid_node)
        vr = query_internal(mid_node, b, n * 2 + 2, mid_node, right)
        @function.call(vl, vr)
      end
    end
  end

  private def build(array, n, a, b)
    # Note: this implementation makes "holes" in the node array
    # because it does not create perfectly balanced tree
    # Implementing tree without holes is tricky. I couldn't develop good algorithm to do it :(
    if n >= @nodes.size
      raise "out of bounds #{n}, array.size=#{array.size}, total num of nodes=#{@nodes.size}"
    end
    if a == b - 1
      @nodes[n] = array[a]
    else
      mid = (a + b) / 2
      # mid = a + (b - a)/2
      left = build(array, n * 2 + 1, a, mid)
      right = build(array, n * 2 + 2, mid, b)
      @nodes[n] = @function.call(left, right)
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  class SegmentTreeArrayBased
    attr_reader :nodes
  end
  require 'colorize'
  arr = 100.times.map{ rand(10) }
  func = ->(a, b) {a+b}
  # func = ->(a, b) {[a,b].min}
  # func = ->(a, b) {[a,b].max}
  # func = ->(a, b) {a*b}
  # func = ->(a, b) {a^b}
  
  t = SegmentTreeArrayBased.new(arr, func)
  puts arr.inspect
  puts t.nodes.inspect
  def brute(arr, a, b, func)
    arr[a..(b-1)].reduce(&func)
  end

  def test(arr, tree, a, b)
    print "Testing #{a}..#{b}, "
    c = brute(arr, a, b, tree.function)
    d = tree.query(a, b)
    puts "brute=#{c}, segment tree=#{d}, num of iterations=#{tree.iterations} #{c!=d ? 'NOT OK'.red : 'OK'.green }"
  end

  test(arr, t, 0, 4)

  50.times do
    a = rand(arr.size)
    b = rand(arr.size)
    next if a == b
    a, b = b, a if b < a
    test(arr, t, a, b)
  end
end
# :nocov: