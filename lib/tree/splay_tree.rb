class SplayTree
  class Node
    attr_accessor :parent, :left ,:right
    attr_reader :value
    def initialize(value, parent=nil)
      @value = value
      @parent = parent
    end
  end

  attr_reader :size

  def initialize
    @root = nil
    @size = 0
  end

  def insert(value)
    @size += 1
    if @root.nil?
      @root = Node.new(value)
    else
      n = @root
      loop do
        if value < n.value
          if n.left.nil?
            n.left = Node.new(value, n)
            splay(n.left)
            break
          else
            n = n.left
          end
        else
          if n.right.nil?
            n.right = Node.new(value, n)
            splay(n.right)
            break
          else
            n = n.right
          end
        end
      end
    end
  end

  def find(value)
    n = @root
    until n.nil? || n.value == value
      if value < n.value
        n = n.left
      else
        n = n.right
      end
    end

    return nil if n.nil?
    splay(n)
    n 
  end

  def include?(value)
    !find(value).nil?
  end

  def empty?
    @root.nil?
  end

  def splay(n)
    until n == @root
      if n.parent == @root
        # zig
        if @root.left == n
          rotate_right(@root)
        else
          rotate_left(@root)
        end
      elsif n == n.parent.left && n.parent == n.parent.parent.left
        rotate_right(n.parent.parent)
        rotate_right(n.parent)
      elsif n == n.parent.right && n.parent == n.parent.parent.right
        rotate_left(n.parent.parent)
        rotate_left(n.parent)
      elsif n == n.parent.left && n.parent == n.parent.parent.right
        rotate_right(n.parent)
        rotate_left(n.parent)
      elsif n == n.parent.right && n.parent == n.parent.parent.left
        rotate_left(n.parent)
        rotate_right(n.parent)
      else
        raise 'invalid case'
      end
    end
  end

  def rotate_right(n)
    left = n.left
    n.left = left.right
    left.right = n

    if n == @root
      @root = left
      @root.parent = nil
    else
      if n.parent.left == n
        n.parent.left = left
      else
        n.parent.right = left
      end
      left.parent = n.parent
    end
    n.parent = left
    n.left.parent = n unless n.left.nil?
  end

  def rotate_left(n)
    right = n.right
    n.right = right.left
    right.left = n

    if n == @root
      @root = right
      @root.parent = nil
    else
      if n.parent.left == n
        n.parent.left = right
      else
        n.parent.right = right
      end
      right.parent = n.parent
    end
    n.parent = right
    n.right.parent = n unless n.right.nil?
  end

  def remove(value)
    n = find(value)
    return if n.nil?

    if n.left.nil?
      @root = n.right
      @root.parent = nil unless @root.nil?
    else
      @root = n.left
      @root.parent = nil
      max = @root
      max = max.right until max.right.nil?
      splay(max)
      raise 'right should be nil' unless @root.right.nil?
      @root.right = n.right
      @root.right.parent = @root unless @root.right.nil?
    end
    @size -= 1
  end
end