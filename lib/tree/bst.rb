class BinarySearchTree
  class Node
    attr_accessor :parent, :left, :right
    attr_reader :value

    def initialize(parent, value)
      @value = value
      @parent = parent
      @left = nil
      @right = nil
    end

    def size
      s = 1
      s += @left.size unless @left.nil?
      s += @right.size unless @right.nil?
      s
    end
  end

  def initialize
    @root = nil
  end

  # tag::insert_1[]
  def insert(value)
    if @root.nil?
      @root = Node.new(nil, value)
      @root
      # end::insert_1[]
      # tag::insert_2[]
    else
      n = @root
      loop do
        if value < n.value
          if n.left.nil?
            n.left = Node.new(n, value)
            return n.left
          else
            n = n.left
          end
        else
          if n.right.nil?
            n.right = Node.new(n, value)
            return n.right
          else
            n = n.right
          end
        end
      end
    end
  end

  def size
    # TODO: size can be easily optimized to O(1), but don't have time to do it;)
    return 0 if @root.nil?

    @root.size
  end

  # end::insert_2[]
  # tag::find[]
  def find(value)
    n = @root
    until n.nil?
      if value == n.value
        return n
      elsif value < n.value
        n = n.left
      else
        n = n.right
      end
    end
    nil
  end

  # end::find[]
  # ruby style:
  def include?(value)
    find(value) != nil
  end

  def empty?
    @root.nil?
  end

  # tag::remove1[]
  def remove(value)
    n = find(value)
    remove_node(n) unless n.nil?
  end

  private def remove_node(n)
    if n.left.nil? && n.right.nil? # i.e. n is leaf
      if n == @root
        @root = nil
      else
        if n.parent.left == n
          n.parent.left = nil
        else
          n.parent.right = nil
        end
      end
      # end::remove1[]
      # tag::remove2[]
    elsif n.left.nil?
      if n.parent.nil?
        @root = n.right
        @root.parent = nil
      else
        if n.parent.left == n
          n.parent.left = n.right
        else
          n.parent.right = n.right
        end
        n.right.parent = n.parent unless n.right.nil?
      end
      # end::remove2[]
      # tag::remove3[]
    elsif n.right.nil?
      if n.parent.nil?
        @root = n.left
        @root.parent = nil
      else
        if n.parent.left == n
          n.parent.left = n.left
        else
          n.parent.right = n.left
        end
        n.left.parent = n.parent unless n.left.nil?
      end
      # end::remove3[]
    else
      # here we can get rightmost of left subtree
      # or leftmost of right subtree
      # tag::remove4[]
      rightmost = n.left
      rightmost = rightmost.right until rightmost.right.nil?

      if rightmost.parent == n
        rightmost.right = n.right
      else
        rightmost.parent.right = rightmost.left
        rightmost.left.parent = rightmost.parent unless rightmost.left.nil?
        rightmost.left = n.left
        rightmost.right = n.right
      end
      if n == @root
        @root = rightmost
      else
        if n.parent.left == n
          n.parent.left = rightmost
        else
          n.parent.right = rightmost
        end
      end

      rightmost.parent = n.parent
      rightmost.left.parent = rightmost unless rightmost.left.nil?
      rightmost.right.parent = rightmost unless rightmost.right.nil?
      # end::remove4[]
    end
  end

  def visit(n = @root, &block)
    return if n.nil?

    visit(n.left, &block)
    block.call(n.value)
    visit(n.right, &block)
  end

  def values
    values = []
    visit { |v| values << v }
    values
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  t = BinarySearchTree.new
  t.insert 0
  t.insert 5
  t.insert 15
  t.insert 20
  t.insert 2
  t.insert 7

  1000.times { t.insert(rand(20..1019)) }

  puts 'Testing include? (should be present):'
  puts "0: #{t.include?(0)}"
  puts "5: #{t.include?(5)}"
  puts "2: #{t.include?(2)}"
  puts "7: #{t.include?(7)}"

  puts 'Testing include? (should not be present):'
  puts "1: #{t.include?(1)}"
  puts "-1: #{t.include?(-1)}"
  puts "21: #{t.include?(21)}"

  puts 'Testing remove:'
  t.remove 5
  puts "removing 5. Still present? #{t.include? 5}"
  t.remove 20
  puts "removing 20. Still present? #{t.include? 20}"
  puts "removing 2. Still present? #{t.include? 2}"
  puts "removing 0. Still present? #{t.include? 0}"
end
# :nocov:
