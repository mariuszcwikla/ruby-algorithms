# In memory BTree
class BTree
  attr_reader :node_capacity, :size
  DEFAULT_NODE_CAPACITY=10
  class Node
    attr_accessor :values, :children, :parent
    def initialize(btree, parent: nil, leaf: true)
      @btree = btree
      @parent = parent
      @values = Array.new(btree.node_capacity)
      @children = if leaf then nil
                  else Array.new(btree.node_capacity + 1)
                  end
    end

    def size
      @values.count { |x| !x.nil? }
    end

    def empty?
      size.zero?
    end

    def full?
      size == @btree.node_capacity
    end

    def assert_size
      raise "num of values array is expected to be #{@btree.node_capacity} but was #{@values.size}" unless @values.size == @btree.node_capacity
      raise "num of children array is expected to be #{@btree.node_capacity + 1} but was #{@children.size}" unless @children.nil? || @children.size == @btree.node_capacity + 1
    end

    def insert_value_non_full(value)
      raise 'node is full' if full?

      idx = @values.rindex { |x| !x.nil? && x < value } || -1
      i = @btree.node_capacity - 2
      while i > idx
        @values[i+1] = @values[i]
        @children[i+2] = @children[i + 1] unless @children.nil?
        i -= 1
      end
      @values[idx + 1] = value
      idx + 1
    end

    def leaf?
      @children.nil?
    end

    def inspect
      values.join ','
    end

    alias to_s inspect

    def include?(value)
      idx = @values.index { |x| x.nil? || value <= x }
      if idx.nil?
        return false if leaf?

        return @children.last.include?(value)
      end
      return false if idx.nil?
      return true if @values[idx] == value
      return false if leaf?

      @children[idx].include? value
    end

    def add_and_split(value, right_child)
      raise "this is leaf. Can't add child" if !right_child.nil? && leaf?

      values = @values.dup
      idx = values.rindex { |x| !x.nil? && x < value } || -1
      values.insert(idx + 1, value)
      @values.fill(nil)

      mid = values.size / 2

      @values[0...mid] = values[0...mid]

      sibling = Node.new(@btree, parent: @parent, leaf: leaf?)
      sibling.values[0...mid] = values[(mid + 1)..-1]

      unless leaf?
        children = @children.dup
        children.insert(idx+2, right_child)
        @children.fill(nil)
        @children[0..mid] = children[0..mid]
        @children.each {|c| c.parent = self unless c.nil?}
        sibling.children[0..mid] = children[(mid+1)..-1]
        sibling.children.each {|c| c.parent = sibling unless c.nil?}
      end
      assert_size
      sibling.assert_size
      [sibling, values[mid]]
    end
  end

  def initialize(node_capacity: DEFAULT_NODE_CAPACITY)
    # raise 'node_capacity must be even and greater or equal 2' if node_capacity < 2 || node_capacity % 2 == 1
    @node_capacity = node_capacity
    @root = Node.new(self)
    @size = 0
  end

  def empty?
    @root.empty?
  end

  def find_leaf(value)
    n = @root
    loop do
      break if n.leaf?

      idx = n.values.rindex { |k| !k.nil? && k < value }
      nn = if idx.nil? then n.children[0]
           else n.children[idx + 1]
           end
      break if nn.nil?

      n = nn
    end
    n
  end

  def insert(value)
    n = find_leaf(value)
    sibling = nil
    left = nil
    loop do
      if n.nil?
        @root = Node.new(self, leaf: false)
        @root.values[0] = value
        @root.children[0] = left
        @root.children[1] = sibling
        left.parent = @root
        sibling.parent = @root
        break
      elsif n.full?
        sibling, value = n.add_and_split(value, sibling)
        left = n
        n = n.parent
      else
        idx = n.insert_value_non_full(value)
        unless sibling.nil?
          n.children[idx+1] = sibling
          sibling.parent = n
        end
        break
      end
    end

    @size += 1
  end

  def include?(value)
    @root.include? value
  end

  def dump
    @root.dump
  end

  def visit(node=@root, &block)
    block.call(node)
    node.children&.each do |c|
      next if c.nil?
      visit(c, &block)
    end
  end
end

# :nocov:
class BTree
  class Node
    def dump
      p @values
      unless leaf?
        @children.each_with_index do |c, i|
          next if c.nil?
          str = "child #{i} #{c.values.inspect}"
          str += " (*)" if c.leaf?
          puts str unless c.nil?
        end
      end
    end

    def graphviz_label
      @values.each_with_index.map do |v, i|
        s = "<f#{i}>"
        s += (v.to_s || '')
        s
      end.join '|'
    end
  end

  def to_graph
    require_relative '../graph/graphviz'
    g = Graph.new
    h = {}

    idx = 0
    visit(@root) do |n|
      h[n] = idx
      vn = g.vertex(idx)
      vn.label = n.graphviz_label
      vn.add_graphviz_attrib("shape", "record")
      idx += 1
    end

    visit(@root) do |n|
      n.children&.each_with_index do |c, i|
        next if c.nil?
        e = g.add_edge(h[n], h[c])
        if i == 0
        # if i == n.children.size - 1
          e.graphviz_source_field_id = "w"
        else
          e.graphviz_source_field_id = "f#{i-1}"
        end
      end
    end
    g
  end
  def display(**opts)
    to_graph.display(**opts)
  end
end

if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  t = BTree.new(node_capacity: 10)
  1.upto(100) do |i|
    t.insert i
  end
  t.display(debug: true)

end
# :nocov: