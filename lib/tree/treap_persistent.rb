#!/usr/bin/env ruby

require_relative 'base'

# Implementacja "Persystentnego Drzewca"
#
# Persystentny, czyli - podstawowe operaje nie modyfikują drzewa, tylko zwracają jego "nowe wersje"
# drzewa, tj. są immutable

# Dla uproszczenia jednak przyjąłem:
# 1. insert, remove - modyfikują korzeń drzewa
# 2. split/join - są w pełni persystentne
#
# Aby insert/remove były persystentne, musiałbym przepisać testy które mam dla innych drzew.
# A tak to je łatwo reużywam.
# BARDZO ŁATWO jednak przerobić strukturę na w pełni persystentną!

class PersistentTreap
  include TreeGraphvizSupport
  class Node
    attr_accessor :left, :right, :priority
    attr_reader :value

    def initialize(value, priority = rand)
      @value = value
      @left = nil
      @right = nil
      @priority = priority
    end

    def graphviz_label
      "#{value}, p=#{priority.round(2)}"
    end

    def inspect
      lval = @left&.value || ''
      rval = @right&.value || ''
      "Treap Node: #{@value} left=#{lval} right=#{rval} priority=#{@priority}"
    end

    #      10                   10        15
    #     /  \    split(13)    /  \         \
    #    5    15      ->      5   12         20
    #   / \   / \            / \
    #  0  7  12  20         0   7
    #
    #   10
    #     \        split (13)    10    15
    #      15
    def split(key)
      n = Node.new(@value, @priority)
      if @value < key
        n.left = @left
        n.right, b = @right.split(key) unless @right.nil?
        [n, b]
      else
        n.right = @right
        b, n.left = @left.split(key) unless @left.nil?
        [b, n]
      end
    end

    #   10(0.6)   15(0.2)   -> 10
    #                           \
    #                            15
    #
    #   10(0.6)   15(0.8)   -> 15
    #                          /
    #                         10
    #
    #   10(0.9)       20(0.8)
    #   / \           /      \
    #  x   15(0.6)   18(0.7)  y
    #
    #   10        10
    #               \
    #                20
    #   10
    #     \
    #      20
    #      /
    #     18
    #    /
    #   15
    def join(other)
      return self if other.nil?

      if @priority > other.priority
        node = Node.new(@value, @priority)
        node.left = @left
        node.right =
          if @right.nil?
            other
          else
            right.join(other)
          end
      else
        node = Node.new(other.value, other.priority)
        node.right = other.right
        node.left =
          if other.left.nil?
            self
          else
            join(other.left)
          end
      end
      node
    end

    def remove(key)
      if @value == key
        return @right if @left.nil?
        return @left if @right.nil?

        @left.join(@right)
      elsif @value < key
        @right = @right.nil? ? nil : @right.remove(key)
        self
      else
        @left = @left.nil? ? nil : @left.remove(key)
        self
      end
    end

    #     6        7                     T2-left   T2-right
    #    /  \     /   \  T2.split(5)       3         7
    #   2   12   3    15 ->               / \         \
    #  / \      / \                      1   5         15
    # 0   4    1   5
    #
    # next iteration:
    #     6
    #    / \
    #   2   ...     3     split(2)   1   3
    #  / \         / \      ->            \
    # 0  4        1   5                    5
    #
    #     6
    #    / \
    #   2
    #  / \
    # 0   4      1

    def union(other)
      return self if other.nil?

      a, b = @priority > other.priority ? [self, other] : [other, self]

      left, right = b.split(a.value)

      node = Node.new(a.value, a.priority)
      node.left = a.left.nil? ? left : a.left.union(left)
      node.right = a.right.nil? ? right : a.right.union(right)
      node
    end

    def size
      s = 1
      s += @left.size unless @left.nil?
      s += @right.size unless @right.nil?
      s
    end
  end

  attr_reader :root

  def initialize(root = nil)
    @root = root
  end

  def size
    # TODO: size can be easily optimized to O(1), but don't have time to do it;)
    return 0 if @root.nil?

    @root.size
  end

  def empty?
    @root.nil?
  end

  def split(k)
    if @root.nil?
      [PersistentTreap.new, PersistentTreap.new]
    else
      a, b = @root.split(k)
      [PersistentTreap.new(a), PersistentTreap.new(b)]
    end
  end

  def join(other)
    if @root.nil?
      other
    else
      node = @root.join(other.root)
      PersistentTreap.new(node)
    end
  end

  def insert(value, priority = rand)
    node = Node.new(value, priority)
    if @root.nil?
      @root = node
    else
      a, b = @root.split(value)
      x =
        if a.nil?
          node
        else
          a.join(node)
        end

      x = x.join(b)
      @root = x
    end
    node
  end

  def find(value)
    n = @root
    until n.nil?
      return n if value == n.value

      n =
        if value < n.value
          n.left
        else
          n.right
        end
    end
    nil
  end

  def find_last
    n = @root
    n = n.right until n.right.nil?
    n
  end

  def find_first
    n = @root
    n = n.left until n.left.nil?
    n
  end

  def include?(value)
    find(value) != nil
  end

  def remove(value)
    return self if @root.nil?

    @root = @root.remove(value)
  end

  def union(other)
    return other if @root.nil?
    return self if other.root.nil?

    root = @root.union(other.root)
    PersistentTreap.new(root)
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  if ARVG[0] == '--big'
    big = PersistentTreap.new
    100.times { big.insert(rand(1_000_000)) }
    big.display
  else
    t = PersistentTreap.new
    t.insert(5)
    t.insert(0)
    puts 'after insert 5, 0: '
    t.dump
    # t.insert(10)

    t.insert(-5)
    puts 'after insert -5: '
    t.dump
    # t.insert(2)
    t.insert(-10)
    puts 'after insert -10: '
    t.dump
    t.insert(-20)
    t.insert(-30)

    puts "contains (5): #{t.include?(5)}"
    puts "contains (-30): #{t.include?(-30)}"
    puts "contains (3): #{t.include?(3)}"
    t.display

    t.remove(-30)
    t.display
    t.remove(-20)
    t.dump
    t.display
    t.remove(5)
    t.display
    t.dump
  end
end
# :nocov:
