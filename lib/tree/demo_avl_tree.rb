require_relative 'avltree'

if __FILE__ == $PROGRAM_NAME
  t = AVLTree.new
  t.insert(5)
  t.insert(0)
  puts 'after insert 5, 0: '
  t.dump
  # t.insert(10)

  t.insert(-5)
  puts 'after insert -5: '
  t.dump
  # t.insert(2)
  t.insert(-10)
  puts 'after insert -10: '
  t.dump
  t.insert(-20)
  t.insert(-30)

  puts "contains (5): #{t.include?(5)}"
  puts "contains (-30): #{t.include?(-30)}"
  puts "contains (3): #{t.include?(3)}"
  # t.display

  t = AVLTree.new
  0.upto(20) { |i| t.insert(i) }
  t.display

  puts 'Insert 1000 elements'
  t = AVLTree.new
  0.upto(1000) { |i| t.insert(i) }

  t.display

  puts 'Insert and remove random 200 elements'
  0.upto(200) do |i|
    if i.even?
      t.insert(rand(1000))
    else
      t.remove(rand(1000))
    end
  end
  t.display
end
