require_relative '../math/euclid'

class KDTree
  include Enumerable
  class Node
    attr_reader :point, :parent, :level
    attr_accessor :left, :right
    def initialize(point, parent = nil)
      @point = point
      @left = nil
      @right = nil
      @parent = parent
      @level = if parent.nil?
                0
              else
                parent.level + 1
              end
    end

    def compare(point, dimensions)
      @point[@level % dimensions] <=> point[@level % dimensions]
    end

    def inspect
      '(' + (@point.join ', ') + ')'
    end
    alias to_s inspect

    def height
      n = self
      h = 0
      until n.nil?
        h += 1
        n = n.parent
      end
      h
    end

    def axis_value(p, dimensions)
      p[@level % dimensions]
    end

    def crosses_left(p, sq_distance, dimensions)
      return false if left.nil?

      return true if (axis_value(point, dimensions) - axis_value(p, dimensions))**2 <= sq_distance

      false
    end

    def crosses_right(p, sq_distance, dimensions)
      return false if right.nil?

      return true if (axis_value(point, dimensions) - axis_value(p, dimensions))**2 <= sq_distance

      false
    end

  end
  def initialize(dimensions = 2)
    @dimensions = dimensions
    @root = nil
  end

  def insert(point)
    raise 'not enough dimensions' if point.size < @dimensions
    if @root.nil?
      @root = Node.new(point)
    else
      n = @root
      while true
        if n.compare(point, @dimensions) > 0
          if n.left.nil?
            n.left = Node.new(point, n)
            return n.left
          else
            n = n.left
          end
        else
          if n.right.nil?
            n.right = Node.new(point, n)
            return n.right
          else
            n = n.right
          end
        end
      end
    end
  end

  alias << insert

  def each(n = @root, &block)
    return if n.nil?
    each(n.left, &block)
    block.call(n)
    each(n.right, &block)
  end

  def height
    h = 0
    each { |n| h = [h, n.height].max }
    h
  end

  def nearest_neighbour_recursive(p)
    raise "not implemented"

    # TODO: implement as per http://andrewd.ces.clemson.edu/courses/cpsc805/references/nearest_search.pdf
    # page 15
  end

  def nearest_neighbour(p, debug: false)
    return nil if @root.nil?
    stack = [@root]
    n = @root
    @current = @root.point
    @current_distance = squared_distance(p, @root.point)
    iterations = 0
    until stack.empty?
      n = stack.pop
      until n.nil?
        iterations += 1
        if (dist = squared_distance(n.point, p)) < @current_distance
          @current = n.point
          @current_distance = dist
        end
        if n.compare(p, @dimensions) > 0
          stack.push n.right if n.crosses_right(p, @current_distance, @dimensions)
          n = n.left
        else
          stack.push n.left if n.crosses_left(p, @current_distance, @dimensions)
          n = n.right
        end
      end
    end
    puts "number of iterations: #{iterations}" if debug
    @current
  end

  def self.build(points, k: 2)
    t = KDTree.new(k)
    points.each { |p| t << p}
    t
  end
end

class KDTree
  def plot(width=500, height = 500, query: [], filename: 'kd.png', maxx: nil, maxy: nil)
    require 'rmagick'

    raise 'supported only for dimension=2' unless @dimensions == 2

    png = Magick::Image.new(width, height) { self.background_color = "white" }
    return png if @root.nil?

    if maxx.nil?
      maxx, maxy = @root.point[0], @root.point[1]
      each do |n|
        maxx = [maxx, n.point[0]].max
        maxy = [maxy, n.point[1]].max
      end
    end

    # TODO: move to new class
    @draw = Magick::Draw.new
    @maxx = maxx
    @maxy = maxy
    @canvas_width = width
    @canvas_height = height
    @maxx += 5
    @maxy += 5
    build_image(@root, 0, 0, @maxx, @maxy)
    @draw.fill('red')
    @draw.stroke('red')
    query.each do |q|
      @draw.ellipse(
        *scale(*q),
        2, 2, 0, 360)
      @draw.stroke('blue')
      nn = nearest_neighbour_brute(to_a.map{|n| n.point}, q)
      @draw.line(*scale(*nn), *scale(*q))

      @draw.stroke('red')
      nn = nearest_neighbour(q, debug: true)
      @draw.line(*scale(*nn), *scale(*q))
    end
    @draw.draw(png)
    png.write(filename)
  end

  def scale(x, y)
    [
      x * @canvas_width / @maxx,
      y * @canvas_height / @maxy
    ]
  end

  def build_image(n, left, top, right, bottom)
    @draw.ellipse(
      *scale(*n.point),
      2, 2, 0, 360)
    # puts "#{left} - #{right}, #{n.point[0]}"
    if n.level % 2 == 1
      #horizontal
      line = [
        *scale(left, n.point[1]),
        *scale(right, n.point[1])
      ]
      @draw.line(*line)
      build_image(n.right, left, n.point[1], right, bottom) unless n.right.nil?
      build_image(n.left, left, top, right, n.point[1]) unless n.left.nil?
    else
      line = [
        *scale(n.point[0], top),
        *scale(n.point[0], bottom)
      ]
      @draw.line(*line)
      build_image(n.left, left, top, n.point[0], bottom) unless n.left.nil?
      build_image(n.right, n.point[0], top, right, bottom) unless n.right.nil?
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  t = KDTree.new
  # t << [1, 2]
  # t << [4, 5]
  # t << [3, 0]
  # t << [5, 2]
  # t << [2, 6]
  animation = false
  if ARGV[0] == '-a'
    animation = true
  end
  step = 0
  points = []
  50.times do points << [rand * 100.0, rand * 100.0] end
  maxx = points.map { |p| p[0] }.max
  maxy = points.map { |p| p[1] }.max
  Dir.mkdir('tmp') unless File.exist?('tmp') if animation
  points.each do |p|
    t << p
    if animation
      t.plot(400, 400, query: [], filename: format("tmp/kd-%05d.png", step), maxx: maxx, maxy: maxy)
      puts "step #{step}"
    end
    step += 1
  end
  puts "Height of KD-Tree #{t.height}"
  q = 50.times.map{[rand * 100.0, rand * 100.0]}
  t.plot(1000, 800, query: [], filename: 'kd.png')
  t.plot(1000, 800, query: q, filename: 'kd-nn.png')

  if animation
    puts 'converting to animaged gif'
    `convert -delay 10 tmp/kd-*.png kdtree.gif`
  end
end
# :nocov: