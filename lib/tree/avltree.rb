require_relative 'base'

class AVLTree
  include TreeGraphvizSupport
  class Node
    attr_accessor :parent, :left, :right, :height
    attr_reader :value

    def initialize(value, parent = nil)
      @value = value
      @height = 0
      @parent = parent
      @left = nil
      @right = nil
    end

    def graphviz_label
      "#{value}, h=#{height}"
    end

    def inspect
      lval = @left.nil? ? '' : @left.value
      rval = @right.nil? ? '' : @right.value
      pval = @parent.nil? ? '' : @parent.value
      "AVLNode: #{@value} left=#{lval} right=#{rval} parent=#{pval}, height=#{@height}"
    end

    alias to_s inspect

    def recalc_height
      lh = @left.nil? ? -1 : @left.height
      rh = @right.nil? ? -1 : @right.height
      h = @height
      @height = [lh, rh].max + 1
      h != height
    end

    def verifyLinkInvariants
      raise 'self==parent' if self == @parent
      if !@parent.nil? && !((@parent.left == self) || (@parent.right == self))
        raise 'self is not child of parent'
      end
      raise 'left.parent is not self' if !@left.nil? && (@left.parent != self)
      if !@right.nil? && (@right.parent != self)
        raise 'right.parent is not self'
      end
    end

    def rotate_left
      verifyLinkInvariants

      r = @right
      unless @parent.nil?
        if @parent.left == self
          @parent.left = r
        else
          @parent.right = r
        end
      end
      @right = r.left
      @right.parent = self unless @right.nil?
      r.left = self

      r.parent = @parent
      @parent = r

      recalc_height
      r.recalc_height

      raise 'cyclic dependency' if r == r.parent

      r
    end

    #
    #         p       p
    #         |       |
    #         self    l
    #        /       /\
    #       l       c  self
    #      / \         /
    #     c   lr       lr
    #     does not work  when fixup-rotate_right
    #
    #       self
    #      /
    #     l
    #     \
    #      lr
    #
    #
    #     when removing x on
    #
    #     self
    #     /  \
    #    l   x
    #    \
    #    lr
    def rotate_right
      verifyLinkInvariants

      # TODO: tutaj trzeba rotacje left-right i right-left

      l = @left

      raise 'cyclic dependency' if l == @parent

      unless @parent.nil?
        if @parent.left == self
          @parent.left = l
        else
          @parent.right = l
        end
      end

      #      if l.left==nil
      @left = l.right
      @left.parent = self unless @left.nil?
      l.right = self
      l.parent = @parent
      @parent = l

      recalc_height
      l.recalc_height

      # raise 'cyclic dependency' if l==l.parent
      raise 'cyclic dependency' if l == l.parent

      l
    end

    def size
      s = 1
      s += @left.size unless @left.nil?
      s += @right.size unless @right.nil?
      s
    end
  end

  attr_reader :root

  def initialize
    @root = nil
  end

  def size
    # TODO: size can be easily optimized to O(1), but don't have time to do it;)
    return 0 if @root.nil?

    @root.size
  end

  def empty?
    @root.nil?
  end

  def insert(value)
    if @root.nil?
      @root = Node.new(value)
      @root
    else
      n = @root
      created = nil
      while created.nil?
        if value < n.value
          if n.left.nil?
            n.left = Node.new(value, n)
            n.left.parent = n
            created = n.left
          else
            n = n.left
          end
        else
          if n.right.nil?
            n.right = Node.new(value, n)
            n.right.parent = n
            created = n.right
          else
            n = n.right
          end
        end
      end

      # fixup
      created.verifyLinkInvariants
      #      created.parent&.verifyLinkInvariants
      fixup(created.parent)

      created.verifyLinkInvariants
      created
    end
  end

  def height(n)
    return -1 if n.nil?

    n.height
  end

  def fixup(n)
    until n.nil?
      n.verifyLinkInvariants
      hl = n.left.nil? ? -1 : n.left.height # replace with height(n.left)
      hr = n.right.nil? ? -1 : n.right.height # replace with height(n.right)

      if hl + 2 == hr
        if height(n.right.left) > height(n.right.right)
          n = n.right.rotate_right.parent
        end
        n = n.rotate_left
        @root = n if n.parent.nil?
        n = n.parent
      elsif hl == hr + 2
        if height(n.left.right) > height(n.left.left)
          n = n.left.rotate_left.parent
        end
        n = n.rotate_right
        @root = n if n.parent.nil?
        n = n.parent
      else
        @root = n if n.parent.nil?
        changed = n.recalc_height
        break unless changed

        n = n.parent
      end
    end
  end

  def find_node(value)
    n = @root
    until n.nil?
      return n if n.value == value

      n =
        if value < n.value
          n.left
        else
          n.right
        end
    end
    nil
  end

  alias find find_node

  def search_node(value)
    n = @root
    until n.nil?
      return n if n.value == value

      n =
        if value < n.value
          return n if n.left.nil?

          n.left
        else
          return n if n.right.nil?

          n.right
        end
    end
  end

  def prev(value)
    n = search_node(value)
    return nil if n.nil?

    return n.value if value > n.value

    prev_node(n)&.value

  end

  def prev_node(n)
    if n.left.nil?
      n = n.parent until n.nil? || n&.parent&.right == n
      n&.parent
    else
      n = n.left
      n = n.right until n.right.nil?
      n
    end
  end

  def not_greater(value)
    n = search_node(value)
    return nil if n.nil?
    return n.value if n.value <= value

    prev_node(n)&.value
  end

  def next(value)
    n = search_node(value)
    return nil if n.nil?

    return n.value if value < n.value

    if n.right.nil?
      n = n.parent until n.nil? || n&.parent&.left == n
      return n&.parent&.value
    else
      n = n.right
      n = n.left until n.left.nil?
      return n.value
    end

    nil
  end

  def include?(value)
    find_node(value) != nil
  end

  def remove(value)
    n = find_node(value)
    return if n.nil?

    if n.left.nil? && n.right.nil?
      if n == @root
        @root = nil
        return
      else
        if n.parent.left == n
          n.parent.left = nil
        else
          n.parent.right = nil
        end
      end
      fixup(n.parent)
    elsif n.left.nil?
      if n.parent.nil?
        @root = n.right
        @root.parent = nil
        @root.recalc_height
      else
        if n.parent.left == n
          n.parent.left = n.right
        else
          n.parent.right = n.right
        end
        n.right.parent = n.parent unless n.right.nil?
        fixup(n.parent)
      end
    elsif n.right.nil?
      if n.parent.nil?
        @root = n.left
        @root.parent = nil
        @root.recalc_height
      else
        if n.parent.left == n
          n.parent.left = n.left
        else
          n.parent.right = n.left
        end
        n.left.parent = n.parent unless n.left.nil?
        fixup(n.parent)
      end
    else
      n.verifyLinkInvariants
      rightmost = n.left
      rightmost = rightmost.right until rightmost.right.nil?
      rightmost.verifyLinkInvariants

      if rightmost.parent != n
        rightmost.parent.right = rightmost.left
        rightmost.left = n.left
        rightmost.left.parent = rightmost unless rightmost.left.nil?
      else
        n.left = nil
      end

      if n.parent.nil?
        @root = rightmost
      else
        if n.parent.left == n
          n.parent.left = rightmost
        else
          n.parent.right = rightmost
        end
      end
      rightmost.height = n.height
      rightmost.parent = n.parent
      rightmost.right = n.right
      rightmost.right.parent = rightmost unless rightmost.right.nil?
      rightmost.verifyLinkInvariants

      fixup(rightmost)
    end
  end

  def visit(n = @root, &block)
    return if n.nil?

    visit(n.left, &block)
    block.call(n)
    visit(n.right, &block)
  end

  def values
    values = []
    visit { |n| values << n.value}
    values
  end
end

class AVLTreeMap
  class Entry
    include Comparable
    attr_reader :key, :value

    def initialize(key, value)
      @key = key
      @value = value
    end

    def <=>(other)
      @key <=> other.key
    end

    def to_s
      "Entry #{@key}->#{value}"
    end
  end

  def initialize
    @tree = AVLTree.new
  end

  def put(key, value)
    @tree.insert Entry.new(key, value)
  end
  alias []= put

  def get(key)
    @tree.find(Entry.new(key, nil))&.value&.value
  end

  alias [] get

  def get_entry(key)
    @tree.find(Entry.new(key, nil))&.value
  end

  def prev_entry(key)
    @tree.prev(Entry.new(key, nil))
  end

  def not_greater_entry(key)
    @tree.not_greater(Entry.new(key, nil))
  end

  def prev_key(key)
    prev_entry(key)&.key
  end

  def next_entry(key)
    @tree.next(Entry.new(key, nil))
  end

  def next_key(key)
    next_entry(key)&.key
  end

  def keys
    @tree.values.map{|e| e.key}
  end

  def values
    @tree.values.map{|e| e.value}
  end
end

if __FILE__ == $PROGRAM_NAME
  t = AVLTree.new
  t.insert(5)
  t.insert(0)
  puts 'after insert 5, 0: '
  t.dump
  # t.insert(10)

  t.insert(-5)
  puts 'after insert -5: '
  t.dump
  # t.insert(2)
  t.insert(-10)
  puts 'after insert -10: '
  t.dump
  t.insert(-20)
  t.insert(-30)

  puts "contains (5): #{t.include?(5)}"
  puts "contains (-30): #{t.include?(-30)}"
  puts "contains (3): #{t.include?(3)}"
  t.display

  t.remove(-30)
  t.display
  t.remove(-20)
  t.dump
  t.display
  t.remove(5)
  t.display
  t.dump

  m = AVLTreeMap.new
  m[4]=3
  puts "m[4]=#{m[4]}"
end
