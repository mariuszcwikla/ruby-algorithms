require_relative '../graph/graphviz'

module TreeGraphvizSupport
  # OLD IMPLEMENTATION
  # def to_graph2
  #     g = Graph.new
  #     h = {}

  #     idx=0
  #     visit(@root){|n|
  #         h[n]=idx
  #         vn = g.vertex(idx)
  #         vn.label=n.graphviz_label

  #         if n.parent!=nil
  #           vp = g.vertex(h[n.parent], false)
  #           g.add_edge(vp, vn)
  #         end
  #         idx+=1
  #     }
  #     return g
  #   end

  def to_graph
    g = Graph.new
    h = {}

    idx = 0
    visit(@root) do |n|
      h[n] = idx
      vn = g.vertex(idx)
      vn.label = n.graphviz_label
      idx += 1
    end

    visit(@root) do |n|
      vn = g.vertex(h[n])

      if !n.left.nil?
        vl = g.vertex(h[n.left], false)
        g.add_edge(vn, vl)
      else
        nn = g.vertex(idx += 1)
        nn.label = 'nil'
        g.add_edge(vn, nn)
      end
      if !n.right.nil?
        vr = g.vertex(h[n.right], false)
        g.add_edge(vn, vr)
      else
        nn = g.vertex(idx += 1)
        nn.label = 'nil'
        g.add_edge(vn, nn)
      end
    end
    g
  end

  def display
    to_graph.display_digraph
  end

  def dump
    visit(@root) { |n| puts n.inspect }
  end

  def visit(n = @root, &block)
    return if n.nil?

    block.call(n)

    idx = visit(n.left, &block) unless n.left.nil?
    idx = visit(n.right, &block) unless n.right.nil?
    idx
  end

  def visit_inorder(n = @root, &block)
    return if n.nil?

    idx = visit_inorder(n.left, &block) unless n.left.nil?
    block.call(n)
    idx = visit_inorder(n.right, &block) unless n.right.nil?
    idx
  end

  def elements
    r = []
    visit_inorder(@root) { |e| r << e.value }
    r
  end
end
