# Radix Tree.

class RadixTree
  class Node
    attr_reader :value, :children, :terminal

    def initialize(value, children: {}, terminal: true)
      @value = value
      @terminal = terminal
      @children = children
    end

    def include?(string)
      # for convenience, "string" must already have prefix removed.
      # i.e. if word to search is "banana"
      # and we enter node "b" that has child "ad" "an"
      # then string="anana"
      # initial removal is done at RadixTree.include?

      if @terminal && string.empty?
        true
      else
        c = @children[string[0]]
        return false if c.nil?

        c.include?(string[c.value.length..-1])
      end
    end

    def longest_common_prefix(a, b)
      n = [a.length, b.length].min
      n.times do |i|
        return i if a[i] != b[i]
      end
      n
    end

    def split(value)
      i = 0
      i += 1 while i < value.length && i < @value.length && value[i] == @value[i]

      c1 = Node.new(@value[i..-1], children: @children, terminal: @terminal)
      c2 = Node.new(value[i..-1], terminal: true)
      @children = { c1.value[0] => c1, c2.value[0] => c2 }
      @value = @value[0...i]
      @terminal = false
    end

    def insert(value)
      i = longest_common_prefix(value, @value)
      if i == @value.length
        if i == value.length
          @terminal = true
        else
          sub = value[@value.length..-1]
          c = @children[sub[0]]
          if c.nil?
            @children[sub[0]] = Node.new(sub)
          else
            c.insert(sub)
          end
        end
      else
        split(value)
      end
    end
  end

  def initialize
    @root = nil
  end

  def insert(string)
    if @root.nil?
      @root = Node.new(string, terminal: true)
    else
      @root.insert(string)
    end
  end

  def include?(string)
    return false if @root.nil?

    return false unless string.start_with? @root.value

    @root.include?(string[@root.value.length..-1])
  end

  def dump(n = @root, level = 0)
    return if n.nil?

    val = n.value
    val = '.' if val.empty?
    s = ('  ' * level) + val
    s << '^' if n.terminal
    puts '' if level == 0
    puts s
    n.children.values.each { |c| dump(c, level + 1) }
  end

  alias << insert
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  t = RadixTree.new
  t << 'foo'
  t << 'banana'
  t << 'bad'
  t << 'broda'
  t << 'bombada'
  t << 'bramka'

  t.dump

  puts 'Switching to interactive mode. Empty Radix tree created. Type some strings. Use empty line to create empty tree.'
  t = RadixTree.new
  loop do
    print '> '
    line = gets
    if line.strip.empty?
      t = RadixTree.new
      puts 'Empty tree created...'
    else
      t << line.strip
      t.dump
    end
  end
end
# :nocov:
