# Node-based segment tree implementation.
# Could be optimized by storing tree as array(s)
class SegmentTree
  attr_reader :iterations, :function
  def initialize(arr, function)
    @function = function
    @root = build(arr, 0, arr.length)
  end

  class Node
    attr_reader :left_idx, :right_idx, :value, :left, :right
    def initialize(value, left_idx, right_idx, left=nil, right=nil)
      @value = value
      @left = left
      @right = right
      @left_idx = left_idx
      @right_idx = right_idx
    end
  end

  def query(a, b, n = @root)
    return nil if a==b
    @iterations = 0 if n == @root
    @iterations += 1
    if a < n.left_idx or b > n.right_idx
      raise "out of bounds (#{a}, #{b}), node=(#{n.left_idx}, #{n.right_idx})"
    end
    if a == n.left_idx and b == n.right_idx
      n.value
    else
      if b <= n.left.right_idx
        query(a, b, n.left)
      elsif a >= n.right.left_idx
        query(a, b, n.right)
      else
        # l..a..b..r
        # -> 
        #  l..a..m..b..r
        #  query (a, m, left)
        #  query (m, b, right)
        #m = (a + b) / 2
        vl = query(a, n.left.right_idx, n.left)
        vr = query(n.right.left_idx, b, n.right)
        @function.call(vl, vr)
      end
    end
  end

  private def build(array, a, b)
    if b - a == 1
      Node.new(array[a], a, b)
    else
      mid = (a + b) / 2
      left = build(array, a, mid)
      right = build(array, mid, b)
      Node.new(@function.call(left.value, right.value), a, b, left, right)
    end
  end
end


# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  require 'colorize'
  arr = 100.times.map{ rand(100) }
  func = ->(a, b) {a+b}
  func = ->(a, b) {[a,b].min}
  func = ->(a, b) {[a,b].max}
  func = ->(a, b) {a*b}
  func = ->(a, b) {a^b}
  t = SegmentTree.new(arr, func)

  def brute(arr, a, b, func)
    arr[a..(b-1)].reduce(&func)
  end

  def test(arr, tree, a, b)
    c = brute(arr, a, b, tree.function)
    d = tree.query(a, b)
    puts "Testing #{a}..#{b}, brute=#{c}, segment tree=#{d}, num of iterations=#{tree.iterations} #{c!=d ? 'NOT OK'.red : '' }"
  end

  # for i in 0...(arr.length) do 
  #   for j in (i+1)...(arr.length) do
  #     test(arr, t, i, j)
  #   end
  # end

  100.times do
    a = rand(arr.size)
    b = rand(arr.size)
    next if a == b
    a, b = b, a if b < a
    #t.query(a, b)
    #puts t.iterations
    test(arr, t, a, b)
  end
end
# :nocov: