require_relative '../math/euclid'
require_relative '../selection/lomuto'
class BallTree
  include Enumerable
  attr_reader :left, :right, :points, :centroid, :radius
  def initialize(points: nil, left: nil, right: nil, centroid:, radius:)
    # internal nodes contain left + right, and points=nil
    # leaf: points nonempty, left=right=nil
    @points = points
    @left = left
    @right = right
    @centroid = centroid
    @radius = radius
  end

  def leaf?
    !@points.nil?
  end

  def height
    return 1 if leaf?
    return 1 + [@left.height, @right.height].max
  end

  def self.build(points, k: 2, ball_size: 3, use_lomuto_quickselect: false)
    return BallTree.new(points: points, centroid: [0, 0], radius: 0) if points.size == 0
    maxspread = nil
    maxdim = nil
    k.times do |i|
      # find max spread
      a, b = points.map {|p| p[i]}.minmax
      spread = (a-b).abs
      if maxspread.nil? or spread > maxspread
        maxspread = spread
        maxdim = i
      end
    end

    c = centroid(points, k)
    r = radius(points, c)
  
    return BallTree.new(points: points, centroid: c, radius: r) if points.size <= ball_size
    
    if use_lomuto_quickselect
      # Trying to replace sort (O(n log n)) with quickselect (O(n)) but
      # I didn't observe any improvements
      # probably because sort is implemented in C but quickselect in ruby
      center = quickselect_lomuto(points, points.size/2){ |a, b| a[maxdim] <=> b[maxdim] }
      left = points[0...points.size/2]
      right = points[points.size/2..-1]
    else
      points.sort_by! {|p| p[maxdim]}
      left = points[0...points.size/2]
      right = points[points.size/2..-1]
    end


    BallTree.new(
      left: build(left, k: k, ball_size: ball_size),
      right: build(right, k: k, ball_size: ball_size),
      centroid: c,
      radius: r
    )
  end

  def nearest_neighbour(point, debug: false)
    if leaf?
      return nearest_neighbour_brute(@points, point)
    end
    stack = []

    steps = 0

    nearest = nil
    sq_distance = nil

    n = self
    until n.leaf?
      a = squared_distance(n.left.centroid, point)
      b = squared_distance(n.right.centroid, point)
      n = if a < b
            n.left
          else
            n.right
          end
      steps += 1
    end

    nearest = nearest_neighbour_brute(n.points, point)
    sq_distance = squared_distance(nearest, point)

    stack.push self
    until stack.empty?
      n = stack.pop
      steps += 1
      if n.leaf?
        nearest2 = nearest_neighbour_brute(n.points, point)
        sq_distance2 = squared_distance(nearest2, point)
        if nearest.nil? || sq_distance2 < sq_distance
          nearest = nearest2
          sq_distance = sq_distance2
        end
        next
      end
      a = Math.sqrt squared_distance(n.left.centroid, point)
      b = Math.sqrt squared_distance(n.right.centroid, point)
      if a < b
        stack.push n.left if a < (n.left.radius + Math.sqrt(sq_distance))
        stack.push n.right if b < (n.right.radius + Math.sqrt(sq_distance))
      else
        stack.push n.right if b < (n.right.radius + Math.sqrt(sq_distance))
        stack.push n.left if a < (n.left.radius + Math.sqrt(sq_distance))
      end
    end
    puts "number of steps: #{steps}" if debug
    nearest
  end

  def each(&block)
    @left.each(&block) unless leaf?
    block.call(self)
    @right.each(&block) unless leaf?
  end
end

# :nocov:
class BallTreeRenderer
  def scale(x, y)
    [
      x * @canvas_width / @maxx,
      y * @canvas_height / @maxy
    ]
  end

  attr_reader :draw

  def plot(tree, width=500, height = 500, query: [], filename: 'balltree.png', maxx: 100, maxy: 100, debug: false)
    require 'rmagick'

    png = Magick::Image.new(width, height) { self.background_color = "white" }

    draw = Magick::Draw.new
    @maxx = maxx
    @maxy = maxy
    @canvas_width = width
    @canvas_height = height

    plot_recursive(tree, draw, 'black')
    points = tree.to_a.filter{|n| n.leaf?}.map{|n| n.points}.flatten(1)
    query.each do |q|
      draw.ellipse(
        *scale(*q),
        2, 2, 0, 360)
      draw.stroke('blue')
      nn = nearest_neighbour_brute(points, q)
      draw.line(*scale(*nn), *scale(*q))

      draw.stroke('red')
      nn = tree.nearest_neighbour(q, debug: debug)
      draw.line(*scale(*nn), *scale(*q))
    end

    draw.draw(png)
    png.write(filename)
  end

  private
  def plot_recursive(tree, draw, ball_color)
    draw.fill(ball_color)
    draw.stroke(ball_color)
    draw.ellipse(*scale(*tree.centroid), 4, 4, 0, 360)
    draw.fill('transparent')
    draw.ellipse(*scale(*tree.centroid), *scale(tree.radius, tree.radius), 0, 360)

    if tree.leaf?

      # draw.fill('red')
      # draw.stroke('red')
      draw.fill(ball_color)
      draw.stroke(ball_color)

      tree.points.each do |p|
        draw.ellipse(*scale(*p), 2, 2, 0, 360)
      end
    else
      plot_recursive(tree.left, draw, 'red')
      plot_recursive(tree.right, draw, 'blue')
    end
  end
end
# :nocov:

# :nocov:
if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  num_of_points, num_of_queries = if ARGV.size >= 2
                                    [ARGV[0].to_i, ARGV[1].to_i]
                                  else
                                    [1000, 40]
                                  end
  threshold = if ARGV.size >= 3
                ARGV[2].to_i
              else
                10
              end
  arr = []
  num_of_points.times do arr << [rand * 60.0 + 20, rand * 60.0 + 40] end
  #1000.times do arr << [rand * 100.0, rand * 100.0] end

  t = BallTree.build(arr, ball_size: threshold)
  puts "Height of the tree #{t.height}"
#t.to_a.map{|n| puts n.points.join ','}
  q = num_of_queries.times.map{[rand * 100.0, rand * 100.0]}
  BallTreeRenderer.new.plot(t, filename: 'balltree.png')
  BallTreeRenderer.new.plot(t, query: q, filename: 'balltree-nn.png', debug: true)
end
# :nocov: