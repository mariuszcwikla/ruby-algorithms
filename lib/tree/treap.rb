#!/usr/bin/env ruby

require_relative '../../graph/graphviz'
require_relative 'base'

class Treap
  include TreeGraphvizSupport
  class Node
    attr_accessor :parent, :left, :right, :priority
    attr_reader :value

    def initialize(value, priority = rand)
      @value = value
      @parent = parent
      @left = nil
      @right = nil
      @priority = priority
    end

    def graphviz_label
      "#{value}, p=#{priority.round(2)}"
    end

    def inspect
      lval = @left&.value || ''
      rval = @right&.value || ''
      pval = @parent&.value || ''
      # lval = @left==nil ? "" : @left.value
      # rval = @right==nil ? "" : @right.value
      # pval = @parent==nil ? "" : @parent.value
      "Treap Node: #{@value} left=#{lval} right=#{rval} parent=#{pval}, priority=#{@priority}"
    end

    def rotate_left
      r = @right
      unless @parent.nil?
        if @parent.left == self
          @parent.left = r
        else
          @parent.right = r
        end
      end
      @right = r.left
      @right.parent = self unless @right.nil?
      r.left = self

      r.parent = @parent
      @parent = r

      raise 'cyclic dependency' if r == r.parent

      r
    end

    #         p       p
    #         |       |
    #         self    l
    #        /       /\
    #       l       c  self
    #      / \         /
    #     c   lr       lr
    def rotate_right
      l = @left
      raise 'cyclic dependency' if l == @parent

      unless @parent.nil?
        if @parent.left == self
          @parent.left = l
        else
          @parent.right = l
        end
      end

      @left = l.right
      @left.parent = self unless @left.nil?
      l.right = self
      l.parent = @parent
      @parent = l

      raise 'cyclic dependency' if l == l.parent

      l
    end

    def size
      s = 1
      s += @left.size unless @left.nil?
      s += @right.size unless @right.nil?
      s
    end
  end

  def size
    # TODO: size can be easily optimized to O(1), but don't have time to do it;)
    return 0 if @root.nil?

    @root.size
  end

  attr_reader :root

  def initialize(root = nil)
    @root = root
  end

  def empty?
    @root.nil?
  end

  def insert(value, priority = rand)
    node =
      if value.instance_of? Node
        value
      else
        Node.new(value, priority)
      end

    if @root.nil?
      @root = node
      @root
    else
      n = @root
      loop do
        if node.value < n.value
          if n.left.nil?
            node.parent = n
            n = n.left = node
            break
          else
            n = n.left
          end
        elsif n.right.nil?
          node.parent = n
          n = n.right = node
          break
        else
          n = n.right
        end
      end

      fixup(n)
    end
  end

  def fixdown(node)
    loop do
      highest = node
      highest = node.left if !node.left.nil? && node.left.priority > node.priority
      highest = node.right if !node.right.nil? && node.right.priority > highest.priority

      break if highest == node

      n =
        if highest == node.left
          node.rotate_right
        else
          node.rotate_left
        end
      @root = n if node == @root
    end
  end

  def fixup(n)
    while !n.nil? && !n.parent.nil? && (n.priority > n.parent.priority)
      n =
        if n == n.parent.left
          n.parent.rotate_right
        else
          n.parent.rotate_left
        end
      @root = n if n.parent.nil?
    end
  end

  def find(value)
    n = @root
    until n.nil?
      return n if value == n.value

      n =
        if value < n.value
          n.left
        else
          n = n.right
        end
    end
    nil
  end

  def include?(value)
    find(value) != nil
  end

  def split(k)
    node = Node.new(k, 1.01)
    insert(node)

    l = @root.left
    r = @root.right
    l&.parent = nil
    r&.parent = nil
    [Treap.new(l), Treap.new(r)]
  end

  # Ta klasa pełni rolę niezmiennika gdzie
  # T1 < guard < T2
  # przydaje sie przy implementacji operacji JOIN

  class JoiningKeyGuard
    include Comparable
    def initialize(a, b)
      a = a.right until a.right.nil?
      b = b.left until b.left.nil?
      @min = a
      @max = b
    end

    # def ==(other)
    #   false
    # end

    def <=>(other)
      if other <= @min
        1
      else
        -1
      end
    end
  end

  def join(other)
    if @root.nil?
      other
    elsif other.root.nil?
      self
    else
      key = JoiningKeyGuard.new(@root, other.root)
      node = Node.new(key, -1)
      node.left = @root
      node.right = other.root
      @root.parent = node
      @root = node

      fixdown(@root)

      # detach guard node
      if node == node.parent.left
        node.parent.left = nil
      else
        node.parent.right = nil
      end
      self
    end
  end

  def remove(value)
    n = find(value)
    remove_node(n) unless n.nil?
  end

  private

  def remove_node(n)
    if n.left.nil? && n.right.nil? # i.e. n is leaf
      if n == @root
        @root = nil
      elsif n.parent.left == n
        n.parent.left = nil
      else
        n.parent.right = nil
      end
    elsif n.left.nil?
      if n.parent.nil?
        @root = n.right
        @root.parent = nil
      else
        if n.parent.left == n
          n.parent.left = n.right
        else
          n.parent.right = n.right
        end
        n.right.parent = n.parent unless n.right.nil?
      end
    elsif n.right.nil?
      if n.parent.nil?
        @root = n.left
        @root.parent = nil
      else
        if n.parent.left == n
          n.parent.left = n.left
        else
          n.parent.right = n.left
        end
        n.left.parent = n.parent unless n.left.nil?
      end
    else
      rightmost = n.left
      rightmost = rightmost.right until rightmost.right.nil?

      if rightmost.parent != n
        rightmost.parent.right = rightmost.left

        rightmost.left = n.left
        rightmost.left.parent = rightmost unless rightmost.left.nil?
      else
        n.left = nil
      end
      if n.parent.nil?
        @root = rightmost
      elsif n.parent.left == n
        n.parent.left = rightmost
      else
        n.parent.right = rightmost
      end

      rightmost.parent = n.parent
      rightmost.right = n.right
      rightmost.right.parent = rightmost unless rightmost.right.nil?
      fixdown(rightmost)
    end
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  if ARGV[0] == '--big'
    big = Treap.new
    100.times { big.insert(rand(1_000_000)) }
    big.display
  else
    t = Treap.new
    t.insert(5)
    t.insert(0)
    puts 'after insert 5, 0: '
    t.dump
    # t.insert(10)

    t.insert(-5)
    puts 'after insert -5: '
    t.dump
    # t.insert(2)
    t.insert(-10)
    puts 'after insert -10: '
    t.dump
    t.insert(-20)
    t.insert(-30)

    puts "contains (5): #{t.include?(5)}"
    puts "contains (-30): #{t.include?(-30)}"
    puts "contains (3): #{t.include?(3)}"

    t.display

    t.remove(-30)
    t.display
    t.remove(-20)
    t.dump
    t.display
    t.remove(5)
    t.display
    t.dump
  end
end
# :nocov:
