# other "btree" module implement two-pass INSERT:
#  - inserts value into leaf
#  - if leaf is full then split leaf
#  - after split median is pushed up to parent
#  - algorithm repeats until node is nonfull or root reached

# Here I'm implementing one-pass algorithm (split is performed while going down the tree)
class BTree2
  attr_reader :degree, :size

  class Node
    attr_accessor :values, :children, :parent
    def initialize(btree, parent: nil, leaf: true)
      @btree = btree
      @parent = parent
      @values = []
      @children = if leaf then nil
                  else []
                  end
    end

    def size
      @values.size
    end

    def empty?
      size.zero?
    end

    def full?
      size == @btree.degree * 2 - 1
    end

    def assert_size
      raise "num of values array is expected to be #{@btree.node_capacity} but was #{@values.size}" unless @values.size == @btree.node_capacity
      raise "num of children array is expected to be #{@btree.node_capacity + 1} but was #{@children.size}" unless @children.nil? || @children.size == @btree.node_capacity + 1
    end

    def leaf?
      @children.nil?
    end

    def inspect
      values.join ','
    end

    alias to_s inspect

    def include?(value)
      idx = @values.index { |x| x.nil? || value <= x }
      if idx.nil?
        return false if leaf?

        return @children.last.include?(value)
      end
      return false if idx.nil?
      return true if @values[idx] == value
      return false if leaf?

      @children[idx].include? value
    end
  end

  def initialize(degree: 4)
    @degree = degree
    @root = Node.new(self)
    @size = 0
  end

  def empty?
    @root.empty?
  end

  def find_leaf(value)
    n = @root
    loop do
      break if n.leaf?

      idx = n.values.rindex { |k| !k.nil? && k < value }
      nn = if idx.nil? then n.children[0]
           else n.children[idx + 1]
           end
      break if nn.nil?

      n = nn
    end
    n
  end

  class Node
    def split_child(idx)
      raise 'this node should be non-full when splitting a child!' if full?

      a = @children[idx]
      b = Node.new(@btree, parent: self, leaf: a.leaf?)
      half = @btree.degree
      median = a.values[half-1]
      b.values = a.values[half..-1]
      unless a.leaf?
        b.children = a.children[half..-1] 
        b.children.each { |c| c.parent = b }
        a.children = a.children[0..(half-1)]
      end
      a.values = a.values[0...(half-1)]
      @values.insert(idx, median)
      @children.insert(idx + 1, b)
    end
  end

  def insert(value)
    n = @root
    if @root.full?
      n = Node.new(self, leaf: false)
      n.children[0] = @root
      n.children[0].parent = n
      @root = n
      n.split_child(0)
    end
    while true
      idx = n.values.rindex { |k| k < value } || -1
      if n.leaf?
        n.values.insert(idx + 1, value)
        break
      elsif n.children[idx+1].full?
        n.split_child(idx+1)
      else
        n = n.children[idx+1]
      end
    end
    @size += 1
  end

  def include?(value)
    @root.include? value
  end

  def dump
    @root.dump
  end

  def visit(node=@root, &block)
    block.call(node)
    node.children&.each do |c|
      next if c.nil?
      visit(c, &block)
    end
  end
end

# :nocov:
class BTree2
  class Node
    def dump
      p @values
      unless leaf?
        @children.each_with_index do |c, i|
          next if c.nil?
          str = "child #{i} #{c.values.inspect}"
          str += " (*)" if c.leaf?
          puts str unless c.nil?
        end
      end
    end

    def graphviz_label
      @values.each_with_index.map do |v, i|
        s = "<f#{i}>"
        s += (v.to_s || '')
        s
      end.join '|'
    end
  end

  def to_graph
    require_relative '../graph/graphviz'
    g = Graph.new
    h = {}

    idx = 0
    visit(@root) do |n|
      h[n] = idx
      vn = g.vertex(idx)
      vn.label = n.graphviz_label
      vn.add_graphviz_attrib("shape", "record")
      idx += 1
    end

    visit(@root) do |n|
      n.children&.each_with_index do |c, i|
        next if c.nil?
        e = g.add_edge(h[n], h[c])
        if i == 0
        # if i == n.children.size - 1
          e.graphviz_source_field_id = "w"
        else
          e.graphviz_source_field_id = "f#{i-1}"
        end
      end
    end
    g
  end
  def display(**opts)
    to_graph.display(**opts)
  end
end

if __FILE__ == $PROGRAM_NAME || $PROGRAM_NAME.include?('ruby-prof')
  t = BTree2.new(degree: 4)
  t.insert 1
  t.insert 2
  t.insert 3
  t.insert 4
  t.insert 0
  t.insert 5
  t.insert 6
  t.insert 7
  t.insert 8
  t.insert 9
  10.upto(50) do |i|
    t.insert i
  end
  t.display(debug: true)

end
# :nocov: