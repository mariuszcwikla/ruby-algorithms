#!/usr/bin/ruby

require_relative 'lomuto'

x = [5, 1, 2, 8, 9, 10, 3, 11, 4]
puts "array: #{x.join ' '}"

puts '-------- Lomuto quickselect ------'
(0..x.size - 1).each do |i|
  xx = x.clone
  v = quickselect_lomuto(xx, i)
  puts "#{i + 1}th: #{v}"
end

puts '-------- Lomuto quickselect (optimized) ------'
(0..x.size - 1).each do |i|
  xx = x.clone
  v = quickselect_lomuto2(xx, i)
  puts "#{i + 1}th: #{v}"
end
