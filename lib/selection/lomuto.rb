# Lomuto partitioning algorithm
def lomuto(array, l = 0, r = array.size, &block)
  block = :<=>.to_proc if block.nil?
  pivot = array[l]
  s = l

  ((l + 1)..(r - 1)).each do |i|
    #if array[i] < pivot
    if block.call(array[i], pivot) < 0
      s += 1
      array[i], array[s] = array[s], array[i]
    end
  end
  array[l], array[s] = array[s], array[l]
  s
end

# Quickselect poprzez partycję Lomuto
# szuka k-tej wartości z tablicy
#
# niezoptymalizowana - zwraca kopie tablic. Prostsza do zrozumienia

def quickselect_lomuto(array, k, &block)
  s = lomuto(array)
  return array[s] if s == k

  if k < s
    quickselect_lomuto(array[0..s - 1], k, &block)
  else
    quickselect_lomuto(array[(s + 1)..-1], k - s - 1, &block)
  end
end

def quickselect_lomuto_median(array, &block)
  quickselect_lomuto(array, array.size/2, &block)
end

# lekko zoptymalizowana wersja - pracuje na przekazanej tablicy
# zamiast robić kopie
def quickselect_lomuto2(array, k, l = 0, r = array.size)
  raise 'k is negative' if k < 0

  s = lomuto(array, l, r)
  return array[s] if s - l == k

  if k < s - l
    quickselect_lomuto2(array, k, l, s)
  else
    quickselect_lomuto2(array, k - (s - l) - 1, s + 1, r)
  end
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  x = [4, 5, 1, 8, 2, 3]

  puts 'Lomuto partition demo '
  puts '---------------------'
  puts "before: #{x.join ' '}"

  #s = lomuto(x) { |p, i| puts "Iteration \##{i}, pivot=#{p}: #{x.join ' '}" }
  #TODO change to
  # s = lomuto(x, debug: true)
  s = lomuto(x) {|a,b| a<=>b}

  puts "left : #{x[0..s - 1].join ' '}"
  puts "right: #{x[s..-1].join ' '}"
end
# :nocov:
