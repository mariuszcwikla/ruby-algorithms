#!/usr/bin/ruby

require_relative 'lomuto'

x = [5, 1, 2, 8, 9, 10, 3, 11, 4]

puts "before: #{x.join ' '}"

s = lomuto(x)

puts 'lomuto: '
puts (x[0..s - 1].join ' ').to_s
puts (x[s..-1].join ' ').to_s
