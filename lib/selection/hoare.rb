def hoare_partition_simple(array)
  pivot = array[0]
  i = 0
  j = array.size - 1

  while i < j
    i += 1 while (array[i] <= pivot) && (i < array.size)
    j -= 1 while (array[j] >= pivot) && (j > 0)

    array[i], array[j] = array[j], array[i] if i < j
  end

  array[0], array[j] = array[j], array[0]
  j
end

# Oryginalna implementacja algorytmu
# wykonuje swapowanie w glownej petli o jeden za duzo
# dlatego na koncu jest "Undo last swap"
#
# Wersja pierwsza jest ciut prostsza do zrozumienia
def hoare_partition_simple_original(array)
  pivot = array[0]
  i = 0
  j = array.size - 1

  while i < j
    i += 1 while (array[i] <= pivot) && (i < array.size - 1)
    j -= 1 while (array[j] >= pivot) && (j > 0)

    array[i], array[j] = array[j], array[i]
  end

  # Undo last swap
  array[i], array[j] = array[j], array[i]
  array[0], array[j] = array[j], array[0]

  j
end

def hoare_partition(array, l = 0, r = array.size)
  return nil if (l == 0) && (r == 0)

  pivot = array[l]
  i = l
  j = r - 1

  while i < j
    i += 1 while (array[i] <= pivot) && (i < r - 1)
    j -= 1 while (array[j] >= pivot) && (j > l)

    array[i], array[j] = array[j], array[i] if i < j
  end

  array[l], array[j] = array[j], array[l]
  j
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  x = [5, 1, 2, 8, 9, 10, 3, 11, 4]

  puts "before: #{x.join ' '}"

  s = hoare_partition_simple(x)

  puts "hoare, pivot=#{x[s]}: "
  puts "left : #{x[0..s - 1].join ' '}"
  puts "right: #{x[s..-1].join ' '}"
  puts "full : #{x.join ' '}"

  x = [5, 5, 5, 5, 5]

  puts "before: #{x.join ' '}"

  s = hoare_partition_simple_original(x)

  puts "hoare (original), pivot=#{x[s]}: "
  puts "left : #{x[0..s - 1].join ' '}"
  puts "right: #{x[s..-1].join ' '}"
  puts "full : #{x.join ' '}"

  puts '----------------------'
  x = [5, 1, 2, 8, 9, 10, 3, 11, 4]
  puts "hoare(2), pivot= #{x[hoare_partition(x)]}"
  puts (x.join ' ').to_s
end

# >> before: 5 1 2 8 9 10 3 11 4
# >> hoare, pivot=5:
# >> left : 3 1 2 4
# >> right: 5 10 9 11 8
# >> full : 3 1 2 4 5 10 9 11 8
# >> before: 5 5 5 5 5
# >> hoare (original), pivot=5:
# >> left : 5 5 5 5 5
# >> right: 5 5 5 5 5
# >> full : 5 5 5 5 5
# >> ----------------------
# >> hoare(2), pivot= 5
# >> 3 1 2 4 5 10 9 11 8
# :nocov:
