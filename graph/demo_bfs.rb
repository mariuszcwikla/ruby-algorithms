# Rysuje działanie Dijkstra search w formie animacji w konsoli

require_relative '../lib/graph/graph'
require_relative '../lib/graph/maze'
require_relative '../lib/graph/bfs'
require_relative '../lib/graph/graphviz'

filename = File.join(File.dirname(__FILE__), 'maze1.txt')
filename = ARGV[0] if ARGV.size == 1

g = Graph.new
s, t, w, h = g.load_maze_from_file(filename)
puts g.to_maze_str(s, t, w, h)
def cls
  # this works in console
  # but not in eclipse console :(

  puts "\e[H\e[2J"

  # so I'm using this hack
  # 50.times {puts""}
end

bfs = BFS.new(g, s, t)
num_of_iterations = 0
path =
  bfs.run do |_, prevs|
    m = g.to_maze_str(s, t, w, h)
    prevs.each { |k, _| m[k.y][k.x] = '.' }
    cls
    puts m
    STDOUT.flush
    sleep 0.2
    num_of_iterations += 1
  end

path.each { |edge| edge.graphviz_color = 'red' }
puts path

m = g.to_maze_str(s, t, w, h)
path.each do |edge|
  x, y = edge.tail.index
  m[y][x] = 'x'
end
puts m
puts "Number of iterations: #{num_of_iterations}"
