require 'set'

WALL = "\u2588".freeze

def cls
  # this works in console
  # but not in eclipse console :(

  puts "\e[H\e[2J"

  # so I'm using this hack
  #  50.times {puts""}
end

def prim_maze(n, animation = false)
  maze = Array.new(2 * n - 1) { Array.new(2 * n - 1) { WALL } }
  maze[0][0] = ' '

  vt = Set.new
  vt << [0, 0]
  et = Set.new

  (1..(n * n - 1)).each do |_i|
    # n*n is not accurate. TODO: calculate num of edges for nxn board
    not_visited_edges = []
    vt.each do |v|
      [[0, -1], [0, 1], [-1, 0], [1, 0]].each do |dx, dy|
        v2 = [v[0] + dx, v[1] + dy]
        next if (v2[0] < 0) || (v2[1] < 0)
        next if (v2[0] >= n) || (v2[1] >= n)

        next if et.include? [v, v2]
        next if vt.include? v2

        not_visited_edges << [v, v2, dx, dy]
      end
    end

    break if not_visited_edges.empty?

    e = not_visited_edges[rand(not_visited_edges.size)]

    v1 = e[0]
    v2 = e[1]

    dx = e[2]
    dy = e[3]

    et << [v1, v2]
    vt << v2

    maze[2 * v1[1] + dy][2 * v1[0] + dx] = ' '
    maze[2 * v1[1] + 2 * dy][2 * v1[0] + 2 * dx] = ' '

    next unless animation

    cls
    maze.each do |row|
      puts row.join('')
    end
    STDOUT.flush
    sleep(0.02)
  end

  maze
end

maze = prim_maze(20, true)
# cls
maze.each do |row|
  puts row.join('')
end
