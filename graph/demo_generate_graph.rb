require_relative '../lib/graph/graph'
require_relative '../lib/graph/graphviz'
require_relative '../lib/graph/planar_graph'

g = Graph.new

# zmien algorytm generacji na Delanuay triangulation

def algorithm1(g, _n)
  x = 0
  y = 0
  7.times do |i|
    g.vertex(i).set_position(x, y)
    g.vertex(i).add_graphviz_attrib('pos', "\"#{x},#{-y}!\"")

    dx = rand(-3..3)
    dy = rand(-3..3)
    x += dx
    y += dy
    next unless i > 0

    g.edge_by_distance(i, i - 1)
    g.edge_by_distance(i, i - 2) if i > 1
    g.edge_by_distance(i, i - 3) if i > 2
  end
end

def algorithm2(g, n_v, n_e)
  n_v.times { |i| g.vertex(i) }
  n_e.times do
    a = rand(n_v)
    begin
      b = rand(n_v)
    end while a == b
    g.add_edge(a, b)
  end
end

algorithm1(g, 10)

g.display_dot(layout: 'neato')
