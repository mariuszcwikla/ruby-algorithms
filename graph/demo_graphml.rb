require_relative '../lib/graph/graphml'
require_relative '../lib/graph/graph'
g = Graph.new
g.load_from_graphml_file('graph1.graphml')
g.display_dot(layout: 'neato', make_digraph: false, debug: true)
