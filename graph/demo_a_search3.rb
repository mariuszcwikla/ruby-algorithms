# Rysuje działanie algorytmu A* search w formie animacji w konsoli

require_relative '../lib/graph/graph'
require_relative '../lib/graph/maze'
require_relative '../lib/graph/a_search'
require_relative '../lib/graph/graphviz'
g = Graph.new

fname = File.join(File.dirname(__FILE__), 'maze1.txt')
fname = ARGV[0] if ARGV.size == 1

s, t, w, h = g.load_maze_from_file(fname)

def cls
  # this works in console
  # but not in eclipse console :(

  puts "\e[H\e[2J"

  # so I'm using this hack
  # 50.times {puts""}
end

num_of_iterations = 0
asearch = ASearch.new(s, t) { |a, b| a.manhattan_distance_to(b) }
path =
  asearch.run do |_v, prevs, _closedset|
    m = g.to_maze_str(s, t, w, h)
    prevs.each { |k, _v| m[k.y][k.x] = '.' }
    cls
    puts m
    STDOUT.flush
    sleep 0.2
    num_of_iterations += 1
  end

path.each { |edge| edge.graphviz_color = 'red' }
puts path

m = g.to_maze_str(s, t, w, h)
path.each do |edge|
  x, y = edge.tail.index
  m[y][x] = 'x'
end
puts m
puts "Number of iterations: #{num_of_iterations}"
