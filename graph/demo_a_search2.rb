require_relative '../lib/graph/graphml'
require_relative '../lib/graph/graph'
require_relative '../lib/graph/a_search'
g = Graph.new
g.load_from_graphml_file('graph1.graphml')
puts g.vertices.join(',')
path = g.a_search(g.vertex('0'), g.vertex('15')) do |a, b|
  puts "#{a.x} #{a.y}"
  puts b
  a.distance_to(b)
end
path.each { |edge| edge.graphviz_color = 'red' }
puts path

g.display_dot(layout: 'neato', make_digraph: false)
