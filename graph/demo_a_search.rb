require_relative '../lib/graph/graph'
require_relative '../lib/graph/graphviz'
require_relative '../lib/graph/a_search'

g = Graph.new

#   0  3     7
# 0 0--2-----\
#    \       |
# 2   -1     |
#      |     |
# 4    3--6  4
#     /   | /
# 6  7----5-
#   0  3  5  7
#
g.vertex(0).set_position(0, 0)
g.vertex(1).set_position(3, 2)
g.vertex(2).set_position(3, 0)
g.vertex(3).set_position(3, 4)
g.vertex(4).set_position(7, 4)
g.vertex(5).set_position(5, 6)
g.vertex(6).set_position(5, 4)
g.vertex(7).set_position(1, 6)
# g.vertex(6)

g.vertices.each { |v| v.add_graphviz_attrib('pos', "\"#{v.x},#{-v.y}!\"") }

g.edge_by_distance(0, 2)
g.edge_by_distance(2, 4)
g.edge_by_distance(4, 5)
g.edge_by_distance(0, 1)
g.edge_by_distance(1, 3)
g.edge_by_distance(3, 6)
g.edge_by_distance(6, 5)
g.edge_by_distance(3, 7)
g.edge_by_distance(7, 5)

path = g.a_search(g.vertex(0), g.vertex(5)) { |a, b| a.distance_to(b) }
path.each { |edge| edge.graphviz_color = 'red' }
puts path

g.display_neato
# TODO: dodaj "kolorowanie" grafu graphviz np.

# 1. niech a_search zwróci tablicę Edge
# 2. potem edges.each{|e| e.graphviz_color('red')
