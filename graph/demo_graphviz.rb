require_relative '../lib/graph/graph'
require_relative '../lib/graph/graphviz'

g = Graph.new
g.add_edge(1, 2)
g.add_edge(1, 3)
g.add_edge(2, 4)
g.add_edge(2, 5)

puts g.to_graphviz

g.display
