require_relative '../lib/graph/graph'

g = {}
g[1] = [2]
g[2] = [3, 6]
g[3] = [1, 4]
g[4] = [5]
g[5] = []
g[6] = []

def reverse_graph(g)
  g2 = Hash.new { |h, k| h[k] = [] }
  g.each do |v, e|
    e.each { |v2| g2[v2] << v }
  end
  g2
end

def strong_connected(g, v)
  stack = []
  stack << v
  visited = { v => true }

  visited_order = []

  until stack.empty?
    v = stack.pop

    g[v].each do |x|
      next if visited[x]

      visited[x] = true
      stack << x
    end

    visited_order << v
  end

  g2 = reverse_graph(g)
  puts visited_order
  puts g2
  visited = {}

  visited_order.reverse!
  visited_order.each do |v|
    strong_component = []

    next if visited[v]

    stack = []
    stack << v
    visited[v] = true
    until stack.empty?
      v = stack.pop
      strong_component << v
      g2[v].each do |v2|
        unless visited[v2]
          visited[v2] = true
          stack.push(v2)
        end
      end
    end
    puts strong_component.join(' ')
  end
end

strong_connected(g, 5)
