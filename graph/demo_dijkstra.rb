require_relative '../lib/graph/graph'
require_relative '../lib/graph/graphviz'
require_relative '../lib/heap/BinaryHeap'
require_relative '../lib/graph/dijkstra'

g = Graph.new
g.add_edge(0, 1).set_weight(2)
g.add_edge(0, 4).set_weight(4)
g.add_edge(1, 2).set_weight(3)
g.add_edge(2, 4).set_weight(1)
g.add_edge(2, 3).set_weight(5)
g.add_edge(4, 3).set_weight(7)
g.add_edge(3, 0).set_weight(8)

Dijkstra.new(g, g.vertex(0)).run

puts 'index - distance'
g.vertices.each do |v|
  puts "#{v.index} - #{v.distance}"
end

g.display_dot(layout: 'neato')
