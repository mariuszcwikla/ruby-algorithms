# simple "smoke test" to verify if demos work
# just run "run tets_demo.rb"
# Need to figure out better solution for this

require_relative '../test/test_helper'
require_relative 'demo_a_search'
require_relative 'demo_a_search2'
require_relative 'demo_a_search3'
require_relative 'demo_bfs'
require_relative 'demo_dijkstra'
require_relative 'demo_dijkstra_dense_graph'
require_relative 'demo_dijkstra2'
require_relative 'demo_generate_graph'
require_relative 'demo_graphml'
require_relative 'demo_graphviz'
require_relative 'demo_prim_maze'
require_relative 'demo_prim_maze_improved'
require_relative 'demo_strongly_connected_component'


class SubjectTest < Minitest::Test

  def test_foo
  end
end
