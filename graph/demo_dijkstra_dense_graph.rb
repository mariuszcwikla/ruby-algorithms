require_relative '../lib/graph/graph'
require_relative '../lib/graph/graphviz'
require_relative '../lib/heap/BinaryHeap'
require_relative '../lib/heap/BinomialHeap'

g = Graph.new
g.add_edge(0, 1).set_weight(2)
g.add_edge(0, 4).set_weight(4)
g.add_edge(1, 2).set_weight(3)
g.add_edge(2, 4).set_weight(1)
g.add_edge(2, 3).set_weight(5)
g.add_edge(4, 3).set_weight(7)
g.add_edge(3, 0).set_weight(8)
g.vertex(5)

#--
# Dotyczy grafów gęstych
#++

class Graph
  class Vertex
    attr_accessor :distance, :heap_handle
    
    def <(other)
      @distance < other.distance
    end
  end
end

def dijkstra(g, initial_vertex)
  num_of_vertices = g.vertices.length
  dist = Array.new(num_of_vertices, Float::INFINITY)
  visited = Array.new(num_of_vertices, false)

  dist[initial_vertex] = 0
  loop do
    min_idx = -1
    (0..num_of_vertices - 1).each do |i|
      unless visited[i]
        if min_idx == -1 or dist[i] < dist[min_idx]                 
          min_idx = i
        end
      end
    end
    break if min_idx == -1

    visited[min_idx] = true
    v = g.vertex(min_idx)
    v.edges.each do |e|
      if dist[min_idx] + e.weight < dist[e.second.index]
        dist[e.second.index] = dist[min_idx] + e.weight
      end
    end
  end

  dist.each_with_index do |v, i|
    g.vertex(i).distance = v
  end
end

dijkstra(g, 0)

puts 'index - distance'
g.vertices.each do |v|
  puts "#{v.index} - #{v.distance}"
end
