require 'set'

WALL = "\u2588".freeze

def cls
  puts "\e[H\e[2J"
end

def prim_maze(n, animation = false)
  maze = Array.new(2 * n - 1) { Array.new(2 * n - 1) { WALL } }
  maze[0][0] = ' '

  vt = Set.new
  vt << [0, 0]
  et = Set.new

  def each_edge_of(n, v, &block)
    [[0, -1, :south], [0, 1, :north], [-1, 0, :west], [1, 0, :east]].each do |dx, dy, dir|
      v2 = [v[0] + dx, v[1] + dy]
      next if (v2[0] < 0) || (v2[1] < 0)
      next if (v2[0] >= n) || (v2[1] >= n)

      block.call(v, v2, dx, dy, dir)
    end
  end

  boundary_edges = {}
  each_edge_of(n, [0, 0]) { |v1, v2, dx, dy, _| boundary_edges[[v1, v2]] = [dx, dy] }

  (1..(n * n - 1)).each do |_i|
    # n*n is not accurate. TODO: calculate num of edges for nxn board
    break if boundary_edges.empty?

    begin
      e = boundary_edges.keys[rand(boundary_edges.size)]
      dx, dy = boundary_edges[e]
      boundary_edges.delete(e)
    end until boundary_edges.empty? || (!vt.include? e[1])

    v1 = e[0]
    v2 = e[1]

    each_edge_of(n, v2) do |v1, v2, dx, dy, _|
      boundary_edges[[v1, v2]] = [dx, dy] unless vt.include? v2
    end

    et << [v1, v2]
    vt << v2

    maze[2 * v1[1] + dy][2 * v1[0] + dx] = ' '
    maze[2 * v1[1] + 2 * dy][2 * v1[0] + 2 * dx] = ' '

    next unless animation

    cls
    maze.each do |row|
      puts row.join('')
    end
    STDOUT.flush
    sleep(0.02)
  end

  # puts et

  maze
end

n = 30
n = ARGV[0].to_i unless ARGV.empty?

maze = prim_maze(n, false)
# cls
maze.each do |row|
  puts row.join('')
end
