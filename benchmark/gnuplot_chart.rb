require 'tempfile'

class BenchmarkChart
  attr_accessor :xlabel, :ylabel

  def initialize(title = nil)
    @title = title
    @datasets =
      Hash.new do |h, k|
        dataset = {}
        def dataset.pointtype=(style)
          @style = style
        end

        def dataset.pointtype
          @style
        end
        h[k] = dataset
      end
  end

  def dataset(name)
    @datasets[name]
  end

  def plot(smooth: false)
    idx = 1
    script = "set terminal wxt\n"   # zoom does not work on default terminal (any more? Was working few months ago though).
    unless @title.nil?
      script << "set title '#{@title}'\n"
      script << "show title\n"
    end
    # script << "set samples 300, 300\n"
    script << "set style data lines\n" if smooth
    script << "set xlabel \"#{@xlabel}\"\n" unless @xlabel.nil?
    script << "set ylabel \"#{@ylabel}\"\n" unless @ylabel.nil?
    max_x = @datasets.values.map{|v| v.values.map{|t| t.total}&.max || 0}.max
    xtics = 5
    #script << "set xtics #{max_x}/#{xtics}\n"
    script << "set key outside\n"
    @datasets.each do |_k, v|
      next if v.size<=1
      script << "$DATA#{idx} << EOD\n"

      v.keys.sort.each do |x|
        script << "#{x} #{v[x].total}\n"
      end
      script << "EOD\n"

      idx += 1
    end
    script << "plot\\\n"
    idx = 1
    @datasets.each do |k, v|
      next if v.size<=1
      script << ',' if idx > 1
      pointtype = ''
      pointtype = "pointtype #{v.pointtype}" unless v.pointtype.nil?
      if smooth
        script << "$DATA#{idx} using 1:2 title \"#{k}\" smooth csplines lw 1 #{pointtype}\\\n"
      else
        script << "$DATA#{idx} using 1:2 title \"#{k}\" lw 1 with linespoints #{pointtype}\\\n"
      end
      idx += 1
    end
    script << "\n"
    script << "pause -1\n"

    Dir.mkdir './tmp' unless Dir.exist? './tmp'
    file = Tempfile.new('benchmark.gnu', './tmp')
    file.write script
    file.close

    #status = system('gnuplot #{file.path}')
    response = `gnuplot #{file.path}`
    if $?.exitstatus != 0
      puts "Error when creating gnuplot chart. Exit status = #{$?.exitstatus}: #{response}"
      puts script
    end
  end
end
