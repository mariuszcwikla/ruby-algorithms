require_relative '../lib/hash/linear_hash'

require 'memory_profiler'

MemoryProfiler.start

linear_hash = LinearHash.new

# NOTE: DO NOT USE VALUES LARGER THAN 200k-500k
# memory_profiler library is using HUGE AMOUNT of memory.
# Example: my algorithm is using 40MB and memory profiler is using about ~900MB

N = 200_000
N.times do |_i|
  linear_hash.insert(rand(1_000_000_000))
end

puts "Finished inserting #{N} records"

report = MemoryProfiler.stop
report.pretty_print(color_output: true, scale_bytes: true)
