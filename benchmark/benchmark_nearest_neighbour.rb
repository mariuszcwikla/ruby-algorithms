require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/tree/balltree'
require_relative '../lib/tree/kdtree'
require 'knnball'
$test_data = {}

# try different dimensions
#
# ruby benchmark/benchmark_nearest_neighbour.rb 2
# ruby benchmark/benchmark_nearest_neighbour.rb 2 1000
# ruby benchmark/benchmark_nearest_neighbour.rb 3 1000
# ruby benchmark/benchmark_nearest_neighbour.rb 5 1000
# ruby benchmark/benchmark_nearest_neighbour.rb 10 1000 5000

DIMENSIONS = ARGV[0]&.to_i || 2
NUM_QUERIES = ARGV[1]&.to_i || 2000
MAX=ARGV[2]&.to_i

def random_point(dimensions)
  dimensions.times.map { rand % 1000.0}
end

[
  50,
  100,
  200,
  300,
  400,
  500,
  600,
  700,
  800,
  900,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  8000,
  9000,
  10_000,
  15_000,
  20_000,
  30_000,
  40_000,
  50_000,
  60_000,
  70_000,
  80_000,
  90_000,
  100_000
].each do |n|

  data = []
  $test_data[n] = data
  n.times { data << random_point(DIMENSIONS) }
end

chart = BenchmarkChart.new "Nearest neighbour search #{DIMENSIONS}D (#{NUM_QUERIES} queries)"
chart.xlabel = 'Set size'
chart.ylabel = 'Time [s]'
ds1 = chart.dataset 'KDTree'
ds2 = chart.dataset 'Ball Tree (t=3)'
ds3 = chart.dataset 'Ball Tree (t=5)'
ds4 = chart.dataset 'Ball Tree (t=10)'
ds5 = chart.dataset 'Brute force'
ds6 = chart.dataset 'KNNBall (github)'

# Need to wrap nearest_neighbour method with this class to be consistend with kd and ball
class BruteForceNN
  def initialize(points, k)
    @points = points
    @k = k
  end

  def nearest_neighbour(q)
    nearest_neighbour_brute(@points, q)
  end

  def self.build(points, k:)
    BruteForceNN.new(points, k)
  end
end

class KNNBallWrapper

  def initialize(knn)
    @knn = knn
  end

  def self.build(data, k:)
    kdata = []
    data.each_with_index do |d, i|
      kdata << { id: i, point: d}
    end
    KNNBallWrapper.new(KnnBall.build(kdata))
  end

  def nearest_neighbour(p)
    @knn.nearest(p)
  end
end

def report(bench, klass, ds, max: nil, label:nil, **params)
  $test_data.keys.sort.each do |n|
    unless max.nil?
      break if n > max
    end
    unless MAX.nil?
      break if n > MAX
    end
    label = klass.to_s if label.nil?
    queries = NUM_QUERIES.times.map { random_point(DIMENSIONS) }
    container = klass.build($test_data[n], k: DIMENSIONS, **params)
    GC.start
    GC.disable
    ds[n] =
      bench.report("#{label}          #{n}") do
        queries.each do |q|
          container.nearest_neighbour q
        end
      end
    GC.enable
  end
end

Benchmark.bm do |bench|
  report(bench, BallTree, ds2, label: "BallTree (ball_size=3)", ball_size: 3)
  report(bench, KNNBallWrapper, ds6, label: "KNNBall (github)")
  report(bench, KDTree, ds1)


  report(bench, BallTree, ds3, label: "BallTree (ball_size=5)", ball_size: 5)
  report(bench, BallTree, ds4, label: "BallTree (ball_size=10)", ball_size: 10)
  report(bench, BruteForceNN, ds5, max: 2000)
end

chart.plot(smooth: true) unless ARGV[0] == '--no-plot'
