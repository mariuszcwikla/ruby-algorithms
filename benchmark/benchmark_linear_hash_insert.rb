require 'benchmark'
require_relative 'gnuplot_chart'

require_relative '../lib/hash/linear_hash'

test_data = {}

[
  100,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  10_000,
  15_000,
  20_000,
  30_000,
  40_000,
  50_000,
  75_000,
  100_000,
  125_000,
  150_000,
  175_000,
  200_000,
  250_000,
  300_000,
  400_000
].each do |n|
  data = []
  test_data[n] = data
  n.times { data << rand(n * 1000) }
end

chart = BenchmarkChart.new('Linear hash - INSERT')
ds1 = chart.dataset 'blocks=2, size=2'
ds2 = chart.dataset 'blocks=8, size=2'
ds3 = chart.dataset 'blocks=8, size=4'
ds4 = chart.dataset 'blocks=8, size=8'
ds5 = chart.dataset 'blocks=16, size=2'
ds6 = chart.dataset 'blocks=16, size=4'
ds7 = chart.dataset 'blocks=32, size=2'
ds8 = chart.dataset 'Ruby Hash'

Benchmark.bm do |bench|
  test_data.keys.sort.each do |n|
    ds1[n] =
      bench.report("blocks=2, size=2 #{n}") do
        lh = LinearHash.new
        test_data[n].each { |i| lh.insert i }
      end
  end

  test_data.keys.sort.each do |n|
    ds2[n] =
      bench.report("blocks=8, size=2 #{n}") do
        lh = LinearHash.new(initial_blocks: 8)
        test_data[n].each { |i| lh.insert i }
      end
  end

  test_data.keys.sort.each do |n|
    ds3[n] =
      bench.report("blocks=8, size=4 #{n}") do
        linear_hash3 = LinearHash.new(initial_blocks: 8, block_size: 4)
        test_data[n].each { |i| linear_hash3.insert i }
      end
  end

  test_data.keys.sort.each do |n|
    ds4[n] =
      bench.report("blocks=8, size=8 #{n}") do
        lh = LinearHash.new(initial_blocks: 8, block_size: 8)
        test_data[n].each { |i| lh.insert i }
      end
  end

  test_data.keys.sort.each do |n|
    ds5[n] =
      bench.report("blocks=16, size=2 #{n}") do
        lh = LinearHash.new(initial_blocks: 16, block_size: 2)
        test_data[n].each { |i| lh.insert i }
      end
  end

  test_data.keys.sort.each do |n|
    ds6[n] =
      bench.report("blocks=16, size=4 #{n}") do
        lh = LinearHash.new(initial_blocks: 16, block_size: 4)
        test_data[n].each { |i| lh.insert i }
      end
  end

  test_data.keys.sort.each do |n|
    ds7[n] =
      bench.report("blocks=32, size=2 #{n}") do
        lh = LinearHash.new(initial_blocks: 32, block_size: 2)
        test_data[n].each { |i| lh.insert i }
      end
  end
  test_data.keys.sort.each do |n|
    ds8[n] =
      bench.report("Ruby Hash #{n}") do
        h = {}
        test_data[n].each { |i| h[i] = i }
      end
  end
end

chart.plot
