require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/stringmatch/aho_corasick'
require_relative '../lib/stringmatch/knuth_morris_pratt'
require_relative '../lib/stringmatch/rabin_karp'
require_relative '../lib/stringmatch/hoorspool'
require_relative '../lib/stringmatch/string_match_automata'
require_relative '../lib/stringmatch/border_strings'

#srand 100

def brute_force(text, pattern)
  for i in 0..(text.length - pattern.length)
    j = 0
    while j < pattern.length
      break if pattern[j] != text[i+j]
      j += 1
    end
    return i if j == pattern.length
  end
  -1
end

def generate_string(length)
  #letters = *('b'..'z')
  letters = ['b']
  s = length.times.map do |i|
    if rand < 0.95
      'a'
    else
      if letters.size == 1
        letters[0]
      else
        letters[rand(letters.size)]
      end
    end
  end.join ''
  #s[-1]='c'   #this fools all the algoritms
  s
end

def string_match_automata(text, pattern)
  a = build_automata(pattern)
  automata_match(a, text)
end

def string_match_automata_Om3(text, pattern)
  a = build_automata_Om3(pattern)
  automata_match(a, text)
end

def string_match_automata_Om(text, pattern)
  a = build_automata_Om(pattern)
  automata_match(a, text)
end

def aho_corasick_match_single_string(text, pattern)
  t = AhoCorasickTrie.new
  t.insert(pattern)
  t.aho_corasick_prepare
  t.search(text) {|string, i| return i}
  -1
end

PATTERN_LENGTH = 2000

queries = [
  1000,
  50_000,
  100_000,
  200_000,
  300_000,
  500_000,
  1_000_000,
  1_500_000,
  2_000_000,
  2_500_000,
  3_000_000,
  4_000_000,
  5_000_000
];

queries = 50_000.step(225_000, 25_000).to_a
queries += 250_000.step(5_000_000, 500_000).to_a

print "Generating large string..."

large_string = generate_string(queries.max)
$pattern = generate_string(PATTERN_LENGTH)

def generate_pattern(s)
  start = [s.length - PATTERN_LENGTH, 0].max
  s[start..-1]
end

puts " Strings generated"

chart = BenchmarkChart.new "String match (preprocessing+matching)"
# chart.xlabel = 'Set size'
# chart.ylabel = 'Time [s]'
ds1 = chart.dataset 'Rabin Karp'
ds_rk_buzz = chart.dataset 'Rabin Karp (buzzhash)'
ds2 = chart.dataset 'Knuth-Morris-Pratt'
ds3 = chart.dataset 'String matching automata (O(m^4|E|+n)'
ds3_m3 = chart.dataset 'String matching automata (O(m^3|E|+n)'
ds3_m  = chart.dataset 'String matching automata (O(m|E|+n)'
ds4 = chart.dataset 'Aho-Corasick'
ds5 = chart.dataset 'Hoorspool'
ds6 = chart.dataset 'Border String match'
ds_brute = chart.dataset 'Brute-force'

Benchmark.bm do |bench|

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds3_m[n] = bench.report("String matching automata (O(m|E| preprocess) #{n}") do
      string_match_automata_Om(s, p)
    end
  end
  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds1[n] = bench.report("Rabin Karp         #{n}") do
      rabin_karp_match(s, p)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds_rk_buzz[n] = bench.report("Rabin Karp (buzzhash)   #{n}") do
      rabin_karp_match(s, p, :buzz)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds2[n] = bench.report("Knuth-Morris-Pratt #{n}") do
      kmp_match(s, p)
    end
  end
  
  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds3[n] = bench.report("String matching automata (O(m^4|E| preprocess) #{n}") do
      string_match_automata(s, p)
    end
    break if ds3[n].total > 10
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds3_m3[n] = bench.report("String matching automata (O(m^3|E| preprocess) #{n}") do
      string_match_automata_Om3(s, p)
    end
    break if ds3_m3[n].total > 10
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds4[n] = bench.report("Aho-Corasick             #{n}") do
      aho_corasick_match_single_string(s, p)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds5[n] = bench.report("Hoorspool                #{n}") do
      hoorspool_string_match(s, p)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds6[n] = bench.report("Border String match      #{n}") do
      border_string_match(s, p)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds_brute[n] = bench.report("Brute-force #{n}") do
      #String.index seems to be well optimized (besides it's implemented in C)
      #String.index for 5M string is 0.016
      #s.index(p) 
      brute_force(s, p)
    end
    break if ds_brute[n].total > 10
  end
end

chart.plot(smooth: false) unless ARGV[0] == '--no-plot'
