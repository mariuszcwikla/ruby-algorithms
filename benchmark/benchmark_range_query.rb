require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/tree/segment_tree'
require_relative '../lib/tree/segment_tree_array_based'
require_relative '../lib/queries/sqrt_decomposition'


INPUT_SIZE = 1_000_000
$input_data = INPUT_SIZE.times.map{ rand(1_000_000) }

func = ->(a, b) {a+b}
segment = SegmentTree.new($input_data, func)
segment2 = SegmentTreeArrayBased.new($input_data, func)
decomp = SqrtDecomposition.new($input_data, func)
decomp2 = SqrtDecomposition.new($input_data, func, optimized_array_operations: true)

$query_data = {}
[
  50,
  100,
  200,
  300,
  400,
  500,
  600,
  700,
  800,
  900,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  8000,
  9000,
  10_000,
  15_000,
  20_000,
  30_000,
  40_000,
  50_000
].each do |n|
  data = []
  $query_data[n] = data
  n.times do 
    a = rand(INPUT_SIZE-2)
    b = rand(INPUT_SIZE-1)
    a, b = b, a if a > b
    b += 1 if b == a
    data << [a, b]
  end
end

def brute(arr, a, b, func)
  arr[a..(b-1)].reduce(&func)
end

chart = BenchmarkChart.new "Range query (sum)"
# chart.xlabel = 'Set size'
# chart.ylabel = 'Time [s]'
ds1 = chart.dataset 'SegmentTree'
ds2 = chart.dataset 'SegmentTree (array based)'
ds3 = chart.dataset 'Brute Force'
ds4 = chart.dataset 'Sqrt Decomposition'
ds5 = chart.dataset 'Sqrt Decomposition (optimized)'

Benchmark.bm do |bench|
  $query_data.each do |n, v|
    ds1[n] = bench.report("SegmentTree         #{n}") do
        v.each do |a, b|
          segment.query(a, b)
        end
      end
  end

  $query_data.each do |n, v|
    ds2[n] = bench.report("SegmentTreeArrayBased #{n}") do
        v.each do |a, b|
          segment2.query(a, b)
        end
      end
  end

  $query_data.each do |n, v|
    break if n >= 500
    ds3[n] = bench.report("Brute force         #{n}") do
        v.each do |a, b|
          brute($input_data, a, b, func)
        end
      end
    break if ds3[n].total > 2
  end

  $query_data.each do |n, v|
    break if n > 10000
    ds4[n] = bench.report("Sqrt decomposition  #{n}") do
        v.each do |a, b|
          decomp.query(a, b)
        end
      end
  end

  $query_data.each do |n, v|
    break if n > 10000
    ds5[n] = bench.report("Sqrt decomposition (optimized) #{n}") do
        v.each do |a, b|
          decomp2.query(a, b)
        end
      end
  end
end

chart.plot(smooth: true) unless ARGV[0] == '--no-plot'
