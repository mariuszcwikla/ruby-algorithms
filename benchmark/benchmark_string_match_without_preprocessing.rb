require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/stringmatch/aho_corasick'
require_relative '../lib/stringmatch/knuth_morris_pratt'
require_relative '../lib/stringmatch/rabin_karp'
require_relative '../lib/stringmatch/hoorspool'
require_relative '../lib/stringmatch/string_match_automata'
require_relative '../lib/stringmatch/border_strings'

srand 100

def brute_force(text, pattern)
  for i in 0..(text.length - pattern.length)
    j = 0
    while j < pattern.length
      break if pattern[j] != text[i+j]
      j += 1
    end
    return i if j == pattern.length
  end
  -1
end

def generate_string(length)
  s = length.times.map do
    if rand < 0.8
      'a'
    else
      'b'
    end
  end.join ''
  s[-1]='c'   #this fools all the algoritms
  s
end

PATTERN_LENGTH = 2000

print "Generating large string..."


queries = [
  1000,
  50_000,
  100_000,
  200_000,
  300_000,
  500_000,
  1_000_000,
  1_500_000,
  2_000_000,
  2_500_000,
  3_000_000,
  4_000_000,
  5_000_000
];

large_string = generate_string(queries.max)

def generate_pattern(s)
  start = [s.length - PATTERN_LENGTH, 0].max
  s[start..-1]
end

puts " Strings generated"

chart = BenchmarkChart.new "String match (matching only, without preprocessing phase)"
# chart.xlabel = 'Set size'
# chart.ylabel = 'Time [s]'
ds1 = chart.dataset 'Rabin Karp (polynormial)'
ds_rk_buzz = chart.dataset 'Rabin Karp (buzzhash)'
ds2 = chart.dataset 'Knuth-Morris-Pratt'
ds3 = chart.dataset 'String matching automata'
ds4 = chart.dataset 'Aho-Corasick'
ds5 = chart.dataset 'Hoorspool'
ds6 = chart.dataset 'Border String match'
ds_brute = chart.dataset 'Brute-force'

Benchmark.bm do |bench|

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds_rk_buzz[n] = bench.report("Rabin Karp (buzzhash)   #{n}") do
      rabin_karp_match(s, p, :buzz)
    end
  end
  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds1[n] = bench.report("Rabin Karp (polynomial)  #{n}") do
      rabin_karp_match(s, p, :poly)
    end
  end


  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    pi = compute_pi_function(p)
    ds2[n] = bench.report("Knuth-Morris-Pratt #{n}") do
      kmp_match(s, p, pi: pi)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    a = build_automata(p)
  
    ds3[n] = bench.report("String matching automata #{n}") do
      automata_match(a, s)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    t = AhoCorasickTrie.new
    t.insert(p)
    t.aho_corasick_prepare

    ds4[n] = bench.report("Aho-Corasick             #{n}") do
      t.search(s) {|string, i| break}
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    t = hoorspool_table(p)
    ds5[n] = bench.report("Hoorspool                #{n}") do
      hoorspool_string_match(s, p, table: t)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    borders = compute_borders(p + '$' + s)
    ds6[n] = bench.report("Border String match      #{n}") do
      border_string_match(s, p, borders: borders)
    end
  end

  queries.each do |n|
    s = large_string[0..n]
    p = generate_pattern(s)
    ds_brute[n] = bench.report("Brute-force #{n}") do
      #String.index seems to be well optimized (besides it's implemented in C)
      #String.index for 5M string is 0.016
      #s.index(p) 
      brute_force(s, p)
    end
    break if ds_brute[n].total > 5
  end
end

chart.plot(smooth: false) unless ARGV[0] == '--no-plot'
