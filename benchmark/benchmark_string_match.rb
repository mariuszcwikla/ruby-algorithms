require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/stringmatch/aho_corasick'
require_relative '../lib/stringmatch/knuth_morris_pratt'
require_relative '../lib/stringmatch/rabin_karp'
require_relative '../lib/stringmatch/string_match_automata'

def brute_force(text, pattern)
  for i in 0..(text.length - pattern.length)
    j = 0
    while j < pattern.length
      break if pattern[j] != text[i+j]
      j += 1
    end
    return i if j == pattern.length
  end
  -1
end

def generate_string(length)
  length.times.map do
    if rand < 0.9
      'a'
    else
      'b'
    end
  end.join ''
end

def string_match_automata(text, pattern)
  a = build_automata(pattern)
  automata_match(a, text)
end

def aho_corasick_match_single_string(text, pattern)
  t = AhoCorasickTrie.new
  t.insert(pattern)
  t.aho_corasick_prepare
  t.search(text) {|string, i| return i}
  -1
end

long_string = generate_string(10000)
len = long_string.length
PATTERN_LEN = 50
pattern = long_string[(len - PATTERN_LEN)..-1]

queries = [
  50,
  100,
  200,
  300,
  400,
  500,
  600,
  700,
  800,
  900
]

queries = 20.step(200, 40).to_a

chart = BenchmarkChart.new "String match"
# chart.xlabel = 'Set size'
# chart.ylabel = 'Time [s]'
ds1 = chart.dataset 'Rabin Karp'
ds2 = chart.dataset 'Knuth-Morris-Pratt'
ds3 = chart.dataset 'String matching automata'
ds4 = chart.dataset 'Aho-Corasick'
ds5 = chart.dataset 'Brute-force'

Benchmark.bm do |bench|
  matches = 0
  
  queries.each do |n|
    ds5[n] = bench.report("Brute-force (String.index) #{n}") do
      n.times do
        long_string.index(pattern)
        # brute_force(long_string, pattern)
      end
    end
  end
  queries.each do |n|
    ds1[n] = bench.report("Rabin Karp         #{n}") do
      matches = 0
      n.times do 
        matches += 1 if rabin_karp_match(long_string, pattern) >=0
      end
    end
  end
  

  queries.each do |n|
    ds2[n] = bench.report("Knuth-Morris-Pratt #{n}") do
      matches = 0
      n.times do
        matches += 1 if kmp_match(long_string, pattern) >=0
      end
    end
  end
  puts "KMP matches: #{matches}"

  matches = 0

  queries.each do |n|
    ds3[n] = bench.report("String matching automata #{n}") do
      n.times do
        matches += 1 if string_match_automata(long_string, pattern) >=0
      end
    end
  end
  puts "String matching automata matches: #{matches}"

  matches = 0
  queries.each do |n|
    ds4[n] = bench.report("Aho-Corasick             #{n}") do
      n.times do
        matches += 1 if aho_corasick_match_single_string(long_string, pattern) >=0
      end
    end
  end
  puts "Aho-Corasick matches: #{matches}"

end

chart.plot(smooth: true) unless ARGV[0] == '--no-plot'
