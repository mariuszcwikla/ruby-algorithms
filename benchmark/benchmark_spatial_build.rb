require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/tree/balltree'
require_relative '../lib/tree/kdtree'
$test_data = {}

# try different dimensions
#
# ruby benchmark/benchmark_nearest_neighbour.rb 2
# ruby benchmark/benchmark_nearest_neighbour.rb 2 1000
# ruby benchmark/benchmark_nearest_neighbour.rb 3 1000
# ruby benchmark/benchmark_nearest_neighbour.rb 5 1000
# ruby benchmark/benchmark_nearest_neighbour.rb 10 1000 5000

DIMENSIONS = ARGV[0]&.to_i || 2
NUM_QUERIES = ARGV[1]&.to_i || 2000
MAX=ARGV[2]&.to_i

def random_point(dimensions)
  dimensions.times.map { rand % 1000.0}
end

[
  50,
  100,
  200,
  300,
  400,
  500,
  600,
  700,
  800,
  900,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  8000,
  9000,
  10_000,
  15_000,
  20_000,
  30_000,
  40_000,
  50_000,
  60_000,
  70_000,
  80_000,
  90_000,
  100_000
].each do |n|

  data = []
  $test_data[n] = data
  n.times { data << random_point(DIMENSIONS) }
end

chart = BenchmarkChart.new "Spatial data structures build time #{DIMENSIONS}D"
chart.xlabel = 'Set size'
chart.ylabel = 'Time [s]'
ds1 = chart.dataset 'KDTree'
ds2 = chart.dataset 'Ball Tree (t=3)'
ds3 = chart.dataset 'Ball Tree (t=3, lomuto median)'
ds4 = chart.dataset 'Ball Tree (t=5)'
ds5 = chart.dataset 'Ball Tree (t=5, lomuto median)'
ds6 = chart.dataset 'Ball Tree (t=10)'


def report(bench, klass, ds, max: nil, label:nil, **params)
  $test_data.keys.sort.each do |n|
    unless max.nil?
      break if n > max
    end
    unless MAX.nil?
      break if n > MAX
    end
    label = klass.to_s if label.nil?

    GC.start
    GC.disable
    ds[n] =
      bench.report("#{label}          #{n}") do
        container = klass.build($test_data[n], k: DIMENSIONS, **params)
      end
    GC.enable
  end
end

Benchmark.bm do |bench|
  report(bench, KDTree, ds1)
  report(bench, BallTree, ds2, label: "BallTree (ball_size=3)", ball_size: 3)
  report(bench, BallTree, ds3, label: "BallTree (ball_size=3, lomuto median)", ball_size: 3, use_lomuto_quickselect: true)
  report(bench, BallTree, ds4, label: "BallTree (ball_size=5)", ball_size: 5)
  report(bench, BallTree, ds5, label: "BallTree (ball_size=5, lomuto median)", ball_size: 5, use_lomuto_quickselect: true)
  report(bench, BallTree, ds6, label: "BallTree (ball_size=10)", ball_size: 10)
end

chart.plot unless ARGV[0] == '--no-plot'
