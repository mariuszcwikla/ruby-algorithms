require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/tree/avltree'
require_relative '../lib/tree/bst'
require_relative '../lib/tree/btree'
require_relative '../lib/tree/treap'
require_relative '../lib/tree/treap_persistent'
require_relative '../lib/probabilistic/skiplist'
require_relative '../lib/probabilistic/skiplist2'
require_relative '../lib/persistent/partial/fat/bst'
require_relative '../lib/persistent/full/fat/bst'
require_relative '../lib/hash/linear_hash'
require_relative '../lib/hash/linear_hash_base2'
require_relative '../lib/heap/van_emde_boyas'

$test_data = {}

[
  100,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  10_000,
  15_000,
  20_000,
  30_000,
  40_000,
  50_000,
  75_000,
  100_000,
  125_000,
  150_000,
  175_000,
  200_000,
  225_000,
  250_000
].each do |n|
  data = []
  $test_data[n] = data
  n.times { data << rand(n) }
end

chart = BenchmarkChart.new('Insert-only benchmark')
ds1 = chart.dataset 'AVL'
ds2 = chart.dataset 'BST'
ds3 = chart.dataset 'B-Tree'
ds4 = chart.dataset 'Treap'
ds5 = chart.dataset 'Persistent Treap'
ds6 = chart.dataset 'SkipList'
ds7 = chart.dataset 'SkipList v2'
ds8 = chart.dataset 'Fat-node BST (partial)'
ds9 = chart.dataset 'Linear Hash'
ds10 = chart.dataset 'Linear Hash base2'
ds11 = chart.dataset 'Fat-node BST (full)'
ds12 = chart.dataset 'van Emde Boyas'

def report(bench, klass, ds, max =nil)
  $test_data.keys.sort.each do |n|
    unless max.nil?
      break if n > max
    end
    GC.start
    GC.disable
    collection = klass.new
    ds[n] =
      bench.report("#{klass}          #{n}") do
        $test_data[n].each { |i| collection.insert i }
      end
    GC.enable
  end
end

Benchmark.bm do |bench|
  report(bench, AVLTree, ds1)
  report(bench, BinarySearchTree, ds2)
  report(bench, BTree, ds3)
  report(bench, Treap, ds4)
  report(bench, PersistentTreap, ds5)
  report(bench, SkipList, ds6)
  report(bench, SkipList2, ds7)
  report(bench, Fat::BinarySearchTree, ds8)
  report(bench, LinearHash, ds9)
  report(bench, LinearHashBase2, ds10)
  report(bench, Persistent::Full::BinarySearchTree, ds11, 150_000)
  report(bench, VanEmdeBoyasTree, ds12)
end

chart.plot unless ARGV[0] == '--no-plot'
