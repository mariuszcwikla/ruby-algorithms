require 'benchmark'
require_relative 'gnuplot_chart'

require_relative '../lib/hash/linear_hash'

test_data = {}

linear_hash = LinearHash.new
linear_hash8 = LinearHash.new(initial_blocks: 8)
linear_hash8_4 = LinearHash.new(initial_blocks: 8, block_size: 4)
linear_hash8_8 = LinearHash.new(initial_blocks: 8, block_size: 8)
linear_hash16_2 = LinearHash.new(initial_blocks: 16, block_size: 2)
linear_hash16_4 = LinearHash.new(initial_blocks: 16, block_size: 4)
ruby_hash = {}
def ruby_hash.insert(i)
  self[i] = i
end

puts 'populating data structures'

INPUT_SIZE = 1_000_000
[linear_hash, linear_hash8, linear_hash8_4, linear_hash8_8, linear_hash16_2, linear_hash16_4, ruby_hash].each do |ds|
  n = INPUT_SIZE
  print("Inserting #{n} into %20s, " % [ds.class])
  t = Time.now
  n.times do
    x = rand(1_000_000)
    ds.insert x
  end
  time = (Time.now - t).round(3)
  puts "done in #{time}s"
end

puts 'population finished'

[
  100,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  10_000,
  20_000,
  30_000,
  40_000,
  50_000,
  75_000,
  100_000,
  125_000,
  150_000,
  175_000,
  200_000,
  250_000,
  300_000,
  500_000
].each do |n|
  data = []
  test_data[n] = data
  n.times { data << rand(n) }
end

chart = BenchmarkChart.new 'Linear Hash - SEARCH'
ds1 = chart.dataset 'blocks=2, size=2'
ds2 = chart.dataset 'blocks=8, size=2'
ds3 = chart.dataset 'blocks=8, size=4'
ds4 = chart.dataset 'blocks=8, size=8'
ds5 = chart.dataset 'blocks=16, size=2'
ds6 = chart.dataset 'blocks=16, size=4'
ds7 = chart.dataset 'Ruby Hash'

Benchmark.bm do |bench|
  test_data.keys.sort.each do |n|
    ds1[n] =
      bench.report("blocks=2, size=2 #{n}") do
        test_data[n].each { |i| linear_hash.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds2[n] =
      bench.report("blocks=8, size=2 #{n}") do
        test_data[n].each { |i| linear_hash8.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds3[n] =
      bench.report("blocks=8, size=4 #{n}") do
        test_data[n].each { |i| linear_hash8_4.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds4[n] =
      bench.report("blocks=8, size=8 #{n}") do
        test_data[n].each { |i| linear_hash8_8.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds5[n] =
      bench.report("blocks=16, size=2 #{n}") do
        test_data[n].each { |i| linear_hash16_2.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds6[n] =
      bench.report("blocks=16, size=4 #{n}") do
        test_data[n].each { |i| linear_hash16_4.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds7[n] =
      bench.report("Ruby Hash         #{n}") do
        test_data[n].each { |i| ruby_hash.has_key?(i) }
      end
  end
end

chart.plot
