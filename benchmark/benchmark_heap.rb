require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/heap/BinaryHeap'
require_relative '../lib/heap/FibonacciHeap'
require_relative '../lib/heap/BinomialHeap'
require_relative '../lib/heap/d_ary_heap'
require_relative '../lib/heap/minmax'
require_relative '../lib/heap/van_emde_boyas'

$test_data = []

$num_of_records = [
  100,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  10_000,
  15_000,
  20_000,
  30_000,
  40_000,
  50_000,
  75_000,
  100_000,
  125_000,
  150_000,
  175_000,
  200_000,
  225_000,
  250_000,
  275_000,
  300_000,
  325_000,
  400_000
]
$num_of_records = 0.step(20_000, 1000).to_a
$num_of_records += 20_000.step(400_000, 20_000).to_a

$num_of_records = [10]
while $num_of_records.last < 200_000
  $num_of_records << ($num_of_records.last * 1.2).to_i
end
# $num_of_records = 100_000.step(400_000, 50_000)
$num_of_records.max.times do |_i|
  # break if n >= 100_000

  size = 0

  if size > 0
    op = rand(10_000) < 0.0 ? :pop : :insert
  else
    op = :insert
  end
  $test_data << if op == :pop
            [ :pop, 0]
          else
            [ :insert, rand(2**16)]
          end

end

chart = BenchmarkChart.new('Insert-only benchmark')
ds1 = chart.dataset 'BinaryHeap'
ds2 = chart.dataset 'FibonacciHeap'
ds3 = chart.dataset 'BinomialHeap'
ds4 = chart.dataset 'MinMaxHeap'
ds5 = chart.dataset 'van Emde Boyas'
ds6 = chart.dataset 'd-ary heap (d=3)'
ds7 = chart.dataset 'd-ary heap (d=4)'
ds8 = chart.dataset 'd-ary heap (d=8)'

Benchmark.bm do |bench|
  def report(bench, ds, label: nil, &block)
    $num_of_records.each do |n|
      GC.start
      GC.disable
      heap = block.call
      label = heap.class if label.nil?
      ds[n] = bench.report("#{label}            #{n}") do
        0.upto(n-1) do |i|
          op, val = $test_data[i]
          if op == :insert
            heap.push val
          else
            heap.pop
          end
        end
      end
      GC.enable
    end
  end

  report(bench, ds1) { BinaryHeap.new }
  report(bench, ds2) { FibonacciHeap.new }
  report(bench, ds3) { BinomialHeap.new }
  report(bench, ds4) { MinMaxHeap.new }
  report(bench, ds5) { VanEmdeBoyasTree.new }
  report(bench, ds6, label: "3-ary heap") { DAryHeap.new(3) }
  report(bench, ds7, label: "4-ary heap") { DAryHeap.new(4) }
  report(bench, ds8, label: "8-ary heap") { DAryHeap.new(8) }
end

chart.plot unless ARGV[0] == '--no-plot'
