require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/tree/avltree'
require_relative '../lib/tree/bst'
require_relative '../lib/tree/treap'
require_relative '../lib/tree/treap_persistent'

test_data = {}

[100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 10_000, 20_000, 30_000, 40_000, 50_000].each do |n|
  # 100_000,
  # 200_000
  data = []
  test_data[n] = data
  n.times { data << rand(n) }
end
# Działanie benchmarku:
# tworzy dwa drzewa T1 T2 o różnych rozmiarach, np.:
# T1 - 10, T2 - 90
# T1 - 20, T2 - 80
# ...
# T1 - 90, T2 - 10
# T1 - 100, T2 - 900
# T1 - 200, T2 - 800
# itd.
# i następnie wykonuje operację UNION
# Treap nie ma dedykowanej funkcji UNION - jest za pomocą INSERT
# Proste wnioski:
# np. dla 100k elementów
# Persistent Treap
#   - dla rozkładu 10/90 najmniejszy - 0.19
#   - dla 50%/50% największy - 0.5
#   - potem znowu maleje, dla 90/10 - 0.37
# Zaś Treap:
#   - dla 10/90 - dużo większy - 1.4
#   - potem maleje dla 90/10 - 0.16
#   - prosto to wytłumaczyć - do "małego" jest dodawane są rekord po rekordize z "dużego"

Benchmark.bm do |bench|
  chart = BenchmarkChart.new

  test_data.keys.sort.each do |n|
    [0.1, 0.2, 0.5, 0.7, 0.9].each do |factor|
      percentage = (factor * 100).to_i
      ds1 = chart.dataset 'Treap %d%%' % [percentage]
      ds2 = chart.dataset 'Persistent Treap %d%%' % [percentage]
      ds1.pointtype = 2
      ds2.pointtype = 5
      treap1 = Treap.new
      treap2 = Treap.new

      persistent_treap1 = PersistentTreap.new
      persistent_treap2 = PersistentTreap.new

      size1 = (factor * n).to_i
      size2 = n - size1
      size1.times do
        x = rand
        treap1.insert x
        persistent_treap1.insert x
      end
      size2.times do
        x = rand
        treap2.insert x
        persistent_treap2.insert x
      end

      ds1[n] =
        bench.report('Treap            %d-%d (%d)%%' % [size1, size2, percentage]) do
          # I don't have MERGE for Treap, only for persistent version
          # So I'm using brute-force approach for now

          # treap1.merge(treap2)
          treap2.visit_inorder { |node| treap1.insert node.value }
        end

      ds2[n] =
        bench.report('Persistent Treap %d-%d (%d)%%' % [size1, size2, percentage]) do
          persistent_treap1.union(persistent_treap2)
        end
    end
  end
  chart.plot
end
