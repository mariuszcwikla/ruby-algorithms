require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/probabilistic/skiplist'
require_relative '../lib/probabilistic/skiplist2'

# Benchmark for skip lists with different probabilities

test_data = {}

[100, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 10_000, 15_000, 20_000].each do |n|
  data = []
  test_data[n] = data
  n.times { data << rand(n) }
end

probabilities = [0.01, 0.1, 0.2, 0.4, 0.5, 0.7, 0.8, 0.9, 0.99]
chart = BenchmarkChart.new
ds1 = chart.dataset "p=#{p} (v1)"
ds2 = chart.dataset "p=#{p} (v2)"

probabilities.each do |p|
  Benchmark.bm do |bench|
    test_data.keys.sort.each do |n|
      s = SkipList.new(p)
      ds1[n] =
        bench.report("p=#{p}     #{n} (v1)") do
          test_data[n].each { |i| s.insert i }
        end
    end

    test_data.keys.sort.each do |n|
      s2 = SkipList2.new(p)
      ds2[n] =
        bench.report("p=#{p}     #{n} (v2)") do
          test_data[n].each { |i| s2.insert i }
        end
    end
  end
end

chart.plot
