require 'benchmark'
require_relative 'gnuplot_chart'
require_relative '../lib/tree/avltree'
require_relative '../lib/tree/bst'
require_relative '../lib/tree/btree'
require_relative '../lib/tree/splay_tree'
require_relative '../lib/tree/treap'
require_relative '../lib/tree/treap_persistent'
require_relative '../lib/probabilistic/skiplist'
require_relative '../lib/probabilistic/skiplist2'
require_relative '../lib/persistent/partial/fat/bst'
require_relative '../lib/hash/linear_hash'
require_relative '../lib/hash/linear_hash_base2'
require_relative '../lib/heap/van_emde_boyas'

test_data = {}

avl = AVLTree.new
bst = BinarySearchTree.new
treap = Treap.new
btree = BTree.new
splay_tree = SplayTree.new
persistent_treap = PersistentTreap.new
skiplist = SkipList.new
skiplist2 = SkipList2.new
fat_bst = Fat::BinarySearchTree.new
linear_hash = LinearHash.new
linear_hash16_4 = LinearHash.new(initial_blocks: 16, block_size: 4)
linear_hash_base2 = LinearHashBase2.new
veb = VanEmdeBoyasTree.new

puts 'populating data structures'

INPUT_SIZE = 100_000
[
  avl,
  bst,
  btree,
  splay_tree,
  treap,
  persistent_treap,
  skiplist,
  skiplist2,
  fat_bst,
  linear_hash,
  linear_hash16_4,
  linear_hash_base2,
  veb
].each do |ds|
  n = INPUT_SIZE

  print("Inserting #{n} into %20s, " % [ds.class])
  t = Time.now
  n.times do
    x = rand(1_000_000)
    ds.insert x
  end
  time = (Time.now - t).round(3)
  puts "done in #{time}s"
end

puts 'population finished'

[
  100,
  1000,
  2000,
  3000,
  4000,
  5000,
  6000,
  7000,
  10_000,
  20_000,
  30_000,
  40_000,
  50_000,
  75_000,
  100_000,
  125_000,
  150_000,
  175_000,
  200_000
].each do |n|
  data = []
  test_data[n] = data
  n.times { data << rand(n) }
end

chart = BenchmarkChart.new
ds1 = chart.dataset 'AVL'
ds2 = chart.dataset 'BST'
ds3 = chart.dataset 'BTree'
ds_splay = chart.dataset 'Splay Tree'
ds4 = chart.dataset 'Treap'
ds5 = chart.dataset 'Persistent Treap'
ds6 = chart.dataset 'SkipList'
ds7 = chart.dataset 'SkipList2'
ds8 = chart.dataset 'Fat-BST'
ds9 = chart.dataset 'Linear Hash'
ds10 = chart.dataset 'Linear Hash (blocks=16, size=4)'
ds11 = chart.dataset 'Linear Hash base2'
ds12 = chart.dataset 'van Emde Boyas'

Benchmark.bm do |bench|
  test_data.keys.sort.each do |n|
    ds1[n] =
      bench.report("AVL              #{n}") do
        test_data[n].each { |i| avl.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds2[n] =
      bench.report("BST              #{n}") do
        test_data[n].each { |i| bst.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds3[n] =
      bench.report("BTree            #{n}") do
        test_data[n].each { |i| btree.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds_splay[n] =
      bench.report("Splay Tree       #{n}") do
        test_data[n].each { |i| splay_tree.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds4[n] =
      bench.report("Treap            #{n}") do
        test_data[n].each { |i| treap.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds5[n] =
      bench.report("Persistent Treap #{n}") do
        test_data[n].each { |i| persistent_treap.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    break if n > 50_000

    # Skip lists (v1) suck also in searching. See benchmark_tree.rb
    ds6[n] =
      bench.report("SkipList         #{n}") do
        test_data[n].each { |i| skiplist.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds7[n] =
      bench.report("SkipList v2      #{n}") do
        test_data[n].each { |i| skiplist2.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds8[n] =
      bench.report("Fat-BST      #{n}") do
        test_data[n].each { |i| fat_bst.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds9[n] =
      bench.report("Linear Hash  #{n}") do
        test_data[n].each { |i| linear_hash.include? i }
      end
  end

  test_data.keys.sort.each do |n|
    ds10[n] =
      bench.report("Linear Hash 16/4  #{n}") do
        test_data[n].each { |i| linear_hash16_4.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds11[n] =
      bench.report("Linear Hash base2  #{n}") do
        test_data[n].each { |i| linear_hash_base2.include? i }
      end
  end
  test_data.keys.sort.each do |n|
    ds12[n] =
      bench.report("van Emde Boyas   #{n}") do
        test_data[n].each { |i| veb.include? i }
      end
  end

end

chart.plot
