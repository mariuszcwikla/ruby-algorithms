require_relative '../test_helper'
require_relative '../../lib/math/euclid'

class EuclidTest < Minitest::Test

  def test_centroid
    assert_equal [2.0, 3.0], centroid([ [1, 1], [2, 2], [3, 6] ], 2)
  end

  def test_radius
    points = [ [1, 1], [2, 2], [3, 6] ]
    c = centroid(points, 2)
    assert_in_epsilon 3.1622, radius(points, c)
  end
end
