# INCLUDE this module in minitest
# until test/unit is completely replaced by minitest
module TestUnitCompatibility
  def assert_true(x)
    assert x
  end

  def assert_false(x)
    assert !x
  end
end

module CollectionInsertTestMixin
  def test_empty
    assert @subject.empty?
  end

  def test_include_1
    @subject.insert(5)

    assert @subject.include? 5
    refute @subject.include? 6
    refute @subject.include? 4

    assert_equal 1, @subject.size
  end

  def test_include_2
    @subject.insert(5)
    @subject.insert(10)

    assert_true @subject.include? 5
    assert_true @subject.include? 10

    refute @subject.include?(-2)

    assert_equal 2, @subject.size
  end

  def test_include_3
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)

    assert_true @subject.include? 5
    assert_true @subject.include? 10
    assert_true @subject.include?(15)

    assert_equal 3, @subject.size
  end

  def test_include_4
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(0)

    assert_true @subject.include? 5
    assert_true @subject.include? 10
    assert_true @subject.include? 15
    assert_true @subject.include? 0

    assert_equal 4, @subject.size
  end

  def test_include_5
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(0)
    @subject.insert(2)

    assert_true @subject.include? 5
    assert_true @subject.include? 10
    assert_true @subject.include? 0
    assert_true @subject.include? 15
    assert_true @subject.include? 2

    assert_equal 5, @subject.size
  end

  def test_include_6
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(0)
    @subject.insert(7)
    @subject.insert(3)

    assert_true @subject.include? 5
    assert_true @subject.include? 10

    assert_true @subject.include? 0

    assert_true @subject.include? 3

    assert_false @subject.include? 2
    assert_false @subject.include? 1
    assert_false @subject.include?(-10)
    assert_false @subject.include? 20

    assert_equal 6, @subject.size
  end

  def test_include_7
    @subject.insert 0
    @subject.insert 1
    @subject.insert 2
    @subject.insert 3
    @subject.insert 4
    @subject.insert 5
    @subject.insert 6

    assert @subject.include? 0
    assert @subject.include? 1
    assert @subject.include? 2
    assert @subject.include? 3
    assert @subject.include? 4
    assert @subject.include? 5
    assert @subject.include? 6

    assert_equal 7, @subject.size
  end

  def test_include_8
    @subject.insert 0
    @subject.insert 1
    @subject.insert 2
    @subject.insert 3
    @subject.insert 4
    @subject.insert 5
    @subject.insert 6
    @subject.insert 7

    assert @subject.include? 0
    assert @subject.include? 1
    assert @subject.include? 2
    assert @subject.include? 3
    assert @subject.include? 4
    assert @subject.include? 5
    assert @subject.include? 6
    assert @subject.include? 7

    assert_equal 8, @subject.size
  end

  def test_include_9
    @subject.insert 0
    @subject.insert 1
    @subject.insert 2
    @subject.insert 3
    @subject.insert 4
    @subject.insert 5
    @subject.insert 6
    @subject.insert 7
    @subject.insert 8

    assert @subject.include? 0
    assert @subject.include? 1
    assert @subject.include? 2
    assert @subject.include? 3
    assert @subject.include? 4
    assert @subject.include? 5
    assert @subject.include? 6
    assert @subject.include? 7
    assert @subject.include? 8

    assert_equal 9, @subject.size

  end

  def test_include_100
    max = 100
    (0...max).each do |x|
      @subject.insert x
      (0..x).each { |i| assert(@subject.include?(i), "#{x} is lost") }
    end
    (0...max).each { |x|
      assert(@subject.include?(x), "#{x} is lost")
    }
  end

  def test_include_100_random
    if ENV['CI']
      puts 'skipping test_include_100_random'
      skip
    end
    max = 100
    data = []
    (0...max).each do |_x|
      i = rand(10_000)
      data << i
      @subject.insert i

      data.each do |x|
        unless @subject.include?(x)
          puts "\n#{x} is lost, data: #{data.join ','}\n"
          assert(false, "#{x} is lost")
        end
      end
    end
  end

  def test_include_random_case1
    # case detected when "random test" was executed
    data = [9439, 9979, 2620, 4852, 6706, 497, 9838, 2843, 1485, 8889, 1714]
    data.each do |x|
      @subject.insert x
    end

    assert @subject.include? 2620
  end

  def test_insert_pbt
    property_of{
      size = range(1, 1000)
      ShrinkableArray.new(array(size){range(0, 2**32)})
    }.check{|arr|
      t = @subject.class.new
      arr.each { |i| t.insert i }
      arr.each do |i|
        assert t.include?(i)
      end
    }
  end
end

module CollectionRemoveTestMixin
  def test_remove1
    @subject.insert(0)
    assert_true @subject.include?(0)

    @subject.remove(0)
    assert_false @subject.include?(0)

    assert_true @subject.empty?
    assert_equal 0, @subject.size
  end

  def test_add2_increasing_remove_root
    @subject.insert(0)
    @subject.insert(2)

    @subject.remove(0)
    assert_false @subject.include? 0
    assert_true @subject.include? 2

    assert_equal 1, @subject.size
  end

  def test_add2_decreasing_remove_root
    @subject.insert(2)
    @subject.insert(0)

    @subject.remove(2)
    assert_false @subject.include? 2
    assert_true @subject.include?(0)

    assert_equal 1, @subject.size
  end

  def test_remove3
    @subject.insert 0
    @subject.insert 2
    @subject.insert 3

    @subject.remove(3)
    assert_false @subject.include?(3)
    assert_true @subject.include?(0)
    assert_true @subject.include?(2)
  end

  def test_remove6
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(0)
    @subject.insert(7)
    @subject.insert(3)

    @subject.remove(0)
    assert_false @subject.include?(0)

    @subject.remove 5
    assert_false @subject.include? 5

    @subject.remove 15
    assert_false @subject.include? 15

    assert_true @subject.include? 10
    assert_true @subject.include? 7
    assert_true @subject.include? 3

    assert_equal 3, @subject.size
  end

  def test_insert_many_remove_one_pbt
    property_of{
      x = array(range(2, 100)){ range(0, 1_000_000) }
      guard x.uniq.size == x.size
      ShrinkableArray.new(x)
    }.check{|arr|
      t = @subject.class.new
      arr.each { |i| t.insert i }
      t.remove arr[0]
      refute t.include?(arr[0]), "#{arr[0]} should be removed"
      arr[1..-1].each do |x|
        assert t.include?(x), "#{x} is missing"
      end
    }
  end

  def test_insert_many_remove_one_example
    # setup
    arr = [32, 46, 65, 40]
    arr.each { |i|
      @subject.insert i
    }
    @subject.remove arr[0]
    refute @subject.include?(arr[0]), "#{arr[0]} should be removed"
    arr[1..-1].each do |x|
      assert @subject.include?(x), "#{x} is missing"
    end
  end

end

module SuccessorTestMixin

  def test_successor_1
    @q.insert 1

    assert_equal 1, @q.successor(0)
    assert_nil @q.successor(1)
  end

  def test_successor_2
    @q.insert 5
    @q.insert 7

    assert_equal 5, @q.successor(4)
    assert_equal 7, @q.successor(5)
    assert_equal 7, @q.successor(6)
    assert_nil @q.successor(7)
  end

  def test_successor_3
    @q.insert 5
    @q.insert 6
    @q.insert 7

    assert_equal 5, @q.successor(4)
    assert_equal 6, @q.successor(5)
    assert_equal 7, @q.successor(6)
    assert_nil @q.successor(7)
  end

  def test_successor_pbt
    property_of{
      x = array{ range(0, 2**16) }
      guard x.uniq.size == x.size
      ShrinkableArray.new(x)
    }.check{|arr|
      t = @subject.class.new
      arr.each { |i| t.insert i }
      arr = arr.array.sort
      1.upto(arr.size - 1).each do |i|
        assert_equal arr[i], t.successor(arr[i - 1])
        assert_equal arr[i], t.successor(arr[i] - 1)
      end
    }
  end

  def test_successor_example
    arr = [5, 15, 7, 8]
    t = VanEmdeBoyasTree.new(16)
    arr.each { |i| t.insert i }
    arr.sort!
    1.upto(arr.size - 1).each do |i|
      assert_equal arr[i], t.successor(arr[i - 1])
      assert_equal arr[i], t.successor(arr[i] - 1)
    end
  end
end

module PredecessorTestMixin
  def test_predecessor_1
    @q.insert 1

    assert_equal 1, @q.predecessor(2)
    assert_nil @q.predecessor(1)
  end

  def test_predecessor_2
    @q.insert 5
    @q.insert 7

    assert_equal 5, @q.predecessor(6)
    assert_equal 5, @q.predecessor(7)
    assert_equal 7, @q.predecessor(8)
    assert_nil @q.predecessor(5)
  end

  def test_predecessor_3
    @q.insert 4
    @q.insert 5
    @q.insert 7

    assert_equal 4, @q.predecessor(5)
    assert_equal 5, @q.predecessor(6)
    assert_equal 5, @q.predecessor(7)
    assert_equal 7, @q.predecessor(8)
    assert_nil @q.predecessor(4)
  end

  def test_predecessor_pbt
    property_of{
      x = array{ range(0, 2**16) }
      guard x.uniq.size == x.size
      ShrinkableArray.new(x)
    }.check{|arr|
      t = @subject.class.new
      arr.each { |i| t.insert i }
      arr = arr.array.sort
      0.upto(arr.size - 2).each do |i|
        assert_equal arr[i], t.predecessor(arr[i + 1])
        assert_equal arr[i], t.predecessor(arr[i] + 1)
      end
    }
  end

  def test_predecessor_example
    arr = [5, 15, 7, 8]
    t = VanEmdeBoyasTree.new(16)
    arr.each { |i| t.insert i }
    arr.sort!
    0.upto(arr.size - 2).each do |i|
      assert_equal arr[i], t.predecessor(arr[i + 1])
      assert_equal arr[i], t.predecessor(arr[i] + 1)
    end
  end
end

module CollectionTestMixin
  include CollectionInsertTestMixin
  include CollectionRemoveTestMixin
  # TODO: add SuccessorTestMixin/PredecessorTestMixin
end
