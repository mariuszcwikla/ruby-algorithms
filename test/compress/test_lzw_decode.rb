require_relative '../test_helper'
require_relative '../../lib/compress/lzw'

class LZWDecodingTest < Minitest::Test
  [
    [[97], 'a'],
    [[97, 97], 'aa'],
    [[97, 256], 'aaa'],
    [[97, 256, 97], 'aaaa'],
    [[97, 256, 256], 'aaaaa'],
    [[97, 256, 257], 'aaaaaa'],
    [[97, 98], 'ab'],
    [[97, 98, 97], 'aba'],
    [[97, 98, 256], 'abab'],
    [[97, 98, 257, 256], 'abbbab'],
    [[97, 98, 257, 256, 259, 98], 'abbbababab']
  ].each_with_index do |s, i|
    define_method("test_decode#{i}") do
      assert_equal s[1], lzw_decode(s[0])[0]
    end
  end

  def test_abab
    assert_equal 'abbbababab', lzw_decode([97, 98, 257, 256, 259, 98])[0]
  end
end
