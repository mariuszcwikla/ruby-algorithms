require_relative '../test_helper'
require_relative '../../lib/compress/lzw'

class LZWCompressionTest < Minitest::Test
  def test_1
    string = 'abababa'
    compressed = lzw_compress(string).unpack('C*')
    expected = [48, 152, 160, 16, 32]
    assert_equal expected, compressed
  end

  def test_compress_to_bits
    string = 'abababa'
    compressed = lzw_compress_to_bits(string)
    assert_equal '001100001001100010100000000100000010', compressed
  end
  %w[aa aaa aaaa aaaaa aaaaaa aaaaaaa aaaaaaaa abababababababababababa].each do |string|
    define_method("test_#{string}") do
      compressed = lzw_compress(string)
      assert compressed.instance_of?(String)

      decompressed = lzw_decompress(compressed)
      assert_equal string, decompressed
    end
  end

  def test_compress_decompress
    property_of { sized(range(5, 100)) {string(:alpha)} }.check do |s|
      compressed = lzw_compress(s)
      assert compressed.instance_of?(String)
      assert_equal s, lzw_decompress(compressed)
    end
  end

  def test_compress_decompress_lorem_ipsum
    # here i'm using lorem ipsum to constrain words that are picked up, i.e. to prevent from being completely random (to achieve lower entropy)
    lorem_ipsum = %w[Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]
    property_of { array(50) {choose(*lorem_ipsum)}.join ' '}
    .check do |s|
      compressed = lzw_compress(s)
      assert compressed.instance_of?(String)
      # if below assertion starts failing then rantly found a random case with high entropy. However in my tests compression ratio is usually 50-60%
      # uncomment below "puts" to see ratio
      # puts "#{compressed.length.to_f / s.length}"
      assert compressed.length < s.length   
      assert_equal s, lzw_decompress(compressed)
    end
  end
end
