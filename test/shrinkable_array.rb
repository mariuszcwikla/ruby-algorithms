# Rantly does shrinking by shrinking array and shrinking individual elements.
# I need to shrink array only and withouty modyfing individual elements.
# This class provides "shrinkable array" without shrinking elements.

# It's a copy-paste from Deflating with few lines removed
class ShrinkableArray
  def initialize(a)
    @array = a
    @position = a.size - 1
  end

  def [](i)
    @array[i]
  end

  def []=(i, value)
    @array[i] = value
  end

  def length
    @array.length
  end

  def size
    length
  end

  def to_s
    @array.to_s.insert(1, 'S ')
  end

  def inspect
    to_s
  end

  def each(&block)
    @array.each(&block)
  end

  attr_reader :array

  def shrink
    shrunk = @array.dup
    if @position >= 0
      shrunk.delete_at(@position)
      @position -= 1
    end
    ShrinkableArray.new(shrunk)
  end

  def retry?
    @position >= 0
  end

  def shrinkable?
    !@array.empty?
  end
end