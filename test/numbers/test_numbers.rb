require_relative '../test_helper'
# require_relative 'test_helper'
require_relative '../../lib/numbertheory/numbers'

class Test_numbers < Minitest::Test
  def test_gcd_10_5
    assert_equal(5, Numbers.gcd(10, 5))
  end

  def test_gcd_10_6
    assert_equal(2, Numbers.gcd(10, 6))
  end

  def test_gcd_recursive_10_5
    assert_equal(5, Numbers.gcd_recursive(10, 5))
  end

  def test_gcd_recursive_10_6
    assert_equal(2, Numbers.gcd_recursive(10, 6))
  end

  def test_mod_inverse_11_5
    assert_equal(9, Numbers.modular_inverse(5, 11))
  end

  def test_gcd_by_subtraction_10_5
    assert_equal(5, Numbers.gcd_by_subtraction(10, 5))
  end

  def test_gcd_by_subtraction_10_6
    assert_equal(2, Numbers.gcd_by_subtraction(10, 6))
  end

  def test_binary_gcd_10_5
    assert_equal(5, Numbers.binary_gcd(10, 5))
  end
  def test_binary_gcd
    property_of {
      [
        range(2, 1000000),
        range(2, 1000000)
      ]
    }.check {|a, b|
      g1 = Numbers.binary_gcd(a, b)
      g2 = Numbers.gcd(a, b)
      assert_equal g2, g1
    }
  end

  def test_mod_inverse_11
    data = { 2 => 6, 3 => 4, 5 => 9, 6 => 2, 7 => 8, 8 => 7, 9 => 5, 10 => 10 }

    # assert_equal(6, Numbers.modular_inverse(2, 11))
    # assert_equal(4, Numbers.modular_inverse(3, 11))
    data.each { |x, y| assert_equal(y, Numbers.modular_inverse(x, 11)) }
  end

  def test_mod_inverse_12
    # assert_equal(-1, Numbers.modular_inverse(4, 12))

    data = { 2 => -1, 3 => -1, 5 => 5, 6 => -1, 7 => 7, 8 => -1, 9 => -1, 10 => -1 }
    data.each { |x, y| assert_equal(y, Numbers.modular_inverse(x, 12), "inv(#{x}, 12)") }
  end

  def test_mod_inverse_5_11
    # TODO
    assert_equal(1, Numbers.modular_inverse(11, 5))
  end

  def test_modexp_2_2
    assert_equal(4, Numbers.modexp(2, 2, 101))
  end

  def test_modexp_2_3
    assert_equal(8, Numbers.modexp(2, 3, 101))
  end

  def test_modexp_2_4
    assert_equal(16, Numbers.modexp(2, 4, 101))
  end

  def test_modexp_2_5
    assert_equal(32, Numbers.modexp(2, 5, 101))
  end

  def test_modexp_2_13
    assert_equal(11, Numbers.modexp(2, 13, 101))
  end

  def test_modexp_6_3
    assert_equal(0, Numbers.modexp(6, 1, 3))
  end

  def test_modexp_pbt
    property_of {
      [
        range(0, 20),
        range(0, 20),
        range(2, 20)
      ]
    }.check {|a, b, n|
      expected = (a**b) % n
      actual = Numbers.modexp(a, b, n)
      assert_equal expected, actual
    }
  end

  def test_modexp_basic_2_exp_mod_101
    data = { 1 => 2, 2 => 4, 3 => 8, 4 => 16, 5 => 32, 6 => 64 }

    data.each do |exponent, expected|
      assert_equal(expected, Numbers.modexp(2, exponent, 101))
    end
  end

  def test_modexp_2_exp_mod_101
    data = { 7 => 27, 8 => 54, 9 => 7, 10 => 14, 11 => 28, 12 => 56, 13 => 11, 14 => 22, 15 => 44 }

    data.each do |exponent, expected|
      assert_equal(expected, Numbers.modexp(2, exponent, 101))
    end
  end

  def test_modexp_random
    #  fail
    data = { [13, 15, 101] => (13**15) % 101, [27, 18, 1051] => (27**18) % 1051 }
    data.each do |vals, expected|
      a, b, n = vals
      assert_equal(expected, Numbers.modexp(a, b, n))
    end
  end

  def test_modexp_big
    # ten przyklad jeszcze ruby ogarnie
    # tj. 213123 ** 343214  % 3173183217831
    assert_equal(56_966_414_631, Numbers.modexp(213_123, 343_214, 3_173_183_217_831))
  end

  def test_modex_verybig
    # ale tutaj 31827895423 ** 47238974982374982730  % 4273847
    # wywalilby bledem
    # dlatego trzebaby posluzyc sie algorytmem "fast modular exponent"
    assert_equal(100_006, Numbers.modexp(31_827_895_423, 472_374_982_730, 4_273_847))
  end

  def test_modex_huge
    # ale tutaj 31827895423 ** 47238974982374982730  % 4273847
    # wywalilby bledem
    # dlatego trzebaby posluzyc sie algorytmem "fast modular exponent"
    assert_equal(
      857_366,
      Numbers.modexp(318_278_582_904_809_123_842_048_023, 245_439_580_358_043_472_374_982_730, 4_273_847)
    )
  end

  def test_sieve()
    primes = Numbers.primes_by_sieve(20)
    expected = [2,3,5,7,11,13,17,19]
    assert_equal expected, primes
  end

  def test_factorize_brute()
    factors = Numbers.factorize_brute(300)
    expected = {2 => 2, 3 => 1, 5 => 2}
    assert_equal expected, factors

  end

  [
    [20, 8],
    [15, 8],
    [10, 4],
    [13, 12],
    [27, 18]
  ].each do |n, expected|
    define_method("test_euler_#{n}_is_equal_to_#{expected}") do 
      assert_equal expected, Numbers.euler(n)
    end
  end

  def test_euler_rantly
    property_of{
      range(2, 100_000)
    }.check(20) do |n|
      coprimes = 0
      1.upto(n) do |i|
        coprimes += 1 if n.gcd(i) == 1
      end

      assert_equal coprimes, Numbers.euler(n)
    end
  end

  [
    [12, 2],
    [13, 12],
    [14, 6],
    [17, 16],
    [20, 4],
    [21, 6],
    [35, 12],
    [36, 6]
  ].each do |n, expected|
    define_method("test_carmichael_#{n}_is_equal_to_#{expected}") do 
      assert_equal expected, Numbers.carmichael(n)
    end
  end
end
