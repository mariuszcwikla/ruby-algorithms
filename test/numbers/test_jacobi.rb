require_relative '../test_helper' # TODO you need to fix PATH to this module!
require_relative '../../lib/numbertheory/jacobi' # TODO you need to fix PATH to this module!

class JacobiTest < Minitest::Test

  [
    [13, 13, 0],
    [13, 15, -1],
    [13, 17, 1],
    [13, 19, -1],
    [13, 21, -1],
    [13, 23, 1],
    [13, 25, 1],
  ].each do |a, n, j|
    define_method("test_jacobi_#{a}_#{n}_#{j}"){
      omit 'does not work :('
      assert_equal j, jacobi(a, n)
    }
  end
end
