require_relative '../test_helper'

require_relative '../../lib/sets/disjoint_sets'

class Test_DisjointSetsQuickFind < Minitest::Test
  def test_insert2_same_set
    s = DisjointSetsQuickFind.new
    s.insert 1
    s.insert 2
    assert_false s.same_set?(1, 2)
  end

  def test_insert4_merge_include
    s = DisjointSetsQuickFind.new
    s.insert 1
    s.insert 2
    s.insert 3
    s.insert 4
    s.merge 1, 2
    s.merge 3, 4

    assert_false s.same_set?(1, 3)
    assert_false s.same_set?(1, 4)

    assert_true s.same_set?(1, 2)
    assert_true s.same_set?(3, 4)
  end

  def test_values
    s = DisjointSetsQuickFind.new
    s.insert 1
    s.insert 2
    s.insert 3
    s.insert 4
    assert_equal [1, 2, 3, 4], s.values.sort
  end

  def test_values2
    s = DisjointSetsQuickFind.new
    s.insert 1
    s.insert 2
    s.insert 3
    s.insert 4
    s.merge 1, 2
    s.merge 3, 4
    s.merge 1, 3

    assert_equal [1, 2, 3, 4], s.values.sort
  end
end
