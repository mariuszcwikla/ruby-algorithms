require 'simplecov'
SimpleCov.start

# Use this file to run coverage for individual tests without need to include 'test_helper'
# Example:
#
# ruby -r ./test/coverage test/persistent/fat/minitest_fat_bst.rb
