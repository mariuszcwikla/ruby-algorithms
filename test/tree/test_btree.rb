require_relative '../test_helper'

require_relative '../../lib/tree/btree'
require_relative '../collection_test_mixins'

class BTree
  attr_reader :root
end

class TestBTree2 < Minitest::Test
  #include CollectionTestMixin
  include CollectionInsertTestMixin

  def setup
    @subject = BTree.new(node_capacity: 2)
  end

  def teardown
    @subject.visit do |n|
      next if n.leaf?
      n.children.each do |c|
        next if c.nil?
        assert n == c.parent, "#{c.inspect} has wrong parent: #{c.parent.inspect}. #{n.inspect} is expected"
      end
    end
  end
end

class TestBTree4 < Minitest::Test
  include CollectionInsertTestMixin

  def setup
    @subject = BTree.new(node_capacity: 4)
  end

end


class TestBTree < Minitest::Test
  include CollectionInsertTestMixin

  def setup
    @subject = BTree.new()
  end

end