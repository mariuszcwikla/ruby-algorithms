require_relative '../test_helper'

require_relative '../../lib/tree/btree2'
require_relative '../collection_test_mixins'

class BTree2
  attr_reader :root
end

class TestBTree2_2 < Minitest::Test
  #include CollectionTestMixin
  include CollectionInsertTestMixin

  def setup
    @subject = BTree2.new(degree: 2)
  end

  def teardown
    @subject.visit do |n|
      assert n.size <= 2 * @subject.degree - 1
      assert n.children.size <= 2 * @subject.degree unless n.leaf?
      next if n.leaf?
      n.children.each do |c|
        next if c.nil?
        assert n == c.parent, "#{c.inspect} has wrong parent: #{c.parent.inspect}. #{n.inspect} is expected"
      end
    end
  end
end

class TestBTree2_4 < Minitest::Test
  include CollectionInsertTestMixin

  def setup
    @subject = BTree2.new(degree: 4)
  end

end


class TestBTree2 < Minitest::Test
  include CollectionInsertTestMixin

  def setup
    @subject = BTree2.new()
  end

end