require_relative '../test_helper'

require_relative '../../lib/tree/avltree'
require_relative '../collection_test_mixins'

class TestAVLTree < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = AVLTree.new
  end

  def test_avl_insert5
    x = @subject.insert(5)
    assert_same x, @subject.root
    assert_nil x.parent
    assert_nil x.left
    assert_nil x.right
  end

  def test_avl_insert5_0
    x = @subject.insert(5)
    y = @subject.insert(0)
    assert_same y.parent, x
    assert_same x.left, y
  end

  def test_avl_insert5_10
    x = @subject.insert(5)
    y = @subject.insert(10)
    assert_same y.parent, x
    assert_same x.right, y
  end

  def test_avl_remove2_right_child
    @subject.insert(0)
    @subject.insert(1)

    assert_equal 0, @subject.root.value

    @subject.remove(1)
    assert_false @subject.empty?
    assert_true @subject.include?(0)
    assert_false @subject.include?(1)

    assert_nil @subject.root.left
    assert_nil @subject.root.right
  end

  def test_avl_remove2_left_child
    @subject.insert(0)
    @subject.insert(-1)

    assert_equal 0, @subject.root.value

    @subject.remove(-1)
    assert_false @subject.empty?
    assert_true @subject.include?(0)
    assert_false @subject.include?(-1)

    assert_nil @subject.root.left
    assert_nil @subject.root.right
  end

  def test_avl_remove2_root_right_child
    @subject.insert(0)
    @subject.insert(1)

    assert_equal 0, @subject.root.value

    @subject.remove(0)
    assert_false @subject.empty?
    assert_true @subject.include?(1)
    assert_false @subject.include?(0)

    assert_equal 1, @subject.root.value
    assert_nil @subject.root.left
    assert_nil @subject.root.right
  end

  def test_avl_remove2_root_left_child
    @subject.insert(0)
    @subject.insert(-1)

    assert_equal 0, @subject.root.value

    @subject.remove(0)
    assert_false @subject.empty?
    assert_true @subject.include?(-1)
    assert_false @subject.include?(0)

    assert_equal(-1, @subject.root.value)
    assert_nil @subject.root.left
    assert_nil @subject.root.right
  end

  def test_avl_3elements_remove_right
    @subject.insert 2
    @subject.insert 0
    @subject.insert 5

    @subject.remove 2

    assert_equal([0, 5], @subject.elements)
  end

  #              0
  #             /\
  # remove->  -5  5
  #           /
  #         -10
  def test_avl_remove4
    @subject.insert(0)
    @subject.insert(5)
    @subject.insert(-5)
    @subject.insert(-10)

    # ensure that this is the tree
    assert_equal(-5, @subject.root.left.value)
    assert_equal(-10, @subject.root.left.left.value)
    assert_nil @subject.root.left.right

    @subject.remove(-5)
    assert_false @subject.include?(-5)
    assert_true @subject.include?(-10)
  end

  #      0
  #     /\
  #   -5  5
  #  /  \
  # -10 -3
  def test_avl_remove5
    @subject.insert(0)
    @subject.insert(5)
    @subject.insert(-5)
    @subject.insert(-10)
    @subject.insert(-3)

    assert_equal(-5, @subject.root.left.value)
    assert_equal(-10, @subject.root.left.left.value)
    assert_equal(-3, @subject.root.left.right.value)

    # remove node that has both children -5

    @subject.remove(-5)
  end

  def test_avl_remove_another
    @subject.insert(5)
    @subject.insert(0)
    @subject.insert(-5)
    @subject.insert(-10)
    @subject.insert(-20)
    @subject.insert(-30)

    @subject.remove(-30)
    @subject.remove(-20)
    @subject.remove(5)
  end

  def test_avl_prev
    @subject.insert(5)
    @subject.insert(0)
    @subject.insert(-5)
    @subject.insert(7)
    @subject.insert(13)
    @subject.insert(8)
    assert_equal 8, @subject.prev(13)
    assert_equal 7, @subject.prev(8)
    assert_equal 5, @subject.prev(7)
    assert_equal 0, @subject.prev(5)
    assert_equal(-5, @subject.prev(0))

    assert_equal 13, @subject.prev(14)
    refute @subject.prev(-5)
  end

  def test_avl_not_greater
    @subject.insert(5)
    @subject.insert(0)
    @subject.insert(-5)
    @subject.insert(7)
    @subject.insert(13)
    @subject.insert(9)

    assert_equal 13, @subject.not_greater(13)
    assert_equal 13, @subject.not_greater(14)
    assert_equal 9, @subject.not_greater(9)
    assert_equal 9, @subject.not_greater(10)
    assert_equal 7, @subject.not_greater(8)
    assert_equal 7, @subject.not_greater(7)
    assert_equal 5, @subject.not_greater(6)
    assert_equal 5, @subject.not_greater(5)
    assert_equal 0, @subject.not_greater(3)
    assert_equal 0, @subject.not_greater(0)
    assert_equal(-5, @subject.not_greater(-1))
    assert_equal(-5, @subject.not_greater(-5))

  end

  def test_avl_prev_pbt
    property_of {
      x = array {integer(1000)}; guard x.size == x.uniq.size; x
    }
    .check { |a|
      t = AVLTree.new
      a.each {|x| t.insert x}

      a.sort!
      (1...a.size).each do |i|
        assert_equal a[i-1], t.prev(a[i])   # This searches "exact nodes"
      end
      a.each do |x|
        assert_equal x, t.prev(x+1) # This searches nodes that does not exist, but still should find prev properly
      end
      refute t.prev(a[0])
      refute t.prev(a[0]-1)
    }
  end

  def test_avl_not_greater_pbt
    property_of {
      x = array {integer(1000)}; guard x.size == x.uniq.size; x
    }
    .check { |a|
      t = AVLTree.new
      a.each {|x| t.insert x}

      a.sort!
      a.each do |a|
        assert_equal a, t.not_greater(a)   # This searches "exact nodes"
      end
      a.each do |x|
        next if t.include? x+1    # should be moved to guard...

        assert_equal x, t.not_greater(x+1) # This searches nodes that does not exist, but still should find prev properly
      end
      # refute_nil t.prev(a[0])
      # assert_nil t.prev(a[0]-1)
    }
  end

  def test_avl_next_pbt
    property_of {
      x = array {integer(1000)}; guard x.size == x.uniq.size; x
    }
    .check { |a|
      t = AVLTree.new
      a.each {|x| t.insert x}

      a.sort!
      (1...a.size).each do |i|
        assert_equal a[i], t.next(a[i-1])
      end
      a.each do |x|
        assert_equal x, t.next(x-1) # This searches nodes that does not exist, but still should find prev properly
      end
      refute t.next(a.last)
      refute t.next(a.last+1)
    }
  end
end

class AVLTreeMapTest < Minitest::Test

  def setup
    @map = AVLTreeMap.new
  end

  def test_put_get_array_indexing
    @map[4]=8
    assert_equal 8, @map[4]
  end

  def test_put_get
    @map.put(4, 8)
    assert_equal 8, @map.get(4)
  end

  def test_next_key
    @map[4] = 4
    @map[3] = 3
    assert_equal 4, @map.next_key(3)
    assert @map.next_entry(3).instance_of?(AVLTreeMap::Entry)
    assert_nil @map.next_key(4)
  end

  def test_prev_key
    @map[4] = 4
    @map[3] = 3
    assert_equal 3, @map.prev_key(4)
    assert @map.prev_entry(4).instance_of?(AVLTreeMap::Entry)
    assert_nil @map.prev_key(3)
  end

end
