require_relative '../test_helper'

require_relative '../../lib/tree/treap'
require_relative '../collection_test_mixins'
require_relative 'base_treap_test'

class TestTreap < Minitest::Test
  include CollectionTestMixin
  include TreapSplitJoinTests

  def setup
    @subject = Treap.new
  end
end
