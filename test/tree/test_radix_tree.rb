require_relative '../test_helper'
require_relative '../../lib/tree/radix_tree'

# A structural test for radix tree - need to test internal structure of the tree
class RadixTree
  attr_reader :root
end

class RadixTreeTest < Minitest::Test
  def setup
    @tree = RadixTree.new
  end

  def test_insert_1
    @tree.insert('na')
    assert_true @tree.include?('na')

    assert_equal(@tree.root.value, 'na')
    # assert_equal(@tree.root.children[0].value, "na")
  end

  def test_insert_2_substring
    @tree.insert('na')
    @tree.insert('nana')
    @tree.dump
    assert_true @tree.include?('na')
    assert_true @tree.include?('nana')

    assert_equal(@tree.root.value, 'na')
    assert_equal(@tree.root.children['n'].value, 'na')
  end

  def test_insert_2_substring_reverse_order
    @tree.insert('nana')
    @tree.insert('na') # this should trigger SPLIT

    assert_true @tree.include?('na')
    assert_true @tree.include?('nana')

    assert_equal(@tree.root.value, 'na')
    assert_equal(@tree.root.children['n'].value, 'na')
  end

  def test_insert_2_different
    @tree.insert('na')
    @tree.insert('foo')

    assert_true @tree.include?('na')
    assert_true @tree.include?('foo')

    assert_equal(@tree.root.value, '')
    assert_equal(@tree.root.children['n'].value, 'na')
    assert_equal(@tree.root.children['f'].value, 'foo')
  end

  def test_insert_bad_banana
    @tree << 'banana'
    @tree << 'bad'

    assert_true @tree.include?('bad')
    assert_true @tree.include?('banana')

    assert_equal(@tree.root.value, 'ba')
    assert_equal(@tree.root.children['n'].value, 'nana')
    assert_equal(@tree.root.children['d'].value, 'd')
    refute @tree.root.terminal

    assert @tree.root.children['n'].terminal
    assert @tree.root.children['d'].terminal
  end

  def test_insert_bad_banana_brada
    @tree << 'banana'
    @tree << 'bad'
    @tree << 'brada'

    assert_true @tree.include?('bad')
    assert_true @tree.include?('banana')
    assert_true @tree.include?('brada')

    assert_equal(@tree.root.value, 'b')
    assert_equal(@tree.root.children['a'].value, 'a')
    assert_equal(@tree.root.children['r'].value, 'rada')
    assert_equal(@tree.root.children['a'].children['n'].value, 'nana')
    assert_equal(@tree.root.children['a'].children['d'].value, 'd')

    refute @tree.root.terminal
    refute @tree.root.children['a'].terminal
    assert @tree.root.children['r'].terminal
    assert @tree.root.children['a'].children['n'].terminal
    assert @tree.root.children['a'].children['d'].terminal
  end

  def test_insert_banana_broda_bramka
    @tree << 'banana'
    @tree << 'broda'
    @tree << 'bramka'

    assert_true @tree.include?('banana')
    assert_true @tree.include?('broda')
    assert_true @tree.include?('bramka')

    assert_equal(@tree.root.value, 'b')
    assert_equal(@tree.root.children['a'].value, 'anana')
    assert_equal(@tree.root.children['r'].value, 'r')
    assert_equal(@tree.root.children['r'].children['o'].value, 'oda')
    assert_equal(@tree.root.children['r'].children['a'].value, 'amka')
    refute @tree.root.terminal

    # assert @tree.root.children[0].terminal
    # assert @tree.root.children[1].terminal
  end

  def test_insert_banana_bantumi_ban
    @tree << 'banana'
    @tree << 'bantumi'

    refute @tree.include?('ban')

    @tree << 'ban'

    assert @tree.include?('banana')
    assert @tree.include?('bantumi')
    assert @tree.include?('ban')
  end
end
