require_relative '../test_helper'
require_relative '../../lib/tree/interval_tree'

class IntervalTreeTest < Minitest::Test
  def setup
    @t = IntervalTree.new
  end

  def test_insert_1_left
    @t.insert(1, 2)
    assert_nil @t.interval_search(0, 1)
    assert_nil @t.interval_search(2, 5)

    refute_nil @t.interval_search(0.5, 1.5)
    refute_nil @t.interval_search(1.5, 1.7)
    refute_nil @t.interval_search(1.5, 3)
    refute_nil @t.interval_search(0, 5)
  end

  def test_insert_2
    @t.insert(1, 2)
    @t.insert(4, 5)

    assert_nil @t.interval_search(2, 4)

    refute_nil @t.interval_search(3, 5)
    refute_nil @t.interval_search(3, 7)
  end

  def test_insert_4
    @t.insert(1, 2)
    @t.insert(40, 50)
    @t.insert(20, 30)
    @t.insert(70, 80)

    assert_nil @t.interval_search(18, 22)
    assert_nil @t.interval_search(33, 38)
    assert_nil @t.interval_search(66, 68)

    refute_nil @t.interval_search(68, 88)
    refute_nil @t.interval_search(55, 75)
    refute_nil @t.interval_search(20, 100)
  end
end
