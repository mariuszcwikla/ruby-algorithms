require_relative '../test_helper'
require_relative '../../lib/tree/suffix_tree'

class BruteForceSuffixTreeTest < Minitest::Test
  def test_banana
    t = build_suffix_tree_brute_force('banana')

    assert t.matches?('banana')

    assert t.matches?('ana')
    assert t.matches?('nana')
    refute t.matches?('baanana')
    refute t.matches?('aana')
  end
end
