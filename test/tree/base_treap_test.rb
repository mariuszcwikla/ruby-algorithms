module TreapSplitJoinTests
  def test_include_hardcoded_priorities
    @subject.insert(5, 0.5)
    @subject.insert(10, 0.9)
    @subject.insert(15, 0.75)
    @subject.insert(-2, 0.3)
    @subject.insert(7, 0.4)
    @subject.insert(3, 0.55)

    assert_true @subject.include? 5
    assert_true @subject.include? 10

    assert_true @subject.include?(-2)
    assert_true @subject.include? 3

    assert_false @subject.include? 0
    assert_false @subject.include? 1
    assert_false @subject.include?(-10)
    assert_false @subject.include? 20
  end

  def test_split
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(-2)
    @subject.insert(7)
    @subject.insert(3)

    assert_priority(@subject.root)

    a, b = @subject.split(8)

    assert_true a.include?(5)
    assert_true a.include?(-2)
    assert_true a.include?(3)
    assert_true a.include?(7)

    assert_false a.include?(10)
    assert_false a.include?(15)

    assert_false b.include?(5)
    assert_false b.include?(-2)
    assert_false b.include?(3)
    assert_false b.include?(7)

    assert_true b.include?(10)
    assert_true b.include?(15)

    assert_priority(a.root)
    assert_priority(b.root)
  end

  def assert_priority(node)
    return if node.nil?

    unless node.left.nil?
      msg =
        'parent priority should be higher than child priority %s (%.2f) < %s (%.2f)' %
          [node.value, node.priority, node.left.value, node.left.priority]

      assert_true node.priority > node.left.priority, msg
      assert_priority(node.left)
    end

    # unless node.right.nil?
    # unless jest raportowane przez rubocop

    return if node.right.nil?

    msg =
      'parent priority should be higher than child priority %s (%.2f) < %s (%.2f)' %
        [node.value, node.priority, node.right.value, node.right.priority]

    assert_true node.priority > node.right.priority, msg
    assert_priority(node.right)
  end

  def test_split_1_lower
    @subject.insert(10)
    a, b = @subject.split(8)
    assert_false a.include? 10
    assert_true b.include? 10
  end

  def test_split_1_greater
    @subject.insert(10)
    a, b = @subject.split(12)
    assert_false b.include? 10
    assert_true a.include? 10
  end

  def test_split_2
    @subject.insert 10
    @subject.insert 15

    a, b = @subject.split(11)

    assert_true a.include?(10)
    assert_false a.include?(15)

    assert_true b.include?(15)
    assert_false b.include?(10)
  end

  def test_split_3
    @subject.insert 10
    @subject.insert 15
    @subject.insert 20

    assert_priority(@subject.root)

    a, b = @subject.split(11)

    assert_true a.include?(10)
    assert_false a.include?(15)

    assert_true b.include?(15)
    assert_false b.include?(10)
  end

  def test_split_6
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(-2)
    @subject.insert(7)
    @subject.insert(3)

    assert_priority(@subject.root)

    a, b = @subject.split(8)

    assert_true a.include?(5)
    assert_true a.include?(-2)
    assert_true a.include?(3)
    assert_true a.include?(7)

    assert_false a.include?(10)
    assert_false a.include?(15)

    assert_false b.include?(5)
    assert_false b.include?(-2)
    assert_false b.include?(3)
    assert_false b.include?(7)

    assert_true b.include?(10)
    assert_true b.include?(15)

    assert_priority(a.root)
    assert_priority(b.root)
  end

  def test_join
    @subject.insert(5)
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(-2)
    @subject.insert(7)
    @subject.insert(3)

    assert_priority(@subject.root)

    a, b = @subject.split(8)

    assert_priority(a.root)
    assert_priority(b.root)

    t = a.join(b)

    assert_true t.include?(5)
    assert_true t.include?(10)
    assert_true t.include?(15)
    assert_true t.include?(-2)
    assert_true t.include?(7)
    assert_true t.include?(3)

    assert_priority(t.root)
  end

  def test_join_hardcoded_priorities
    @subject.insert(5, 0.28)
    @subject.insert(10, 0.19)
    @subject.insert(15, 0.25)
    @subject.insert(-2, 0.46)
    @subject.insert(7, 0.63)
    @subject.insert(3, 0.76)

    assert_priority(@subject.root)

    a, b = @subject.split(8)

    assert_priority(a.root)
    assert_priority(b.root)

    t = a.join(b)

    assert_true t.include?(5)
    assert_true t.include?(10)
    assert_true t.include?(15)
    assert_true t.include?(-2)
    assert_true t.include?(7)
    assert_true t.include?(3)

    assert_priority(t.root)
  end

  def test_remove2_hardcoded_priorities
    @subject.insert(0, 0.9)
    @subject.insert(2, 0.2)

    @subject.remove(0)
    assert_false @subject.include? 0
    assert_true @subject.include? 2
  end

  def test_remove2_2_hardcoded_priorities
    @subject.insert(0, 0.2)
    @subject.insert(2, 0.9)

    @subject.remove(0)
    assert_false @subject.include? 0
    assert_true @subject.include? 2
  end

  def test_remove6_hardcoded_priorities
    @subject.insert(5, 0.59)
    @subject.insert(10, 0.35)
    @subject.insert(15, 0.31)
    @subject.insert(-2, 0.28)
    @subject.insert(7, 0.49)
    @subject.insert(3, 0.08)

    @subject.remove(-2)
    assert_priority(@subject.root)
    assert_false @subject.include?(-2)

    @subject.remove 5
    assert_priority(@subject.root)
    assert_false @subject.include? 5

    @subject.remove 15
    assert_priority(@subject.root)
    assert_false @subject.include? 15

    assert_true @subject.include? 10
    assert_true @subject.include? 7
    assert_true @subject.include? 3
  end
end
