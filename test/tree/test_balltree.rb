require_relative '../test_helper'
require_relative '../../lib/tree/balltree'
require_relative 'spatial_mixin'

class BalltreeTest < Minitest::Test
  include NearestNeighbourMixin

  def setup
    @clazz = BallTree
    @params = {}
  end

end

class BalltreeSize10Test < Minitest::Test
  include NearestNeighbourMixin

  def setup
    @clazz = BallTree
    @params = {ball_size: 10}
  end

end

class BalltreeLomutoOptimizationTest < Minitest::Test
  include NearestNeighbourMixin

  def setup
    @clazz = BallTree
    @params = {use_lomuto_quickselect: true}
  end

end
