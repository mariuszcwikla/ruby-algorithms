require_relative '../test_helper'
require_relative '../../lib/tree/segment_tree'
require_relative '../../lib/tree/segment_tree_array_based'
require_relative '../queries/range_query_test_mixin'


class SegmentTreeTest < Minitest::Test
  include RangeQueryTestMixin
  def setup
    @clazz = SegmentTree
  end
end

class SegmentTreeArrayBasedTest < Minitest::Test
  include RangeQueryTestMixin
  def setup
    @clazz = SegmentTreeArrayBased
  end
end
