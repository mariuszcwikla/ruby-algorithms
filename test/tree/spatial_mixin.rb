# Test mixins for spatial data structures

module NearestNeighbourMixin
  def test_nearest_neighbour
    property_of {
      length = range(5, 500)
      points = array(length) { [range(0, 1000), range(0, 1000)] }
      guard points.uniq.size == points.size
      queries = array(10) { [range(0, 1000), range(0, 1000)] }
      points = ShrinkableArray.new (points)
      queries = ShrinkableArray.new (queries)
      Tuple.new [points, queries]
    }.check do |tuple|
      # this is bit weird
      # when I did below
      #
      # check do |points, queries|
      #
      # then it was failing becuase queries is always nil
      # To fix it, need to extract data from the array exlicitely
      #points = tuple[0].array
      points = tuple[0]
      points = points.array if points.respond_to? :array
      queries = tuple[1]
      t = @clazz.build(points, **@params || {})

      queries.each do |q|
        n1 = t.nearest_neighbour(q)
        n2 = nearest_neighbour_brute(points, q)
        assert_equal squared_distance(q, n2), squared_distance(q, n1), "failed at #{q}"
      end
    end
  end

  def test_nearest_neighbour_3d
    #omit 'Not working for 3D'
    property_of {
      length = range(5, 500)
      points = array(length) { [range(0, 1000), range(0, 1000), range(0, 1000)] }
      guard points.uniq.size == points.size
      points = ShrinkableArray.new (points)
      queries = ShrinkableArray.new (array(10) { [range(0, 1000), range(0, 1000), range(0, 1000)] })
      Tuple.new [points, queries]
    }.check do |tuple|
      points = tuple[0]
      points = points.array if points.respond_to? :array
      queries = tuple[1]
      t = @clazz.build(points, k: 3, **@params || {})

      queries.each do |q|
        n1 = t.nearest_neighbour(q)
        n2 = nearest_neighbour_brute(points, q)
        assert_equal squared_distance(q, n2), squared_distance(q, n1), "failed at #{q}"
      end
    end
  end


  def test_nearest_neighbour_10D
    #omit 'Not working for 10D'
    dimensions = 10
    property_of {
      length = range(5, 500)
      points = array(length) { dimensions.times.map{range(0, 1000)} }
      guard points.uniq.size == points.size
      queries = array(10) { dimensions.times.map{range(0, 1000)} }
      points = ShrinkableArray.new (points)
      queries = ShrinkableArray.new (queries)
      Tuple.new [points, queries]
    }.check do |tuple|
      # this is bit weird
      # when I did below
      #
      # check do |points, queries|
      #
      # then it was failing becuase queries is always nil
      # To fix it, need to extract data from the array exlicitely
      #points = tuple[0].array
      points = tuple[0]
      points = points.array if points.respond_to? :array
      queries = tuple[1]
      t = @clazz.build(points, k: dimensions, **@params || {})

      queries.each do |q|
        n1 = t.nearest_neighbour(q)
        n2 = nearest_neighbour_brute(points, q)
        assert_equal squared_distance(q, n2), squared_distance(q, n1), "failed at #{q}"
      end
    end
  end

  def test_nearest_neighbour_example
    points = [[284, 0], [506, 9], [567, 11], [401, 21], [486, 28], [463, 31], [164, 32], [468, 33], [979, 46], [98, 56], [756, 70], [510, 70], [916, 70], [563, 92], [811, 94], [381, 96], [188, 98],
    [476, 103], [24, 115], [220, 144], [113, 145], [466, 159], [514, 159], [666, 167], [676, 190], [207, 227], [759, 239], [210, 250], [221, 253], [374, 253], [717, 256], [649, 259], [489, 259],
    [508, 264], [909, 267], [86, 272], [446, 275], [11, 280], [128, 293], [400, 298], [801, 300], [724, 303], [586, 326], [71, 331], [699, 337], [124, 349], [197, 359], [147, 364], [236, 367], [893, 376],
    [910, 388], [542, 390], [883, 421], [227, 433], [943, 433], [729, 433], [79, 436], [836, 436], [847, 437], [31, 442], [889, 447], [878, 449], [775, 455], [87, 459], [325, 462], [404, 471],
    [181, 479], [202, 485], [252, 490], [429, 491], [465, 511], [936, 513], [435, 517], [124, 519], [271, 523], [720, 531], [181, 537], [592, 541], [76, 554], [447, 560], [647, 571], [125, 578],
    [108, 579], [357, 586], [742, 586], [285, 594], [134, 598], [36, 601], [242, 614], [159, 619], [481, 633], [619, 638], [819, 643], [509, 643], [830, 646], [566, 655], [765, 663], [61, 672],
    [640, 685], [383, 691], [855, 702], [143, 710], [265, 719], [191, 725], [502, 729], [903, 732], [626, 732], [429, 736], [99, 737], [2, 739], [5, 745], [451, 766], [30, 767], [639, 784],
    [760, 790], [392, 799], [100, 806], [135, 832], [555, 836], [828, 838], [923, 848], [249, 849], [82, 850], [327, 861], [921, 869], [759, 873], [373, 879], [616, 888], [218, 890], [119, 895], 
    [260, 899], [799, 900], [381, 903], [90, 909], [114, 910], [400, 918], [360, 922], [932, 923], [184, 940], [828, 950], [254, 961], [737, 963], [904, 964], [116, 967], [923, 969], [138, 969],
    [218, 970], [178, 977], [181, 977], [743, 980]]
    queries = [[751, 536]]
    # points = [[16, 44], [70, 69], [23, 26]]
    # queries = [[13, 29]]
    t = @clazz.build(points)
    queries.each do |q|
      n1 = t.nearest_neighbour(q)
      n2 = nearest_neighbour_brute(points, q)
      assert_equal squared_distance(q, n2), squared_distance(q, n1), "failed at #{q}"
    end
  end
end