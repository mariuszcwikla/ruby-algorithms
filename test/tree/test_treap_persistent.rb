require_relative '../test_helper'

require_relative '../../lib/tree/treap_persistent'
require_relative '../collection_test_mixins'
require_relative 'base_treap_test'

class TestPersistentTreap < Minitest::Test
  include TreapSplitJoinTests
  include CollectionTestMixin

  def setup
    @subject = PersistentTreap.new

    @a = PersistentTreap.new
    @b = PersistentTreap.new
  end

  def test_split_1_lower
    @subject.insert(10)
    a, b = @subject.split(8)
    assert_false a.include? 10
    assert_true b.include? 10

    # TIP: recursive implementation has a property that tree.split returns two new trees
    # but old tree is still valid! I.e. it still has pointers to all the nodes.
    # a, b has pointers only to lower of k or greater than k

    # THAT'S WHY this (and other) tests can't be moved to TreapSplitJoinTests
    assert_true @subject.include? 10
  end

  def test_split_1_greater
    @subject.insert(10)
    a, b = @subject.split(12)
    assert_false b.include? 10
    assert_true a.include? 10

    assert_true @subject.include? 10
  end

  def test_split_2
    @subject.insert(10)
    @subject.insert(15)
    a, b = @subject.split(12)
    assert_true a.include? 10
    assert_false a.include? 15

    assert_false b.include? 10
    assert_true b.include? 15

    assert_true @subject.include? 10
    assert_true @subject.include? 15
  end

  def test_split_3
    @subject.insert(10)
    @subject.insert(15)
    @subject.insert(11)
    a, b = @subject.split(12)
    assert_true a.include? 10
    assert_true a.include? 11
    assert_false a.include? 15

    assert_false b.include? 10
    assert_false b.include? 11
    assert_true b.include? 15
  end

  def test_union_1
    @a.insert(6, 0.9)
    @b.insert(7, 0.6)

    c = @a.union @b

    assert_true c.include? 6
    assert_true c.include? 7

    assert_priority(c.root)
  end

  def test_union_1b
    @a.insert(6, 0.6)
    @b.insert(7, 0.9)

    c = @a.union @b

    assert_true c.include? 6
    assert_true c.include? 7

    assert_priority(c.root)
  end

  def test_union_2_2
    @a.insert(6, 0.9)
    @a.insert(2, 0.2)

    @b.insert(7, 0.8)
    @b.insert(3, 0.44)

    c = @a.union @b

    assert_true c.include? 6
    assert_true c.include? 7
    assert_true c.include? 2
    assert_true c.include? 3

    assert_priority(c.root)
  end

  def test_union_height3_height3
    @a.insert(6, 0.9)
    @a.insert(2, 0.2)
    @a.insert(12, 0.22)
    @a.insert(0, 0.1)
    @a.insert(4, 0.15)

    @b.insert(7, 0.8)
    @b.insert(3, 0.44)
    @b.insert(15, 0.77)
    @b.insert(1, 0.23)
    @b.insert(5, 0.33)

    c = @a.union @b

    assert_true c.include? 6
    assert_true c.include? 2
    assert_true c.include? 12
    assert_true c.include? 0
    assert_true c.include? 4

    assert_true c.include? 7
    assert_true c.include? 3
    assert_true c.include? 1
    assert_true c.include? 5
    assert_true c.include? 15

    assert_priority(c.root)
  end
  #     6        7
  #    /  \     /  \
  #   2   12   3    15
  #  / \      / \
  # 0   4    1   5
end
