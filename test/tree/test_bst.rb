require_relative '../test_helper'
require 'set'
require_relative '../../lib/tree/bst'
require_relative '../collection_test_mixins'

class TestBinarySearchTree < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = BinarySearchTree.new
  end

  def test_rantly_insert_remove
    elements = nil    # need to declare variable here, otherwise they are unknown in "check"
    property_of {
      elements = []
      size = range(1, 20)
      array(size) {
        action = choose(:insert, :remove)
        if action == :insert
          x = integer(1000)
          elements << x
          guard elements.uniq.size == elements.size
        else
          guard elements.size > 0
          x = choose(*elements)
          elements.delete x
        end
        [action, x]
      }
    }.check(1000) { |data|
      @t = BinarySearchTree.new
      set = Set.new
      data.each do |action, value|
        begin
          if action == :insert
            @t.insert value
            set << value
          else
            @t.remove value
            set.delete value
          end
        rescue Exception => e
          puts e.message
          puts e.backtrace    # When NoMethodError is thrown, then rantly does not output backtrace... :( That's why own exception logging here
          raise e
        end
        # puts set.to_a.join ' '
        assert_equal set.sort, @t.values
      end
    }
  end

  def test_rantly_example
    # "Example" test for debugging cases found by rantly
    # Just paste test data here
    data =   [[:insert, 72], [:insert, -376], [:insert, -436], [:insert, -510], [:insert, -773]]
    set = Set.new
    data.each do |action, value|
      if action == :insert
        set << value
        @subject.insert value
      else
        set.delete value
        @subject.remove value
      end
      vals = @subject.values
      assert_equal set.sort, @subject.values
    end
  end
end
