require_relative '../test_helper'
require_relative '../../lib/heap/van_emde_boyas'
require_relative 'heap_test_mixin'
require_relative '../collection_test_mixins'

class VanEmdeBoyasTreeTest < Minitest::Test
  include HeapTestMixin
  include CollectionInsertTestMixin
  include CollectionRemoveTestMixin
  include SuccessorTestMixin
  include PredecessorTestMixin

  def setup
    @subject = VanEmdeBoyasTree.new(lazy: true)
    @q = @subject # because heap tests use "@q" name
  end

  def test_member_1
    @subject.insert 5
    assert @subject.include?(5)
    refute @subject.include?(4)
    refute @subject.include?(6)
  end

  def test_minmax
    property_of{
      array(range(5, 100)){range(0, 2**32)}
    }.check {|array|
      v = VanEmdeBoyasTree.new
      min = array[0]
      max = array[0]
      array.each do |i|
        v.insert i
        min = [i, min].min
        max = [i, max].max
        assert_equal min, v.min
        assert_equal max, v.max
      end
    }
  end

end

class VanEmdeBoyasEagerTreeTest < Minitest::Test
  # include HeapTestMixin
  include CollectionInsertTestMixin
  include SuccessorTestMixin
  include PredecessorTestMixin

  def setup
    # eager VEB tree takes time to construct.
    # therefore I'm restricting universe size to 2^16
    @subject = VanEmdeBoyasTree.new(2**16, lazy: false)
    @q = @subject # because heap tests use "@q" name
  end
end