require_relative '../test_helper'
require_relative '../../lib/heap/FibonacciHeap'

class FibonacciHeap
  class Element
    def num_of_elements
      n = 0
      each { n += 1 }
      n
    end
  end
end

class TestFibonacciHeap2 < Minitest::Test
  def setup
    @q = FibonacciHeap.new
  end

  def test_push_five_pop
    @q.push(5)
    @q.push(2)
    @q.push(0)
    @q.push(4)
    @q.push(1)

    assert_equal 0, @q.pop
    assert_equal 1, @q.pop
    assert_equal 2, @q.pop
    assert_equal 4, @q.pop
    assert_equal 5, @q.pop
  end

  def test_bla
    @q.push(0)
    @q.push(1)
    @q.push(2)
    @q.push(3)
    @q.push(-1)

    @q.pop
    @q.pop
    @q.pop
    @q.pop
    @q.pop
  end

  def test_bla_bla
    n = 100
    n.times { |i| @q.push(i) }
    @q.pop

    n.times do
      @q.push(rand(10_000))
      @q.pop
    end

    # puts @q.min.num_of_elements
  end

  def test_insert_random_10000
    min = nil

    n = 10
    n.times do
      r = rand(100)
      min = [r, min].compact.min
      @q.push(r)
    end

    n.times do
      v = @q.pop
      raise "v<min #{v}<#{min}" if v < min

      min = v
    end
  end
end
