require_relative '../test_helper'
require_relative '../../lib/heap/d_ary_heap'
require_relative 'heap_test_mixin'

class TestDAryHeapQueue < Minitest::Test
  include HeapTestMixin
  include HeapCustomKeyTypeTestMixin
  include IncreaseKeyTestsMixin
  include DecreaseKeyTestsMixin

  def setup
    @q = DAryHeap.new
  end

  def test_heap_custom_compare1
    q = DAryHeap.new { |a, b| b <=> a }

    q.insert 1
    q.insert 5
    q.insert 0

    assert_equal 5, q.pop
    assert_equal 1, q.pop
    assert_equal 0, q.pop
  end

  def test_heap_custom_compare_with_decrease_key
    q = DAryHeap.new { |a, b| b <=> a }

    q.insert 1
    q.insert 5
    c = q.insert 0
    q.decrease_key(c, 10)

    assert_equal 10, q.pop
    assert_equal 5, q.pop
    assert_equal 1, q.pop
  end

  def test_heap_custom_compare_with_update_key
    q = DAryHeap.new { |a, b| b <=> a }

    a = q.insert 1
    q.insert 5
    q.insert 0

    q.update_key(a, 10)

    assert_equal 10, q.pop
    assert_equal 5, q.pop
    assert_equal 0, q.pop
  end
end

class Test3AryHeapQueue < Minitest::Test
  include HeapTestMixin
  include HeapCustomKeyTypeTestMixin
  include IncreaseKeyTestsMixin
  include DecreaseKeyTestsMixin

  def setup
    @q = DAryHeap.new
  end

end