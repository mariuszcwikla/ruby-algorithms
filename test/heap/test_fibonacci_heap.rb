require_relative '../test_helper'
require_relative '../../lib/heap/FibonacciHeap'
require_relative 'heap_test_mixin'

class TestFibonacciHeap < Minitest::Test
  include HeapTestMixin
  include HeapCustomKeyTypeTestMixin
  include DecreaseKeyTestsMixin

  def setup
    @q = FibonacciHeap.new
  end

  def test_degree
    @q.push(0)
    @q.push(1)
    @q.push(2)
    @q.push(3)
    @q.pop

    assert_equal 0, @q.min.degree
  end
end
