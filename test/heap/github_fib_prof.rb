require 'benchmark'

require 'fibonacci_heap'
require 'securerandom'

n = 100_000

fh = FibonacciHeap::Heap.new
n /
  2.times do
    r = rand(n * 10_000)
    node = FibonacciHeap::Node.new(r, SecureRandom.uuid.to_s)
    fh.insert(node)
  end if true
fh.pop

n.times do
  r = rand(n * 10_000)
  node = FibonacciHeap::Node.new(r, SecureRandom.uuid.to_s)
  fh.insert(node)
  fh.pop
end
