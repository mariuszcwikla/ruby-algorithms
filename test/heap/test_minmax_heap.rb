require_relative '../test_helper'
require_relative '../../lib/heap/minmax'
require_relative 'heap_test_mixin'

class MinMaxHeap
  attr_reader :elems
end

class MinMaxHeapTest < Minitest::Test
  include HeapTestMixin
  include HeapCustomKeyTypeTestMixin
  def setup
    @q = MinMaxHeap.new
  end

  def test_popmax_2
    @q.insert 5
    @q.insert 0

    assert_equal 5, @q.pop_max
    assert_equal 0, @q.pop_max
  end

  def test_popmax_6
    @q.insert 5
    @q.insert 0
    @q.insert 7
    @q.insert 9
    @q.insert 3
    @q.insert 11

    assert_equal 11, @q.pop_max
    assert_equal 9, @q.pop_max
    assert_equal 7, @q.pop_max
    assert_equal 5, @q.pop_max
  end

  [
    [-214, -203, 940, -957, -355, 145, 786],
    [549, 172, 998, -893, -738, 655, -313],
    [ -526, 426, 206, 630, 324, -296, -397],
    [-957, 350, -36, -656, -31, 103, -315, -404, 131, -192]
  ].each_with_index do |arr, i|
    define_method("test_example_#{i}") {
      arr.each do |v|
        @q.push v
      end

      last = @q.pop
      (arr.size - 1).times do
        v = @q.pop
        assert v >= last, "next element #{v} is not >= than #{last}"
        last = v
      end
      assert 0, @q.size
      }
    end

  def test_rantly_insert_pop_minmax
    property_of {
      arr = array(100) {integer}
      ShrinkableArray.new(arr)
    }.check { |arr|
      q = @q.class.new
      nodes = []
      max = arr.array.max

      arr.each do |i|
        nodes << (q.push i)
      end

      verify_internal_structure(q)

      last = q.pop
      verify_internal_structure(q)
      assert_equal max, q.max
      (arr.size - 1).times do
        v = q.pop
        # commented out because it takes huge amount of time
        # verify_internal_structure(q)
        assert v >= last, "#{v} is not >= than #{last}"
        last = v
        assert_equal max, q.max unless q.empty?
      end
      assert 0, q.size
    }
  end

  def verify_internal_structure(q)
    1.upto(q.elems.size-1) do |i|
      h = (i + 1).to_s(2).size - 1
      p = (i - 1)/2
      if h.odd?
        assert q.elems[i].value >= q.elems[p].value, "element #{q.elems[i].value} should be >= than #{q.elems[p].value}. Structure #{q.inspect}"
      else
        assert q.elems[i].value <= q.elems[p].value, "element #{q.elems[i].value} should be <= than #{q.elems[p].value}. Structure #{q.inspect}"
      end
    end
  end

  def test_rantly_insert_pop_example_minmax_sandbox
    arr = [-657, -506, 945, -202, -465, 254]
    arr.each { |i|
      @q.push i
    }
    verify_internal_structure(@q)
    last = @q.pop
    verify_internal_structure(@q)
    (arr.size - 1).times do
      v = @q.pop
      verify_internal_structure(@q)
      assert v >= last, "next element #{v} is not >= than #{last}"
      last = v
    end
    assert 0, @q.size
  end

  def test_rantly_insert_pop_max
    property_of {
      arr = array(100) {integer}
      ShrinkableArray.new(arr)
    }.check { |arr|
      q = @q.class.new
      nodes = []
      arr.each do |i|
        nodes << (q.push i)
      end

      last = q.pop_max
      (arr.size - 1).times do
        v = q.pop_max
        assert v <= last, "#{v} is not <= than #{last}"
        last = v
      end
      assert 0, q.size
    }
  end


end
