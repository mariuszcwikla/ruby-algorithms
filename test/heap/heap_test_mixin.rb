module HeapTestMixin
  def test_empty
    assert_true @q.empty?
  end

  def test_size_0
    assert_equal 0, @q.size
  end

  def test_not_empty
    @q.push(1)
    assert_false @q.empty?
  end

  def test_size_1
    @q.push(1)
    assert_equal 1, @q.size
  end

  def test_peek_empty
    assert_raise (RuntimeError) do
      @q.peek
    end
  end

  def test_push_peek_one
    @q.push 1
    assert_equal 1, @q.peek
    assert_equal 1, @q.size
  end

  def test_push_pop_one
    @q.push(1)
    assert_equal 1, @q.pop
    assert_true @q.empty?
  end

  def test_push_two_peek
    @q.push 5
    @q.push 3
    assert_equal 3, @q.peek
    assert_equal 2, @q.size
  end

  def test_push_two_pop
    @q.push(1)
    @q.push(2)
    assert_equal 2, @q.size
    assert_equal 1, @q.pop
    assert_equal 2, @q.pop

    assert_true @q.empty?
  end

  def test_push_two_pop_reversed
    @q.push(2)
    @q.push(1)
    assert_equal 2, @q.size
    assert_equal 1, @q.pop
    assert_equal 2, @q.pop

    assert_true @q.empty?
  end

  def test_push_three_peek
    @q.push(2)
    @q.push(0)
    @q.push(1)

    assert_false @q.empty?
    assert_equal 0, @q.peek
    assert_equal 3, @q.size
  end

  def test_push_five_peek
    @q.push(5)
    @q.push(2)
    @q.push(0)
    @q.push(4)
    @q.push(1)

    assert_false @q.empty?
    assert_equal 0, @q.peek
    assert_equal 5, @q.size
  end

  def test_push_five_pop
    @q.push(5)
    @q.push(2)
    @q.push(0)
    @q.push(4)
    @q.push(1)

    assert_equal 0, @q.pop
    assert_equal 1, @q.pop
    assert_equal 2, @q.pop
    assert_equal 4, @q.pop
    assert_equal 5, @q.pop
  end

  def test_push_three_pop
    @q.push(2)
    @q.push(0)
    @q.push(1)

    assert_equal 0, @q.pop
    assert_equal 1, @q.pop
    assert_equal 2, @q.pop

    assert_true @q.empty?
  end

  def test_push_two_pop_peek
    @q.push(2)
    @q.push(1)

    assert_equal 2, @q.size

    assert_equal 1, @q.peek
    assert_equal 2, @q.size

    assert_equal 1, @q.pop

    assert_equal 2, @q.peek
    assert_equal 1, @q.size
  end

  def test_update_key
    omit 'method needs to be implemented'
  end

  def test_shift_operator
    @q << 1
    @q << 2

    assert_equal 1, @q.pop
    assert_equal 2, @q.pop
  end

  def test_rantly_insert_pop
    property_of {
      arr = array(100) {range(0, 1_000_000)}
      guard arr.size == arr.uniq.size # because van Emde Boyas does not support duplicates
      ShrinkableArray.new(arr)
    }.check { |arr|
      setup # hack to recreate new heap
      nodes = []
      arr.each do |i|
        nodes << (@q.push i)
      end

      last = @q.pop
      (arr.size - 1).times do
        v = @q.pop
        assert v >= last, "#{v} is not >= than #{last}"
        last = v
      end
      assert 0, @q.size
    }
  end

  def test_rantly_insert_pop_example_sandbox
    arr = [1, 2, 3, 4]
    arr.each { |i|
      @q.push i
    }

    last = @q.pop
    (arr.size - 1).times do
      v = @q.pop
      assert v >= last, "next element #{v} is not >= than #{last}"
      last = v
    end
    assert 0, @q.size
  end
end

module HeapCustomKeyTypeTestMixin
  class Wrapper
    include Comparable
    attr_reader :value

    def initialize(value)
      @value = value
    end

    def <=>(other)
      @value <=> other.value
    end
  end

  def test_user_class
    @q.push(Wrapper.new(5))
    @q.push(Wrapper.new(2))
    @q.push(Wrapper.new(8))

    assert_equal 2, @q.pop.value
    assert_equal 5, @q.pop.value
    assert_equal 8, @q.pop.value
  end
end

module DecreaseKeyTestsMixin

  def test_decrease_key_one_elem
    a = @q.push(5)

    @q.decrease_key(a, 2)
    assert_equal 2, @q.peek
    assert_equal 2, @q.pop
    assert_true @q.empty?
  end

  def test_decrease_key_two_elems
    # omit if @q.is_a? MinMaxHeap or @q.is_a? VanEmdeBoyasTree
    a = @q.push(5)
    @q.push(3)

    assert_equal 3, @q.peek
    @q.decrease_key(a, 2)

    assert_equal 2, @q.peek

    assert_equal 2, @q.pop
    assert_equal 3, @q.pop

    assert_true @q.empty?
  end

  def test_decrease_key_five_elems
    @q.push(2)
    @q.push(5)
    c = @q.push(0)
    @q.push(3)
    e = @q.push(1)

    assert_equal 0, @q.peek

    @q.decrease_key(e, -1)
    assert_equal(-1, @q.peek)
    @q.decrease_key(c, -2)
    assert_equal(-2, @q.peek)

    assert_equal(-2, @q.pop)
    assert_equal(-1, @q.pop)
    assert_equal(2, @q.pop)
    assert_equal(3, @q.pop)
    assert_equal(5, @q.pop)
  end

  def test_many_eq_items
    @q.push(1)
    @q.push(2)
    c = @q.push(2)

    assert_equal 1, @q.pop
    assert_equal 2, @q.peek
    @q.decrease_key(c, 1)
  end

  def test_decrease_key_many_eq_elems
    @q.push 9999
    @q.push 9999
    @q.push 9999
    @q.push 9999
    @q.push 9999

    f = @q.push 9999
    g = @q.push 9999
    h = @q.push 9999
    i = @q.push 9999
    j = @q.push 9999

    @q.decrease_key(j, 123)
    assert_equal(123, @q.pop)

    @q.decrease_key(i, 15)
    @q.decrease_key(h, 10)
    @q.decrease_key(g, 7)
    @q.decrease_key(f, 11)

    assert_equal 7, @q.pop
    assert_equal 10, @q.pop
    assert_equal 11, @q.pop
    assert_equal 15, @q.pop
  end

  def test_rantly_insert_pop_decrease_key
    property_of {
      x = range(5, 100)
      Deflating.new(array(x) {integer(1000)})
    }.check { |arr|
      setup
      nodes = []
      arr.each do |i|
        nodes << (@q.push i)
      end

      nodes.each do |n|
        @q.decrease_key(n, n.value - 10)
      end

      last = @q.pop
      (arr.size - 1).times do
        v = @q.pop
        assert v >= last
        last = v
      end
      assert 0, @q.size
    }
  end
end

module IncreaseKeyTestsMixin
  def test_increase_key
    @q.push(5)
    b = @q.push(3)

    assert_equal 3, @q.peek

    @q.increase_key(b, 7)

    assert_equal(5, @q.pop)
    assert_equal(7, @q.pop)
    assert_true @q.empty?
  end

  def test_increase_key_five_elems
    @q.push(2)
    b = @q.push(5)
    c = @q.push(0)
    @q.push(3)
    e = @q.push(1)

    assert_equal 0, @q.peek

    @q.increase_key(c, 7)
    assert_equal 1, @q.peek
    @q.increase_key(e, 4)
    assert_equal 2, @q.peek
    @q.increase_key(b, 9)

    assert_equal(2, @q.pop)
    assert_equal(3, @q.pop)
    assert_equal(4, @q.pop)
    assert_equal(7, @q.pop)
    assert_equal(9, @q.pop)
    assert_true @q.empty?
  end
end
