require 'benchmark'
require 'fibonacci_heap'

# Poniewaz jest name clash (moja klasa: FibonacciHeap, oraz biblioteka 'fibonacci_heap' ma module FibonacciHeap)
# wiec ponizsze rozwiazanie to HACK na to ten name clash
GithubFibonacciHeapClass = FibonacciHeap::Heap
GithubFibonacciHeapNodeClass = FibonacciHeap::Node

class GithubFibonacciHeap
  def initialize
    @heap = GithubFibonacciHeapClass.new
  end

  class NodeWrapper
    attr_reader :node

    def initialize(node)
      @node = node
    end

    def value
      @node.key
    end
  end

  def push(x)
    node = GithubFibonacciHeapNodeClass.new(x, x)
    @heap.insert(node)
    NodeWrapper.new(node)
  end

  def decrease_key(node, x)
    @heap.decrease_key(node.node, x)
  end

  def pop
    @heap.pop
  end
end

Object.send(:remove_const, :FibonacciHeap)

require_relative '../../lib/heap/BinomialHeap'
require_relative '../../lib/heap/BinaryHeap'
require_relative '../../lib/heap/FibonacciHeap'

# TODO: Utwórz lepsze benchmarki (bardziej czytelne), np.

=begin

Benchmarks on Dell XPS L502X

NOTE: run it couple of times - each time it will differ a bit. Sometimes Binoial Heap is faster, sometimes Fibonacci HEap is faster (perhaps due to GC?)

standard ruby

user     system      total        real
BinaryHeap - 10            0.000000   0.000000   0.000000 (  0.000049)
BinomialHeap - 10          0.000000   0.000000   0.000000 (  0.000065)
FibonacciHeap - 10         0.000000   0.000000   0.000000 (  0.000070)
BinaryHeap - 100           0.015000   0.000000   0.015000 (  0.000129)
BinomialHeap - 100         0.000000   0.000000   0.000000 (  0.000231)
FibonacciHeap - 100        0.000000   0.000000   0.000000 (  0.000185)
BinaryHeap - 1000          0.000000   0.000000   0.000000 (  0.000759)
BinomialHeap - 1000        0.015000   0.000000   0.015000 (  0.001449)
FibonacciHeap - 1000       0.000000   0.000000   0.000000 (  0.001261)
BinaryHeap - 10000         0.015000   0.000000   0.015000 (  0.006640)
BinomialHeap - 10000       0.016000   0.000000   0.016000 (  0.012008)
FibonacciHeap - 10000      0.000000   0.000000   0.000000 (  0.011186)
BinaryHeap - 100000        0.079000   0.000000   0.079000 (  0.065723)
BinomialHeap - 100000      0.125000   0.000000   0.125000 (  0.115279)
FibonacciHeap - 100000     0.110000   0.000000   0.110000 (  0.112630)
BinaryHeap - 1000000       0.703000   0.000000   0.703000 (  0.700510)
BinomialHeap - 1000000     1.156000   0.000000   1.156000 (  1.153693)
FibonacciHeap - 1000000    1.391000   0.015000   1.406000 (  1.409103)
BinaryHeap - 5000000       3.313000   0.000000   3.313000 (  3.319380)
BinomialHeap - 5000000     7.296000   0.047000   7.343000 (  7.341257)
FibonacciHeap - 5000000    5.860000   0.015000   5.875000 (  5.864458)
BinaryHeap - 10000000      6.453000   0.047000   6.500000 (  7.072886)
BinomialHeap - 10000000   17.078000   0.141000  17.219000 ( 17.209248)
FibonacciHeap - 10000000  16.281000   0.062000  16.343000 ( 16.348878)

On jruby

user     system      total        real
BinaryHeap - 10            0.000000   0.000000   0.000000 (  0.000186)
BinomialHeap - 10          0.000000   0.000000   0.000000 (  0.000193)
FibonacciHeap - 10         0.001000   0.000000   0.001000 (  0.000559)
BinaryHeap - 100           0.001000   0.000000   0.001000 (  0.000292)
BinomialHeap - 100         0.000000   0.000000   0.000000 (  0.000371)
FibonacciHeap - 100        0.001000   0.000000   0.001000 (  0.001217)
BinaryHeap - 1000          0.000000   0.000000   0.000000 (  0.000684)
BinomialHeap - 1000        0.001000   0.000000   0.001000 (  0.001093)
FibonacciHeap - 1000       0.006000   0.000000   0.006000 (  0.006708)
BinaryHeap - 10000         0.004000   0.000000   0.004000 (  0.004559)
BinomialHeap - 10000       0.007000   0.000000   0.007000 (  0.007250)
FibonacciHeap - 10000      0.013000   0.000000   0.013000 (  0.012309)
BinaryHeap - 100000        0.092000   0.000000   0.092000 (  0.092654)
BinomialHeap - 100000      0.067000   0.000000   0.067000 (  0.067599)
FibonacciHeap - 100000     0.160000   0.000000   0.160000 (  0.159314)
BinaryHeap - 1000000       0.434000   0.000000   0.434000 (  0.434613)
BinomialHeap - 1000000     0.727000   0.000000   0.727000 (  0.728241)
FibonacciHeap - 1000000    0.864000   0.000000   0.864000 (  0.864217)          <- FH slower than BH ~15%
BinaryHeap - 5000000       2.494000   0.000000   2.494000 (  2.493762)
BinomialHeap - 5000000     3.618000   0.000000   3.618000 (  3.618467)
FibonacciHeap - 5000000    7.866000   0.000000   7.866000 (  7.866673)          <- NOTE: FH slower 2x than BH
BinaryHeap - 10000000      8.304000   0.000000   8.304000 (  8.303433)
BinomialHeap - 10000000   16.289000   0.000000  16.289000 ( 16.288990)
FibonacciHeap - 10000000  11.332000   0.000000  11.332000 ( 11.332757)          <- NOTE: FH faster 30% than BH

=end

Benchmark.bmbm do |bench|
  [10, 100, 1000, 10_000, 100_000, 1_000_000].each do |#  5_000_000
  n|
    data = []
    n.times { data << rand(n * 10_000) }

    [BinaryHeap, BinomialHeap, GithubFibonacciHeap, FibonacciHeap].each do |queue_class|
      bench.report("#{queue_class} - #{n}") do
        q = queue_class.new
        nodes = {}
        n.times do |i|
          node = q.push(data[i])
          nodes[i] = node
        end
        (n * 2).times do
          r = rand(n - 1)
          node = nodes[r]
          new_value = node.value - rand(n) - 1
          puts "foo #{node.value}" if new_value > node.value
          q.decrease_key(node, new_value)
        end
      end
    end
    GC.start
  end
end
