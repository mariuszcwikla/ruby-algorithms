require 'benchmark'
require 'fibonacci_heap'
require 'securerandom'

# TODO: skonsoliduj benchmark z githuba z własnym, np. w ten sposób (a'la adapter)
# module FibonacciHeap
#  class Heap:
#   def push(r)
#    node = FibonacciHeap::Node.new(r, "#{SecureRandom.uuid}")
#    insert(node)
#    node
#   end

Benchmark.bmbm do |bench|
  [10, 100, 1000, 10_000, 100_000, 1_000_000].each do |n|
    data = []
    n.times do
      r = rand(n * 10_000)
      name = SecureRandom.uuid.to_s
      data << [r, name]
    end
    bench.report("https://github.com/mudge/fibonacci_heap - #{n}") do
      fh = FibonacciHeap::Heap.new

      nodes = {}
      n.times do |i|
        r, name = data[i]
        node = FibonacciHeap::Node.new(r, name)
        fh.insert(node)
        nodes[i] = node
      end
      n.times do |_i|
        r = rand(n - 1)
        node = nodes[r]
        new_key = rand(node.key) - 10
        fh.decrease_key(node, new_key)
      end
      #  bench.report("https://github.com/mudge/fibonacci_heap - #{n}"){
      #    (n/2).times { |i|
      #      r, name = data[i]
      #      node = FibonacciHeap::Node.new(r, name)
      #      fh.insert(node)
      #      fh.pop
      #    }
    end
  end
end
