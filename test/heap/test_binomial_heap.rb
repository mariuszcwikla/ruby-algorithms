require_relative '../test_helper'

require_relative '../../lib/heap/BinomialHeap'
require_relative 'heap_test_mixin'

class BinomialHeap
  def num_trees
    @trees.size
  end

  def dump
    @visited = {}
    strings = @trees.map { |e| dump_tree(e) }
    strings.join("\n")
  end

  def dump_tree(e)
    s = dump_tree_recursive([e], '')
    s + "----------\n"
  end

  def dump_tree_recursive(trees, d)
    trees.each do |t|
      if @visited[t]
        puts "CYCLE DETECTED for #{t}"
      end
      @visited[t] = true
    end

    d += '['
    children = []
    trees.each do |e|
      d += "#{e.value} (#{e.degree})"
      children.concat(e.children)
    end
    d += ']'

    d += "\n"
    d = dump_tree_recursive(children, d) unless children.empty?
    d
  end
end

class TestBinomialQueue < Minitest::Test
  include HeapTestMixin
  include HeapCustomKeyTypeTestMixin
  include DecreaseKeyTestsMixin
  include IncreaseKeyTestsMixin

  def setup
    @q = BinomialHeap.new
  end

  def test_push_6
    6.times { |i| @q.push(i) }
    assert_equal 6, @q.size
  end

  # sprawdzam ile jest drzew po wlozeniu 1, 2, 3 ... elementow
  def test_1_num_trees
    @q.push(0)
    assert_equal 1, @q.num_trees
  end

  def test_2_num_trees
    @q.push(0)
    @q.push(1)
    assert_equal 1, @q.num_trees
  end

  def test_3_num_trees
    @q.push(0)
    @q.push(1)
    @q.push(2)
    assert_equal 2, @q.num_trees
    assert_equal 3, @q.size
  end

  def test_4_num_trees
    @q.push(0)
    @q.push(1)
    @q.push(2)
    @q.push(3)
    assert_equal 1, @q.num_trees
    assert_equal 4, @q.size
  end

  def test_5_num_trees
    @q.push(0)
    @q.push(1)
    @q.push(2)
    @q.push(3)
    @q.push(4)
    assert_equal 2, @q.num_trees
    assert_equal 5, @q.size
  end

  def test_6_num_trees
    @q.push(0)
    @q.push(1)
    @q.push(2)
    @q.push(3)
    @q.push(4)
    @q.push(5)
    assert_equal 2, @q.num_trees
    assert_equal 6, @q.size
  end

  def test_num_trees_generic
    # test opiera sie na wlasciwosci,
    # ze jezeli rozmiar drzewa zamienimy na dwojkowy (np. 5 dex = 101 bin)
    # to liczba jedynek okresla liczbe drzew

    def numOnes(n)
      k = 0
      while n > 0
        k += 1 if n & 1 == 1
        n = n >> 1
      end
      k
    end

    10_000.times do |i|
      @q.push(i)
      assert_equal numOnes(i + 1), @q.num_trees
    end
  end

  def test_increase_key_five_elems_2
    _ = @q.push(2)
    b = @q.push(5)
    c = @q.push(0)
    _ = @q.push(3)
    e = @q.push(1)
    # puts @q.dump

    assert_equal 0, @q.peek

    @q.increase_key(c, 7)
    assert_equal 1, @q.peek
    @q.increase_key(e, 4)
    assert_equal 2, @q.peek
    @q.increase_key(b, 9)

    #    puts "q.increase_key\n" + @q.dump

    assert_equal(2, @q.pop)
    #    puts "q.pop: \n" + @q.dump
    assert_equal(3, @q.pop)
    #    puts "q.pop: \n" + @q.dump
    assert_equal(4, @q.pop)
    assert_equal(7, @q.pop)
    assert_equal(9, @q.pop)
    assert_true @q.empty?
  end
end
