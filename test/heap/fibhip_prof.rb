# gem install ruby-prof

# ruby-prof fibhip_prof.rb

require_relative '../../lib/heap/FibonacciHeap'

n = 10_000

q = FibonacciHeap.new
if true
  (n / 2).times do
    r = rand(n * 10_000)
    q.push(r)
  end
  q.pop
end

n.times do
  r = rand(n * 10_000)
  q.push(r)
  q.pop
end
