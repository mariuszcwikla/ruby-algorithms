require_relative '../../../test_helper'
require_relative '../../../../lib/persistent/full/fat/bst'
require 'set'
class FullyPersistentFatBSTTest < Minitest::Test

  def setup
    @t = Persistent::Full::BinarySearchTree.new
  end

  def test_empty
    assert @t.empty?
  end

  def test_tree_1_element
    v0 = @t.current_version
    v1 = @t.insert 'M'

    assert_equal [], @t.values(v0)
    assert_equal %w[M], @t.values(v1)
  end

  def test_tree1
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert @t.find('M')
    refute @t.find('C', v1)
    assert @t.find('C', v2)

    refute @t.empty?

    assert @t.include?('M')
    assert @t.include?('C')

    refute @t.include?('C', v1)

    assert_equal 2, @t.size
    assert_equal 1, @t.size(v1)
  end

  def test_tree2
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'I'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[C I M], @t.values(v3)

    assert @t.find('M')
    
    refute @t.find('C', v1)
    assert @t.find('C', v2)
    assert @t.find('C', v3)

    refute @t.find('I', v1)
    refute @t.find('I', v2)
    assert @t.find('I', v3)
  end

  def test_tree3
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'I'
    v4 = @t.insert 'P'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[C I M], @t.values(v3)
    assert_equal %w[C I M P], @t.values(v4)
  end

  def test_tree4
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'I'
    v4 = @t.insert 'P'

    v2_1 = @t.insert('A', v2)

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[C I M], @t.values(v3)
    assert_equal %w[C I M P], @t.values(v4)

    assert_equal %w[A C M], @t.values(v2_1)
  end

  def test_tree5
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'I'
    v4 = @t.insert 'P'

    v2_1 = @t.insert('A', v2)
    v2_2 = @t.insert('L', v2_1)

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[C I M], @t.values(v3)
    assert_equal %w[C I M P], @t.values(v4)

    assert_equal %w[A C M], @t.values(v2_1)
    assert_equal %w[A C L M], @t.values(v2_2)
  end

  def test_remove_leaf
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.remove 'C'
    v4 = @t.insert 'D'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[M], @t.values(v3)
    assert_equal %w[D M], @t.values(v4)
  end

  def test_remove_root
    v1 = @t.insert 'M'
    v2 = @t.remove 'M'
    assert_equal %w[M], @t.values(v1)
    assert_equal [], @t.values(v2)
  end

  def test_remove_node_with_one_left_child_being_left_child
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'A'
    v4 = @t.remove 'C'
    v5 = @t.insert 'B'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[A C M], @t.values(v3)
    assert_equal %w[A M], @t.values(v4)
    assert_equal %w[A B M], @t.values(v5)
  end

  def test_remove_node_with_one_left_child_being_rightchild
    v1 = @t.insert 'M'
    v2 = @t.insert 'P'
    v3 = @t.insert 'O'
    v4 = @t.remove 'P'
    v5 = @t.insert 'N'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[M P], @t.values(v2)
    assert_equal %w[M O P], @t.values(v3)
    assert_equal %w[M O], @t.values(v4)
    assert_equal %w[M N O], @t.values(v5)
  end

  def test_remove_node_with_one_right_child_being_left_child
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'D'
    v4 = @t.remove 'C'
    v5 = @t.insert 'E'

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[C D M], @t.values(v3)
    assert_equal %w[D M], @t.values(v4)
    assert_equal %w[D E M], @t.values(v5)
  end

  def test_three_branches
    v1 = @t.insert 'M'
    v2 = @t.insert 'C'
    v3 = @t.insert 'I'
    v4 = @t.insert 'P'
    v5 = @t.remove 'C'

    v1_1_1 = @t.insert('A', v1)
    v1_1_2 = @t.insert('L', v1_1_1)
    v1_1_3 = @t.remove('A', v1_1_2)

    v1_2_1 = @t.insert('F', v1)
    v1_2_2 = @t.insert('G', v1_2_1)
    v1_2_3 = @t.remove('F', v1_2_2)

    assert_equal %w[M], @t.values(v1)
    assert_equal %w[C M], @t.values(v2)
    assert_equal %w[C I M], @t.values(v3)
    assert_equal %w[C I M P], @t.values(v4)
    assert_equal %w[I M P], @t.values(v5)
    assert_equal %w[A M], @t.values(v1_1_1)
    assert_equal %w[A L M], @t.values(v1_1_2)
    assert_equal %w[L M], @t.values(v1_1_3)
    assert_equal %w[F M], @t.values(v1_2_1)
    assert_equal %w[F G M], @t.values(v1_2_2)
    assert_equal %w[G M], @t.values(v1_2_3)
  end

  # OK, I think this amount of "example tests" is fine
  # Let's try PBT ;)
  def test_rantly_insert_remove_latest_version
    elements = nil    # need to declare variable here, otherwise they are unknown in "check"
    property_of {
      # The point of this generator-block is to generate some "real use cases", i.e. insert some values, then remove some of previously inserted values
      # Sample generated result: [:insert, -227], [:insert, -872], [:remove, -872], [:insert, -337], [:insert, -732], [:remove, -227]
      elements = []
      size = range(1, 20)
      array(size) {
        # action = choose(:insert, :remove)   # This was throwing exception:  Rantly::TooManyTries: 8641 successful tests, too many tries: 91359
        # Changing to frequency-based randomness like below solves issue
        action = freq([5, :choose, :insert], [3, :choose, :remove])
        if action == :insert
          x = integer(1000)
          guard !elements.include?(x)
          elements << x
        else
          guard elements.size > 0     # I require size to be greater than 1. Size=0 creates "sentinel" node at the root which I need to refactor. Instead of sentinel node, let's replace tree with a forest!
          x = choose(*elements)
          elements.delete x
        end
        [action, x]
      }
    }.check { |data|
      @t = Persistent::Full::BinarySearchTree.new
      set = Set.new
      data.each do |action, value|
        begin
          if action == :insert
            @t.insert value
            set << value
          else
            @t.remove value
            set.delete value
          end
          assert_equal set.sort, @t.values
          assert_equal set.size, @t.size
        rescue Exception => e
          puts e.message
          puts e.backtrace    # When NoMethodError is thrown, then rantly does not output backtrace... :( That's why own exception logging here
          raise e
        end
      end
    }
  end  

  def test_rantly_example
    # "Example" test for debugging cases found by rantly
    # Just paste test data here
    data = [[:insert, 726], [:insert, 261], [:insert, -92], [:insert, -793], [:insert, 586], [:remove, 261]]
    set = Set.new
    data.each do |action, value|
      if action == :insert
        set << value
        @t.insert value
      else
        set.delete value
        @t.remove value
      end
      vals = @t.values
      assert_equal set.sort, @t.values
    end
  end

  def test_rantly_insert_remove_any_version
    versions = nil
    property_of {
      versions = [ [] ]
      size = range(1, 20)
      array(size) {
        begin
          if versions.size == 1
            version_index = 0
          else
            version_index = range(1, versions.size - 1)
          end
          version_data = versions[version_index].dup
          if version_data.size > 1
            action = choose(:insert, :remove)
          else
            action = :insert
          end

          if action == :insert
            x = integer(1000)
            guard !version_data.include?(x)
            version_data << x
          else
            guard version_data.size > 0
            x = choose(*version_data[1..-1])
            version_data.delete x
          end
          versions.insert(version_index + 1, version_data)
          [action, x, version_index, version_data]
        rescue Rantly::GuardFailure => e
          raise e
        rescue Exception => e
          puts e.message
          puts e.backtrace
          raise e
        end
      }
    }.check { |data|
      @t = Persistent::Full::BinarySearchTree.new
      versions = [@t.current_version]
      data.each do |action, value, version_index, expected_data|
        begin
          version = versions[version_index]
          if action == :insert
            new_version = @t.insert(value, version)
          else
            new_version =  @t.remove(value, version)
          end
          versions.insert(version_index + 1, new_version)
          assert_equal expected_data.sort, @t.values(new_version), "failed at version #{new_version.label}"
        rescue StandardError => e
          puts e.message
          puts e.backtrace
          raise e
        end
      end
    }
  end
end
