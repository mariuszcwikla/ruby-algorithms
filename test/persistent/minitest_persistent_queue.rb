require_relative '../test_helper'
require_relative '../../lib/persistent/persistent_queue'

class PersistenQueueTest < Minitest::Test
  def setup
    @q = Persistent::Queue.new
  end

  def test_empty
    assert @q.empty?
  end

  def test_1
    s = @q.insert 1
    assert @q.empty?
    assert !s.empty?
    assert s.include? 1
  end

  def test_2
    s = @q.insert 1
    s2 = s.insert 5
    assert @q.empty?
    assert !s.empty?

    assert s.include? 1
    assert !s.include?(5)

    assert s2.include? 1
    assert s2.include? 5
  end

  def test_pop_1
    qq = @q.push 1
    v, q2 = qq.pop

    assert !@q.include?(1)
    assert qq.include? 1
    assert !q2.include?(1)

    assert_equal 1, v
  end

  def test_pop_2
    q = @q.push 1
    q = q.push 2
    v, q2 = q.pop

    assert_equal 1, v

    v, q3 = q2.pop
    assert_equal 2, v
    assert q3.empty?

    assert q2.include? 2
  end

  def test_pop_5
    q = @q.push 1
    q = q.push 2
    q = q.push 3
    q = q.push 4
    q = q.push 5

    v, q = q.pop
    assert_equal 1, v

    v, q = q.pop
    assert_equal 2, v

    v, q = q.pop
    assert_equal 3, v

    v, q = q.pop
    assert_equal 4, v

    v, q = q.pop
    assert_equal 5, v

    assert q.empty?
  end
end
