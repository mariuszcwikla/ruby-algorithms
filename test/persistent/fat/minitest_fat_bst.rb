require_relative '../../test_helper'
require_relative '../../../lib/persistent/partial/fat/bst'
require_relative '../../collection_test_mixins'
class FatBstTest < Minitest::Test
  include CollectionTestMixin
  include TestUnitCompatibility
  def setup
    @subject = @tree = Fat::BinarySearchTree.new
  end

  def test_0
    assert_equal 0, @tree.version
    assert_equal [], @tree.values(0)
  end

  def test_1
    @tree.insert(0)
    assert_equal 1, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)

    assert !@tree.include?(0, 0)
    assert @tree.include?(0, 1)
  end

  def test_2
    @tree.insert(0)
    @tree.insert(1)
    assert_equal 2, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 1], @tree.values(2)

    v = @tree.version_at(0)
    assert !v.include?(0)
    assert !v.include?(1)

    v = @tree.version_at(1)
    assert v.include?(0)
    assert !v.include?(1)

    v = @tree.version_at(2)
    assert v.include?(0)
    assert v.include?(1)
  end

  def test_3
    @tree.insert(0)
    @tree.insert(1)
    @tree.insert(2)
    assert_equal 3, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 1], @tree.values(2)
    assert_equal [0, 1, 2], @tree.values(3)
  end

  def test_4
    @tree.insert 0
    @tree.insert 5
    @tree.insert 15
    @tree.insert 20
    @tree.insert 2
    @tree.insert 7

    assert_equal 6, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 5], @tree.values(2)
    assert_equal [0, 5, 15], @tree.values(3)
    assert_equal [0, 5, 15, 20], @tree.values(4)
    assert_equal [0, 2, 5, 15, 20], @tree.values(5)
    assert_equal [0, 2, 5, 7, 15, 20], @tree.values(6)
  end

  def test_remove_1
    @tree.insert 0
    @tree.insert 5

    @tree.remove 5

    assert_equal 3, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 5], @tree.values(2)
    assert_equal [0], @tree.values(3)
  end

  def test_remove_2
    @tree.insert 0
    @tree.insert 5
    @tree.insert 15
    @tree.remove 5

    assert_equal 4, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 5], @tree.values(2)
    assert_equal [0, 5, 15], @tree.values(3)
    assert_equal [0, 15], @tree.values(4)
  end

  def test_remove_4
    @tree.insert 0
    @tree.insert 5
    @tree.insert 15
    @tree.insert 20
    @tree.remove 5

    assert_equal 5, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 5], @tree.values(2)
    assert_equal [0, 5, 15], @tree.values(3)
    assert_equal [0, 5, 15, 20], @tree.values(4)
    assert_equal [0, 15, 20], @tree.values(5)
  end

  def test_remove_5
    @tree.insert 0
    @tree.insert 5
    @tree.insert 15
    @tree.insert 20
    @tree.insert 2

    #     0
    #      \
    #       5
    #      / \
    #     2   15
    #          \
    #          20

    @tree.remove 5

    assert_equal 6, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 5], @tree.values(2)
    assert_equal [0, 5, 15], @tree.values(3)
    assert_equal [0, 5, 15, 20], @tree.values(4)
    assert_equal [0, 2, 5, 15, 20], @tree.values(5)
    assert_equal [0, 2, 15, 20], @tree.values(6)
  end

  def test_remove_6
    @tree.insert 0
    @tree.insert 5
    @tree.insert 15
    @tree.insert 20
    @tree.insert 2
    @tree.insert 7

    @tree.remove 5

    assert_equal 7, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [0, 5], @tree.values(2)
    assert_equal [0, 5, 15], @tree.values(3)
    assert_equal [0, 5, 15, 20], @tree.values(4)
    assert_equal [0, 2, 5, 15, 20], @tree.values(5)
    assert_equal [0, 2, 5, 7, 15, 20], @tree.values(6)
    assert_equal [0, 2, 7, 15, 20], @tree.values(7)
  end

  def test_insert_and_make_empty_again
    # this test verifies if sentinel node is handled correctly
    @tree.insert(0)
    @tree.remove(0)
    assert_equal 2, @tree.version

    assert_equal [], @tree.values(0)
    assert_equal [0], @tree.values(1)
    assert_equal [], @tree.values(2)

    v = @tree.version_at(0)
    assert !v.include?(0)

    v = @tree.version_at(1)
    assert v.include?(0)

    v = @tree.version_at(2)
    assert !v.include?(0)
  end

  # some ignored tests
  # Because of this:
  # if n.parent.nil?
  #  raise "not implemented"
  #  #@root = rightmost
  # TO fix it, need to replace root+sentinel by forest of roots
  def test_insert_many_remove_one_pbt
    omit 'need to replace sentinel with forest'
  end

  def test_remove6
    omit 'need to replace sentinel with forest'
  end

  def test_insert_many_remove_one_example
    omit 'need to replace sentinel with forest'
  end
end
