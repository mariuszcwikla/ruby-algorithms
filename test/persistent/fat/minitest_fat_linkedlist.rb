require_relative '../../test_helper'
require_relative '../../../lib/persistent/partial/fat/linkedlist'

class FatLinkedListTest < Minitest::Test
  def setup
    @list = Fat::LinkedList.new
  end

  def test_0
    assert_equal 0, @list.version
    assert_equal [], @list.values(0)
  end

  def test_1
    @list.insert_after(@list.head, 0)
    assert_equal 1, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
  end

  def test_2
    @list.insert_after(@list.head, 0)
    @list.insert_after(@list.head, 1)
    assert_equal 2, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [1, 0], @list.values(2)
  end

  def test_3
    @list.insert_after(@list.head, 0)
    @list.insert_after(@list.head, 1)
    @list.insert_after(@list.head, 2)
    assert_equal 3, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [1, 0], @list.values(2)
    assert_equal [2, 1, 0], @list.values(3)
  end

  def test_4
    a = @list.insert_after(@list.head, 0)
    @list.insert_after(a, 1)
    assert_equal 2, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [0, 1], @list.values(2)
  end

  def test_5
    a = @list.insert_after(@list.head, 0)
    @list.insert_after(a, 1)
    @list.insert_after(a, 2)
    assert_equal 3, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [0, 1], @list.values(2)
    assert_equal [0, 2, 1], @list.values(3)
  end

  def test_6
    n = @list.insert_after(@list.head, 0)
    @list.insert_after(n, 1)
    @list.insert_after(@list.head, 2)

    assert_equal 3, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [0, 1], @list.values(2)
    assert_equal [2, 0, 1], @list.values(3)
  end

  def test_7
    a = @list.insert_after(@list.head, 0)
    b = @list.insert_after(a, 9)
    @list.insert_after(b, 10)
    @list.insert_after(@list.head, 1)
    @list.insert_after(b, 2)
    @list.insert_after(@list.head, 3)
    # @list.insert_after(@list.head, 4)
    # @list.insert_after(a, 5)
    # @list.insert_after(a, 3)

    # assert_equal 5, @list.version

    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [0, 9], @list.values(2)
    assert_equal [0, 9, 10], @list.values(3)
    assert_equal [1, 0, 9, 10], @list.values(4)
    assert_equal [1, 0, 9, 2, 10], @list.values(5)
    assert_equal [3, 1, 0, 9, 2, 10], @list.values(6)
  end

  def test_remove_1
    n = @list.insert_after(@list.head, 0)
    @list.remove(n)

    assert_equal 2, @list.version
    assert_equal [], @list.values(0)
    assert_equal [0], @list.values(1)
    assert_equal [], @list.values(2)
  end
end
