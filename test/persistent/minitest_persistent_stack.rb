require_relative '../test_helper'
require_relative '../../lib/persistent/persistent_stack'

class PersistentStackTest < Minitest::Test
  def setup
    @stack = Persistent::Stack.new
  end

  def test_empty
    assert @stack.empty?
  end

  def test_1
    s = @stack.push 1
    assert @stack.empty?
    assert !s.empty?

    assert s.include? 1
  end

  def test_2
    s = @stack.push 1
    s2 = s.push 5
    assert @stack.empty?
    assert !s.empty?

    assert s.include? 1
    assert !s.include?(5)

    assert s2.include? 1
    assert s2.include? 5
  end

  def test_pop_1
    s = @stack.push 1
    v, s2 = s.pop

    assert !@stack.include?(1)
    assert s.include? 1
    assert !s2.include?(1)

    assert_equal 1, v
  end

  def test_pop_2
    s = @stack.push 1
    s = s.push 2
    v, s2 = s.pop

    assert_equal 2, v

    v, s3 = s2.pop
    assert_equal 1, v
    assert s3.empty?

    assert s2.include? 1
  end
end
