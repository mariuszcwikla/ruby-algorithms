require_relative '../test_helper'
require_relative '../../lib/stringmatch/hoorspool'
require_relative 'string_match_mixin'

class Test_Hoorspool < Minitest::Test
  include StringMatchMixin

  def setup
    @method = :hoorspool_string_match
  end
end
