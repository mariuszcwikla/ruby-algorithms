require_relative '../test_helper'
require_relative '../../lib/stringmatch/border_strings'
require_relative 'string_match_mixin'

class Test_BorderStringMatch < Minitest::Test
  include StringMatchMixin

  def setup
    @method = :border_string_match
  end
end
