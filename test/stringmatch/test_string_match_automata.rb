require_relative '../test_helper'
require_relative '../../lib/stringmatch/string_match_automata'
require_relative 'string_match_mixin'

def string_match_automata(text, pattern)
  a = build_automata(pattern)
  automata_match(a, text)
end

def string_match_automata_Om3(text, pattern)
  a = build_automata_Om3(pattern)
  automata_match(a, text)
end

def string_match_automata_Om(text, pattern)
  a = build_automata_Om(pattern)
  automata_match(a, text)
end

class Test_StringMatchAutomata < Minitest::Test
  include StringMatchMixin
  def setup
    @method = :string_match_automata
  end

  def test_case
                           #ababbabaaa  (15 znaków)
    text = "abababbabaaabbbabbbbabaaabbbbaabababbbababaaaababaaabbbbbabaaabbbbbbabbbbaabaabaabbbaa"
    pattern = "ababbabaaa"
    expected = text.index(pattern) || 0
    assert_equal expected, string_match_automata(text, pattern)
  end

  def test_delta
    #def delta(x, pattern)
    assert_equal 0, delta("a", "abab")
    assert_equal 0, delta("aa", "abab")
    assert_equal 1, delta("b", "abab")
    assert_equal 2, delta("ab", "abab")
    assert_equal 3, delta("bab", "abab")
    assert_equal 4, delta("abab", "abab")
  end
end
class Test_StringMatchAutomataOm3 < Minitest::Test
  include StringMatchMixin
  def setup
    @method = :string_match_automata_Om3
  end

  def test_delta
    #def delta(x, pattern)
    pattern = "abab"
    # positive cases
    assert_equal 1, delta_Om3("abab", 0, "a")
    assert_equal 2, delta_Om3("abab", 1, "b")
    assert_equal 3, delta_Om3("abab", 2, "a")
    assert_equal 4, delta_Om3("abab", 3, "b")

    # negative cases
    assert_equal 0, delta_Om3("abab", 0, "b")
    assert_equal 1, delta_Om3("abab", 1, "a")
    assert_equal 0, delta_Om3("abab", 2, "b")
    assert_equal 1, delta_Om3("abab", 3, "a")
  end
end
class Test_StringMatchAutomataOm   < Minitest::Test
  include StringMatchMixin
  def setup
    @method = :string_match_automata_Om
  end
end