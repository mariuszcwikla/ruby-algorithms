require_relative '../test_helper'
require_relative '../../lib/stringmatch/rabin_karp'
require_relative 'string_match_mixin'

class Test_RabinKarp < Minitest::Test
  include StringMatchMixin
  def setup
    @method = :rabin_karp_match
  end
end

def rabin_karp_match_buzzhash(a, b)
  rabin_karp_match(a, b, :buzz)
end

class Test_RabinKarp_Buzzhash < Minitest::Test
  include StringMatchMixin
  def setup
    @method = :rabin_karp_match_buzzhash
  end
end
