require_relative "string_match_mixin"
require_relative '../test_helper'
require_relative '../../lib/stringmatch/aho_corasick'

def aho_corasick_match_single_string(text, pattern)
  t = AhoCorasickTrie.new
  t.insert(pattern)
  t.aho_corasick_prepare
  t.search(text) {|string, i| return i}
  -1
end

# reusing single-string tests to prove correctness of Aho-Corasick implementation
# For multiple patterns we need different tests.
class AhoCorasickSingleStringTest < Minitest::Test
  include StringMatchMixin

  def setup
    @method = :aho_corasick_match_single_string
  end
end

def find_all_occurrences_brute(text, pattern)
  occurrences = []
  i = 0
  until (i = text.index(pattern, i)).nil? || i >= text.length-1 
    occurrences << i
    i += 1
  end
  occurrences
end

class AhoCorasickMultiTest < Minitest::Test
  def test_rantly
    property_of {
      num_patterns = range(5, 50)

      patterns = []
      num_patterns.times do
      len = range(5, 20)
      patterns << array(len){choose('a', 'b')}.join('')
      guard patterns.uniq.size == patterns.size
      end
      [
        array(1000){choose('a', 'b')}.join(''),
        patterns
      ]
    }.check { |text, patterns|
      begin
      t = AhoCorasickTrie.new
      patterns.each do |p|
        t.insert p
      end
      t.aho_corasick_prepare

      results = Hash.new { |h, k| h[k]=[] }
      t.search(text) { |string, index| results[string] << index }

      patterns.each do |p|
        expected = find_all_occurrences_brute(text, p).sort
        actual = results[p].sort
        assert_equal(expected, actual, "Failed for pattern #{p}")
      end
      rescue => e
        puts e.message
        puts e.backtrace
        raise e
      end
    }
  end

  def test_example
    data = ["bbabababab", ["bbbbaba", "babab"]]
    text, patterns = data
    t = AhoCorasickTrie.new
    patterns.each do |p|
      t.insert p
    end
    t.aho_corasick_prepare
    results = Hash.new { |h, k| h[k]=[] }
    t.search(text) { |string, index| results[string] << index }

    patterns.each do |p|
      expected = find_all_occurrences_brute(text, p).sort
      actual = results[p].sort
      assert_equal(expected, actual, "Failed for pattern #{p}")
    end
  end
end