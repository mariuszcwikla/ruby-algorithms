require_relative '../test_helper'
require_relative '../../lib/stringmatch/knuth_morris_pratt'
require_relative 'string_match_mixin'

class KnutMorrisPrattTest < Minitest::Test
  include StringMatchMixin

  def setup
    @method = :kmp_match
  end

  def test_compute_pi_function_not_optimized
    pi = compute_pi_function_not_optimized('ababaca')
    expected = [0, 0, 1, 2, 3, 0, 1]
    assert_equal expected, pi
  end

  def test_compute_pi_function_not_optimized2
    pi = compute_pi_function_not_optimized('aabaaab')
    expected = [0, 1, 0, 1, 2, 2, 3]
    assert_equal expected, pi
  end

  def test_compute_pi_function
    pi = compute_pi_function('ababaca')
    expected = [0, 0, 1, 2, 3, 0, 1]
    assert_equal expected, pi
  end

  def test_compute_pi_function2
    pi = compute_pi_function('babbaa')
    expected = [0, 0, 1, 1, 2, 0]
    assert_equal expected, pi
  end

  def test_compute_pi_function3
    pi = compute_pi_function('bbabbb')
    expected = [0, 1, 0, 1, 2, 2]
    assert_equal expected, pi
  end

  def test_compute_pi_function4
    pi = compute_pi_function('babbaba')
    expected =  [0, 0, 1, 1, 2, 3, 2]
    assert_equal expected, pi
  end

  def test_both_compute_pi_function_are_equivalent
    property_of {array(20){choose('a', 'b')}.join('')}
    .check(1000) do |s|
      expected = compute_pi_function_not_optimized(s)
      actual = compute_pi_function(s)
      assert_equal expected, actual
    end
  end

  def test_pi_star
    p = 'aabaaab'
    assert_equal([], pi_star(p, 0))
    assert_equal(["a"], pi_star(p, 1))
    assert_equal([], pi_star(p, 2))
    assert_equal(["a"], pi_star(p, 3))
    assert_equal(["aa", "a"], pi_star(p, 4))
    assert_equal(["aa", "a"], pi_star(p, 5))
    assert_equal(["aab"], pi_star(p, 6))
  end
end
