module StringMatchMixin
  def call(text, pattern)
    method(@method).call(text, pattern)
  end

  def test_hello_hello
    assert_equal 0, call('hello', 'hello')
  end

  def test_hehelloworld_hello
    assert_equal 2, call('hehelloworld', 'hello')
  end

  def test_hehelloworld_world
    assert_equal 7, call('hehelloworld', 'world')
  end

  def test_hehelloworld_worldz
    assert_equal(-1, call('hehelloworld', 'worldz'))
  end

  def test_aaaaab_aab
    assert_equal(3, call('aaaaab', 'aab'))
  end

  def test_ABBBBRBBFOOBARCCC_FOOBAR
    assert_equal(8, call('ABBBBRBBFOOBARCCC', 'FOOBAR'))
  end

  def test_1
    assert_equal(2, call('abababba', 'ababba'))
  end

  def test_2
    assert_equal(-1, call('abbaaaabbb', 'bbbaa'))
  end

  def test_rantly
    property_of {
      len = range(5, 20)
      [
        array(100){choose('a', 'b')}.join(''),
        array(len){choose('a', 'b')}.join('')
      ]
    }.check(100) { |text, pattern|
      a = text.index(pattern)
      a = -1 if a.nil?
      b = call(text, pattern)
      assert_equal a, b
    }
  end

  def test_example
    text, pattern = ["bababbbaaabababbbbbababbbabbaabbabaaabaabababbbbaabaabababbbaabbabaabbaaabbbbbbaababbbaaabbbababaaba", "ababb"]
    a = text.index(pattern)
    a = -1 if a.nil?
    b = call(text, pattern)
    assert_equal a, b
  end
end