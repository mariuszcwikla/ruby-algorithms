require_relative '../test_helper'
require_relative '../../lib/cryptography/rsa'

class RSATest < Minitest::Test

  def test_textbook_rsa
    priv, pub = TextbookRSA.generate_key_pair
    property_of{
      range(0, pub.n)
    }.check {|m|
      c = TextbookRSA.encrypt(m, pub)
      decrypted = TextbookRSA.decrypt(c, priv)
      assert_equal(m, decrypted)
    }
  end

  def test_rsa
    priv, pub = TextbookRSA.generate_key_pair
    property_of{
      range(1, 1 << (pub.size_in_bits - 4*8 - 1 ))   # subtract -4*8 due to pkcs padding. Not sure why I need "-1"?
    }.check {|m|
      c = RSA.encrypt(m, pub)
      decrypted = RSA.decrypt(c, priv)
      assert_equal(m, decrypted)
    }
  end
end
