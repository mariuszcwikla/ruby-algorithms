require_relative '../test_helper'
require_relative '../../lib/cryptography/feal'

class FealTest < Minitest::Test
  def test_encrypt
    m = from_hex '0000000000000000'
    k = from_hex '0123456789abcdef'
    assert_equal 'CEEF2C86F2490752', Feal.encrypt_block(m, k).to_hex
  end
  
  def test_decrypt
    c = from_hex 'CEEF2C86F2490752'
    k = from_hex '0123456789abcdef'
    assert_equal '0000000000000000', Feal.decrypt_block(c, k).to_hex
  end

  def test_rantly
    property_of{
      m = [
        range(256),
        range(256),
        range(256),
        range(256),

        range(256),
        range(256),
        range(256),
        range(256)
      ]

      k = [
        range(256),
        range(256),
        range(256),
        range(256),

        range(256),
        range(256),
        range(256),
        range(256)
      ]
      [m, k]
    }.check {|m, k|
      c = Feal.encrypt_block(m, k)
      m2 = Feal.decrypt_block(c, k)
      assert_equal m, m2
    }
  end
end
