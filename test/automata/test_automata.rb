require_relative '../test_helper'
require_relative '../../lib/automata/automata'

class TestGenericDFA < Minitest::Test
  def test_accept_1
    n = Automaton.new
    n.start_state = :p1
    n.final_states << :p3
    n.add_transition(:p1, :p2, 'a')
    n.add_transition(:p2, :p3, 'b')

    assert n.accept?('ab')
    refute n.accept?('aa')
    refute n.accept?('bb')
    refute n.accept?('a')
    refute n.accept?('abc')
  end

  def test_accept_with_other
    n = Automaton.new
    n.start_state = :p1
    n.final_states << :p3
    n.add_transition(:p1, :p2, 'a')
    n.add_transition(:p2, :p3, :other)

    assert n.accept?('ab')
    assert n.accept?('aa')
    assert n.accept?('ac')

    refute n.accept?('a')
    refute n.accept?('bb')
  end
end
