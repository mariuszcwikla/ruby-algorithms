require_relative '../test_helper'
require_relative '../../lib/automata/automata'

class TestNDFAToDFA < Minitest::Test
  def test_states
    n = Automaton.new
    n.start_state = :p1
    n.final_states << :p3
    n.add_transition(:p1, :p2, :epsilon)
    n.add_transition(:p2, :p3, :epsilon)
    n.add_transition(:p1, :p3, 'a')
    assert_equal %i[p1 p2 p3], n.states.sort
    assert !n.is_dfa?
  end

  def test_epsilon_closure1
    n = Automaton.new
    n.start_state = :p1
    n.final_states << :p3
    n.add_transition(:p1, :p2, :epsilon)
    n.add_transition(:p2, :p3, :epsilon)
    n.add_transition(:p1, :p3, 'a')

    assert_equal %i[p1 p2 p3], n.epsilon_closure(:p1)
    assert_equal %i[p2 p3], n.epsilon_closure(:p2)
    assert_equal %i[p3], n.epsilon_closure(:p3)
    assert !n.is_dfa?
  end

  def test_epsilon_closure_set
    n = Automaton.new
    n.start_state = :p1
    n.final_states << :p3
    n.add_transition(:p1, :p2, :epsilon)
    n.add_transition(:p2, :p3, :epsilon)
    n.add_transition(:p1, :p3, 'a')

    assert_equal %i[p1 p2 p3], n.epsilon_closure(:p1, :p2)
    assert_equal %i[p2 p3], n.epsilon_closure(:p2, :p3)
    assert !n.is_dfa?
  end

  def test_epsilon_closure_set2
    n = Automaton.new
    n.start_state = :p1
    n.final_states << :p4
    n.add_transition(:p1, :p2, :epsilon)
    n.add_transition(:p2, :p3, 'a')
    n.add_transition(:p3, :p4, :epsilon)
    n.add_transition(:p1, :p5, 'b')
    n.add_transition(:p5, :p4, :epsilon)
    assert_equal %i[p1 p2], n.epsilon_closure(:p1, :p2).sort
    assert_equal %i[p1 p2 p3 p4], n.epsilon_closure(:p1, :p3).sort
    assert_equal %i[p2 p4 p5], n.epsilon_closure(:p5, :p2).sort
    assert !n.is_dfa?
  end

  def test_epsilon_closure2
    n = Automaton.new
    n.start_state = :A
    n.final_states << :C
    n.add_transition(:A, :A, 1)
    n.add_transition(:A, :B, :epsilon)
    n.add_transition(:A, :B, 0)
    n.add_transition(:A, :C, 0)

    n.add_transition(:B, :B, 1)
    n.add_transition(:B, :C, :epsilon)

    n.add_transition(:C, :C, 0)
    n.add_transition(:C, :C, 1)
    assert_equal %i[A B C], n.epsilon_closure(:A)
    assert !n.is_dfa?
  end

  def test_eliminate_epsilon
    n = Automaton.new
    n.start_state = :A
    n.final_states << :D
    n.add_transition(:A, :E, 0)
    n.add_transition(:A, :B, 1)
    n.add_transition(:B, :C, 1)
    n.add_transition(:B, :D, :epsilon)

    n.add_transition(:C, :D, 1)

    n.add_transition(:E, :F, 0)
    n.add_transition(:E, :B, :epsilon)
    n.add_transition(:E, :C, :epsilon)

    n.add_transition(:F, :D, 0)

    # n.display
    n = n.eliminate_epsilon
    # n.display
    assert_equal %i[B D E], n.final_states.sort.to_a
    assert_equal :A, n.start_state

    assert_equal 0, n.transitions.select { |t| t.symbol == :epsilon }.size

    expected = [[:A, 0, :E], [:A, 1, :B], [:B, 1, :C], [:C, 1, :D], [:E, 0, :F], [:E, 1, :C], [:E, 1, :D], [:F, 0, :D]]
    actual = n.transitions.map { |t| [t.state1, t.symbol, t.state2] }.sort
    assert_equal expected, actual
  end

  def test_ndfa_to_dfa_without_epsilon
    n = Automaton.new
    n.start_state = :q0
    n.final_states << :q2
    n.add_transition(:q0, :q0, 0)
    n.add_transition(:q0, :q1, 1)
    n.add_transition(:q1, :q1, 0)
    n.add_transition(:q1, :q1, 1)
    n.add_transition(:q1, :q2, 0)
    n.add_transition(:q2, :q2, 0)
    n.add_transition(:q2, :q2, 1)
    n.add_transition(:q2, :q1, 1)

    assert !n.is_dfa?

    d = n.to_dfa(true)
    assert_equal [%i[q0], %i[q1], %i[q1 q2], %i[q2]], d.states.sort

    assert d.is_dfa?
    assert d.include_transition?([:q0], [:q1], 1)
    assert d.include_transition?([:q0], [:q0], 0)
    assert d.include_transition?([:q1], [:q1], 1)
    assert d.include_transition?([:q1], %i[q1 q2], 0)
    assert d.include_transition?(%i[q1 q2], %i[q1 q2], 0)
    assert d.include_transition?(%i[q1 q2], %i[q1 q2], 1)
    assert d.include_transition?([:q2], %i[q1 q2], 1)
    assert d.include_transition?([:q2], [:q2], 0)
    assert_equal 2, d.final_states.size
  end

  def test_ndfa_to_dfa_without_epsilon2
    n = Automaton.new
    n.start_state = :q1
    n.final_states += %i[q1 q2]
    n.add_transition(:q1, :q2, 0)
    n.add_transition(:q2, :q2, 0)

    d = n.to_dfa
    assert d.final_state?([:q1])
    assert d.final_state?([:q2])
    assert_equal 2, d.final_states.size
    assert d.is_dfa?
  end
end
