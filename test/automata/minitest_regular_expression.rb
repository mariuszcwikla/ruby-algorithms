require_relative '../test_helper'
require_relative '../../lib/automata/automata'

class TestRegularExpression < Minitest::Test
  # def test_epsilon
  #   n = RegularExpression.ndfa('')
  #   assert_equal :S0, n.start_state
  #   assert_nil n.final_state
  # end

  def test_single_char
    r = RegularExpression.new('a')
    assert r.matches('a')
    refute r.matches('aa')
    refute r.matches('')
    refute r.matches('b')
  end

  def test_concat
    r = RegularExpression.new('ab')
    assert r.matches('ab')
    refute r.matches('')
    refute r.matches('a')
    refute r.matches('b')
  end

  def test_concat2
    n = RegularExpression.new('abc')
    assert n.matches('abc')
  end

  def test_or
    r = RegularExpression.new('a|b')
    assert r.matches('a')
    assert r.matches('b')
    refute r.matches('')
  end

  def test_or2
    r = RegularExpression.new('ab|b')
    assert r.matches('ab')
    assert r.matches('b')
    refute r.matches('')
  end

  def test_klenee
    r = RegularExpression.new('a*')
    assert r.matches('')
    assert r.matches('a')
    assert r.matches('aa')
    assert r.matches('aaa')
    assert r.matches('aaaaaaaaaa')
  end

  def test_klenee2
    r = RegularExpression.new('a*b')
    assert r.matches('b')
    assert r.matches('ab')
    assert r.matches('aab')
    assert r.matches('aaaaaaaaaab')

    refute r.matches('abb')
  end

  def test_single_char_parenthesis
    r = RegularExpression.new('(a)')
    assert r.matches('a')
  end

  def test_concat_parenthesis1
    r = RegularExpression.new('(a)b')
    assert r.matches('ab')
  end

  def test_concat_parenthesis2
    r = RegularExpression.new('a(b)')
    assert r.matches('ab')
  end

  def test_concat_parenthesis3
    r = RegularExpression.new('(a)(b)')
    assert r.matches('ab')
  end

  def test_klenee_parenthesis
    r = RegularExpression.new('(ab)*')

    assert r.matches('')
    assert r.matches('ab')
    assert r.matches('abab')
    assert r.matches('ababab')

    refute r.matches('a')
    refute r.matches('aba')
  end

  def test_complex1
    r = RegularExpression.new('(ab|c*)*|d')

    assert r.matches('ab')
    assert r.matches('abc')
    assert r.matches('abcccab')
    assert r.matches('c')
    assert r.matches('d')

    assert !r.matches('abcd')
  end
end
