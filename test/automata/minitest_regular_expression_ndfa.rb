require_relative '../test_helper'
require_relative '../../lib/automata/automata'

class TestRegularExpressionToNDFA < Minitest::Test
  # def test_epsilon
  #   n = RegularExpression.ndfa('')
  #   assert_equal :S0, n.start_state
  #   assert_nil n.final_state
  # end

  def test_single_char
    n = RegularExpression.ndfa('a')
    assert_equal :S0, n.start_state
    assert_equal [:S1], n.final_states.to_a
    assert n.include_transition?(:S0, :S1, 'a')
  end

  def test_concat
    n = RegularExpression.ndfa('ab')
    assert_equal 3, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_concat2
    n = RegularExpression.ndfa('abc')
    assert_equal 5, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert n.include_transition?(:S3, :S4, :epsilon)
    assert n.include_transition?(:S4, :S5, 'c')
    assert_equal [:S5], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_or
    n = RegularExpression.ndfa('a|b')
    # n.to_graphviz.display(true, 'LR')
    assert_equal 6, n.transitions.size

    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S2, :S3, 'b')
    assert n.include_transition?(:S4, :S0, :epsilon)
    assert n.include_transition?(:S4, :S2, :epsilon)
    assert n.include_transition?(:S1, :S5, :epsilon)
    assert n.include_transition?(:S3, :S5, :epsilon)

    assert_equal :S4, n.start_state
    assert_equal [:S5], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_or2
    n = RegularExpression.ndfa('ab|b')
    # n.to_graphviz.display(true, 'LR')
    assert_equal 8, n.transitions.size

    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S2, :S3, 'b')
    assert n.include_transition?(:S4, :S5, 'b')
    assert n.include_transition?(:S6, :S0, :epsilon)
    assert n.include_transition?(:S6, :S4, :epsilon)
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S3, :S7, :epsilon)
    assert n.include_transition?(:S5, :S7, :epsilon)
    assert !n.is_dfa?
  end

  def test_klenee
    n = RegularExpression.ndfa('a*')
    # n.to_graph.display(true, 'LR')

    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S2, :S0, :epsilon)
    assert n.include_transition?(:S1, :S3, :epsilon)
    assert n.include_transition?(:S2, :S3, :epsilon)
    assert n.include_transition?(:S3, :S2, :epsilon)

    assert_equal :S2, n.start_state
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_klenee2
    n = RegularExpression.ndfa('a*b')
    # n.to_graph.display(true, 'LR')

    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S2, :S0, :epsilon)
    assert n.include_transition?(:S1, :S3, :epsilon)
    assert n.include_transition?(:S2, :S3, :epsilon)
    assert n.include_transition?(:S3, :S2, :epsilon)
    assert n.include_transition?(:S3, :S4, :epsilon)
    assert n.include_transition?(:S4, :S5, 'b')

    assert_equal :S2, n.start_state
    assert_equal [:S5], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_single_char_parenthesis
    # skip 'foo'
    n = RegularExpression.ndfa('(a)')
    assert_equal :S0, n.start_state
    assert_equal :S1, n.final_states.first
    assert n.include_transition?(:S0, :S1, 'a')
  end

  def test_concat_parenthesis1
    n = RegularExpression.ndfa('(a)b')
    assert_equal 3, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_concat_parenthesis2
    n = RegularExpression.ndfa('a(b)')
    assert_equal 3, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_concat_parenthesis3
    n = RegularExpression.ndfa('(a)(b)')
    assert_equal 3, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_concat_parenthesis4
    n = RegularExpression.ndfa('((a))(b)')
    assert_equal 3, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_concat_parenthesis5
    n = RegularExpression.ndfa('(((a))(b))')
    assert_equal 3, n.transitions.size
    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert_equal [:S3], n.final_states.to_a
    assert !n.is_dfa?
  end

  def test_klenee_parenthesis
    n = RegularExpression.ndfa('(ab)*')

    assert n.include_transition?(:S0, :S1, 'a')
    assert n.include_transition?(:S1, :S2, :epsilon)
    assert n.include_transition?(:S2, :S3, 'b')
    assert n.include_transition?(:S3, :S5, :epsilon)

    assert n.include_transition?(:S4, :S5, :epsilon)
    assert n.include_transition?(:S5, :S4, :epsilon)

    assert_equal :S4, n.start_state
    assert_equal [:S5], n.final_states.to_a
    assert !n.is_dfa?
    # n.display

    # n.eliminate_epsilon.to_dfa.display
  end
end
