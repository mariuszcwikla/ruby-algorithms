#!/usr/bin/env ruby

require_relative 'test_numbers'

require_relative 'graph/test_dfs'
require_relative 'graph/test_graph'
require_relative 'graph/test_graphviz'
require_relative 'graph/test_prim'
require_relative 'graph/test_kruskal'

require_relative 'heap/test_heap'
require_relative 'heap/test_binomial_heap'

require_relative 'sorting/test_bubble_sort'
require_relative 'sorting/test_merge_sort'
require_relative 'sorting/test_selection_sort'

require_relative '../selection/test_selection'
require_relative '../sorting/test_quicksort'

require_relative '../tree/test_avltree'

require_relative 'sets/test_disjoint_sets'
require_relative 'sets/test_disjoint_sets_quickmerge'
