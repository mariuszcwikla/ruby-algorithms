require_relative '../test_helper'
require_relative '../../lib/string/levenstein'
require_relative '../../lib/string/levenhstein_automata'

class LevensteinTest < Minitest::Test
  [
    ['a', 'b', 1],
    ['b', 'b', 0],
    ['b', 'b', 0],
    ['ab', 'a', 1],
    ['aa', 'a', 1],
    ['bb', 'a', 2],
    ['a', 'ab', 1],
    ['a', 'aa', 1],
    ['a', 'bb', 2],
    ['kitten', 'sitting', 3],
    ['Saturday', 'Sunday', 3]
  ].each_with_index do |d, i|
    define_method("test_#{i}") do
      msg = "levenstein(#{d[0]}, #{d[1]}) should be #{d[2]}"
      assert_equal d[2], levenstein(d[0], d[1]), msg

      msg = "levenstein(#{d[1]}, #{d[0]}) should be #{d[2]}"
      assert_equal d[2], levenstein(d[1], d[0]), msg
    end
  end
end

class LevensteinAutomataTest < Minitest::Test
  def test_automata_food_1_build
    automata = levenhstein_automata_nfa('food', 1)

    assert_equal(10, automata.states.size)

    # f->o->o->d
    assert automata.include_transition?('0_0', '1_0', 'f')
    assert automata.include_transition?('1_0', '2_0', 'o')
    assert automata.include_transition?('2_0', '3_0', 'o')
    assert automata.include_transition?('3_0', '4_0', 'd')

    # deletions - vertical
    assert automata.include_transition?('0_0', '0_1', :other)
    assert automata.include_transition?('1_0', '1_1', :other)
    assert automata.include_transition?('2_0', '2_1', :other)
    assert automata.include_transition?('3_0', '3_1', :other)

    # substitution - diagonal
    assert automata.include_transition?('0_0', '1_1', :other)
    assert automata.include_transition?('1_0', '2_1', :other)
    assert automata.include_transition?('2_0', '3_1', :other)
    assert automata.include_transition?('3_0', '4_1', :other)

    # removal - diagonal
    assert automata.include_transition?('0_0', '1_1', :epsilon)
    assert automata.include_transition?('1_0', '2_1', :epsilon)
    assert automata.include_transition?('2_0', '3_1', :epsilon)
    assert automata.include_transition?('3_0', '4_1', :epsilon)
  end

  def test_automata_food_1_process
    automata = levenhstein_automata_dfa('food', 1)
    assert automata.accept?('food')

    # deletion of single char
    assert automata.accept?('xfood')
    assert automata.accept?('fxood')
    assert automata.accept?('foxod')
    assert automata.accept?('fooxd')
    assert automata.accept?('foodx')

    # substitution of single char
    assert automata.accept?('wood')
    assert automata.accept?('fwod')
    assert automata.accept?('fowd')
    assert automata.accept?('foos')

    # removal of single char
    assert automata.accept?('ood')
    assert automata.accept?('fod')
    assert automata.accept?('foo')
  end
end
