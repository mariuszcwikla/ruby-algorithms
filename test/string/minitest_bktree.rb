require_relative '../test_helper'
require_relative '../../lib/string/bktree'

class BKTreeTest < Minitest::Test
  def setup
    @t = BKTree.new

    @t.insert('test')

    @t.insert('have')
    @t.insert('had')
    @t.insert("haven't")
    @t.insert('has')

    @t.insert('drive')
    @t.insert('drove')
    @t.insert('driven')
    @t.insert('driver')
    @t.insert('drives')
    @t.insert('drivers')
  end

  def test_1
    words = @t.find_similar('drive').sort
    assert_equal %w[drive driven driver drives drove], words
  end
end
