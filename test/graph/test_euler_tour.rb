require_relative '../test_helper' # TODO you need to fix PATH to this module!
require_relative '../../lib/graph/euler_tour' # TODO you need to fix PATH to this module!

# Implementation based on https://codeforces.com/blog/entry/18369?
class EulerTourTest < Minitest::Test

  def setup
    # tree from https://codeforces.com/blog/entry/18369?
    @g = Hash.new{|h, k| h[k]=[]}
    @g[1] = [2, 5]
    @g[2] = [3, 4]
  end

  def test_euler_tour1
    expected = [[1, 2], [2, 3], [3, 2], [2, 4], [4, 2], [2, 1], [1, 5], [5, 1]]
    assert_equal expected, euler_tour1(@g, 1)
  end

  def test_euler_tour2
    expected = [1, 2, 3, 3, 4, 4, 2, 5, 5, 1]
    assert_equal expected, euler_tour2(@g, 1)
  end

  def test_euler_tour3
    expected = [1, 2, 3, 2, 4, 2, 1, 5, 1]
    assert_equal expected, euler_tour3(@g, 1)
  end
end
