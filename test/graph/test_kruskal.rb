require_relative '../test_helper'

require_relative '../../lib/graph/kruskal'

class Test_Kruskal < Minitest::Test
  def setup
    @g = Graph.new
  end

  def test_kruskal1
    g = Graph.new
    e1 = g.add_uedge(1, 2)
    e2 = g.add_uedge(1, 3)
    e3 = g.add_uedge(2, 3)
    e4 = g.add_uedge(2, 4)
    e5 = g.add_uedge(3, 4)

    e1.weight = 2
    e2.weight = 3
    e3.weight = 2
    e4.weight = 5
    e5.weight = 4

    indices = g.kruskal.map { |e| [e.first.index, e.second.index] }

    assert_equal [[1, 2], [2, 3], [3, 4]], indices.sort
  end

  def test_kruskal2
    g = Graph.new
    e1 = g.add_uedge(1, 2)
    e2 = g.add_uedge(1, 3)
    e3 = g.add_uedge(2, 3)
    e4 = g.add_uedge(2, 4)
    e5 = g.add_uedge(3, 4)

    e1.weight = 2
    e2.weight = 3
    e3.weight = 2
    e4.weight = 5
    e5.weight = 8

    indices = g.kruskal.map { |e| [e.first.index, e.second.index] }

    assert_equal [[1, 2], [2, 3], [2, 4]], indices.sort
  end

  def test_prim3
    g = Graph.new
    e1 = g.add_uedge(1, 2)
    e2 = g.add_uedge(1, 3)
    e3 = g.add_uedge(2, 3)
    e4 = g.add_uedge(2, 4)
    e5 = g.add_uedge(3, 4)
    e6 = g.add_uedge(4, 5)
    e7 = g.add_uedge(3, 5)

    e1.weight = 2
    e2.weight = 3
    e3.weight = 2
    e4.weight = 5
    e5.weight = 8
    e6.weight = 1
    e7.weight = 4

    indices = g.kruskal.map { |e| [e.first.index, e.second.index] }.sort

    assert_equal [[1, 2], [2, 3], [3, 5], [4, 5]], indices
  end
end
