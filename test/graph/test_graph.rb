require_relative '../test_helper'
require_relative '../../lib/graph/graph'

class GraphTest < Minitest::Test
  def setup
    @g = Graph.new
  end

  def test_create_one_vertex
    v = @g.vertex(0)
    assert_not_nil v
    assert_equal 0, v.index
  end

  def test_create_and_get_vertex
    v1 = @g.vertex(5)
    v2 = @g.vertex(5)
    assert_same v1, v2
  end

  def test_add_edge_creates_vertices
    @g.add_edge(1, 2)

    assert_equal [1, 2], @g.vertices.map(&:index)
  end

  def test_add_add_edge_and_get
    @g.add_edge(1, 2)
    assert_equal [[1, 2]], @g.edge_indices
  end

  def test_vertex_add_edge
    @g.vertex(1).add_edge(2)
    assert_equal [[1, 2]], @g.edge_indices
  end

  def test_vertex_exists
    assert_false @g.vertex_exists?(0)

    @g.vertex(5)
    assert_true @g.vertex_exists?(5)
  end

  def test_add_1_uedge
    e = @g.add_uedge(1, 2)
    assert_false e.directed?
    assert_equal [[1, 2]], @g.edge_indices
  end

  def test_add_2_uedges
    @g.add_uedge(1, 2)
    @g.add_uedge(2, 3)
    assert_equal [[1, 2], [2, 3]], @g.edge_indices
  end

  def test_constructor_with_hash
    g = Graph.new({ 1 => [2, 3], 2 => [4] })
    assert_equal [[1, 2], [1, 3], [2, 4]], g.edge_indices
  end

  def test_array_operators
    g = Graph.new
    g[1] << 2
    g[1] << 4
    g[2] << 3
    assert_equal [[1, 2], [1, 4], [2, 3]], g.edge_indices
  end

  def test_graph_to_s
    g = Graph.new
    g[1] << 2
    g[1] << 4
    g[2] << 3
    expected =
      '1 -> 2 4
2 -> 3
4 ->
3 ->
'
    assert_equal expected, g.to_s
  end

  def test_edge
    g = Graph.new
    g[1] << 2
    g[1] << 4
    g[2] << 3

    assert_not_nil(g.edge(1, 2))
    assert_nil(g.edge(2, 1))
  end

  def test_incoming_edges
    g = Graph.new
    g.add_edge(1, 2)

    incoming = g.vertex(2).incoming_edges
    assert_equal [1, 2], incoming[0].to_a
  end
end
