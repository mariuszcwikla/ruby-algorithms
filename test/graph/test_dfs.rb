require_relative '../test_helper'

require_relative '../../lib/graph/dfs'

class Test_Dfs < Minitest::Test
  def setup
    @g = Graph.new
  end

  def test_dfs1
    graph = Graph.new
    g = {}
    g[1] = [2, 3]
    g[2] = [4]
    g[3] = [5]

    graph.load_adj_list_graph(g)

    path = graph.dfs(1)
    assert_equal [1, 2, 4, 3, 5], path
  end

  def test_dfs_tree_3
    @g.add_edge(1, 2)
    @g.add_edge(1, 3)

    path = @g.dfs(1)
    assert_equal [1, 2, 3], path
  end

  def test_dfs_tree_3_undirected
    @g.add_uedge(1, 2)
    @g.add_uedge(1, 3)

    path = @g.dfs(1)
    assert_equal [1, 2, 3], path
  end

  def test_dfs_tree_5
    @g.add_edge(1, 2)
    @g.add_edge(1, 3)
    @g.add_edge(2, 4)
    @g.add_edge(2, 5)

    path = @g.dfs(1)
    assert_equal [1, 2, 4, 5, 3], path
  end

  def test_dfs_tree_5_undirected
    @g.add_uedge(1, 2)
    @g.add_uedge(1, 3)
    @g.add_uedge(2, 4)
    @g.add_uedge(2, 5)

    path = @g.dfs(1)
    assert_equal [1, 2, 4, 5, 3], path
  end

  def test_dfs_graph_3
    @g.add_edge(1, 2)
    @g.add_edge(1, 3)
    @g.add_edge(2, 3)

    path = @g.dfs(1)
    assert_equal [1, 2, 3], path
  end

  def test_dfs_graph_5_undirected
    @g.add_uedge(1, 2)
    @g.add_uedge(1, 3)
    @g.add_uedge(2, 3)
    @g.add_uedge(2, 4)
    @g.add_uedge(4, 5)
    @g.add_uedge(5, 2)

    path = @g.dfs(1)
    assert_equal [1, 2, 3, 4, 5], path
  end
end
