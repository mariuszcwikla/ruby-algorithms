require_relative '../test_helper'
require_relative '../../lib/graph/graphml'
require_relative '../../lib/graph/nearest_neighbour'
require_relative '../../lib/graph/planar_graph'

# just smoke testss
class NearestNeighbourTest < Minitest::Test

  def test_nearest_neighbour1
    g = Graph.new

    g.vertex(0).set_position(0, 0)
    g.vertex(1).set_position(4, 0)
    g.vertex(2).set_position(2, 1)
    g.vertex(3).set_position(1, 2)
    g.vertex(4).set_position(3, 3)
    g.vertex(5).set_position(3, 5)

    0.upto(5).each do |i|
      (i+1).upto(5).each do |j|
        g.uedge_by_distance(i, j)
      end
    end

    path = TravellingSalesman.nearest_neighbour(g)
    
    path.map!{ |e| [e.first.index, e.second.index] }
    assert_equal [[0, 2], [2, 3], [3, 4], [4, 5], [5, 1], [1, 0]], path
  end

  def test_two_opt
    g = Graph.new
    g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph2.graphml', true)
    path = TravellingSalesman.nearest_neighbour(g)
    assert_equal [[42, 43], [43, 47], [47, 45], [45, 46], [46, 44], [44, 42]], path.map(&->(e){[e.first.index.to_i, e.second.index.to_i] })

    opted_iteration, = TravellingSalesman.two_opt_iteration(g, path)
    opted_iteration.map!{ |e| [e.first.index.to_i, e.second.index.to_i] }
    assert_equal [[43, 44], [43, 45], [45, 47], [46, 47], [42, 46], [42, 44]], opted_iteration  # NOTE: 43-47 changed to 43-45 by 2-opt algorithm. Same with 45-46

    opted = TravellingSalesman.two_opt(g, path)
    opted.map!{ |e| [e.first.index.to_i, e.second.index.to_i] }
    assert_equal [[44, 43], [43, 45], [45, 47], [47, 46], [46, 42], [42, 44]], opted

  end

  def test_brute_force
    g = Graph.new
    g.load_from_graphml_file(File.dirname(__FILE__) + '/../../graph/graph2.graphml', true)
    path = TravellingSalesman.brute_force(g)
    path.map!{ |e| [e.first.index.to_i, e.second.index.to_i] }
    assert_equal [[42, 44], [43, 44], [43, 45], [45, 47], [46, 47], [42, 46]], path
  end
end

