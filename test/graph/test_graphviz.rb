require_relative '../test_helper'
require_relative '../../lib/graph/graph'
require_relative '../../lib/graph/graphviz'

class GraphvizTest < Minitest::Test
  def setup
    @g = Graph.new
  end

  def test_graphviz1
    g = Graph.new
    g[1] << 2
    g[1] << 4
    g[2] << 3
    s = g.to_graphviz

    assert_equal 'digraph G{
node [shape=circle,
  ]
  1 -> 2;
  1 -> 4;
  2 -> 3;
}',
                 s
  end

  def test_graphviz_weighted
    e1 = @g.vertex(1).add_edge(2)
    e2 = @g.vertex(2).add_edge(3)

    e1.weight = 2
    e2.weight = 3
    s = @g.to_graphviz

    assert_equal 'digraph G{
node [shape=circle,
  ]
  1 -> 2 [label=2];
  2 -> 3 [label=3];
}',
                 s
  end

  def test_graphviz_unidirected
    g = Graph.new
    g.add_uedge(1, 2)

    s = g.to_graphviz(false)

    assert_equal 'graph G{
node [shape=circle,
  ]
  1 -- 2;
}',
                 s
  end

  def test_graphviz_rankdir
    g = Graph.new
    g[1] << 2
    g[1] << 4
    g[2] << 3
    s = g.to_graphviz(true, 'LR')

    assert_equal 'digraph G{
  rankdir=LR
node [shape=circle,
  ]
  1 -> 2;
  1 -> 4;
  2 -> 3;
}',
                 s
  end

  def test_graphviz_edge_label
    e1 = @g.vertex(1).add_edge(2)
    e1.graphviz_label = 'some label'
    s = @g.to_graphviz
    assert_equal 'digraph G{
node [shape=circle,
  ]
  1 -> 2 [label="some label"];
}',
                 s
  end

  def test_define_verticess_order
    @g.vertex(1).add_edge(2)
    @g.vertex(2).add_edge(3)

    @g.graphviz_define_vertices_order([3, 2, 1])

    s = @g.to_graphviz

    assert_equal 'digraph G{
node [shape=circle,
  ]
  3 []
  2 []
  1 []
  1 -> 2;
  2 -> 3;
}',
                 s
  end

  def test_graphviz_records
    v1 = @g.vertex(1)
    v2 = @g.vertex(2)
    v2.label="1<f0>|2<f1>"
    v2.add_graphviz_attrib("shape", "record")   # TODO: replace by v1.graphviz_shape = :record ???

    e = v1.add_edge(v2)
    e.graphviz_source_field_id = "f1"
    s = @g.to_graphviz
puts s
    assert_equal 'digraph G{
node [shape=circle,
  ]
  1:f1 -> 2;
  2 [shape=record label="1<f0>|2<f1>" ]
}',
                 s
  end
end
