require_relative '../test_helper'

require_relative '../../lib/graph/topsort'

class Test_TopSort < Minitest::Test

  def test_topsort_dfs
    g={}
    g[1]=[3]
    g[2]=[3]
    g[3]=[4, 5]
    g[4]=[5]
    graph = Graph.from_adj_list(g)
    sorted = graph.topsort_dfs.map{|v|v.index}
    
    assert_equal [2, 1, 3, 4, 5], sorted
  end
end
