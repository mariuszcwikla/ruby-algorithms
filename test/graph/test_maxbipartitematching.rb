require_relative '../test_helper'
require_relative '../../lib/graph/maximumbipartite'

module MaximumBipartiteMatchingMixin
  def test_matching1
    @g.add_uedge(1, 6)
    @g.add_uedge(1, 7)
    @g.add_uedge(2, 6)

    m = @algorithm.call([1, 2], [6, 7])
    m = m.map{|a, b| [a.key, b.key]}.sort
    assert_equal [[1, 7], [2, 6]], m
  end

  def test_matching2
    @g.add_uedge(1, 6)
    @g.add_uedge(1, 7)
    @g.add_uedge(2, 6)
    @g.add_uedge(3, 6)
    @g.add_uedge(3, 8)
    @g.add_uedge(4, 8)
    @g.add_uedge(4, 9)
    @g.add_uedge(4, 10)
    @g.add_uedge(5, 9)
    @g.add_uedge(5, 10)

    m = @algorithm.call([1, 2, 3, 4, 5], [6, 7, 8, 9, 10]).map{|a, b| [a.key, b.key]}.sort

    assert_equal [[1, 7], [2, 6], [3, 8], [4, 9], [5, 10]], m
  end

  def test_matching3
    @g.add_uedge(1, 6)
    @g.add_uedge(1, 7)
    @g.add_uedge(1, 8)
    @g.add_uedge(2, 6)
    @g.add_uedge(2, 8)
    @g.add_uedge(3, 6)

    m = @algorithm.call([1, 2, 3, 4, 5], [6, 7, 8, 9, 10]).map{|a, b| [a.key, b.key]}.sort

    assert_equal [[1, 7], [2, 8], [3, 6]], m
  end
end

class MaximumBipartiteMatchingSimplisticTest < Minitest::Test
  include MaximumBipartiteMatchingMixin

  def setup
    @g = Graph.new
    @algorithm = @g.public_method(:max_bipartite_matching)
  end
end

class MaximumBipartiteMatchingEdmondTest < Minitest::Test
  include MaximumBipartiteMatchingMixin
  def setup
    @g = Graph.new
    @algorithm = @g.public_method(:edmond_max_bipartite_matching)
  end
end


