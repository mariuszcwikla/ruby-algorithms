require_relative '../test_helper'

require_relative '../../lib/graph/fordfulkerson'

class Test_FordFulkerson < Minitest::Test
  def test_max_flow1
    g = Graph.new
    g.add_edge(1, 2).weight = 2
    g.add_edge(1, 3).weight = 2
    g.add_edge(3, 2).weight = 1
    g.add_edge(2, 4).weight = 2
    g.add_edge(3, 4).weight = 2

    x, f = g.ford_fulkerson(1, 4)

    assert_equal 4, x[g.vertex(4)]

    assert_equal 2, f[g.edge(1, 2)]
    assert_equal 2, f[g.edge(1, 3)]
    assert_equal 0, f[g.edge(3, 2)]
    assert_equal 2, f[g.edge(2, 4)]
    assert_equal 2, f[g.edge(3, 4)]
  end
end
