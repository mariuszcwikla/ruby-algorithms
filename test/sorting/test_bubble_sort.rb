require_relative '../test_helper'

require_relative '../../lib/sorting/bubble'

class Test_Bubble_sort < Minitest::Test
  def test_sort
    array = [5, -1, 8, 3, 2]
    bubble_sort(array)

    assert_equal([-1, 2, 3, 5, 8], array)
  end

  def test_sort_optimiz
    array = [5, -1, 8, 3, 2]
    bubble_sort_optimiz(array)

    assert_equal([-1, 2, 3, 5, 8], array)
  end
end
