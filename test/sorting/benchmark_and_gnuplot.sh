#!/bin/bash

ruby benchmark_sort.rb | tee benchmark.txt

grep "selection-sort " benchmark.txt | awk 'BEGIN {print "#selection-sort" } {print $2 " " $3}' > selection-sort.txt
grep "bubble-sort " benchmark.txt | awk 'BEGIN {print "#bubble-sort" } {print $2 " " $3}' > bubble-sort.txt
grep "bubble-sort-optimized " benchmark.txt | awk 'BEGIN {print "#bubble-sort-optimized" } {print $2 " " $3}' > bubble-sort-optimized.txt
grep "merge-sort " benchmark.txt | awk 'BEGIN {print "#merge-sort" } {print $2 " " $3}' > merge-sort.txt
grep "merge-sort-linked-list " benchmark.txt | awk 'BEGIN {print "#merge-sort-linked-list" } {print $2 " " $3}' > merge-sort-linked-list.txt
grep "heap-sort " benchmark.txt | awk 'BEGIN {print "#heap-sort" } {print $2 " " $3}' > heap-sort.txt
grep "quicksort-Hoare " benchmark.txt | awk 'BEGIN {print "#quicksort-Hoare" } {print $2 " " $3}' > quicksort-Hoare.txt




gnuplot -e '
set term png size 800,600;
set output "benchmark-sort1.png";
set xrange[0:20000];
plot "selection-sort.txt" with lines lt rgb "green" lw 2 title  "selection sort",
  "bubble-sort.txt" with lines lt rgb "red" lw 3 title  "bubble sort",
  "bubble-sort-optimized.txt" with lines lw 2 title "bubble sort (optimized)",
  "merge-sort.txt" with lines lw 2 title "merge sort",
  "merge-sort-linked-list.txt" with lines lw 2 title "merge-sort-linked-list",
  "heap-sort.txt" with lines lw 2 title "heap-sort",
  "quicksort-Hoare.txt" with lines lw 2 title "quicksort-Hoare"
'

gnuplot -e '
set term png size 800,600;
set output "benchmark-sort2.png";
plot "selection-sort.txt" with lines lt rgb "green" lw 2 title  "selection sort",
  "bubble-sort.txt" with lines lt rgb "red" lw 3 title  "bubble sort",
  "bubble-sort-optimized.txt" with lines lw 2 title "bubble sort (optimized)",
  "merge-sort.txt" with lines lw 2 title "merge sort",
  "merge-sort-linked-list.txt" with lines lw 2 title "merge-sort-linked-list",
  "heap-sort.txt" with lines lw 2 title "heap-sort",
  "quicksort-Hoare.txt" with lines lw 2 title "quicksort-Hoare"
'

#I'm using svg. PNG displays "rugged" linear function
gnuplot -e '
set term svg size 800,600 background "white";
set output "benchmark-sort-and-linear.svg";
plot "selection-sort.txt" with lines lt rgb "green" lw 2 title  "selection sort",
  "bubble-sort.txt" with lines lt rgb "red" lw 3 title  "bubble sort",
  "bubble-sort-optimized.txt" with lines lw 2 title "bubble sort (optimized)",
  "merge-sort.txt" with lines lw 2 title "merge sort",
  "merge-sort-linked-list.txt" with lines lw 2 title "merge-sort-linked-list",
  "heap-sort.txt" with lines lw 2 title "heap-sort",
  [0:1000000] 80.0*x/1000000 lw 2 title "linear c*x"
'

display benchmark-sort1.png
display benchmark-sort2.png