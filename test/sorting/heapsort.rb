def heap_insert(heap, value)
  heap << value
  heapify_up(heap, heap.size - 1)
end

def heapify_up(heap, i)
  while i > 0
    p = (i - 1) / 2 # TIP: division by 2 can be changed to binary shift >>
    if heap[i] < heap[p]
      heap[i], heap[p] = heap[p], heap[i]
      i = p
    else
      break
    end
  end
end

def heap_extract_min(heap)
  temp = heap[0]

  heap[0] = heap.last
  heap.pop

  i = 0
  # heapify-down!
  loop do
    l = i * 2 + 1
    r = i * 2 + 2

    min = i
    min = l if (l < heap.size) && (heap[l] < heap[i])
    min = r if (r < heap.size) && (heap[r] < heap[min])

    break if min == i

    heap[min], heap[i] = heap[i], heap[min]
    i = min
  end
  temp
end

def heapsort(array)
  1.upto(array.size - 1) { |i| heapify_up(array, i) }
  out = []
  array.size.times { |_i| out << heap_extract_min(array) }
  out
end

# :nocov:
if __FILE__ == $PROGRAM_NAME
  array = [1, 5, 8, 2, 3, 4]
  puts heapsort(array).join ' '
end
# :nocov:
