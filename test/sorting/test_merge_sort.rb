require_relative '../test_helper'
require_relative '../../lib/sorting/merge_sort'

require 'timeout'

class TestMergeSort < Minitest::Test
  #  def self.suite
  #      mysuite = super
  #      def mysuite.run(*args)
  #          Timeout::timeout(2) do
  #              super
  #          end
  #      end
  #      mysuite
  #  end

  def test_sort
    array = [5, -1, 8, 3, 2]
    array = merge_sort(array, 0, array.size)

    assert_equal([-1, 2, 3, 5, 8], array)
  end

  def test_sort_optimiz
    array = [5, -1, 8, 3, 2]
    merge_sort_optimiz(array, 0, array.size)

    assert_equal([-1, 2, 3, 5, 8], array)
  end

  def test_sort_optimiz2
    omit 'Algorytm nie dziala poprawnie'
    array = [4, 16, 18, 14, 19, 4]
    merge_sort_optimiz(array)

    assert_equal([4, 4, 14, 16, 18, 19], array)
  end

  def test_sort_optimiz3
    skip 'not working'
    array = [7, 16, 19, 4, 17, 9, 4, 4]
    merge_sort_optimiz(array)

    assert_equal([4, 4, 4, 7, 9, 16, 17, 19], array)
  end

  def test_merge
    array = [4, 4, 14, 16, 18, 19]
    merge(array, 0, 3, array.size)
    assert_equal([4, 4, 14, 16, 18, 19], array)
  end

  def test_merge2
    skip 'not working'
    array = [7, 16, 4, 19]
    merge(array, 0, 2, array.size)
    assert_equal([4, 7, 16, 19], array)
  end

  def test_merge3
    array = [1, 5, 2, 3, 8]
    merge(array, 0, 2, array.size)
    assert_equal([1, 2, 3, 5, 8], array)
  end

  def test_sort_list
    x = List.new
    x.add_last 5
    x.add_last(-1)
    x.add_last 8
    x.add_last 3
    x.add_last 2

    x = merge_sort_list(x)

    assert_equal([-1, 2, 3, 5, 8], x.to_a)
  end
end
