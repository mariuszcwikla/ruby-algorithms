require 'benchmark'

require_relative '../../lib/sorting/bubble'
require_relative '../../lib/sorting/selection'
require_relative '../../lib/sorting/merge_sort'
require_relative '../../lib/sorting/quicksort'
require_relative 'heapsort'
test_data = {}

[100, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 10_000, 100_000, 200_000, 500_000, 1_000_000].each do |n|
  data = []
  test_data[n] = data
  n.times { data << rand(n) }
end

Benchmark.bm do |bench|
  test_data.keys.sort.each do |n|
    if n <= 10_000 # 10000 takes 15 seconds.... skip O(n^2) algorithms
      x = test_data[n].dup
      bench.report("selection-sort #{n}") { selection_sort(x) }
    end
  end
  test_data.keys.sort.each do |n|
    if n <= 10_000 # 10000 takes 15 seconds.... skip O(n^2) algorithms
      x = test_data[n].dup
      bench.report("bubble-sort #{n}") { bubble_sort(x) }
    end
  end
  test_data.keys.sort.each do |n|
    if n <= 10_000
      x = test_data[n].dup
      bench.report("bubble-sort-optimized #{n}") { bubble_sort_optimiz(x) }
    end
  end

  test_data.keys.sort.each do |n|
    x = test_data[n].dup
    bench.report("merge-sort #{n}") { merge_sort(x, 0, x.size) }
  end

  test_data.keys.sort.each do |n|
    x = List.new
    test_data[n].dup.each { |i| x.insert_last(i) }
    bench.report("merge-sort-linked-list #{n}") { merge_sort_list(x) }
  end

  test_data.keys.sort.each do |n|
    x = test_data[n].dup
    bench.report("heap-sort #{n}") { heapsort(x) }
  end

  test_data.keys.sort.each do |n|
    x = test_data[n].dup
    bench.report("quicksort-Hoare #{n}") { quicksort_hoare(x) }
  end
end
