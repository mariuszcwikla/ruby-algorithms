require_relative '../test_helper'

require_relative '../../lib/sorting/selection'

class Test_Selection_sort < Minitest::Test
  def test_sort
    array = [5, -1, 8, 3, 2]
    selection_sort(array)

    assert_equal([-1, 2, 3, 5, 8], array)
  end
end
