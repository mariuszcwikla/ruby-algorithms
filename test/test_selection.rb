require_relative 'test_helper'

require_relative '../lib/selection/lomuto'
require_relative '../lib/selection/hoare'

class Test_Lomuto < Minitest::Test
  def setup
    @x = [5, 1, 2, 8, 9, 10, 5, 3, 11, 4]
  end

  def test_lomuto
    s = lomuto(@x)
    assert_equal([4, 1, 2, 3], @x[0..s - 1])
    assert_equal([5, 10, 5, 8, 11, 9], @x[s..-1])
  end
end

class Test_Hoare < Minitest::Test
  def setup
    @x = [5, 1, 2, 8, -3, 9, 10, -5, 5, 3, 12, 11, 4]
  end

  def test_hoare_simple
    s = hoare_partition_simple(@x)
    assert_equal([-5, 1, 2, 4, -3, 3], @x[0..s - 1])
    assert_equal([5, 10, 5, 9, 12, 11, 8], @x[s..-1])
  end

  def test_hoare_simple_original
    s = hoare_partition_simple_original(@x)
    assert_equal([-5, 1, 2, 4, -3, 3], @x[0..s - 1])
    assert_equal([5, 10, 5, 9, 12, 11, 8], @x[s..-1])
  end

  def test_hoare_0
    assert_nil hoare_partition([])
  end

  def test_hoare_1
    assert_equal 0, hoare_partition([5])
  end

  def test_hoare_2
    x = [2, 1]
    assert_equal 1, hoare_partition(x)
    assert_equal [1, 2], x
  end

  def test_hoare
    s = hoare_partition(@x)
    assert_equal([-5, 1, 2, 4, -3, 3], @x[0..s - 1])
    assert_equal([5, 10, 5, 9, 12, 11, 8], @x[s..-1])
  end

  def test_hoare_subarray
    s = hoare_partition(@x, 3, 11)

    # this is not modifed
    assert_equal([5, 1, 2], @x[0..2])
    assert_equal([11, 4], @x[11..-1])

    # array 2..8 should be modified
    assert_equal(8, @x[s])
    assert_equal([-5, -3, 3, 5], @x[3..s - 1]) # on left <= 8
    assert_equal([8, 10, 9, 12], @x[s..10]) # on right>= 8
  end
end

class Test_Quickselect < Minitest::Test
  def setup
    @x = [5, 1, 2, 8, 9, 10, 5, 3, 11, 4]
  end

  def test_quickselect_lomuto_0
    assert_equal 1, quickselect_lomuto(@x, 0)
  end

  def test_quickselect_lomuto_1
    assert_equal 2, quickselect_lomuto(@x, 1)
  end

  def test_quickselect_lomuto_4
    assert_equal 5, quickselect_lomuto(@x, 4)
  end

  def test_quickselect_lomuto_7
    assert_equal 9, quickselect_lomuto(@x, 7)
  end

  def test_quickselect_lomuto_8
    assert_equal 10, quickselect_lomuto(@x, 8)
  end

  def test_quickselect_lomuto_9
    assert_equal 11, quickselect_lomuto(@x, 9)
  end

  def test_quickselect_lomuto2_0
    assert_equal 1, quickselect_lomuto2(@x, 0)
  end

  def test_quickselect_lomuto2_1
    assert_equal 2, quickselect_lomuto2(@x, 1)
  end

  def test_quickselect_lomuto2_4
    assert_equal 5, quickselect_lomuto2(@x, 4)
  end

  def test_quickselect_lomuto2_7
    assert_equal 9, quickselect_lomuto2(@x, 7)
  end

  def test_quickselect_lomuto2_8
    assert_equal 10, quickselect_lomuto2(@x, 8)
  end

  def test_quickselect_lomuto2_9
    assert_equal 11, quickselect_lomuto2(@x, 9)
  end
  
  def test_lomuto_pbt
    property_of {
      s = range(5, 100)
      [array(s) { range(0, 1000)}, range(0, s-1)]
    }.check {|arr, i|
      a = quickselect_lomuto(arr, i)
      b = arr.sort[i]
      assert_equal b, a
    }
  end
end
