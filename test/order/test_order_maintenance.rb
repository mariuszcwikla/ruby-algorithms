require_relative '../test_helper'
require_relative '../../lib/order/order_maintenance'
require_relative '../../lib/order/farey_order_maintenance'

module OrderMaintenanceTestMixin
  def test_list1
    a = @head.insert(2)
    b = @head.insert(3)
    c = @head.insert(18)
    d = a.insert(7)
    e = a.insert(8)
    assert @head.before?(a)
    assert @head.before?(b)
    assert @head.before?(c)
    assert @head.before?(d)
    assert @head.before?(e)

    assert b.before?(a)
    assert c.before?(a)

    assert e.before?(d)
  end

  def test_spaceship
    a = @head.insert(2)
    b = @head.insert(3)
    c = @head.insert(18)
    d = a.insert(7)
    e = a.insert(8)

    assert @head < a
    assert @head < b
    assert @head < c
    assert @head < d
    assert @head < e

    assert b < a
    assert c < a

    assert e < d
  end
end

class OrderMaintenanceTest < Minitest::Test
  include OrderMaintenanceTestMixin
  def setup
    @head = OrderMaintenance::Node.new(5)
  end
end

class FareyMaintenanceTest < Minitest::Test
  include OrderMaintenanceTestMixin
  def setup
    @head = FareyOrderMaintenance::Node.new(5)
  end
end