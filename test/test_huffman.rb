require_relative 'test_helper'
require_relative '../lib/huffman/huffman'

class HuffmanTest < Minitest::Test
  def test_huffman_encode_decode
    # nothing fancy, just a smoke test ;)
    s = 'aaaaaaaaabbbbbaaaccddddef'
    h, hs = huffman_encode(s)

    s2 = huffman_decode(h, hs)
    assert_equal(s, s2)
  end

  def test_serialize_huffman_tree
    s = 'aaaaaaaaabbbbbaaaccddddef'
    t, encoded = huffman_encode(s)

    serialized_tree = t.serialize
    expected_serialized_tree = '01011000010101100010010110010001011000110101100101101100110'
    assert_equal expected_serialized_tree, serialized_tree

    t2, bits_consumed = HuffmanNode.deserialize(serialized_tree + '010111010101') # random data at the end. Should not be consumed!

    assert_equal t.serialize, t2.serialize
    assert_equal serialized_tree.size, bits_consumed

    s2 = huffman_decode(t2, encoded)
    assert_equal s, s2
  end

  def test_huffman_compress
    # just a smoke test.
    # it does not test padding byte well

    s = 'aaaaaaaaabbbbbaaaccddddef'
    compressed = huffman_compress(s)
    assert_equal s, huffman_decompress(compressed)
  end
end
