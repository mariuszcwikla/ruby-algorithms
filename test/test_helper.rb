require 'simplecov'
SimpleCov.start do
  add_filter %r{^/test/}
end

require 'minitest/autorun'
require 'rantly'
require 'rantly/minitest_extensions'
require 'rantly/shrinks'
require_relative 'shrinkable_array'
# When running rake it outputs lots of below warnings:
# warning: instance variable @default_size not initialized
# Below is a workaround to get rid of these warnings

Rantly.default_size = Rantly.default_size
# require 'minitest/color'
# require 'test/unit'
# require 'minitest_tu_shim'

# def assert_true(*args)
#   assert(*args)
# end
# def assert_false(*args)
# puts args
# refute(*args)
# if args.size==2
#   assert !args[0], args[1]
# else
#   assert !args[0]
# end
# end
# def assert_not_nil(*args)
#   refute_nil(*args)
# end
require "minitest/reporters"
# Minitest::Reporters.use!
Minitest::Reporters.use! [
  Minitest::Reporters::SpecReporter.new,
  Minitest::Reporters::JUnitReporter.new,
  Minitest::Reporters::HtmlReporter.new
]

# NOTE: this does not work. Allure is not integrated with minitest.
# require "allure-ruby-commons"
# Allure.configure do |config|
#   puts 'configuring allure'
#   config.results_directory = "allure/minitest"
#   config.clean_results_directory = true
#   config.logging_level = Logger::INFO
#   # these are used for creating links to bugs or test cases where {} is replaced with keys of relevant items
#   # config.link_tms_pattern = "http://www.jira.com/browse/{}"
#   # config.link_issue_pattern = "http://www.jira.com/browse/{}"
# end

module Minitest
  class Test
    alias assert_raise assert_raises
    alias assert_true assert
    alias assert_false refute
    alias assert_not_nil refute_nil
    alias omit skip
    def loggable(&block)
      begin
        block.call
      rescue Rantly::GuardFailure => e
        raise e
      rescue Exception => e
        puts e.message
        puts e.backtrace
        raise e
      end
    end
  end
end
