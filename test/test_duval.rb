require_relative 'test_helper'

require_relative '../lib/text/duval'

class TestDuval < Minitest::Test
  def test_duval
    assert_equal %w[bbcj bbcj b a], duval('bbcjbbcjba')
  end

  def test_duval2
    assert_equal %w[bbcj bbcj ab], duval('bbcjbbcjab')
  end

  def test_duval3
    assert_equal %w[bbcj bbcj az], duval('bbcjbbcjaz')
  end
end
