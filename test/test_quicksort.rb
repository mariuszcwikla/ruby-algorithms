require_relative 'test_helper'

require_relative '../lib/sorting/quicksort'

class Test_Quicksort < Minitest::Test
  def test_quicksort_hoare_simple
    x = [5, 1, 2, 8, 9, 10, 3, 11, 4]
    assert_equal [1, 2, 3, 4, 5, 8, 9, 10, 11], quicksort_hoare_simple(x)
  end

  def test_quicksort_hoare2
    x = [5, 1]
    quicksort_hoare(x)
    assert_equal [1, 5], x
  end

  def test_quicksort_hoare3
    x = [5, 1, 2]
    quicksort_hoare(x)
    assert_equal [1, 2, 5], x
  end

  def test_quicksort_hoare
    x = [5, 1, 2, 8, 9, 10, 3, 11, 4]
    quicksort_hoare(x)
    assert_equal [1, 2, 3, 4, 5, 8, 9, 10, 11], x
  end

  def test_quicksort_rantly
    property_of {
      array(100) {integer}
    }.check { |a|
      quicksort_hoare(a)
      (1...a.size).each { |i| assert a[i-1] <= a[i]}
    }
  end
end
