# Segment trees allow efficient range query for multiple functions
# like: sum, multiplication, min, max, xor
module RangeQueryTestMixin
  
  def brute(arr, a, b, func)
    arr[a..(b-1)].reduce(&func)
  end

  def test_sum
    rantly_test(->(a, b) { a + b })
  end

  def test_mul
    rantly_test(->(a, b) { a * b })
  end
  
  def test_xor
    rantly_test(->(a, b) { a ^ b })
  end

  def test_min
    rantly_test(->(a, b) { [a, b].min })
  end

  def test_max
    rantly_test(->(a, b) { [a, b].max })
  end

  def rantly_test(func)
    property_of {
      begin
      max = 1000
      num_queries = 10

      [
        array(max) { integer },
        array(num_queries) {
          a = range(0, max-1)
          b = range(0, max)
          b += 1 if a == b    # this is workaround. Otherwise there are too many GuardFailures when "guard a != b"
          a, b = b, a if b < a
          [a, b]
        }
      ]

      rescue => e
        puts e.message
        puts e.backtrace
        raise e
      end
    }.check{ |arr, query|
      begin
        if instance_variable_defined? "@args"
          tree = @clazz.new(arr, func, **@args)
        else
          tree = @clazz.new(arr, func)
        end
        query.each do |a, b|
          vt = tree.query(a, b)
          vb = brute(arr, a, b, func)
          assert_equal vb, vt
        end
      rescue => e
        puts e.message
        puts e.backtrace
        return
      end
    }
  end
end