require_relative '../test_helper'
require_relative '../../lib/queries/sqrt_decomposition'
require_relative '../queries/range_query_test_mixin'


class SqrtDecompositionTest < Minitest::Test
  include RangeQueryTestMixin
  def setup
    @clazz = SqrtDecomposition
  end
end


class SqrtDecompositionOptimizedTest < Minitest::Test
  include RangeQueryTestMixin
  def setup
    @clazz = SqrtDecomposition
    @args = {:optimized_array_operations => true}
  end
end