require_relative '../test_helper'
require_relative '../../lib/combinatorics/heap_permute'

# zmieniam nazwe pliku na minitest tymczasowo bo gdy jest `test_heap_permute.rb`
# to rake nie uruchamia testow test::unit
# rozwiazania:
# 1. tymczasowe: uzyj "minittest_*"
# 2. przenies wszystkie "test_*.rb" do katalogu test/
# 3. zmigruj z test::unit na minitest
class HeapPermuteTest < Minitest::Test
  def test_permute_1
    out = []
    heap_permute([1]) { |x| out << x.dup }
    assert_equal [[1]], out
  end

  def test_permute_2
    out = []
    heap_permute([1, 2]) { |x| out << x.dup }
    assert out.include?([1, 2])
    assert out.include?([2, 1])
  end

  def test_permute_3
    out = []
    heap_permute([1, 2, 3]) { |x| out << x.dup }
    assert out.include?([1, 2, 3])
    assert out.include?([1, 3, 2])
    assert out.include?([2, 1, 3])
    assert out.include?([2, 3, 1])
    assert out.include?([3, 2, 1])
    assert out.include?([3, 1, 2])
  end
end
