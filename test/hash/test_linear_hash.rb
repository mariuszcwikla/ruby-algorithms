require_relative '../test_helper'
require_relative '../../lib/hash/linear_hash'
require_relative '../../lib/hash/linear_hash_base2'
require_relative '../collection_test_mixins'

class LinearHashTest < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = LinearHash.new
  end
end

class LinearHashBlocks16Size4Test < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = LinearHash.new(initial_blocks: 16, block_size: 4)
  end
end

class LinearHashBlocks3Size4Test < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = LinearHash.new(initial_blocks: 3, block_size: 4)
  end
end

class LinearHashBlocks5Size3Test < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = LinearHash.new(initial_blocks: 5, block_size: 3)
  end
end

class LinearHashBase2Test < Minitest::Test
  include CollectionTestMixin

  def setup
    @subject = LinearHashBase2.new
  end
end
