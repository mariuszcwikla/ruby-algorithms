require_relative '../test_helper'
require_relative '../../lib/hash/minimal_perfect_hash'

require 'set'
class MinimalPerfectHashTest < Minitest::Test

  def test_mph
    property_of {
      array(range(10, 1000)){ integer(100_000) }
    }.check(10) do |arr|
      arr = arr.uniq
      mph = MinimalPerfectHash.build(arr)
      s = Set.new(arr)
      s.each do |x|
        assert_true mph.include? x
      end
      for i in 0..1000
        next if s.include? i
        assert_false mph.include? i
      end
      # for i in 0..(arr.max)
      #   expected = s.include?(i)
      #   assert_equal(expected, mph.include?(i))
      # end
    end
  end
end
