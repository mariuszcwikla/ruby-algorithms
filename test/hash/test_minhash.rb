require_relative '../test_helper'
require_relative '../../lib/hash/minhash'

class MinhashTest < Minitest::Test
  # just simple smoke test
  def test_signature
    h = ->(x) { x * 7 % 13 } # need custom hash fun to get predictable results. Ruby's Object.hash uses random seed at each run
    sig = MinHash.signature([1, 2, 8, 10, 15, 20, 0, 9], hash_function: h)
    assert_equal [0, 15, 8, 10], sig
  end

  def test_similarity
    h = ->(x) { x * 7 % 13 } # need custom hash fun to get predictable results. Ruby's Object.hash uses random seed at each run
    a = [1, 2, 8, 10, 15, 20, 0, 9]
    b = [1, 3, 8, 10, 11, 20, 0, 9]
    sig1 = MinHash.signature(a, hash_function: h)
    sig2 = MinHash.signature(b, hash_function: h)
    assert_equal 0.75, MinHash.similarity(sig1, sig2, hash_function: h)
  end

  def test_jaccard
    a = [1, 2, 8, 10, 15, 20, 0, 9]
    b = [1, 3, 8, 10, 11, 20, 0, 9]
    assert_equal 0.6, jaccard(a, b)
  end
end
