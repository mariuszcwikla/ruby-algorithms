require_relative '../test_helper'
require_relative '../../lib/hash/cuckoo_hash'

class CuckooHashTest < Minitest::Test

  def test_collision_occurs_after_50_percent_occupancy
    load_factors = []
    property_of{
      size = range(10, 100)
      ar = array(size){range(0, 100_000)}
      guard ar.uniq.size == ar.size
      ar
    }.check(20  ){|arr|
      loggable {
        t = CuckooHash.new(arr.size)
        count = 0
        begin
          arr.each do |i| 
            t.insert i 
            count += 1
          end
        rescue CollisionException
          load_factor = count.to_f * 100 / arr.size
          load_factors << load_factor
          puts "Cucko hash load factor #{load_factor.round(1)}%"
          # Default implementation of Cucko hashing gives 50-70% load factors but in some cases it drops down to ~30 - see below. In one case it was 28%
          assert_true load_factor >= 20.0, "load factor should be greater than 20% but was #{load_factor.round(1)}%"
        end
        count.times do |i|
          # assert t.include?(arr[i])
        end
      }
    }
    # Usually load factor is above 50%, but sometimes it drops down e.g. to 38%. But average of all should to be > 50%
    # Cucko hash load factor 69.8%
    # Cucko hash load factor 63.6%
    # Cucko hash load factor 53.3%
    # Cucko hash load factor 67.9%
    # Cucko hash load factor 38.8%
    avg = load_factors.sum / load_factors.size
    assert_true avg >= 50.0, "average load factor should be greater than 50% but was #{avg.round(1)}%"
  end
end
