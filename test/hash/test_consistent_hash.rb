require_relative '../test_helper'
require_relative '../../lib/hash/consistent_hash'

class ConsistentHashTest < Minitest::Test
  def test_insert_drop_bucket_test_include
    size = 7
    property_of {
      [
        range(0, size - 1),
        ShrinkableArray.new(array(100) {range(0, 10_000_000)})
      ]
    }.check {|bucket_to_drop, array|
      h = ConsistentHash.new(number_of_buckets: size)
      array.each {|v| h << v}
      h.drop_bucket bucket_to_drop
      array.each do |v|
        assert_true h.include?(v), "should include #{v}"
      end
    }
  end

  def test_insert_create_bucket_test_include
    size = 7
    property_of {
      [
        range(0, size - 1),
        ShrinkableArray.new(array(100) {range(0, 10_000_000)})
      ]
    }.check {|bucket_to_drop, array|
      h = ConsistentHash.new(number_of_buckets: size)
      array.each {|v| h << v}
      h.create_bucket
      array.each do |v|
        assert_true h.include?(v), "should include #{v}"
      end
    }
  end
end
