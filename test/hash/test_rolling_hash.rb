require_relative '../test_helper'
require_relative '../../lib/hash/rolling_hash'

class RollingHashTest < Minitest::Test
  def test_rolling_hash
    a = [8, 5, 13, 7, 18, 19, 20, 4, 2, 1]
    rh = PolyRollingHash.new(a[0...4])
    assert_equal 66, rh.hash_value
    assert_equal 14, rh.roll(a[4])
    assert_equal 35, rh.roll(a[5])
    assert_equal 15, rh.roll(a[6])
  end

  def test_rolling_hash_string
    s = 'helloworld'
    rh = PolyRollingHash.new(s[0...4])

    assert_equal 60, rh.hash_value
    assert_equal 51, rh.roll(s[4])
    assert_equal 70, rh.roll(s[5])
    assert_equal 34, rh.roll(s[6])
  end
end
