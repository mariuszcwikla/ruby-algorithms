require_relative '../test_helper'
require_relative '../../lib/probabilistic/skiplist'
require_relative '../../lib/probabilistic/skiplist2'
require_relative '../collection_test_mixins'

class SkipListTest < Minitest::Test
  include CollectionTestMixin
  include TestUnitCompatibility

  def setup
    @subject = SkipList.new
  end
end

class SkipList2Test < Minitest::Test
  include CollectionTestMixin
  include TestUnitCompatibility

  def setup
    @skiplist_seed = Random.new_seed
    srand @skiplist_seed
    @subject = SkipList2.new
  end

  def teardown
    unless passed?
      puts "SkipList test failed with seed=#{@skiplist_seed}"
    end
  end
  
  def test_rantly
    property_of {
      Deflating.new( array(range(0,20)) { integer } )
    }.check { |arr|
      arr.each do |x|
        @subject.insert(x)
        assert @subject.include?(x)
      end
      arr.each do |x|
        @subject.remove(x)
        refute @subject.include?(x)
      end
    }
  end
end
